﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;
using Xinying.Common;

namespace wxHelper
{
    public class WeiXinResponse
    {
        /// <summary>
        /// 回复消息(text)
        /// </summary>
        public static void ResText(WeiXinRequest requestXML, string content)
        {
            StringBuilder resxml = new StringBuilder(string.Format("<xml><ToUserName><![CDATA[{0}]]></ToUserName><FromUserName><![CDATA[{1}]]></FromUserName><CreateTime>{2}</CreateTime>", requestXML.FromUserName, requestXML.ToUserName, common.ConvertDateTimeInt(DateTime.Now)));
            resxml.AppendFormat("<MsgType><![CDATA[text]]></MsgType><Content><![CDATA[{0}]]></Content><FuncFlag>0</FuncFlag></xml>", content);
            HttpContext.Current.Response.Write(resxml);
            return;
        }
        /// <summary>
        /// 回复消息(音乐)
        /// </summary>
        public static void ResMusic(WeiXinRequest requestXML, Music mu)
        {
            StringBuilder resxml = new StringBuilder(string.Format("<xml><ToUserName><![CDATA[{0}]]></ToUserName><FromUserName><![CDATA[{1}]]></FromUserName><CreateTime>{2}</CreateTime>", requestXML.FromUserName, requestXML.ToUserName, common.ConvertDateTimeInt(DateTime.Now)));
            resxml.Append(" <MsgType><![CDATA[music]]></MsgType>");
            resxml.AppendFormat("<Music><Title><![CDATA[{0}]]></Title><Description><![CDATA[{1}]]></Description>", requestXML.Title, requestXML.Description);
            resxml.AppendFormat("<MusicUrl><![CDATA[{0}]]></MusicUrl><HQMusicUrl><![CDATA[{1}]]></HQMusicUrl></Music><FuncFlag>0</FuncFlag></xml>", mu.MusicUrl, mu.HQMusicUrl);
            HttpContext.Current.Response.Write(resxml);
            return;
        }

        /// <summary>
        /// 回复消息(图片)
        /// </summary>
        public static void ResPicture(WeiXinRequest requestXML, Picture pic, string domain)
        {
            StringBuilder resxml = new StringBuilder(string.Format("<xml><ToUserName><![CDATA[{0}]]></ToUserName><FromUserName><![CDATA[{1}]]></FromUserName><CreateTime>{2}</CreateTime>", requestXML.FromUserName, requestXML.ToUserName, common.ConvertDateTimeInt(DateTime.Now)));
            resxml.Append(" <MsgType><![CDATA[image]]></MsgType>");
            resxml.AppendFormat("<PicUrl><![CDATA[{0}]]></PicUrl></xml>", domain + pic.PictureUrl);
            Utils.WriteTxt("resxml:" + resxml.ToString());
            HttpContext.Current.Response.Write(resxml);
            return;
        }

        /// <summary>
        /// 回复消息（图文列表）
        /// </summary>
        /// <param name="requestXML"></param>
        /// <param name="art"></param>
        public static void ResArticles(WeiXinRequest requestXML, List<Articles> art)
        {
            StringBuilder resxml = new StringBuilder(string.Format("<xml><ToUserName><![CDATA[{0}]]></ToUserName><FromUserName><![CDATA[{1}]]></FromUserName><CreateTime>{2}</CreateTime>", requestXML.FromUserName, requestXML.ToUserName, common.ConvertDateTimeInt(DateTime.Now)));
            resxml.AppendFormat("<MsgType><![CDATA[news]]></MsgType><ArticleCount>{0}</ArticleCount><Articles>", art.Count);
            for (int i = 0; i < art.Count; i++)
            {
                resxml.AppendFormat("<item><Title><![CDATA[{0}]]></Title>  <Description><![CDATA[{1}]]></Description>", art[i].Title, art[i].Description);
                resxml.AppendFormat("<PicUrl><![CDATA[{0}]]></PicUrl><Url><![CDATA[{1}]]></Url></item>", art[i].PicUrl.Contains("http://") ? art[i].PicUrl : "http://" + XYRequest.GetCurrentFullHost() + art[i].PicUrl, art[i].Url.Contains("http://") ? art[i].Url : "http://" + XYRequest.GetCurrentFullHost() + art[i].Url);
            }
            resxml.Append("</Articles><FuncFlag>0</FuncFlag></xml>");
            Utils.WriteTxt(resxml.ToString());
            HttpContext.Current.Response.Write(resxml.ToString());
            return;
        }
    }
}
