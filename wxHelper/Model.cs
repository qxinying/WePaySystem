﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using LitJson;

namespace wxHelper
{
    public class Music
    {
        #region 属性
        /// <summary>
        /// 音乐链接
        /// </summary>
        public string MusicUrl { get; set; }
        /// <summary>
        /// 高质量音乐链接，WIFI环境优先使用该链接播放音乐
        /// </summary>
        public string HQMusicUrl { get; set; }
        #endregion
    }

    public class Articles
    {
        #region 属性
        /// <summary>
        /// 图文消息标题
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 图文消息描述
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// 图片链接，支持JPG、PNG格式，较好的效果为大图640*320，小图80*80。
        /// </summary>
        public string PicUrl { get; set; }
        /// <summary>
        /// 点击图文消息跳转链接
        /// </summary>
        public string Url { get; set; }
        #endregion
    }

    public class Picture
    {
        #region 属性
        /// <summary>
        /// 音乐链接
        /// </summary>
        public string PictureUrl { get; set; }
        #endregion
    }

    /// <summary>
    /// 生成带参数的二维码(临时)
    /// </summary>
    public class Qr
    {
        #region 属性
        /// <summary>
        /// 该二维码有效时间，以秒为单位。 最大不超过1800。
        /// </summary>
        public int expire_seconds { get; set; }
        /// <summary>
        /// 二维码类型，QR_SCENE为临时,QR_LIMIT_SCENE为永久
        /// </summary>
        public int action_name { get; set; }
        /// <summary>
        /// 二维码详细信息
        /// </summary>
        public int action_info { get; set; }
        /// <summary>
        /// 场景值ID，临时二维码时为32位整型，永久二维码时最大值为1000
        /// </summary>
        public int scene_id { get; set; }
        #endregion
        #region 方法
        /// <summary>
        /// 获取二维码ticket字符串
        /// </summary>
        /// <param name="expire">有效时间，以秒为单位。 最大不超过1800。永久二维码时，此参数无效</param>
        /// <param name="scene_id">场景值ID，临时二维码时为32位整型，永久二维码时最大值为1000</param>
        /// <param name="type">二维码类型。 0为临时，1为永久</param>
        /// <param name="access_token">access_token</param>
        /// <returns>ticket字符串</returns>
        public static string GetQrStr(int expire, int scene_id,int type,string access_token)
        {
            StringBuilder strData = new StringBuilder();
            switch (type)
            {
                case 0: {
                    strData.Append("{\"expire_seconds\": " + (expire > 1800 ? 1800 : expire) + ", \"action_name\": \"QR_SCENE\", \"action_info\": {\"scene\": {\"scene_id\": "+scene_id+"}}}");
                } break;
                case 1: {
                    strData.Append("{\"action_name\": \"QR_LIMIT_SCENE\", \"action_info\": {\"scene\": {\"scene_id\": "+scene_id+"}}}");
                }break;
                default: return "";
            }
            string url = string.Format("https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token={0}",access_token);
            string returnStr = common.HttpPost(url,strData.ToString());
            if (returnStr.Contains("ticket"))
            {
                JsonData jd = JsonMapper.ToObject(returnStr);
                string ticket = (string)jd["ticket"];
                return ticket;
            }
            else
                return "";

        }
        #endregion

    }

    /// <summary>
    ///登陆微信返回的数据
    /// </summary>
    public class WeiXinRetInfo
    {
        public string Ret { get; set; }
        public string ErrMsg { get; set; }
        public string ShowVerifyCode { get; set; }
        public string ErrCode { get; set; }
    }

    /// <summary>
    /// 登陆成功后的用户信息
    /// </summary>
    public static class LoginInfo
    {
        /// <summary>
        /// 登录后得到的令牌
        /// </summary>        
        public static string Token { get; set; }
        /// <summary>
        /// 登录后得到的cookie
        /// </summary> 
        public static CookieContainer LoginCookie { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public static DateTime CreateDate { get; set; }

    }
}
