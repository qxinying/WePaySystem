﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using Xinying.Common;

namespace wxHelper
{
    public class common
    {
        /// <summary>
        /// unix时间转换为datetime
        /// </summary>
        /// <param name="timeStamp"></param>
        /// <returns></returns>
        public static DateTime UnixTimeToTime(string timeStamp)
        {
            DateTime dtStart = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1));
            long lTime = long.Parse(timeStamp + "0000000");
            TimeSpan toNow = new TimeSpan(lTime);
            return dtStart.Add(toNow);
        }

        /// <summary>
        /// datetime转换为unixtime
        /// </summary>
        /// <param name="time"></param>
        /// <returns></returns>
        public static int ConvertDateTimeInt(System.DateTime time)
        {
            System.DateTime startTime = TimeZone.CurrentTimeZone.ToLocalTime(new System.DateTime(1970, 1, 1));
            return (int)(time - startTime).TotalSeconds;
        }
        /// <summary>
        /// 记录bug，以便调试
        /// </summary>
        public static bool WriteTxt(string str)
        {
            try
            {
                FileStream fs = new FileStream(HttpContext.Current.Request.MapPath("/bugLog.txt"), FileMode.Append);
                StreamWriter sw = new StreamWriter(fs);
                //开始写入
                sw.WriteLine(str);
                //清空缓冲区
                sw.Flush();
                //关闭流
                sw.Close();
                fs.Close();
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        public static string HttpPost(string Url, string postDataStr)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(Url);
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = Encoding.UTF8.GetByteCount(postDataStr);
            Stream myRequestStream = request.GetRequestStream();
            StreamWriter myStreamWriter = new StreamWriter(myRequestStream, Encoding.GetEncoding("gb2312"));
            myStreamWriter.Write(postDataStr);
            myStreamWriter.Close();

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Stream myResponseStream = response.GetResponseStream();
            StreamReader myStreamReader = new StreamReader(myResponseStream, Encoding.GetEncoding("utf-8"));
            string retString = myStreamReader.ReadToEnd();
            myStreamReader.Close();
            myResponseStream.Close();

            return retString;
        }

        public static string HttpGet(string Url, string postDataStr)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(Url + (postDataStr == "" ? "" : "?") + postDataStr);
            request.Method = "GET";
            request.ContentType = "text/html;charset=UTF-8";

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Stream myResponseStream = response.GetResponseStream();
            StreamReader myStreamReader = new StreamReader(myResponseStream, Encoding.GetEncoding("utf-8"));
            string retString = myStreamReader.ReadToEnd();
            myStreamReader.Close();
            myResponseStream.Close();

            return retString;
        }

        ///// <summary>
        ///// 模拟登陆
        ///// </summary>
        //public static bool ExecLogin(string name, string pass)
        //{
        //    bool result = false;
        //    string password = DESMD5.GetMd5Str32(pass).ToUpper();
        //    string padata = "username=" + name + "&pwd=" + password + "&imgcode=&f=json";
        //    string url = "http://mp.weixin.qq.com/cgi-bin/login?lang=zh_CN ";//请求登录的URL
        //    try
        //    {
        //        CookieContainer cc = new CookieContainer();//接收缓存
        //        byte[] byteArray = Encoding.UTF8.GetBytes(padata); // 转化
        //        HttpWebRequest webRequest2 = (HttpWebRequest)WebRequest.Create(url);  //新建一个WebRequest对象用来请求或者响应url
        //        webRequest2.CookieContainer = cc;                                      //保存cookie  
        //        webRequest2.Method = "POST";                                          //请求方式是POST
        //        webRequest2.Accept = "application/json, text/javascript, */*; q=0.01";
        //        webRequest2.Referer = "https://mp.weixin.qq.com/";
        //        webRequest2.Host = "mp.weixin.qq.com";
        //        //webRequest2.Connection = "Keep-Alive";
        //        webRequest2.ContentType = "application/x-www-form-urlencoded";       //请求的内容格式为application/x-www-form-urlencoded
        //        webRequest2.ContentLength = byteArray.Length;
        //        Stream newStream = webRequest2.GetRequestStream();           //返回用于将数据写入 Internet 资源的 Stream。
        //        // Send the data.
        //        newStream.Write(byteArray, 0, byteArray.Length);    //写入参数
        //        newStream.Close();
        //        HttpWebResponse response2 = (HttpWebResponse)webRequest2.GetResponse();
        //        StreamReader sr2 = new StreamReader(response2.GetResponseStream(), Encoding.Default);
        //        string text2 = sr2.ReadToEnd();

        //        //此处用到了newtonsoft来序列化
        //        WeiXinRetInfo retinfo = Newtonsoft.Json.JsonConvert.DeserializeObject<WeiXinRetInfo>(text2);
        //        string token = string.Empty;
        //        if (retinfo.ErrMsg.Length > 0)
        //        {
        //            token = retinfo.ErrMsg.Split(new char[] { '&' })[2].Split(new char[] { '=' })[1].ToString();//取得令牌
        //            LoginInfo.LoginCookie = cc;
        //            LoginInfo.CreateDate = DateTime.Now;
        //            LoginInfo.Token = token;
        //            result = true;
        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //        throw new Exception(ex.StackTrace);
        //    }
        //    return result;
        //}

        /// <summary>
        /// 发送信息
        /// </summary>
        /// <param name="Message">信息内容</param>
        /// <param name="fakeid">微信ID</param>
        public static bool SendMessage(string Message, string fakeid)
        {
            bool result = false;
            CookieContainer cookie = null;
            string token = null;
            cookie = LoginInfo.LoginCookie;//取得cookie
            token = LoginInfo.Token;//取得token

            string strMsg = System.Web.HttpUtility.UrlEncode(Message);  //对传递过来的信息进行url编码
            string padate = "type=1&content=" + strMsg + "&error=false&tofakeid=" + fakeid + "&token=" + token + "&ajax=1";
            string url = "https://mp.weixin.qq.com/cgi-bin/singlesend?t=ajax-response&lang=zh_CN";

            byte[] byteArray = Encoding.UTF8.GetBytes(padate); // 转化

            HttpWebRequest webRequest2 = (HttpWebRequest)WebRequest.Create(url);

            webRequest2.CookieContainer = cookie; //登录时得到的缓存

            webRequest2.Referer = "https://mp.weixin.qq.com/cgi-bin/singlemsgpage?token=" + token + "&fromfakeid=" + fakeid + "&msgid=&source=&count=20&t=wxm-singlechat&lang=zh_CN";

            webRequest2.Method = "POST";

            webRequest2.UserAgent = "Mozilla/5.0 (Windows NT 5.1; rv:2.0.1) Gecko/20100101 Firefox/4.0.1";

            webRequest2.ContentType = "application/x-www-form-urlencoded";

            webRequest2.ContentLength = byteArray.Length;

            Stream newStream = webRequest2.GetRequestStream();

            // Send the data.            
            newStream.Write(byteArray, 0, byteArray.Length);    //写入参数    

            newStream.Close();

            HttpWebResponse response2 = (HttpWebResponse)webRequest2.GetResponse();

            StreamReader sr2 = new StreamReader(response2.GetResponseStream(), Encoding.Default);

            string text2 = sr2.ReadToEnd();
            if (text2.Contains("ok"))
            {
                result = true;
            }
            return result;
        }
    }
}
