﻿using LitJson;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml;

namespace wxHelper
{
    /// <summary>
    /// 微信请求类
    /// </summary>
    public class WeiXinRequest
    {
        #region 属性
        /// <summary>
        /// 开发者微信号
        /// </summary>
        public string ToUserName { get; set; }
        /// <summary>
        /// 发送方帐号（一个OpenID）
        /// </summary>
        public string FromUserName { get; set; }
        /// <summary>
        /// 消息创建时间 （整型）
        /// </summary>
        public string CreateTime { get; set; }
        /// <summary>
        /// 消息类型（文本消息：text；图片消息：image;地理位置消息:location;链接消息:link;事件推送:event 语音为voice）
        /// </summary>
        public string MsgType { get; set; }
        /// <summary>
        /// 消息内容
        /// </summary>
        public string Content { get; set; }
        /// <summary>
        /// 消息id
        /// </summary>
        public string MsgId { get; set; }
        /// <summary>
        /// 图片链接
        /// </summary>
        public string PicUrl { get; set; }  
        /// <summary>
        /// 地理位置纬度
        /// </summary>
        public string Location_X { get; set; }
        /// <summary>
        /// 地理位置经度
        /// </summary>
        public string Location_Y { get; set; }
        /// <summary>
        /// 地图缩放大小
        /// </summary>
        public string Scale { get; set; }
        /// <summary>
        /// 地理位置信息
        /// </summary>
        public string Label { get; set; }
        /// <summary>
        /// 消息标题
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 消息描述
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// 消息链接
        /// </summary>
        public string Url { get; set; }
        /// <summary>
        /// 事件类型，subscribe(订阅)、unsubscribe(取消订阅)、CLICK(自定义菜单点击事件)、LOCATION(地理位置)
        /// </summary>
        public string Event { get; set; }
        /// <summary>
        /// 事件KEY值，与自定义菜单接口中KEY值对应
        /// </summary>
        public string EventKey { get; set; }
        /// <summary>
        /// 地理位置纬度(用户上报地理位置)
        /// </summary>
        public string Latitude { get; set; }
        /// <summary>
        /// 地理位置经度(用户上报地理位置)
        /// </summary>
        public string Longitude { get; set; }
        /// <summary>
        /// 地理位置精度(用户上报地理位置)
        /// </summary>
        public string Precision { get; set; }
        /// <summary>
        /// 语音消息媒体id，可以调用多媒体文件下载接口拉取该媒体
        /// </summary>
        public string MediaID { get; set; }
        /// <summary>
        /// 语音识别结果，UTF8编码
        /// </summary>
		public string Recognition { get; set; }
        #endregion
        #region 方法
        /// <summary>
        /// 获取请求实体
        /// </summary>
        public static WeiXinRequest RequestHelper(bool bug)
        {
            string postStr="";
            Stream s = System.Web.HttpContext.Current.Request.InputStream;
                byte[] b = new byte[s.Length];
                s.Read(b, 0, (int)s.Length);
                postStr = Encoding.UTF8.GetString(b);
                if (bug)
                {
                    common.WriteTxt(postStr);
                }
                if (!string.IsNullOrEmpty(postStr))
                {
                    //封装请求类
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(postStr);
                    XmlElement rootElement = doc.DocumentElement;

                    XmlNode MsgType = rootElement.SelectSingleNode("MsgType");

                    WeiXinRequest requestXML = new WeiXinRequest();
                    requestXML.ToUserName = rootElement.SelectSingleNode("ToUserName").InnerText;
                    requestXML.FromUserName = rootElement.SelectSingleNode("FromUserName").InnerText;
                    requestXML.CreateTime = rootElement.SelectSingleNode("CreateTime").InnerText;
                    requestXML.MsgType = MsgType.InnerText;
                    switch (requestXML.MsgType)
                    {
                        case "text":
                            {
                                requestXML.Content = rootElement.SelectSingleNode("Content").InnerText;
                                requestXML.MsgId = rootElement.SelectSingleNode("MsgId").InnerText;
                            }
                            break;
                        case "location":
                            {
                                requestXML.Location_X = rootElement.SelectSingleNode("Location_X").InnerText;
                                requestXML.Location_Y = rootElement.SelectSingleNode("Location_Y").InnerText;
                                requestXML.Scale = rootElement.SelectSingleNode("Scale").InnerText;
                                requestXML.Label = rootElement.SelectSingleNode("Label").InnerText;
                            }break;
                            case "image":
                            {
                                requestXML.PicUrl = rootElement.SelectSingleNode("PicUrl").InnerText;
                            }
                            break;
                            case "event":
                            {
                                requestXML.Event = rootElement.SelectSingleNode("Event").InnerText;
                                requestXML.EventKey = rootElement.SelectSingleNode("EventKey").InnerText;
                            }
                            break;
							case "voice":
							{
								requestXML.Recognition = rootElement.SelectSingleNode("Recognition").InnerText;
							}
							break;
                    }
                    return requestXML;
                }
                else
                {
                    return null;
                }
        }

        public static string GetPage(string posturl, string postData)
        {
            Stream outstream = null;
            Stream instream = null;
            StreamReader sr = null;
            HttpWebResponse response = null;
            HttpWebRequest request = null;
            Encoding encoding = Encoding.UTF8;
            byte[] data = encoding.GetBytes(postData);
            // 准备请求...
            try
            {
                // 设置参数
                request = WebRequest.Create(posturl) as HttpWebRequest;
                CookieContainer cookieContainer = new CookieContainer();
                request.CookieContainer = cookieContainer;
                request.AllowAutoRedirect = true;
                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = data.Length;
                outstream = request.GetRequestStream();
                outstream.Write(data, 0, data.Length);
                outstream.Close();
                //发送请求并获取相应回应数据
                response = request.GetResponse() as HttpWebResponse;
                //直到request.GetResponse()程序才开始向目标网页发送Post请求
                instream = response.GetResponseStream();
                sr = new StreamReader(instream, encoding);
                //返回结果网页（html）代码
                string content = sr.ReadToEnd();
                string err = string.Empty;
                return content;
            }
            catch (Exception ex)
            {
                string err = ex.Message;
                return string.Empty;
            }
        }
        /// <summary>
        /// 获取token
        /// </summary>
        public static string GetAccess_token(string appid,string secret)
        {
                string strData = common.HttpGet("https://api.weixin.qq.com/cgi-bin/token", string.Format("grant_type=client_credential&appid={0}&secret={1}", appid,secret));
                if (strData.Contains("access_token"))
				{
					JsonData jd = JsonMapper.ToObject(strData);
					return  (string)jd["access_token"];
				}
            return "";
        }

        /// <summary>
        /// 获取jsapi_ticket
        /// </summary>
        public static string GetJsapi_ticket(string accesstoken)
        {
            string strData = common.HttpGet("https://api.weixin.qq.com/cgi-bin/ticket/getticket", string.Format("access_token={0}&type=jsapi", accesstoken));
            Xinying.Common.Utils.WriteTxt("strData:" + strData);
            if (strData.Contains("ticket"))
            {
                JsonData jd = JsonMapper.ToObject(strData);
                return (string)jd["ticket"];
            }
            return "";
        }
        #endregion

    }
}
