﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace wxHelper
{
	public class CusService
	{
		#region 属性
        /// <summary>
        /// 普通用户openid 
        /// </summary>
        public string touser { get; set; }
        /// <summary>
        /// 消息类型
        /// </summary>
        public string msgtype { get; set; }
        /// <summary>
        /// 文本消息内容 
        /// </summary>
        public string content { get; set; }
        /// <summary>
        /// 发送的图片的媒体ID 
        /// </summary>
        public string media_id { get; set; }
        /// <summary>
        /// 视频缩略图的媒体ID 
        /// </summary>
        public string thumb_media_id { get; set; }
        /// <summary>
        /// 音乐标题 
        /// </summary>
        public string title { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        public string description { get; set; }
       /// <summary>
        /// 链接 
       /// </summary>
        public string musicurl { get; set; }
        /// <summary>
        /// 高品质音乐链接，wifi环境优先使用该链接播放音乐
        /// </summary>
        public string hqmusicurl { get; set; }
        /// <summary>
        /// 点击后跳转的链接 
        /// </summary>
        public string url { get; set; }
        /// <summary>
        /// 图文消息的图片链接
        /// </summary>
        public string picurl { get; set; }
		#endregion
        #region 方法
        /// <summary>
        /// 发送文本消息
        /// </summary>
        /// <param name="model"></param>
        public static string ResText(CusService model,string token)
        {
            StringBuilder str = new StringBuilder();
            str.Append("{\"touser\":\""+model.touser+"\",");
            str.Append("\"msgtype\":\"text\",\"text\":{\"content\":\""+model.content+"\"}}");
            return WeiXinRequest.GetPage(string.Format("https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token={0}",token),str.ToString());
        }

        /// <summary>
        /// 发送图片
        /// </summary>
        public static string ResPic(CusService model, string token)
        {
            StringBuilder str = new StringBuilder();
            str.Append("{\"touser\":\"" + model.touser + "\",");
            str.Append("\"msgtype\":\"image\",\"image\":{\"media_id\":\"" + model.content + "\"}}");
            return WeiXinRequest.GetPage(string.Format("https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token={0}", token), str.ToString());
        }
        #endregion
    }
}
