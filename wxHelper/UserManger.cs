﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace wxHelper
{
    public class UserManger
    {
        #region 属性
        /// <summary>
        /// 分组ID
        /// </summary>
        public int id { get; set; }
        /// <summary>
        /// 分组名字
        /// </summary>
        public string name { get; set; }
        #endregion
        /// <summary>
        /// 查询分组
        /// </summary>
        public static string Query(string token)
        {
            string str = string.Format("https://api.weixin.qq.com/cgi-bin/groups/get?access_token={0}",token);
            return common.HttpGet(str,"");
        }
        /// <summary>
        /// 创建分组
        /// </summary>
        public static string Create(string name, string token)
        {
            string str = string.Format("https://api.weixin.qq.com/cgi-bin/groups/create?access_token={0}",token);
            string strData = "{\"group\":{\"name\":\"" + name + "\"}}";
            return WeiXinRequest.GetPage(str,strData);

        }
        /// <summary>
        /// 修改分组
        /// </summary>
        public static string Update(int id, string newname, string token)
        {
            string str = string.Format("https://api.weixin.qq.com/cgi-bin/groups/update?access_token={0}",token);
            string strData = "{\"group\":{\"id\":"+id.ToString()+",\"name\":\""+newname+"\"}}";
            return WeiXinRequest.GetPage(str,strData);
        }
    }
}
