﻿# WePaySystem

#### Description
Wechat authorized payment system can provide APIs for third-party systems. It is used to solve the problem that there is only one pain point in the authorization callback address when the multi station group authorization payment of the same merchant is made. The third party can call wechat authorization through this system, receive the relevant information obtained after authorization, call wechat payment through this system, obtain payment callback, and timely notify the third party of the callback information for processing.

#### Software Architecture
Software architecture description
1. The third party sends the order request to the payment system (/ ASPX / pay / wxpay. ASPX)
2. The payment system performs security verification (including the compliance of identity parameters of third-party developers), and returns the actual payment address.
3. Enter the actual payment address and pass in the order parameters. Verify again. The normal order data is saved to the table wepayorder.
4. Call wechat payment. The wechat payment order number is generated and submitted to Tencent server when the page is loaded.
5. Asynchronous and synchronous callback addresses are set according to version.
5.1 v1.0: set to the asynchronous and synchronous callback address corresponding to the third-party system
5.2 v2.0: set as the callback address of the system, and send a notice to the third party after callback processing
6. Wechat payment callback
6.1 repeated payment verification, amount verification
6.2 update payment status, payment time and other relevant information according to the order number of the payment system submitted to Tencent server
6.3 synchronous callback = = "return to the third party synchronous callback address with parameters and receive the response (httpget)
6.4 asynchronous callback = = "if the unresponsive parameters with on board are returned to the third party and jump to the asynchronous callback address of the third party and receive the response (httpget), the responded ones are ignored
6.5 number of global asynchronous notifications: a total of 13 notifications (Global notifications, httpget) will be initiated. After synchronous and asynchronous notifications are forwarded, the notification frequency can be slightly reduced. The system has successfully recalled but failed to receive the third party's response to the request version 2.0 order sending notice, the notice time is (1m / 2m / 3m / 4m / 5m / 10m / 20m / 30m / 1H / 2H / 5h / 12h / 24h from the callback time), a total of 13 times, the third party will not asynchronous notice after returning "success"

#### Several Points of Consideration
1. Why do we have to repeatedly verify the relevant parameters when making payment after the first handshake?
Answer: Make sure that the data transmitted twice is correct, because after the first handshake, the third-party server can add logic processing.
2.What's the point of v1.0?
A: stability: if a large number of successful payment notices to the third party are not sent due to the system downtime, the third party system will lose a large number of orders.
   Security: the direct communication between Tencent server and the third-party server must be more secure than adding a transit server in the middle.
   Complexity of system development and efficiency of system operation: callback is directly handled by a third party, which makes the system lighter, simpler and more efficient.
   Of course, if the account splitting operation with the third-party system is considered, the system needs to obtain the callback, and then notify the third party, otherwise it is impossible to determine which orders are actually paid.

#### Format conventions for third-party developers
1. appid: 16 bits
2. appsecret: 32 bits
3. Third party order number: 10 bits. Different types of orders can be distinguished by the first character.
4. The synchronous address and asynchronous address must be complete, accessible and not allowed to carry parameters.

####Operating instructions:
1. Deploy the website and grant read-write permission to the log folder
2. Public account management background (certified service number) configuration: JS security domain name, callback domain name, business domain name, server IP white list
3. Merchant platform setting: JS payment directory
4.xmlconfig/wxpay.config
5.xmlconfig/site.config wechat related parameters (original ID: wxid / token: wxtoken / developer ID: wxappid / developer password: wxappsecret)
6. Enter the management background http://www.xxx.com/admin/login.aspx (log in with the default account admin and password Admin888)
7. Site management - in the third-party developer management, create the third-party developer account. You can randomly generate the appid and appsecret, or fill in them manually.
8. Third party call method:
8.1 authorization of sub parent account: refer to / demo / wxoauth.aspx for wechat authorization call
8.2 payment call: refer to / demo / wepay.aspx for payment call
8.3 payment callback: demo / wxpay folder
9. Debugging method: log recording can be used for debugging (xinying.common.utilities.writetext method can write logs)