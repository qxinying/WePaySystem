﻿using System;
using System.IO;
using System.Net;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Xinying.Common
{
    public class Sms
    {
        private string sid;
        private string token;
        private string appid;
        public Sms(string sid, string token, string appid)
        {
            this.sid = sid;
            this.token = token;
            this.appid = appid;
        }

        public bool Send(string mobiles, string templateid, string[] datas)
        {
            DateTime dt = DateTime.Now;
            var temp =$"{sid}{token}{dt:yyyyMMddHHmmss}";
            var authstr = Utils.GetCoding($"{sid}:{dt:yyyyMMddHHmmss}");
            var url =
                $"https://app.cloopen.com:8883/2013-12-26/Accounts/{sid}/SMS/TemplateSMS?sig={Utils.MD5(temp).ToUpper()}";
            var json = new
            {
                to = mobiles,
                appId = appid,
                templateId = templateid,
                datas =datas
            };
            var data = JsonConvert.SerializeObject(json);
            var req = WebRequest.Create(url) as HttpWebRequest;
            req.ContentType = "application/json;charset=utf-8";
            req.ContentLength = Encoding.UTF8.GetBytes(data).Length;
            req.Method = "POST";
            req.Accept = "application/json";
            req.Headers.Add("Authorization", authstr);
            using (var reqstream = new StreamWriter(req.GetRequestStream()))
            {
                reqstream.Write(data);
                reqstream.Close();
            }
            using (var response = req.GetResponse().GetResponseStream())
            {
                using (var reader = new StreamReader(response, Encoding.UTF8))
                {
                    var str = reader.ReadToEnd();
                    JObject jobj = JsonConvert.DeserializeObject<JObject>(str);
                    if (jobj.GetValue("statusCode").Value<string>() == "000000")
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
        }
    }
}