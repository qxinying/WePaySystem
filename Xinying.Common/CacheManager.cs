﻿using System.Collections.Concurrent;

namespace Xinying.Common
{
    /// <summary>
    /// 缓存管理类。
    /// </summary>
    /// <typeparam name="TValue"></typeparam>
    public class CacheManager<TValue>
    {
         private static  readonly  object Locker = new object();
        private static CacheManager<TValue> _instance; 

        readonly  ConcurrentDictionary<string,TValue> _cache = new ConcurrentDictionary<string, TValue>(); 

        private CacheManager()
        {
        }
        public  static CacheManager<TValue> GetInstance()
        {
            if (_instance == null)
            {
                lock (Locker)
                {
                    if (_instance == null)
                    {
                        _instance = new CacheManager<TValue>();
                    }
                }
            }
            return _instance;
        }
        /// <summary>
        /// 验证key是否存在
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public bool ContainsKey(string key)
        {
            return _cache.ContainsKey(key);
        }
        /// <summary>
        /// 根据key获取value
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public TValue Get(string key)
        {
            TValue result;
            return _cache.TryGetValue(key, out result) ? result : default(TValue);
        }
        /// <summary>
        /// 根据key获取value
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public TValue this[string key] => this.Get(key);

        public void Add(string key, TValue value)
        {
            _cache.GetOrAdd(key, value);
        }

        public void Remove(string key)
        {
            TValue result;
            _cache.TryRemove(key, out result);
        }
        public void RemoveAll()
        {
            _cache.Clear();
        }
    }
}