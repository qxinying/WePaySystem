﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Xinying.Common
{
    public static class ExtMethod
    {
        #region 字符串类型转换
        /// <summary>
        /// 字符串转换成数字
        /// </summary>
        /// <param name="str">源字符串</param>
        /// <param name="defvalue">转换失败的默认值</param>
        /// <returns>转换后的结果</returns>
        public static int ToInt(this string str, int defvalue = 0)
        {
            return Utils.StrToInt(str, defvalue);
        }

        public static float ToFloat(this string str, float defvalue = 0)
        {
            return Utils.StrToFloat(str, defvalue);
        }
        #endregion

        #region 字符串相关操作扩展
        /// <summary>
        /// 判断是否为Null或者为Empty
        /// </summary>
        /// <param name="str">源字符串</param>
        /// <returns></returns>
        public static bool IsNullOrEmpty(this string str)
        {
            return string.IsNullOrEmpty(str);
        }
        #endregion

        #region Object类型转换
        /// <summary>
        /// Object类型转换成时间类型
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="defaultTime">转换失败的默认值</param>
        /// <returns></returns>
        public static DateTime ToDateTime(this object obj, DateTime? defaultTime = null)
        {
            if (defaultTime == null)
                defaultTime = DateTime.Now;
            return Utils.ObjectToDateTime(obj, defaultTime.Value);
        }
        /// <summary>
        /// 转换成字符串， 如果对象为空，则返回空字符
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string ToStr(this object obj)
        {
            if (obj == null)
            {
                return "";
            }
            else
            {
                return obj.ToString();
            }
        }
        /// <summary>
        /// 转换成字符串， 如果对象为空，则返回空字符
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string ToStr(this object obj,string defaultString)
        {
            if (obj == null)
            {
                return defaultString;
            }
            else
            {
                return obj.ToString();
            }
        }

        public static int ToInt(this object obj, int defvalue = 0)
        {
            return Utils.ObjToInt(obj, defvalue);
        }
        public static int ToInt(this object obj)
        {
            return Utils.ObjToInt(obj, 0);
        }

        public static decimal ToDecimal(this object obj)
        {
            return Utils.ObjToDecimal(obj, 0);
        }

        public static decimal ToDecimal(this object obj, int defvalue = 0)
        {
            return Utils.ObjToDecimal(obj, defvalue);
        }

        /// <summary>
        /// 将object转换成DateTime的“yyyy-MM-dd”格式的字符串，如非时间格式输出当前时间格式，如null默认输出""
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string ToDateStr(this object obj)
        {
            if (obj != null)
            {
                DateTime dt=Utils.StrToDateTime(obj.ToString(),DateTime.Parse("1900-01-01"));
                if (dt != null)
                {
                    return dt.ToString("yyyy-MM-dd");
                }
            }
            return "";
        }

        /// <summary>
        /// 将object转换成DateTime的“yyyy-MM”格式的字符串，如非时间格式输出当前时间格式，如null默认输出""
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string ToDateYearMonthStr(this object obj)
        {
            if (obj != null)
            {
                DateTime dt=Utils.StrToDateTime(obj.ToString(),DateTime.Parse("1900-01"));
                if (dt != null)
                {
                    return dt.ToString("yyyy-MM");
                }
            }
            return "";
        }

        /// <summary>
        /// 将object转换成DateTime的“yyyy-MM-dd HH:mm:ss”格式的字符串，如非时间格式输出"1900-01-01 00:00:00"，如null默认输出""
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string ToDateTimeStr(this object obj)
        {
            if (obj != null)
            {
                DateTime dt=Utils.StrToDateTime(obj.ToString(),DateTime.Parse("1900-01-01"));
                if (dt != null)
                {
                    return dt.ToString("yyyy-MM-dd HH:mm:ss");
                }
            }
            return "";
        }

        /// <summary>
        /// 将object转换成DateTime的“August 17, 2017 AM 00:01:01”格式的字符串，如非时间格式输出“January 1, 1900 AM 00:00:00”，如null默认输出""
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string ToEnglishDateTimeStr(this object obj)
        {
            if (obj != null)
            {
                DateTime dt=Utils.StrToDateTime(obj.ToString(),DateTime.Parse("1900-01-01 00:00:00"));
                if (dt != null)
                {
                    return Utils.GetEnglishMonth(dt.Month) + dt.ToString(" dd, yyyy ") + (dt.Hour > 12 ? "PM" : "AM") + dt.ToString(" hh:mm:ss");
                }
            }
            return "";
        }

        /// <summary>
        /// 将object转换成DateTime的“August 17, 2017”格式的字符串，如非时间格式输出"January 1, 1900"，如null默认输出""
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string ToEnglishDateStr(this object obj)
        {
            if (obj != null)
            {
                DateTime dt=Utils.StrToDateTime(obj.ToString(),DateTime.Parse("1900-01-01"));
                if (dt != null)
                {
                    return Utils.GetEnglishMonth(dt.Month) + dt.ToString(" dd, yyyy");
                }
            }
            return "";
        }

        /// <summary>
        /// 将object转换成DateTime的“yyyy-MM-dd HH:mm:ss”格式的字符串，如非时间格式输出当前时间格式，如null默认输出""
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string ToDateTimeStrExcept1900(this object obj)
        {
            if (obj != null)
            {
                DateTime dt=Utils.StrToDateTime(obj.ToString(),DateTime.Parse("1900-01-01"));
                if (dt != null && dt != DateTime.Parse("1900-01-01") && dt != DateTime.Parse("0001-01-01"))
                {
                    return dt.ToString("yyyy-MM-dd HH:mm:ss");
                }
            }
            return "";
        }

        /// <summary>
        /// 类似java中的charat
        /// 返回字符串中的某个字符
        /// </summary>
        /// <param name="s"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        public static char CharAt(this string s, int index)
        {
            if ((index >= s.Length) || (index < 0))
                return '\0';
            char[] charArray=s.ToCharArray();
            return charArray[index];
        }
        #endregion
    }
}
