﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WxApi.ReceiveEntity;
using WxApi.ReceiveEntity.Shop;

namespace WxApi.Shop
{
    public class OrderManage
    {
        /// <summary>
        /// 获取订单信息
        /// </summary>
        /// <param name="orderId"></param>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        public static OrderRev GetOrder(string orderId, string accessToken)
        {
            var url =
             string.Format("https://api.weixin.qq.com/merchant/order/getbyid?access_token={0}", accessToken);
            var obj = new { order_id = orderId };
            return Utils.PostResult<OrderRev>(obj, url);
        }
        /// <summary>
        /// 获取订单列表
        /// </summary>
        /// <param name="accessToken"></param>
        /// <param name="status"></param>
        /// <param name="beginTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        public static OrderList GetOrderList(string accessToken, int? status = null,
DateTime? beginTime = null, DateTime? endTime = null)
        {
            var url =
            string.Format("https://api.weixin.qq.com/merchant/order/getbyfilter?access_token={0}", accessToken);
            object obj = null;
            if (beginTime != null && endTime != null)
            {
                obj = new
                {
                    status = status,
                    begintime = Utils.ConvertDateTimeInt(beginTime.Value),
                    endtime = Utils.ConvertDateTimeInt(endTime.Value)
                };
            }
            else
            {
                obj = new { status = status };
            }
            return Utils.PostResult<OrderList>(obj, url);
        }
        /// <summary>
        /// 设置发货信息
        /// </summary>
        /// <param name="accessToken"></param>
        /// <param name="orderId"></param>
        /// <param name="deliveryCompany"></param>
        /// <param name="deliveryTrackNo"></param>
        /// <param name="needDelivery"></param>
        /// <param name="isOthers"></param>
        /// <returns></returns>
        public static ErrorEntity SetDelivery(string accessToken, string orderId,
string deliveryCompany,
    string deliveryTrackNo, int needDelivery = 1, int isOthers = 0)
        {
            var url =
            string.Format("https://api.weixin.qq.com/merchant/order/setdelivery?access_token={0}", accessToken);
            var obj = new
            {
                order_id = orderId,
                delivery_company = deliveryCompany,
                delivery_track_no = deliveryTrackNo,
                need_delivery = needDelivery,
                is_others = isOthers
            };
            return Utils.PostResult<ErrorEntity>(obj, url);
        }
        /// <summary>
        /// 关闭订单
        /// </summary>
        /// <param name="orderId"></param>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        public static ErrorEntity CloseOrder(string orderId, string accessToken)
        {
            var url =
            string.Format("https://api.weixin.qq.com/merchant/order/close?access_token={0}", accessToken);
            var obj = new { order_id = orderId };
            return Utils.PostResult<ErrorEntity>(obj, url);
        }

    }


}
