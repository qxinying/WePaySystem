﻿using WxApi.ReceiveEntity.Shop;

namespace WxApi.Shop
{
    /// <summary>
    /// 微信小店通用工具类
    /// </summary>
    public class Common
    {
        /// <summary>
        /// 图片上传
        /// </summary>
        /// <param name="filepath"></param>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        public static PicInfo UploadImg(string filepath, string accessToken)
        {
            var fsi = Utils.GetFileStreamInfo(filepath);
            var url =
            string.Format("https://api.weixin.qq.com/merchant/common/upload_img?access_token={0}&filename ={1}", accessToken, fsi.FileName);
            return Utils.PostResult<PicInfo>(fsi, url);
        }

    }
}
