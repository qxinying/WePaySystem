﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Web.Security;
using WxApi.ReceiveEntity;
using WxApi.SendEntity;

namespace WxApi
{
    public class JsApi
    {
        /// <summary>
        /// 获取jssdk Ticket
        /// </summary>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        public static JsApiTicket GetHsJsApiTicket(string accessToken)
        {
            var url = string.Format("https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token={0}&type=jsapi", accessToken);
            return Utils.GetResult<JsApiTicket>(url);
        }
        /// <summary>
        /// 获取jssdk签名
        /// </summary>
        /// <param name="noncestr">随机字符串</param>
        /// <param name="jsapi_ticket">ticket</param>
        /// <param name="timestamp">时间戳</param>
        /// <param name="url">当前网页的URL</param>
        /// <returns></returns>
        public static string GetJsApiSign(string noncestr, string jsapi_ticket, string timestamp, string url)
        {
            //将字段添加到列表中
            string[] arr = new[]
            {
                string.Format("noncestr={0}",noncestr),
                string.Format("jsapi_ticket={0}",jsapi_ticket),
                string.Format("timestamp={0}",timestamp),
                string.Format("url={0}",url)
             };
            //字典排序
            Array.Sort(arr);
            //使用URL键值对的格式拼接成字符串
            var temp = string.Join("&", arr);
            return FormsAuthentication.HashPasswordForStoringInConfigFile(temp,
            "SHA1");
        }
        public static WxJsParam GetJsParam(string appId,bool debug)
        {
            var param = new WxJsParam
            {
                appId = appId,
                debug =debug,
                nonceStr = Utils.GetTimeStamp().ToString(),
                timestamp =  Utils.GetTimeStamp(),
                url = Utils.GetRequestUrl()
            };
            return param;
        }
    }
}
