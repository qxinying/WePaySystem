﻿namespace WxApi.SendEntity.Card
{
    public class Member_Card : BaseCard
    {
        /// <summary>
        /// 是否支持易物币。若填写true，则易物币相关字段均为必填。若填写false，则易物币字段无须填写。 储值字段处理方式相同
        /// </summary>
        public bool supply_bonus { get; set; }
        /// <summary>
        /// 是否支持储值，填写true或false
        /// </summary>
        public bool supply_balance { get; set; }
        /// <summary>
        /// 易物币清零规则
        /// </summary>
        public string bonus_cleared { get; set; }
        /// <summary>
        /// 易物币规则
        /// </summary>
        public string bonus_rules { get; set; }
        /// <summary>
        /// 储值说明
        /// </summary>
        public string balance_rules { get; set; }
        /// <summary>
        /// 特权说明
        /// </summary>
        public string prerogative { get; set; }
        /// <summary>
        /// 绑定旧卡的URL，与“activate_url”字段二选一，必填
        /// </summary>
        public string bind_old_card_url { get; set; }
        /// <summary>
        /// 激活会员卡的URL，与“bind_old_card_url”字段二选一，必填
        /// </summary>
        public string activate_url { get; set; }
        /// <summary>
        /// 用户点击进入会员卡时是否推送事件
        /// </summary>
        public bool need_push_on_view { get; set; }
    }
}
