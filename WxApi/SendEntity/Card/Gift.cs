﻿namespace WxApi.SendEntity.Card
{
    public class Gift : BaseCard
    {
        /// <summary>
        /// 礼品券专用，表示礼品名字
        /// </summary>
        public string gift { get; set; }
    }
}
