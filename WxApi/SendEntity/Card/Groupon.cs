﻿namespace WxApi.SendEntity.Card
{
    public class Groupon : BaseCard
    {
        /// <summary>
        /// 团购详情
        /// </summary>
        public string deal_detail { get; set; }
    }
}
