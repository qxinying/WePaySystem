﻿namespace WxApi.SendEntity.Card
{
    /// <summary>
    /// 卡券基础信息类
    /// </summary>
    public abstract class BaseCard
    {
        public BaseInfo base_info { get; set; }
    }
}
