﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace WxApi.SendEntity
{
    public class FileStreamInfo : MemoryStream
    {
        /// <summary>
        /// 文件名
        /// </summary>
        public string FileName { get; set; }
    }

}
