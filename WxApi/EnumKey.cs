﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WxApi
{
    /// <summary>
    /// 素材类型枚举
    /// </summary>
    public enum MaterialType
    {
        /// <summary>
        /// 图片（image）: 2MB，支持bmp/png/jpeg/jpg/gif格式
        /// </summary>
        image,
        /// <summary>
        /// 语音（voice）：5MB，播放长度不超过60s，支持mp3/wma/wav/amr格式
        /// </summary>
        voice,
        /// <summary>
        /// 视频（video）：20MB，支持rm/rmvb/wmv/avi/mpg/mpeg/mp4格式
        /// </summary>
        video,
        /// <summary>
        /// 缩略图（thumb）：64KB，支持jpg格式
        /// </summary>
        thumb,
        /// <summary>
        /// 图文
        /// </summary>
        news
    }


    /// <summary>
    /// 消息类型枚举
    /// </summary>
    public enum MsgType
    {
        /// <summary>
        ///文本类型
        /// </summary>
        TEXT,
        /// <summary>
        /// 图片类型
        /// </summary>
        IMAGE,
        /// <summary>
        /// 语音类型
        /// </summary>
        VOICE,
        /// <summary>
        /// 视频类型
        /// </summary>
        VIDEO,
        /// <summary>
        /// 地理位置类型
        /// </summary>
        LOCATION,
        /// <summary>
        /// 链接类型
        /// </summary>
        LINK,
        /// <summary>
        /// 事件类型
        /// </summary>
        EVENT,
        /// <summary>
        /// 小视频
        /// </summary>
        SHORTVIDEO
    }


    /// <summary>
    /// 事件类型枚举
    /// </summary>
    public enum EventType
    {
        /// <summary>
        /// 非事件类型
        /// </summary>
        NOEVENT,
        /// <summary>
        /// 订阅
        /// </summary>
        SUBSCRIBE,
        /// <summary>
        /// 取消订阅
        /// </summary>
        UNSUBSCRIBE,
        /// <summary>
        /// 扫描带参数的二维码
        /// </summary>
        SCAN,
        /// <summary>
        /// 地理位置
        /// </summary>
        LOCATION,
        /// <summary>
        /// 单击按钮
        /// </summary>
        CLICK,
        /// <summary>
        /// 链接按钮
        /// </summary>
        VIEW,
        /// <summary>
        /// 扫码推事件
        /// </summary>
        SCANCODE_PUSH,
        /// <summary>
        /// 扫码推事件且弹出“消息接收中”提示框
        /// </summary>
        SCANCODE_WAITMSG,
        /// <summary>
        /// 弹出系统拍照发图
        /// </summary>
        PIC_SYSPHOTO,
        /// <summary>
        /// 弹出拍照或者相册发图
        /// </summary>
        PIC_PHOTO_OR_ALBUM,
        /// <summary>
        /// 弹出微信相册发图器
        /// </summary>
        PIC_WEIXIN,
        /// <summary>
        /// 弹出地理位置选择器
        /// </summary>
        LOCATION_SELECT,
        /// <summary>
        /// 模板消息推送
        /// </summary>
        TEMPLATESENDJOBFINISH,
        /// <summary>
        /// 群发消息推送
        /// </summary>
        MASSSENDJOBFINISH,
        /// <summary>
        /// 创建客服会话
        /// </summary>
        KF_CREATE_SESSION,
        /// <summary>
        /// 关闭客服会话
        /// </summary>
        KF_CLOSE_SESSION,
        /// <summary>
        /// 转接客服会话
        /// </summary>
        KF_SWITCH_SESSION,
        /// <summary>
        /// 微信小店订单
        /// </summary>
        MERCHANT_ORDER,
        /// <summary>
        /// 门店审核事件
        /// </summary>
        POI_CHECK_NOTIFY,
        /// <summary>
        /// 卡券通过审核
        /// </summary>
        CARD_PASS_CHECK,
        /// <summary>
        /// 卡券未通过审核
        /// </summary>
        CARD_NOT_PASS_CHECK,
        /// <summary>
        /// 用户领取卡券
        /// </summary>
        USER_GET_CARD,
        /// <summary>
        /// 用户删除卡券
        /// </summary>
        USER_DEL_CARD,
        /// <summary>
        /// 进入会员卡事件
        /// </summary>
        USER_VIEW_CARD,
        /// <summary>
        /// 卡券核销
        /// </summary>
        USER_CONSUME_CARD
    }
    /// <summary>
    /// 自定义菜单类型
    /// </summary>
    public enum MenuType
    {
        /// <summary>
        /// 点击推事件
        /// </summary>
        click,
        /// <summary>
        /// 跳转URL
        /// </summary>
        view,
        /// <summary>
        /// 扫码推事件
        /// </summary>
        scancode_push,
        /// <summary>
        /// 扫码推事件且弹出“消息接收中”提示框
        /// </summary>
        scancode_waitmsg,
        /// <summary>
        /// 弹出系统拍照发图
        /// </summary>
        pic_sysphoto,
        /// <summary>
        /// 弹出拍照或者相册发图
        /// </summary>
        pic_photo_or_album,
        /// <summary>
        /// 弹出微信相册发图器
        /// </summary>
        pic_weixin,
        /// <summary>
        /// 弹出地理位置选择器
        /// </summary>
        location_select,
        /// <summary>
        /// 下发消息（除文本消息）
        /// </summary>
        media_id,
        /// <summary>
        /// 跳转图文消息URL
        /// </summary>
        view_limited
    }

    public enum AuthType
    {
        /// <summary>
        /// 静默授权并自动跳转到回调页
        /// </summary>
        snsapi_base,
        /// <summary>
        /// 询问授权
        /// </summary>
        snsapi_userinfo
    }
    /// <summary>
    /// 客服会话操作类型
    /// </summary>
    public enum KfSessionType
    {
        /// <summary>
        /// 创建会话
        /// </summary>
        create,
        /// <summary>
        /// 关闭会话
        /// </summary>
        close
    }
    public enum CardStatus
    {
        /// <summary>
        /// 待审核
        /// </summary>
        CARD_STATUS_NOT_VERIFY,
        /// <summary>
        /// 审核失败
        /// </summary>
        CARD_STATUS_VERIFY_FALL,
        /// <summary>
        /// 通过审核
        /// </summary>
        CARD_STATUS_VERIFY_OK,
        /// <summary>
        /// 卡券被用户删除
        /// </summary>
        CARD_STATUS_USER_DELETE,
        /// <summary>
        /// 在公众平台投放过的卡券
        /// </summary>
        CARD_STATUS_DISPATCH
    }
    /// <summary>
    /// 卡券类型枚举
    /// </summary>
    public enum CardType
    {
        /// <summary>
        /// 通用券
        /// </summary>
        GENERAL_COUPON,
        /// <summary>
        /// 团购券
        /// </summary>
        GROUPON,
        /// <summary>
        /// 折扣券
        /// </summary>
        DISCOUNT,
        /// <summary>
        /// 礼品券
        /// </summary>
        GIFT,
        /// <summary>
        /// 代金券
        /// </summary>
        CASH,
        /// <summary>
        /// 会员卡
        /// </summary>
        MEMBER_CARD,
        /// <summary>
        /// 景点门票
        /// </summary>
        SCENIC_TICKET,
        /// <summary>
        /// 电影票
        /// </summary>
        MOVIE_TICKET,
        /// <summary>
        /// 飞机票
        /// </summary>
        BOARDING_PASS,
        /// <summary>
        /// 红包
        /// </summary>
        LUCKY_MONEY,
        /// <summary>
        /// 会议门票
        /// </summary>
        MEETING_TICKET
    }
    /// <summary>
    /// 账单类型
    /// </summary>
    public enum BillType
    {
        /// <summary>
        /// 返回当日所有订单信息，默认值
        /// </summary>
        ALL,
        /// <summary>
        /// 返回当日成功支付的订单
        /// </summary>
        SUCCESS,
        /// <summary>
        /// 返回当日退款订单
        /// </summary>
        REFUND,
        /// <summary>
        /// 已撤销的订单
        /// </summary>
        REVOKED
    }
}
