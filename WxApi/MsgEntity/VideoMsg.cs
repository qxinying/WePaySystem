﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WxApi.MsgEntity
{
    public class VideoMsg : BaseMsg
    {
        /// <summary>
        /// 缩略图ID
        /// </summary>
        public string ThumbMediaId { get; set; }
        /// <summary>
        /// 消息ID，64位整型
        /// </summary>
        public string MsgId { get; set; }
        /// <summary>
        /// 媒体ID
        /// </summary>
        public string MediaId { get; set; }
    }

}
