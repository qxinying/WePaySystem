﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WxApi.MsgEntity
{
    public class SubEventMsg : EventMsg
    {
        public string EventKey { get; set; }
        public string Ticket { get; set; }
    }

}
