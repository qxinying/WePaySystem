﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using WxApi.ReceiveEntity;
using WxApi.SendEntity;

namespace WxApi
{
    public class Menu
    {
        public static ErrorEntity Create(MenuEntity menuEntity, string accessToken)
        {
            var url =
            string.Format("https://api.weixin.qq.com/cgi-bin/menu/create?access_token={0}", accessToken);
            return Utils.PostResult<ErrorEntity>(menuEntity, url);
        }
        /// <summary>
        /// 自定义菜单查询
        /// </summary>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        public static MenuEntity Query(string accessToken)
        {
            var url =
            string.Format("https://api.weixin.qq.com/cgi-bin/menu/get?access_token={0}", accessToken);
            var jobj = Utils.GetResult<JObject>(url);
            var menu = jobj["menu"].ToString();
            return JsonConvert.DeserializeObject<MenuEntity>(menu);
        }
        /// <summary>
        /// 自定义菜单删除
        /// </summary>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        public static ErrorEntity Delete(string accessToken)
        {
            var url =
         string.Format("https://api.weixin.qq.com/cgi-bin/menu/delete?access_token={0}", accessToken);
            return Utils.GetResult<ErrorEntity>(url);
        }


    }
}
