﻿namespace WxApi.ReceiveEntity
{
    public class ConsumeRes : ErrorEntity
    {
        public CardId card { get; set; }
        public string openid { get; set; }
    }
    public class CardId
    {
        public string card_id { get; set; }
    }
}
