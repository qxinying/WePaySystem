﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WxApi.ReceiveEntity
{
    /// <summary>
    /// 用户openid列表
    /// </summary>
    public class UserListEntity : ErrorEntity
    {
        /// <summary>
        /// 关注该公众账号的总用户数
        /// </summary>
        public int total { get; set; }
        /// <summary>
        /// 拉取的openID个数，最大值为10 000
        /// </summary>
        public int count { get; set; }
        /// <summary>
        /// 列表数据，openID的列表
        /// </summary>
        public OpenidList data { get; set; }
        /// <summary>
        /// 拉取列表的后一个用户的openID
        /// </summary>
        public string next_openid { get; set; }
    }
    public class OpenidList
    {
        public List<string> openid { get; set; }
    }

}
