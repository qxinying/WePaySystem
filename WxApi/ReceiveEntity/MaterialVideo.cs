﻿namespace WxApi.ReceiveEntity
{
    /// <summary>
    /// 视频素材实体类
    /// </summary>
    public class MaterialVideo:ErrorEntity
    {
        public string title { get; set; }
        public string description { get; set; }
        public string down_url { get; set; }
    }
}
