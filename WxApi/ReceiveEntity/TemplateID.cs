﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WxApi.ReceiveEntity
{
    public class TemplateID : ErrorEntity
    {
        /// <summary>
        /// 模板ID
        /// </summary>
        public string template_id { get; set; }
    }

}
