﻿using System.Collections.Generic;
namespace WxApi.ReceiveEntity
{
    public class CardColors : ErrorEntity
    {
        public List<Color> colors { get; set; }
    }
    public class Color
    {
        public string name { get; set; }
        public string value { get; set; }
    }
}
