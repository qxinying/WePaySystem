﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WxApi.ReceiveEntity
{
    public class ShortUrl : ErrorEntity
    {
        public string short_url { get; set; }
    }

}
