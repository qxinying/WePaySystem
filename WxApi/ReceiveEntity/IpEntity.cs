﻿namespace WxApi.ReceiveEntity
{
    /// <summary>
    /// 获取IP列表接口返回实体
    /// </summary>
    public class IpEntity : ErrorEntity
    {
        /// <summary>
        /// IP列表
        /// </summary>
        public string[] ip_list { get; set; }
    }
}
