﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WxApi.ReceiveEntity.Shop
{
    public class ProductSku
    {
        /// <summary>
        /// sku信息 规则：id_info的组合个数必须与sku_table个数一致(若商品无sku信息, 
        ///即商品为统一规格，则此处赋值为空字符串即可)
    /// </summary>
    public string sku_id { get; set; }
        /// <summary>
        /// sku原价(单位 : 分)
        /// </summary>
        public int ori_price { get; set; }
        /// <summary>
        /// sku微信价(单位：分, 微信价必须比原价小；否则添加商品失败)
        /// </summary>
        public int price { get; set; }
        /// <summary>
        /// sku iconurl(图片需调用图片上传接口获得图片URL)
        /// </summary>
        public string icon_url { get; set; }
        /// <summary>
        /// sku库存
        /// </summary>
        public int quantity { get; set; }
        /// <summary>
        /// 商家商品编码
        /// </summary>
        public string product_code { get; set; }
    }

}
