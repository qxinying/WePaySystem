﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WxApi.ReceiveEntity.Shop
{
    /// <summary>
    /// 运费实体
    /// </summary>
    public class express
    {
        /// <summary>
        /// 快递ID
        /// </summary>
        public string id { get; set; }
        /// <summary>
        /// 运费(单位：分)
        /// </summary>
        public string price { get; set; }
    }
    /// <summary>
    /// 运费信息
    /// </summary>
    public class DeliveryInfo
    {
        /// <summary>
        /// 运费类型0——使用express字段的默认模板，1——使用template_id代表的邮费模板
        /// </summary>
        public int delivery_type { get; set; }
        /// <summary>
        /// 邮费模板ID
        /// </summary>
        public string template_id { get; set; }
    }

}
