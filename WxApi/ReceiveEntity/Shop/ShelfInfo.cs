﻿using WxApi.SendEntity.Shop;
namespace WxApi.ReceiveEntity.Shop
{
    public class ShelfInfo : ErrorEntity
    {
        public ShelfData shelf_info { get; set; }
        public int shelf_id { get; set; }
        public string shelf_banner { get; set; }
        public string shelf_name { get; set; }
    }
}
