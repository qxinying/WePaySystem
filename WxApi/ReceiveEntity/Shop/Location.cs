﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WxApi.ReceiveEntity.Shop
{
    /// <summary>
    /// 商品所在地地址
    /// </summary>
    public class Location
    {
        /// <summary>
        /// 国家
        /// </summary>
        public string country { get; set; }
        /// <summary>
        /// 省份
        /// </summary>
        public string province { get; set; }
        /// <summary>
        /// 城市
        /// </summary>
        public string city { get; set; }
        /// <summary>
        /// 详细地址
        /// </summary>
        public string address { get; set; }
    }
    public class AttrExt
    {
        /// <summary>
        /// 是否包邮（0——否，1——是)，如果包邮，则商品运费信息delivery_info字段可省略
        /// </summary>
        public int isPostFree { get; set; }
        /// <summary>
        /// 是否提供发票（0——否，1——是)
        /// </summary>
        public int isHasReceipt { get; set; }
        /// <summary>
        /// 是否保修（0——否，1——是)
        /// </summary>
        public int isUnderGuaranty { get; set; }
        /// <summary>
        /// 是否支持退换货（0——否，1——是)
        /// </summary>
        public int isSupportReplace { get; set; }
        /// <summary>
        /// 商品所在地地址
        /// </summary>
        public Location Location { get; set; }
    }

}
