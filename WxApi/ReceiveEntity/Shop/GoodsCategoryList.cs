﻿using System.Collections.Generic;
namespace WxApi.ReceiveEntity.Shop
{
    /// <summary>
    /// 获取指定分类返回的实体类
    /// </summary>
    public class GoodsCategoryList : ErrorEntity
    {
        public List<BaseEntity> cate_list { get; set; }
    }

}
