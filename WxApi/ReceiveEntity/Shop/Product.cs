﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WxApi.ReceiveEntity.Shop
{
    public class Product
    {
        /// <summary>
        /// 商品基本信息
        /// </summary>
        public ProductBase product_base { get; set; }
        /// <summary>
        /// sku列表
        /// </summary>
        public List<ProductSku> sku_list { get; set; }
        /// <summary>
        /// 其他属性
        /// </summary>
        public AttrExt attrext { get; set; }
        /// <summary>
        /// 运费信息
        /// </summary>
        public DeliveryInfo delivery_info { get; set; }
    }

}
