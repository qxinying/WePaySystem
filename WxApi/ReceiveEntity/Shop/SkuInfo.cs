﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WxApi.ReceiveEntity.Shop
{
    /// <summary>
    ///sku信息实体
    ///</summary>
    public class SkuInfo
    {
        public string id { get; set; }
        public List<string> vid { get; set; }
    }

    ///<summary>
    ///    属性键值实体
    ///  </summary>
    public class Property
    {
        ///   <summary>
        ///      属性ID
        ///     </summary>
        public string id { get; set; }
        /// <summary>
        ///     属性值
        ///    </summary>
        public string vid { get; set; }
    }
    ///<summary>
    ///  商品详情
    ///    </summary>
    public class Detail
    {
        /// <summary>
        ///    文字描述
        ///  </summary>
        public string text { get; set; }
        ///  <summary>
        /// 图片(图片需调用图片上传接口获得图片Url填写至此，否则无法添加商品)
        /// </summary>
        public string img { get; set; }
    }

    ///<summary>
    ///  商品基本信息
    /// </summary>
    public class ProductBase
    {
        ///  <summary>
        ///     商品名称
        ///     </summary>
        public string name { get; set; }
        ///  <summary>
        ///     商品分类id
        ///     </summary>
        public List<string> category_id { get; set; }
        /// <summary>
        /// 商品主图(图片需调用图片上传接口获得图片Url填写至此；否则无法添加商品。图片分辨率
        ///   推荐尺寸为640×600)
        ///  </summary>
        public string main_img { get; set; }
        ///  <summary>
        /// 商品图片列表(图片需调用图片上传接口获得图片Url填写至此；否则无法添加商品。图片分
        ///     辨率推荐尺寸为640px×600px)
        ///  </summary>
        public List<string> img { get; set; }
        /// <summary>
        ///    商品详情列表，显示在客户端的商品详情页内
        /// </summary>
        public List<Detail> detail { get; set; }
        ///  <summary>
        ///     商品属性列表
        ///     </summary>
        public List<Property> property { get; set; }
        /// <summary>
        ///     商品sku定义。如需自定义sku，格式为"$xxx"。如id="$整件库存"  vid="$1000"
        ///  </summary>
        public List<SkuInfo> sku_info { get; set; }
        ///<summary>
        ///     用户商品限购数量
        ///   </summary>
        public int buy_limit { get; set; }
    }

}
