﻿namespace WxApi.ReceiveEntity.Shop
{
    /// <summary>
    /// 微信小店基础类
    /// </summary>
    public class BaseEntity
    {
        public string id { get; set; }
        public string name { get; set; }
    }
}
