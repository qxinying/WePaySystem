﻿namespace WxApi.ReceiveEntity
{
    public class CardIdInfo : ErrorEntity
    {
        /// <summary>
        /// 卡券ID
        /// </summary>
        public string card_id { get; set; }
    }
}
