﻿namespace WxApi.ReceiveEntity
{
    /// <summary>
    /// 卡券logoURL实体
    /// </summary>
    public class LogoUrlEntity : ErrorEntity
    {
        public string url { get; set; }
    }
}
