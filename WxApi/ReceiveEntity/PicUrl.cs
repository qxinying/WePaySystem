﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WxApi.ReceiveEntity
{
    /// <summary>
    /// 上传图片获取URL实体
    /// </summary>
    public class PicUrl : ErrorEntity
    {
        public string url { get; set; }
    }
}
