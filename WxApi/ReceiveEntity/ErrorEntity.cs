﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace WxApi.ReceiveEntity
{
    /// <summary>
    /// 错误信息实体
    /// </summary>
    public class ErrorEntity
    {
        public int _errCode { get; set; }
        /// <summary>
        /// 错误编码
        /// </summary>
        public int ErrCode
        {
            get { return _errCode; }
            set
            {
                _errCode = value;
                //根据错误码，从错误列表中找到错误信息，并给ErrDescription赋值
                ErrDescription = ErrList.FirstOrDefault(e => e.Key == value).
                Value;
            }
        }
        /// <summary>
        /// 错误描述
        /// </summary>
        public string ErrDescription { get; set; }
        /// <summary>
        /// 错误信息。部分接口可能找不到对应的中文错误解释，此时英文错误信息可以作为一个参考。
        /// </summary>
        public string errmsg { get; set; }

        private static Dictionary<int, string> _errorDic;
        /// <summary>
        /// 用于多线程下的线程安全
        /// </summary>
        private static  object obj = new object();
        public static Dictionary<int, string> ErrList
        {
            get
            {
                lock (obj)
                {
                    if (_errorDic != null && _errorDic.Count > 0)
                        return _errorDic;
                    _errorDic = new Dictionary<int, string>();
                    var temp = Code.CodeInfo.Split(new char[] { '\r', '\n' },
                        StringSplitOptions.RemoveEmptyEntries);
                    foreach (var strArr in temp.Select(str => str.Split('\t')))
                    {
                        _errorDic.Add(int.Parse(strArr[0]), strArr[1]);
                    }
                    return _errorDic;
                }
               
            }
        }
    }
}
