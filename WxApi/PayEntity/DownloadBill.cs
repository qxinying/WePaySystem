﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WxApi.PayEntity
{
    public class DownloadBill : BasePay
    {
        /// <summary>
        /// 订单日期
        /// </summary>
        public string bill_date { get; set; }
        /// <summary>
        /// 订单类型
        /// </summary>
        public BillType bill_type { get; set; }
    }
}
