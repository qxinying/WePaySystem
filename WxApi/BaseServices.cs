﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using WxApi.ReceiveEntity;

namespace WxApi
{
    public class BaseServices
    {
        /// <summary>
        /// 接入验证URL
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public static bool ValidUrl(string token)
        {
            //获取参与校验的参数
            var signature = HttpContext.Current.Request.QueryString["signature"];
            var timestamp = HttpContext.Current.Request.QueryString["timestamp"];
            var nonce = HttpContext.Current.Request.QueryString["nonce"];
            string[] temp = { token, timestamp, nonce };
            Array.Sort(temp);//字典序排序
            var tempstr = string.Join("", temp);//拼接字符串 
                                                // SHA1加密
            var tempsign = FormsAuthentication.
                    HashPasswordForStoringInConfigFile(tempstr, "SHA1").ToLower();
            if (tempsign == signature)
            {
                var echostr = HttpContext.Current.Request.QueryString["echostr"];
                HttpContext.Current.Response.Write(echostr);
                return true;
            }
            return false;
        }
        /// <summary>
        /// 获取access_token
        /// </summary>
        /// <param name="appid">应用ID</param>
        /// <param name="secret">应用密钥</param>
        /// <returns>AccessToken实体</returns>
        public static AccessToken GetAccessToken(string appid, string secret)
        {
            var url =
            string.Format("https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid={0}&secret={1}", appid, secret);
            return Utils.GetResult<AccessToken>(url);
        }
        /// <summary>
        /// 获取微信服务器的IP列表
        /// </summary>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        public static IpEntity GetIpArray(string accessToken)
        {
            var url =
            string.Format("https://api.weixin.qq.com/cgi-bin/getcallbackip?access_token={0}", accessToken);
            return Utils.GetResult<IpEntity>(url);
        }

        /// <summary>
        /// 将一条长链接转成短链接
        /// </summary>
        /// <param name="longurl">长链接</param>
        /// <param name="accessToken">accessToken</param>
        /// <returns>包含短连接和错误代码的实体</returns>
        public static ShortUrl LongUrlToShortUrl(string longurl, string accessToken)
        {
            var url =
         string.Format("https://api.weixin.qq.com/cgi-bin/shorturl?access_token={0}", accessToken);
            var json = new { action = "long2short", long_url = longurl };
            return Utils.PostResult<ShortUrl>(json, url);
        }


    }
}
