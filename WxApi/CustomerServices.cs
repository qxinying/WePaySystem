﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using WxApi.ReceiveEntity;
using WxApi.SendEntity;
using WxApi.SendEntity.Card;

namespace WxApi
{
    /// <summary>
    /// 客服消息接口
    /// </summary>
    public class CustomerServices
    {
        /// <summary>
        /// http请求方式: POST
        /// </summary>
        private static string url =
       "https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token={0}";
        /// <summary>
        /// 发送文本
        /// </summary>
        public static ErrorEntity SendText(string openid, string content, string accessToen, string kfAccount = "")
        {
            var json = new
            {
                touser = openid,
                msgtype = "text",
                text = new
                {
                    content = content
                },
                customservice = new { kf_account = kfAccount }
            };
            return Send(json, accessToen);
        }
        /// <summary>
        /// 发送图片
        /// </summary>
        public static ErrorEntity SendImg(string openid, string media_id, string accessToen, string kfAccount = "")
        {
            var json = new
            {
                touser = openid,
                msgtype = "image",
                image = new
                {
                    media_id = media_id
                },
                customservice = new { kf_account = kfAccount }
            };
            return Send(json, accessToen);
        }
        /// <summary>
        /// 发送语音消息
        /// </summary>
        public static ErrorEntity SendVoice(string openid, string media_id, string accessToen, string kfAccount = "")
        {
            var json = new
            {
                touser = openid,
                msgtype = "voice",
                voice = new
                {
                    media_id = media_id
                },
                customservice = new { kf_account = kfAccount }
            };
            return Send(json, accessToen);
        }
        /// <summary>
        /// 发送视频消息
        /// </summary>
        public static ErrorEntity SendVideo(string openid, CustomVideo Video, string accessToen, string kfAccount = "")
        {
            var json = new
            {
                touser = openid,
                msgtype = "video",
                video = Video,
                customservice = new { kf_account = kfAccount }
            };
            return Send(json, accessToen);
        }
        /// <summary>
        /// 发送音乐消息
        /// </summary>
        public static ErrorEntity SendMusic(string openid, CustomMusic music, string accessToen, string kfAccount = "")
        {
            var json = new
            {
                touser = openid,
                msgtype = "music",
                music = music,
                customservice = new { kf_account = kfAccount }
            };
            return Send(json, accessToen);
        }
        /// <summary>
        /// 发送图文消息
        /// </summary>
        public static ErrorEntity SendArticle(string openid, CustomArticles article, string accessToen, string kfAccount = "")
        {
            var json = new
            {
                touser = openid,
                msgtype = "news",
                news = article,
                customservice = new { kf_account = kfAccount }
            };
            return Send(json, accessToen);
        }
        /// <summary>
        /// 客服接口发送卡券
        /// </summary>
        /// <param name="openid">接收者</param>
        /// <param name="cardId">卡券编号</param>
        /// <param name="cardTicket">卡券 api_ticket</param>
        /// <param name="accessToen"></param>
        /// <param name="kfAccount"></param>
        /// <returns></returns>
        public static ErrorEntity SendCard(string openid, string cardId, string cardTicket, string accessToen, string kfAccount = "")
        {
            #region 生成卡券ext参数
            var ext = new CardExt
            {
                api_ticket = cardTicket,
                timestamp = Utils.ConvertDateTimeInt(DateTime.Now).ToString(),
                card_id = cardId,
                openid = openid
            };
            //获取ext签名
            ext.signature = CardVoucher.GetJsApiSign(ext);
            //获取签名后，从实体对象中将api_ticket置空
            ext.api_ticket = null;
            ext.card_id = null;
            var set = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
                DefaultValueHandling = DefaultValueHandling.Ignore
            };
            var cardExt = JsonConvert.SerializeObject(ext, set);
            #endregion
            var json = new
            {
                touser = openid,
                msgtype = "wxcard",
                wxcard = new
                {
                    card_id = cardId,
                    card_ext = cardExt
                },
                customservice = new { kf_account = kfAccount }
            };
            return Send(json, accessToen);
        }
        private static ErrorEntity Send(object obj, string accessToen)
        {
            return Utils.PostResult<ErrorEntity>(obj, string.Format(url, accessToen));
        }

        #region 客服管理
        /// <summary>
        /// 设置客服信息
        /// </summary>
        /// <param name="account">账号</param>
        /// <param name="nickName">昵称</param>
        /// <param name="password">密码</param>
        /// <param name="accessToken"></param>
        /// <param name="isUpdate">是否是更新</param>
        /// <returns></returns>
        public static ErrorEntity SetAccount(string account, string nickName, string password, string accessToken, bool isUpdate = false)
        {
            var url =
            string.Format("https://api.weixin.qq.com/customservice/kfaccount/{0}?access_token={1}", isUpdate ? "update" : "add", accessToken);
            var obj = new
            {
                kf_account = account,
                nickname = nickName,
                password = Utils.MD5(password)
            };
            return Utils.PostResult<ErrorEntity>(obj, url);
        }
        /// <summary>
        /// 上传头像
        /// </summary>
        /// <param name="filepath">文件路径</param>
        /// <param name="account">账号</param>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        public static ErrorEntity UploadHeadImg(string filepath, string account, string accessToken)
        {
            var url = string.Format("http://api.weixin.qq.com/customservice/kfaccount/uploadheadimg?access_token={0}&kf_account={1}", accessToken, account);
            var formlist = new List<FormEntity> { new FormEntity { IsFile = true, Name = "media", Value = filepath } };
            return Utils.PostFormResult<ErrorEntity>(formlist, url);
        }
        /// <summary>
        /// 删除客服
        /// </summary>
        /// <param name="account"></param>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        public static ErrorEntity DeleteAccount(string account, string accessToken)
        {
            var url =
            string.Format("https://api.weixin.qq.com/customservice/kfaccount/del?access_token={0}&kf_account={1}", accessToken, account);
            return Utils.GetResult<ErrorEntity>(url);
        }
        /// <summary>
        /// 获取客服接待情况
        /// </summary>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        public static KfOnLineList GetKfOnLineList(string accessToken)
        {
            var url =
            string.Format("https://api.weixin.qq.com/cgi-bin/customservice/getonlinekflist?access_token={0}", accessToken);
            return Utils.GetResult<KfOnLineList>(url);
        }
        /// <summary>
        /// 获取客服列表
        /// </summary>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        public static KfList GetKfList(string accessToken)
        {
            var url =
            string.Format("https://api.weixin.qq.com/cgi-bin/customservice/getkflist?access_token={0}", accessToken);
            return Utils.GetResult<KfList>(url);
        }
        public static RecordList GetRecordList(DateTime startTime, DateTime endTime,
    int pageIndex, int pageSize,
    string accessToken, string openid = "")
        {
            var url =
            string.Format("https://api.weixin.qq.com/cgi-bin/customservice/getrecord?access_token={0}", accessToken);
            var obj = new
            {
                endtime = Utils.ConvertDateTimeInt(endTime),
                starttime = Utils.ConvertDateTimeInt(startTime),
                openid = openid,
                pageindex = pageIndex,
                pagesize = pageSize
            };
            return Utils.PostResult<RecordList>(obj, url);
        }
        /// <summary>
        /// 客服会话创建与关闭
        /// </summary>
        /// <param name="kfAccount">客服工号</param>
        /// <param name="openId">用户ID</param>
        /// <param name="text">附加文本</param>
        /// <param name="sessionType">请求的会话状态</param>
        public static ErrorEntity KfSession(string kfAccount, string openId, string
        text, KfSessionType sessionType, string accessToken)
        {
            var url = string.Format("https://api.weixin.qq.com/customservice/kfsession/{0}?access_token={1}", sessionType, accessToken);
            var obj = new { kf_account = kfAccount, openid = openId, text = text };
            return Utils.PostResult<ErrorEntity>(obj, url);
        }
        /// <summary>
        /// 获取客户会话状态
        /// </summary>
        /// <param name="openId"></param>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        public static KfSessionStatus GetsKfSessionStatus(string openId, string accessToken)
        {
            var url = string.Format("https://api.weixin.qq.com/customservice/kfsession/getsession?access_token={0}&openid={1}", accessToken, openId);
            return Utils.GetResult<KfSessionStatus>(url);
        }
        /// <summary>
        /// 获取客服的会话列表
        /// </summary>
        /// <param name="kfAccount"></param>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        public static KfSessionList GetKfSessionList(string kfAccount, string accessToken)
        {
            var url = string.Format("https://api.weixin.qq.com/customservice/kfsession/getsessionlist?access_token={0}&kf_account={1}", accessToken, kfAccount);
            return Utils.GetResult<KfSessionList>(url);
        }
        /// <summary>
        /// 获取未接入的会话列表
        /// </summary>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        public static WaitCaseList GetWaitCaseList(string accessToken)
        {
            var url = string.Format("https://api.weixin.qq.com/customservice/kfsession/getwaitcase?access_token={0}", accessToken);
            return Utils.GetResult<WaitCaseList>(url);
        }


        #endregion
    }
}
