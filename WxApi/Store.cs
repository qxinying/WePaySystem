﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using WxApi.ReceiveEntity;
using WxApi.SendEntity;

namespace WxApi
{
    public class Store
    {
        /// <summary>
        /// 上传门店logo
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        public static PicUrl UploadPic(string filePath, string accessToken)
        {
            var url = string.Format("https://file.api.weixin.qq.com/cgi-bin/media/uploadimg?access_token={0}", accessToken);
            var formlist = new List<FormEntity>();
            formlist.Add(new FormEntity
            {
                IsFile = true,
                Name = "buffer",
                Value = filePath
            });
            return Utils.PostResult<PicUrl>(formlist, url);
        }

        public static ErrorEntity Add(BaseStoreInfo info, string accessToken)
        {
            var url = string.Format("http://api.weixin.qq.com/cgi-bin/poi/addpoi?access_token={0}", accessToken);
            var obj = new { business = new { base_info = info } };
            return Utils.PostResult<ErrorEntity>(obj, url);
        }
        public static List<StoreCategory> GetCategory()
        {
            return JsonConvert.DeserializeObject<List<StoreCategory>>(Code.
            StoreCategory);
        }


    }
}
