﻿using System.Web;
using System.Text;
using System.IO;
using System.Net;
using System;
using System.Collections.Generic;
using System.Xml;
using Xinying.Common;

namespace Xinying.API.Payment.WxPay
{
    /// <summary>
    /// 类名：Submit
    /// 功能：微信接口日志目录
    /// 详细：记录调试信息
    /// 版本：1.0
    /// 修改日期：2015-11-23
    /// 说明：
    /// 以下代码只是为了方便商户测试而提供的样例代码，商户可以根据自己网站的需要，按照技术文档编写,并非一定要使用该代码。
    /// 该代码仅供学习和研究支付宝接口使用，只是提供一个参考
    /// </summary>
    public class WxPayLog
    {
        //日志目录
        public static string path = Utils.GetMapPath("/log/WxPaylog.txt");

        /// <summary>
        /// 记录日志
        /// </summary>
        /// <param name="content"></param>
        public static void Write(string content)
        {
            File.AppendAllText(path, content, System.Text.Encoding.UTF8);
        }
    }
}
