﻿using System.Web;
using System.Text;
using System.IO;
using System.Net;
using System;
using System.Xml;
using System.Collections.Generic;
using Xinying.Common;

namespace Xinying.API.Payment.WxPay
{
    /// <summary>
    /// 类名：Config
    /// 功能：基础配置类
    /// 详细：设置帐户有关信息及返回路径
    /// 版本：1.0
    /// 日期：2015-11-23
    /// 说明：
    /// 以下代码只是为了方便商户测试而提供的样例代码，商户可以根据自己网站的需要，按照技术文档编写,并非一定要使用该代码。
    /// 该代码仅供学习和研究支付宝接口使用，只是提供一个参考。
    /// </summary>
    public class WxPayConfig
    {
        #region 字段
        public static string AppID { get; set; }
        public static string AppSecret { get; set; }
        public static string MCHID { get; set; }
        public static string Key { get; set; }
        public static string NotifyUrl { get; set; }
        public static string ReturnUrl { get; set; }
        #endregion

        static WxPayConfig()
        {
            //↓↓↓↓↓↓↓↓↓↓请在这里配置您的基本信息↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
            
            //读取XML配置信息
            string fullPath = Utils.GetMapPath("~/xmlconfig/wxpay.config");
            XmlDocument doc = new XmlDocument();
            doc.Load(fullPath);
            XmlNode _appid = doc.SelectSingleNode(@"Root/appid");
            XmlNode _appsecret = doc.SelectSingleNode(@"Root/appsecret");
            XmlNode _mch_id = doc.SelectSingleNode(@"Root/mch_id");
            XmlNode _key = doc.SelectSingleNode(@"Root/key");
            //读取站点配置信息
            Model.siteconfig model = new BLL.siteconfig().loadConfig();

            //微信公众平台账户
            AppID = _appid.InnerText;
            //
            AppSecret = _appsecret.InnerText;
            //微信商户号
            MCHID = _mch_id.InnerText;
            //交易安全检验码，由数字和字母组成的32位字符串
            Key = _key.InnerText;
            //微信支付异步回调地址
            NotifyUrl = "/api/payment/wechatpay/payNotifyUrl.ashx";
            //微信支付同步回调地址
            ReturnUrl = "/api/payment/wechatpay/payReturnUrl.aspx";

            //↑↑↑↑↑↑↑↑↑↑请在这里配置您的基本信息↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑
        }
    }
}
