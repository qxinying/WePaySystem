﻿using System.Web;
using System.Text;
using System.IO;
using System.Net;
using System;
using System.Collections.Generic;
using System.Xml;

namespace Xinying.API.Payment.WxPay
{
    /// <summary>
    /// 类名：Submit
    /// 功能：微信各接口请求提交类
    /// 详细：构造微信各接口文本，获取远程HTTP数据
    /// 版本：1.0
    /// 修改日期：2015-11-23
    /// 说明：
    /// 以下代码只是为了方便商户测试而提供的样例代码，商户可以根据自己网站的需要，按照技术文档编写,并非一定要使用该代码。
    /// 该代码仅供学习和研究支付宝接口使用，只是提供一个参考
    /// </summary>
    public class WxPaySubmit
    {
        /// <summary>
        /// 生成扫描支付模式一URL
        /// </summary>
        /// <param name="productId">商品ID或订单标识</param>
        /// <returns></returns>
        public static string GetPrePayUrl(string productId)
        {
            SortedDictionary<string, string> sParaTemp = new SortedDictionary<string, string>();

            sParaTemp.Add("appid", WxPayConfig.AppID);//公众帐号id
            sParaTemp.Add("mch_id", WxPayConfig.MCHID);//商户号
            sParaTemp.Add("time_stamp", WxPayCore.GenerateTimeStamp());//时间戳
            sParaTemp.Add("nonce_str", WxPayCore.GenerateNonceStr());//随机字符串
            sParaTemp.Add("product_id", productId);//商品ID

            //过滤签名参数数组
            Dictionary<string, string> sPara = WxPayCore.FilterPara(sParaTemp);

            //签名结果
            string mysign = WxPayCore.BuildMysign(sPara, WxPayConfig.Key);
            sPara.Add("sign", mysign);//签名

            string str = WxPayCore.CreateLinkString(sPara);//转换为URL串
            string url = "weixin://wxpay/bizpayurl?" + str;

            return url;
        }
    }
}
