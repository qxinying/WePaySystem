﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web;
using Xinying.Common;

namespace Xinying.Web.UI
{
    public partial class WxuserBasePage : System.Web.UI.Page
    {
        protected internal Model.siteconfig config = new BLL.siteconfig().loadConfig();
        protected internal Model.userconfig uconfig = new BLL.userconfig().loadConfig();
        protected internal Model.channel_site site = new Model.channel_site();
        public string accessToken => WxApi.AccessTokenBox.GetTokenValue(config.wxappid, config.wxappsecret);
        public Model.wxuserinfo wxUserModel = null;
        /// <summary>
        /// 父类的构造函数
        /// </summary>
        public WxuserBasePage()
        {
            //是否关闭网站
            if (config.webstatus == 0)
            {
                HttpContext.Current.Response.Redirect(linkurl("error", "?msg=" + Utils.UrlEncode(config.webclosereason)));
                return;
            }
            //取得站点信息
            site = GetSiteModel();
            //抛出一个虚方法给继承重写
            ShowPage();
            #region 微信授权登录
            #region 如果是微信环境：如果已有微会员登录，那么不管；如果没有微会员登录，那么进行授权，并跳转到原来页面
            if (Utils.IsWeixin())
            {
                #region 获取授权的用户信息并更新数据库，同时记录微会员登录信息，如果已关联会员，同时登陆会员信息
                try
                {
                    string state = HttpContext.Current.Request.QueryString["state"];
                    if (state == "wechatoauth")
                    {
                        #region 更新用户信息
                        UserInfo model_userinfo = new UserInfo();
                        #region 从独立系统获取授权的用户信息
                        model_userinfo.openid = HttpContext.Current.Request.QueryString["openid"];
                        model_userinfo.city = HttpContext.Current.Request.QueryString["city"];
                        model_userinfo.country = HttpContext.Current.Request.QueryString["country"];
                        model_userinfo.headimgurl = HttpUtility.UrlDecode(HttpContext.Current.Request.QueryString["headimgurl"]);
                        model_userinfo.nickname = HttpContext.Current.Request.QueryString["nickname"];
                        model_userinfo.province = HttpContext.Current.Request.QueryString["province"];
                        model_userinfo.sex = HttpContext.Current.Request.QueryString["sex"].ToInt();//值为1时是男性，值为2时是女性，值为0时是未知
                        string o_is_subscribe = HttpContext.Current.Request.QueryString["is_subscribe"];
                        string o_wxpublicid = HttpContext.Current.Request.QueryString["wxpublicid"];
                        #endregion
                        #region 微信粉丝信息 存入本地或更新
                        if (model_userinfo != null && (!string.IsNullOrEmpty(model_userinfo.openid)))
                        {
                            UpdateWXUser(model_userinfo);
                            Model.wxuserinfo.UpdateUsersByOpenid(model_userinfo.openid);
                            #region 记录微会员登录信息，如果已关联会员，同时登陆会员信息
                            Model.wxuserinfo WxUserInfoOauthModel = Model.wxuserinfo.GetModelByOpenid(model_userinfo.openid);
                            if (WxUserInfoOauthModel != null)
                            {
                                Utils.WriteCookie("Xinying", "QXUOPID", Common.DESEncrypt.Encrypt(WxUserInfoOauthModel.openid));
                                Utils.WriteCookie("Xinying", "QXUserName", Common.DESEncrypt.Encrypt(WxUserInfoOauthModel.nickname));
                            }
                            #endregion
                        }
                        #endregion
                        #endregion
                    }
                }
                catch (Exception ex)
                {
                    Utils.WriteTxt("oauth error:" + ex);
                }
                #endregion
                //获得登录的微会员信息
                wxUserModel = GetWxUserInfo();
                if (wxUserModel != null)
                {

                }
                else
                {
                    #region 向独立支付系统发起授权请求
                    string uri = HttpContext.Current.Request.Url.ToString();
                    uri = Xinying.Common.Utils.WxreturnurlFilter(uri);//这里去除state参数，避免使下方拼接的url的真实的state参数失效
                    string urigetoauth = String.Format("{0}/aspx/oauth/oauthwx_forson.aspx?apiuser_appid={1}&apiuser_redirect_uri={2}&state={3}", XYKeys.STRING_WXPAY_SERVERURL, XYKeys.STRING_WXPAY_APPID, System.Web.HttpUtility.UrlEncode(uri), "wechatoauth");
                    HttpContext.Current.Response.Redirect(urigetoauth);
                    #endregion
                    return;
                }
            }
            #endregion
            #endregion
        }

        /// <summary>
        /// 页面处理虚方法
        /// </summary>
        protected virtual void ShowPage()
        {
            //虚方法代码
        }

        #region 页面通用方法==========================================
        /// <summary>
        /// 返回站点信息
        /// </summary>
        protected Model.channel_site GetSiteModel()
        {
            string requestDomain = HttpContext.Current.Request.Url.Authority.ToLower(); //获得来源域名含端口号
            string requestPath = HttpContext.Current.Request.RawUrl.ToLower(); //当前的URL地址
            string sitePath = GetSitePath(requestPath, requestDomain);
            Model.channel_site modelt = SiteDomains.GetSiteDomains().SiteList.Find(p => p.build_path == sitePath);
            return modelt;
        }

        /// <summary>
        /// 判断PC端还是移动端访问
        /// </summary>
        /// <param name="strUserAgent"></param>
        /// <returns>默认0为PC端,1为移动端</returns>
        public int JudgePCorMobile(out string strUserAgent)
        {
            int result = 0;//默认为PC端
            strUserAgent = HttpContext.Current.Request.UserAgent.ToString().ToLower();
            if (!string.IsNullOrEmpty(strUserAgent))
            {
                if (strUserAgent.Contains("mobile") || strUserAgent.Contains("iphone") || strUserAgent.Contains("android") || strUserAgent.Contains("blackberry") || strUserAgent.Contains("windows ce") || strUserAgent.Contains("opera") || strUserAgent.Contains("palm"))
                {
                    result = 1;
                }
            }

            return result;
        }

        /// <summary>
        /// 返回URL重写统一链接地址
        /// </summary>
        public string linkurl(string _key, params object[] _params)
        {
            Hashtable ht = new BLL.url_rewrite().GetList(); //获得URL配置列表
            Model.url_rewrite model = ht[_key] as Model.url_rewrite; //查找指定的URL配置节点

            //如果不存在该节点则返回空字符串
            if (model == null)
            {
                return string.Empty;
            }

            string requestDomain = HttpContext.Current.Request.Url.Authority.ToLower(); //获得来源域名含端口号
            string requestPath = HttpContext.Current.Request.RawUrl.ToLower(); //当前的URL地址
            string linkStartString = GetLinkStartString(requestPath, requestDomain); //链接前缀

            //如果URL字典表达式不需要重写则直接返回
            if (model.url_rewrite_items.Count == 0)
            {
                //检查网站重写状态
                if (config.staticstatus > 0)
                {
                    if (_params.Length > 0)
                    {
                        return linkStartString + GetUrlExtension(model.page, config.staticextension) + string.Format("{0}", _params);
                    }
                    else
                    {
                        return linkStartString + GetUrlExtension(model.page, config.staticextension);
                    }
                }
                else
                {
                    if (_params.Length > 0)
                    {
                        return linkStartString + model.page + string.Format("{0}", _params);
                    }
                    else
                    {
                        return linkStartString + model.page;
                    }
                }
            }
            //否则检查该URL配置节点下的子节点
            foreach (Model.url_rewrite_item item in model.url_rewrite_items)
            {
                //如果参数个数匹配
                if (IsUrlMatch(item, _params))
                {
                    //检查网站重写状态
                    if (config.staticstatus > 0)
                    {
                        return linkStartString + string.Format(GetUrlExtension(item.path, config.staticextension), _params);
                    }
                    else
                    {
                        string queryString = Regex.Replace(string.Format(item.path, _params), item.pattern, item.querystring, RegexOptions.None | RegexOptions.IgnoreCase);
                        if (queryString.Length > 0)
                        {
                            queryString = "?" + queryString;
                        }
                        return linkStartString + model.page + queryString;
                    }
                }
            }

            return string.Empty;
        }

        /// <summary>
        /// 根据站点目录和已生成的链接重新组合(实际访问页面用到)
        /// </summary>
        /// <param name="sitepath">站点目录</param>
        /// <param name="urlpath">URL链接</param>
        /// <returns>String</returns>
        public string getlink(string sitepath, string urlpath)
        {
            if (string.IsNullOrEmpty(sitepath) || string.IsNullOrEmpty(urlpath))
            {
                return urlpath;
            }
            string requestDomain = HttpContext.Current.Request.Url.Authority.ToLower(); //获取来源域名含端口号
            Dictionary<string, string> dic = SiteDomains.GetSiteDomains().Paths; //获取站点键值对
            //如果当前站点为默认站点则直接返回
            if (SiteDomains.GetSiteDomains().DefaultPath == sitepath.ToLower())
            {
                return urlpath;
            }
            //如果当前域名存在于域名列表则直接返回
            if (dic.ContainsKey(sitepath.ToLower()) && dic.ContainsValue(requestDomain))
            {
                return urlpath;
            }
            int indexNum = config.webpath.Length; //安装目录长度
            if (urlpath.StartsWith(config.webpath))
            {
                urlpath = urlpath.Substring(indexNum);
            }
            //安装目录+站点目录+URL
            return config.webpath + sitepath.ToLower() + "/" + urlpath;
        }

        /// <summary>
        /// 返回分页字符串
        /// </summary>
        /// <param name="pagesize">页面大小</param>
        /// <param name="pageindex">当前页</param>
        /// <param name="totalcount">记录总数</param>
        /// <param name="_key">URL映射Name名称</param>
        /// <param name="_params">传输参数</param>
        protected string get_page_link(int pagesize, int pageindex, int totalcount, string _key, params object[] _params)
        {
            return Utils.OutPageList(pagesize, pageindex, totalcount, linkurl(_key, _params), 8);
        }

        /// <summary>
        /// 返回分页字符串
        /// </summary>
        /// <param name="pagesize">页面大小</param>
        /// <param name="pageindex">当前页</param>
        /// <param name="totalcount">记录总数</param>
        /// <param name="linkurl">链接地址</param>
        protected string get_page_link(int pagesize, int pageindex, int totalcount, string linkurl)
        {
            return Utils.OutPageList(pagesize, pageindex, totalcount, linkurl, 8);
        }
        #endregion

        #region 会员用户方法==========================================
        /// <summary>
        /// 判断用户是否已经登录(解决Session超时问题)
        /// </summary>
        public bool IsUserLogin()
        {
            //如果Session为Null
            if (HttpContext.Current.Session[XYKeys.SESSION_USER_INFO] != null)
            {
                return true;
            }
            else
            {
                //检查Cookies
                string username = Utils.GetCookie(XYKeys.COOKIE_USER_NAME_REMEMBER, "Xinying");
                string password = Utils.GetCookie(XYKeys.COOKIE_USER_PWD_REMEMBER, "Xinying");
                if (username != "" && password != "")
                {
                    BLL.users bll = new BLL.users();
                    Model.users model = bll.GetModel(username, password, 0, 0, false);
                    if (model != null)
                    {
                        HttpContext.Current.Session[XYKeys.SESSION_USER_INFO] = model;
                        return true;
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// 取得用户信息
        /// </summary>
        public Model.users GetUserInfo()
        {
            if (IsUserLogin())
            {
                Model.users model = HttpContext.Current.Session[XYKeys.SESSION_USER_INFO] as Model.users;
                if (model != null)
                {
                    //为了能查询到最新的用户信息，必须查询最新的用户资料
                    model = new BLL.users().GetModel(model.id);
                    return model;
                }
            }
            return null;
        }
        #endregion

        #region 辅助方法(私有)========================================
        /// <summary>
        /// 获取当前页面包含的站点目录
        /// </summary>
        private string GetFirstPath(string requestPath)
        {
            int indexNum = config.webpath.Length; //安装目录长度
            //如果包含安装目录和aspx目录也要过滤掉
            if (requestPath.StartsWith(config.webpath + XYKeys.DIRECTORY_REWRITE_ASPX + "/"))
            {
                indexNum = (config.webpath + XYKeys.DIRECTORY_REWRITE_ASPX + "/").Length;
            }
            string requestFirstPath = requestPath.Substring(indexNum);
            if (requestFirstPath.IndexOf("/") > 0)
            {
                requestFirstPath = requestFirstPath.Substring(0, requestFirstPath.IndexOf("/"));
            }
            if (requestFirstPath != string.Empty && SiteDomains.GetSiteDomains().Paths.ContainsKey(requestFirstPath))
            {
                return requestFirstPath;
            }
            return string.Empty;
        }

        /// <summary>
        /// 获取链接的前缀
        /// </summary>
        /// <param name="requestPath">当前的URL地址</param>
        /// <param name="requestDomain">获得来源域名含端口号</param>
        /// <returns>String</returns>
        private string GetLinkStartString(string requestPath, string requestDomain)
        {
            string requestFirstPath = GetFirstPath(requestPath);//获得二级目录(不含站点安装目录)

            //检查是否与绑定的域名或者与默认频道分类的目录匹配
            if (SiteDomains.GetSiteDomains().Paths.ContainsValue(requestDomain))
            {
                return "/";
            }

            else if (requestFirstPath == string.Empty || requestFirstPath == SiteDomains.GetSiteDomains().DefaultPath)
            {
                return config.webpath;
            }
            else
            {
                return config.webpath + requestFirstPath + "/";
            }
        }

        /// <summary>
        /// 获取站点的目录
        /// </summary>
        /// <param name="requestPath">获取的页面，包含目录</param>
        /// <param name="requestDomain">获取的域名(含端口号)</param>
        /// <returns>String</returns>
        private string GetSitePath(string requestPath, string requestDomain)
        {
            //当前域名是否存在于站点目录列表
            if (SiteDomains.GetSiteDomains().Paths.ContainsValue(requestDomain))
            {
                return SiteDomains.GetSiteDomains().Domains[requestDomain];
            }

            // 获取当前页面包含的站点目录
            string pagePath = GetFirstPath(requestPath);
            if (pagePath != string.Empty)
            {
                return pagePath;
            }
            return SiteDomains.GetSiteDomains().DefaultPath;
        }

        /// <summary>
        /// 参数个数是否匹配
        /// </summary>
        private bool IsUrlMatch(Model.url_rewrite_item item, params object[] _params)
        {
            int strLength = 0;
            if (!string.IsNullOrEmpty(item.querystring))
            {
                strLength = item.querystring.Split('&').Length;
            }
            if (strLength == _params.Length)
            {
                //注意__id__代表分页页码，所以须替换成数字才成进行匹配
                if (Regex.IsMatch(string.Format(item.path, _params).Replace("__id__", "1"), item.pattern, RegexOptions.None | RegexOptions.IgnoreCase))
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// 替换扩展名
        /// </summary>
        private string GetUrlExtension(string urlPage, string staticExtension)
        {
            return Utils.GetUrlExtension(urlPage, staticExtension);
        }

        #endregion

        #region 微会员相关
        public class CodeModel
        {
            public string access_token { get; set; }
            public int expires_in { get; set; }
            public string refresh_token { get; set; }
            public string openid { get; set; }
            public string scope { get; set; }
        }

        public class UserInfo
        {
            public string openid { get; set; }
            public string nickname { get; set; }
            public int sex { get; set; }//用户的性别，值为1时是男性，值为2时是女性，值为0时是未知 
            public string province { get; set; }
            public string city { get; set; }
            public string country { get; set; }
            public string headimgurl { get; set; }
        }

        public class UserInfoIsfocus
        {
            public string openid { get; set; }
            public string nickname { get; set; }
            public int sex { get; set; }//用户的性别，值为1时是男性，值为2时是女性，值为0时是未知 
            public string province { get; set; }
            public string city { get; set; }
            public string country { get; set; }
            public string headimgurl { get; set; }
            public string subscribe_time { get; set; }
        }

        #region 微信粉丝信息 存入本地或更新 并且登录状态
        public static void UpdateWXUser(UserInfo model_userinfo)
        {
            if (Model.wxuserinfo.ExistsWhere("openid='" + model_userinfo.openid + "'"))
            {
                Model.wxuserinfo model_wxuserinfo = Model.wxuserinfo.GetModelByOpenid(model_userinfo.openid);
                if (model_wxuserinfo == null)
                {
                    model_wxuserinfo = new Model.wxuserinfo();
                }
                if (model_wxuserinfo.addtime == null)
                {
                    model_wxuserinfo.addtime = DateTime.Now;
                }
                model_wxuserinfo.city = model_userinfo.city;
                model_wxuserinfo.country = model_userinfo.country;
                model_wxuserinfo.headimgurl = model_userinfo.headimgurl;
                model_wxuserinfo.nickname = model_userinfo.nickname;
                model_wxuserinfo.openid = model_userinfo.openid;
                model_wxuserinfo.province = model_userinfo.province;
                model_wxuserinfo.sex = model_userinfo.sex;

                if (model_wxuserinfo.id > 0)
                {
                    Model.wxuserinfo.Update(model_wxuserinfo);
                    #region 存入session cookie 保持登录状态
                    //HttpContext.Current.Session["QxUserOpenid"] = model_wxuserinfo;
                    if (!string.IsNullOrEmpty(model_userinfo.openid))
                        Utils.WriteCookie("Xinying", "QXUOPID", Common.DESEncrypt.Encrypt(model_userinfo.openid), 120);
                    if (!string.IsNullOrEmpty(model_userinfo.nickname))
                        Utils.WriteCookie("Xinying", "QXUserName", Common.DESEncrypt.Encrypt(model_userinfo.nickname), 120);
                    #endregion
                }
                else
                {
                    Model.wxuserinfo.Add(model_wxuserinfo);
                    #region 存入session cookie 保持登录状态
                    //HttpContext.Current.Session["QxUserOpenid"] = model_wxuserinfo;
                    if (!string.IsNullOrEmpty(model_userinfo.openid))
                        Utils.WriteCookie("Xinying", "QXUOPID", Common.DESEncrypt.Encrypt(model_userinfo.openid), 120);
                    if (!string.IsNullOrEmpty(model_userinfo.nickname))
                        Utils.WriteCookie("Xinying", "QXUserName", Common.DESEncrypt.Encrypt(model_userinfo.nickname), 120);
                    #endregion
                }
            }
            else
            {
                Model.wxuserinfo model_wxuserinfo = new Model.wxuserinfo();
                model_wxuserinfo.addtime = DateTime.Now;
                model_wxuserinfo.city = model_userinfo.city;
                model_wxuserinfo.country = model_userinfo.country;
                model_wxuserinfo.headimgurl = model_userinfo.headimgurl;
                model_wxuserinfo.nickname = model_userinfo.nickname;
                model_wxuserinfo.openid = model_userinfo.openid;
                model_wxuserinfo.province = model_userinfo.province;
                model_wxuserinfo.sex = model_userinfo.sex;
                Model.wxuserinfo.Add(model_wxuserinfo);
                #region 存入session cookie 保持登录状态
                //HttpContext.Current.Session["QxUserOpenid"] = model_wxuserinfo;
                if (!string.IsNullOrEmpty(model_userinfo.openid))
                    Utils.WriteCookie("Xinying", "QXUOPID", Common.DESEncrypt.Encrypt(model_userinfo.openid), 120);
                if (!string.IsNullOrEmpty(model_userinfo.nickname))
                    Utils.WriteCookie("Xinying", "QXUserName", Common.DESEncrypt.Encrypt(model_userinfo.nickname), 120);
                #endregion
            }
        }
        #endregion

        #region 微会员相关方法
        /// <summary>
        /// 判断微会员是否已经登录(解决Session超时问题)
        /// </summary>
        public bool IsWxUserLogin()
        {
            //如果Session为Null
            //if (HttpContext.Current.Session != null && HttpContext.Current.Session["QxUserOpenid"] != null)
            //{
            //    try
            //    {
            //        wxUserModel = (Model.wxuserinfo)HttpContext.Current.Session["QxUserOpenid"];
            //        return true;
            //    }
            //    catch (Exception ex)
            //    {
            //        //检查Cookies
            //        string wxgcid = Utils.GetCookie("Xinying", "QXUOPID");
            //        if (!string.IsNullOrEmpty(wxgcid))
            //        {
            //            wxUserModel = Model.wxuserinfo.GetModelByOpenid(Xinying.Common.DESEncrypt.Decrypt(wxgcid));
            //            if (wxUserModel != null)
            //            {
            //                HttpContext.Current.Session["QxUserOpenid"] = wxUserModel;
            //                return true;
            //            }
            //        }
            //    }
            //}
            //else
            //{
            //检查Cookies
            string useropenidstr = Utils.GetCookie("Xinying", "QXUOPID");
            string usernamestr = Utils.GetCookie("Xinying", "QXUserName");
            if (!string.IsNullOrEmpty(useropenidstr) && (!string.IsNullOrEmpty(usernamestr)))
            {
                string wxuopenid = Common.DESEncrypt.Decrypt(useropenidstr);
                string username = Common.DESEncrypt.Decrypt(usernamestr);
                wxUserModel = Model.wxuserinfo.GetModelByOpenid(wxuopenid);
                if (wxUserModel != null)
                {
                    //HttpContext.Current.Session["QxUserOpenid"] = wxUserModel;
                    return true;
                }
            }
            //}
            return false;
        }

        /// <summary>
        /// 取得用户信息
        /// </summary>
        public Model.wxuserinfo GetWxUserInfo()
        {
            if (IsWxUserLogin())
            {
                //Model.wxuserinfo model = HttpContext.Current.Session["QxUserOpenid"] as Model.wxuserinfo;
                //if (model != null)
                //{
                //    //为了能查询到最新的用户信息，必须查询最新的用户资料
                //    model = Model.wxuserinfo.GetModelByOpenid(model.openid);
                //    return model;
                //}
                //else
                //{
                //检查Cookies
                string useropenidstr = Utils.GetCookie("Xinying", "QXUOPID");
                string usernamestr = Utils.GetCookie("Xinying", "QXUserName");
                if (!string.IsNullOrEmpty(useropenidstr) && (!string.IsNullOrEmpty(usernamestr)))
                {
                    string wxuopenid = Common.DESEncrypt.Decrypt(useropenidstr);
                    string username = Common.DESEncrypt.Decrypt(usernamestr);
                    Model.wxuserinfo model = Model.wxuserinfo.GetModelByOpenid(wxuopenid);
                    return model;
                }
                //}
            }
            return null;
        }

        /// <summary>
        /// 获取当前微信用户的openid
        /// </summary>
        /// <returns></returns>
        public string GetWxUserOpenId()
        {
            string result = "";
            Model.wxuserinfo WxUserInfoModel = GetWxUserInfo();
            if (WxUserInfoModel != null)
            {
                result = WxUserInfoModel.openid;
            }
            return result;
        }
        #endregion
        #endregion
    }
}
