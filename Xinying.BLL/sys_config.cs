﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Web;
using System.Web.Caching;
using Xinying.Common;

namespace Xinying.BLL
{
    public partial class siteconfig
    {
        private readonly DAL.Mysql.siteconfig dal = new DAL.Mysql.siteconfig();

        /// <summary>
        ///  读取配置文件
        /// </summary>
        public Model.siteconfig loadConfig()
        {
            Model.siteconfig model = CacheHelper.Get<Model.siteconfig>(XYKeys.CACHE_SITE_CONFIG);
            if (model == null)
            {
                CacheHelper.Insert(XYKeys.CACHE_SITE_CONFIG, dal.loadConfig(Utils.GetXmlMapPath(XYKeys.FILE_SITE_XML_CONFING)),
                    Utils.GetXmlMapPath(XYKeys.FILE_SITE_XML_CONFING));
                model = CacheHelper.Get<Model.siteconfig>(XYKeys.CACHE_SITE_CONFIG);
            }
            return model;
        }

        /// <summary>
        ///  保存配置文件
        /// </summary>
        public Model.siteconfig saveConifg(Model.siteconfig model)
        {
            return dal.saveConifg(model, Utils.GetXmlMapPath(XYKeys.FILE_SITE_XML_CONFING));
        }

    }
}
