﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Web;
using System.Web.Caching;
using Xinying.Common;

namespace Xinying.BLL
{
    public partial class orderconfig
    {
        private readonly DAL.Mysql.orderconfig dal = new DAL.Mysql.orderconfig();
        /// <summary>
        ///  读取用户配置文件
        /// </summary>
        public Model.orderconfig loadConfig()
        {
            Model.orderconfig model = CacheHelper.Get<Model.orderconfig>(XYKeys.CACHE_ORDER_CONFIG);
            if (model == null)
            {
                CacheHelper.Insert(XYKeys.CACHE_ORDER_CONFIG, dal.loadConfig(Utils.GetXmlMapPath(XYKeys.FILE_ORDER_XML_CONFING)),
                    Utils.GetXmlMapPath(XYKeys.FILE_ORDER_XML_CONFING));
                model = CacheHelper.Get<Model.orderconfig>(XYKeys.CACHE_ORDER_CONFIG);
            }
            return model;
        }

        /// <summary>
        ///  保存用户配置文件
        /// </summary>
        public Model.orderconfig saveConifg(Model.orderconfig model)
        {
            return dal.saveConifg(model, Utils.GetXmlMapPath(XYKeys.FILE_ORDER_XML_CONFING));
        }
    }
}
