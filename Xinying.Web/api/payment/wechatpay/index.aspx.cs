﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WxApi;
using WxApi.PayEntity;
using LitJson;
using Xinying.Common;

namespace Xinying.Web.api.payment.wechatpay
{
    /// <summary>
    /// 功能：微信扫码支付交易接口接入页
    /// 版本：模式二 1.0
    /// 日期：2015-12-16
    /// 说明：
    /// 以下代码只是为了方便商户测试而提供的样例代码，商户可以根据自己网站的需要，按照技术文档编写,并非一定要使用该代码。
    /// 该代码仅供学习和研究微信支付接口使用，只是提供一个参考。
    /// 
    /// </summary>
    public partial class index : System.Web.UI.Page
    {
        protected Xinying.API.Payment.WxPay.WxPayConfig WePayConfig = new API.Payment.WxPay.WxPayConfig();
        protected Model.siteconfig siteconfig = Model.siteconfig.loadConfig();
        protected string orderno = "";
        private static object lockThis = new object();
        protected string errStr = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            //获取订单信息
            orderno = Xinying.Common.Utils.UrlDecode(Xinying.Common.XYRequest.GetQueryString("re"));
            if (!string.IsNullOrEmpty(orderno))
            {
                #region 本系统订单号28位+补足的随机数4位
                orderno = orderno.Substring(0, 28);
                Model.wepayorder OrderModel = Model.wepayorder.GetModelBySysOrderNo(orderno);
                if (OrderModel != null)
                {
                    if (OrderModel.paymentstatus == 2)//已支付
                    {
                        return;
                    }
                    else
                    {
                        errStr = "订单未支付完成，请稍后";
                        return;
                    }
                }
                else
                {
                    errStr = "订单不存在";
                    return;
                }
                #endregion
            }
            else
            {
                Response.Redirect("/api/payment/wechatpay/error.aspx");
                return;
            }
        }
    }
}