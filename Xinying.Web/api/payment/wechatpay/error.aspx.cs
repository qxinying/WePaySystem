﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WxApi;
using WxApi.PayEntity;
using LitJson;
using Xinying.Common;

namespace Xinying.Web.api.payment.wechatpay
{
    public partial class error : System.Web.UI.Page
    {
        protected Xinying.API.Payment.WxPay.WxPayConfig WePayConfig = new API.Payment.WxPay.WxPayConfig();
        protected Model.siteconfig siteconfig = Model.siteconfig.loadConfig();
        protected string errStr = "哎呀出错了";
        protected int status = XYRequest.GetInt("status", 0);
        protected void Page_Load(object sender, EventArgs e)
        {
            status = XYRequest.GetInt("status", 0);
            switch (status)
            {
                case 1:
                    errStr = "非法第三方应用";
                    break;
                case 2:
                    errStr = "支付方式参数有误";
                    break;
                case 3:
                    errStr = "订单号为空";
                    break;
                case 4:
                    errStr = "同步回调地址为空";
                    break;
                case 5:
                    errStr = "异步回调地址为空";
                    break;
                case 6:
                    errStr = "支付金额小于等于0";
                    break;
                case 7:
                    errStr = "版本号有误";
                    break;
                case 8:
                    errStr = "请联系管理员添加请求网站的域名白名单";
                    break;
                case 9:
                    errStr = "订单号长度必须为10位";
                    break;
                case 10:
                    errStr = "支付订单添加失败";
                    break;
            }
        }
    }
}