﻿<%@ Page Language="C#" MasterPageFile="~/api/payment/wechatpay/template.Master" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="Xinying.Web.api.payment.wechatpay.index" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>支付成功 -
        <%=siteconfig.webname %></title>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="main" runat="server">
    <div class="container js_container">
        <div class="page slideIn msg">
            <div class="weui_msg">
                <%="" %>
                <%if (string.IsNullOrEmpty(errStr))
                    {%>
                <div class="weui_icon_area"><i class="weui_icon_success weui_icon_msg"></i></div>
                <div class="weui_text_area">
                    <h2 class="weui_msg_title">您的订单<%=orderno%>支付成功</h2>
                </div>
                <%}
                    else
                    {%>
                <div class="weui_icon_area"><i class="weui_icon_warn weui_icon_msg"></i></div>
                <div class="weui_text_area">
                    <h2 class="weui_msg_title"><%=errStr%></h2>
                </div>
                <script type="text/javascript">
                    setInterval(checkOrderStatus, 3000);
                    function checkOrderStatus() {
                        var order_no = "<%=orderno%>";
                        if (order_no != "") {
                            $.get("<%=Xinying.Common.XYKeys.AJAX_PAY_API%>?action=CheckOrderStatusByOrderno", { timestamp: new Date().getTime(), order_no: order_no }, function (data) {
                                if (data.status.toString() == "1") {
                                    clearInterval();
                                    window.location.href = window.location.href;
                                    return;
                                }
                            }, "json")
                        }
                    }
                </script>
                <%} %>
            </div>
        </div>
    </div>
</asp:Content>
