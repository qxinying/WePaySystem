﻿<%@ Page Language="C#" MasterPageFile="~/api/payment/wechatpay/template.Master" AutoEventWireup="true" CodeBehind="payReturnUrl.aspx.cs" Inherits="Xinying.Web.api.payment.wechatpay.payReturnUrl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>支付成功 -
        <%=siteconfig.webname %></title>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="main" runat="server">
    <div class="container js_container">
        <div class="page slideIn msg">
            <div class="weui_msg">
                <div class="weui_icon_area"><i class="weui_icon_success weui_icon_msg"></i></div>
                <div class="weui_text_area">
                    <h2 class="weui_msg_title">您的订单<%=orderno%>支付成功</h2>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

