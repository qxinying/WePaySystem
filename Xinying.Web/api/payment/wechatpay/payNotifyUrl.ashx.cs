﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WxApi;
using Xinying.Common;
using LitJson;

namespace Xinying.Web.api.payment.wechatpay
{
    /// <summary>
    /// payNotifyUrl 的摘要说明
    /// v2.0进入此异步回调
    /// </summary>
    public class payNotifyUrl : IHttpHandler
    {
        /// <summary>
        /// 密钥
        /// </summary>
        private string key = Xinying.API.Payment.WxPay.WxPayConfig.Key;
        private static object lockThis = new object();
        public void ProcessRequest(HttpContext context)
        {
            //写错误日志
            //System.IO.File.AppendAllText(Xinying.Common.Utils.GetMapPath("/log/wepaylog.txt"), "异步回调", System.Text.Encoding.UTF8);
            Pay.GetNotifyRes(key, i =>
            {
                //订单业务逻辑处理。需要先判断订单是否已经被处理，如已经被处理则直接回复处理结果。
                #region 根据订单号更新订单支付状态
                string orderno = i.out_trade_no;
                string wepaytrade_no = i.transaction_id;
                int total_fee = i.total_fee;
                if (!string.IsNullOrEmpty(orderno))
                {
                    lock (lockThis)
                    {
                        #region 本系统订单号28位+补足的随机数4位
                        orderno = orderno.Substring(0, 28);
                        Model.wepayorder OrderModel = Model.wepayorder.GetModelBySysOrderNo(orderno);
                        if (OrderModel != null)
                        {
                            if (OrderModel.paymentstatus == 2)//已支付
                            {
                                #region 获取第三方的同步回调地址和参数，使用微信签名方法用第三方的appsecret进行签名，第三方也使用他们自己的appsecret进行验签。同时接收其反馈信息，更新该订单对应的回调信息
                                Model.wepayorder.UpdateNoticeInfo(OrderModel.sys_orderno);
                                if (!string.IsNullOrEmpty(OrderModel.apiuser_global_notifyurl))
                                {
                                    var res = Xinying.Common.Utils.HttpGet(OrderModel.apiuser_global_notifyurl);
                                    Model.wepayorder.UpdateResInfoByRes(OrderModel.sys_orderno, res);
                                }
                                #endregion
                            }
                            else
                            {
                                JsonData Jd = Model.wepayorder.PaySuccessWithWxPay_DBTransaction(orderno, total_fee, wepaytrade_no);
                                if (Jd["state"].ToInt() == 1 || Jd["state"].ToInt() == 5)
                                {
                                    #region 获取第三方的同步回调地址和参数，使用微信签名方法用第三方的appsecret进行签名，第三方也使用他们自己的appsecret进行验签。同时接收其反馈信息，更新该订单对应的回调信息
                                    Model.wepayorder.UpdateNoticeInfo(OrderModel.sys_orderno);
                                    if (!string.IsNullOrEmpty(OrderModel.apiuser_global_notifyurl))
                                    {
                                        var res = Xinying.Common.Utils.HttpGet(OrderModel.apiuser_global_notifyurl);
                                        Model.wepayorder.UpdateResInfoByRes(OrderModel.sys_orderno, res);
                                    }
                                    #endregion
                                    WxApi.Pay.BackMessage();
                                    return;
                                }
                                else
                                {
                                    WxApi.Pay.BackMessage("商品订单支付状态更新失败");
                                    return;
                                }
                            }
                        }
                        else
                        {
                            WxApi.Pay.BackMessage("订单不存在");
                            return;
                        }
                        #endregion
                    }
                }
                else
                {
                    WxApi.Pay.BackMessage("订单号为空");
                    return;
                }
                #endregion

                Pay.BackMessage();
                return;
            });
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}