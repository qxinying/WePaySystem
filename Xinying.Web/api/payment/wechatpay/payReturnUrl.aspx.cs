﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WxApi;
using Xinying.Common;
using LitJson;

namespace Xinying.Web.api.payment.wechatpay
{
    public partial class payReturnUrl : System.Web.UI.Page
    {
        /// <summary>
        /// 此处调用jsapi支付，同步地址回调不可靠，jsapi完成后的同步回调仅用于展示
        /// </summary>
        #region 支付结果
        protected string orderno = string.Empty;
        protected int alreadyDone = 0;
        protected string returnurl = "";
        #endregion
        public Model.siteconfig siteconfig = Model.siteconfig.loadConfig();
        private static object lockThis = new object();
        protected void Page_Load(object sender, EventArgs e)
        {
            //获取订单信息
            orderno = Xinying.Common.XYRequest.GetQueryString("no");
            var reqdata = Request.Url.Query.Trim('?');
            var arr = reqdata.Split("&".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            string data;
            if (XYRequest.GetQueryString("sign") == Xinying.Common.Utils.WxPaySign(arr, Xinying.API.Payment.WxPay.WxPayConfig.AppSecret, out data))
            {
                lock (lockThis)
                {
                    #region 本系统订单号28位+补足的随机数4位
                    orderno = orderno.Substring(0, 28);
                    Model.wepayorder OrderModel = Model.wepayorder.GetModelBySysOrderNo(orderno);
                    if (OrderModel != null)
                    {
                        if (OrderModel.paymentstatus == 2)//已支付
                        {
                            alreadyDone = 1;
                            #region 获取第三方的同步回调地址和参数，使用微信签名方法用第三方的appsecret进行签名，第三方也使用他们自己的appsecret进行验签。同时接收其反馈信息，更新该订单对应的回调信息
                            try
                            {
                                if (OrderModel.apiuser_id > 0)
                                {
                                    Model.apiuser ApiUserModel = Model.apiuser.GetModel(OrderModel.apiuser_id);
                                    if (ApiUserModel != null && (!string.IsNullOrEmpty(OrderModel.apiuser_returnurl)))
                                    {
                                        string ApiData;
                                        string ApiSign = Xinying.Common.Utils.WxPaySign(arr, Xinying.Model.apiuser.GetApiAppSecret(OrderModel.apiuser_id), out ApiData);
                                        string FullUrl = OrderModel.apiuser_returnurl + "?" + ApiData + "&sign=" + ApiSign;
                                        Model.wepayorder.UpdateNoticeInfo(OrderModel.sys_orderno);
                                        var res = Xinying.Common.Utils.HttpGet(FullUrl);
                                        Model.wepayorder.UpdateResInfoByRes(OrderModel.sys_orderno, res);
                                        Response.Redirect("api/payment/wechatpay/index.aspx?re=" + OrderModel.orderno);
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                WxApi.Utils.WriteTxt("支付回调同步通知报错：" + ex);
                            }
                            #endregion
                            return;
                        }
                        else
                        {
                            JsonData Jd = Model.wepayorder.PaySuccessWithWxPay_DBTransaction(OrderModel.sys_orderno, OrderModel.realamount, "");
                            if (Jd["state"].ToInt() == 1 || Jd["state"].ToInt() == 5)
                            {
                                alreadyDone = 1;
                                #region 获取第三方的同步回调地址和参数，使用微信签名方法用第三方的appsecret进行签名，第三方也使用他们自己的appsecret进行验签。同时接收其反馈信息，更新该订单对应的回调信息
                                try
                                {
                                    if (OrderModel.apiuser_id > 0)
                                    {
                                        Model.apiuser ApiUserModel = Model.apiuser.GetModel(OrderModel.apiuser_id);
                                        if (ApiUserModel != null && (!string.IsNullOrEmpty(OrderModel.apiuser_returnurl)))
                                        {
                                            string ApiData;
                                            string ApiSign = Xinying.Common.Utils.WxPaySign(arr, Xinying.Model.apiuser.GetApiAppSecret(OrderModel.apiuser_id), out ApiData);
                                            string FullUrl = OrderModel.apiuser_returnurl + "?" + ApiData + "&sign=" + ApiSign;
                                            Model.wepayorder.UpdateNoticeInfo(OrderModel.sys_orderno);
                                            var res = Xinying.Common.Utils.HttpGet(FullUrl);
                                            Model.wepayorder.UpdateResInfoByRes(OrderModel.sys_orderno, res);
                                            Response.Redirect("api/payment/wechatpay/index.aspx?re=" + OrderModel.orderno);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    WxApi.Utils.WriteTxt("支付回调同步通知报错：" + ex);
                                }
                                #endregion
                                return;
                            }
                            else
                            {
                                WxApi.Utils.WriteTxt("微信支付状态更新失败");
                            }
                        }
                    }
                    else
                    {
                        WxApi.Utils.WriteTxt("订单不存在");
                        return;
                    }
                    #endregion
                }
            }
            else
            {
                WxApi.Utils.WriteTxt("验签不通过");
                Response.Redirect("/api/payment/wechatpay/index.aspx?re=" + orderno);
                return;
            }
        }
    }
}