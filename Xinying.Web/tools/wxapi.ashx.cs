﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LitJson;
using wxHelper;
using Xinying.Common;
using Xinying.Web.UI;
using System.Text;
using System.Threading;
using System.Data;
using System.Xml.Linq;
using Xinying.Model;
using Newtonsoft.Json;
using System.Web.UI;
using System.Web.SessionState;
namespace Xinying.Web.tools
{
    /// <summary>
    /// wxapi 的摘要说明
    /// </summary>
    public class wxapi : IHttpHandler, IRequiresSessionState
    {
        WeiXinRequest req = new WeiXinRequest();
        public static Model.siteconfig config = Model.siteconfig.loadConfig();
        BasePage bp = new BasePage();
        public void ProcessRequest(HttpContext context)
        {
            if (context.Request.HttpMethod.ToLower() == "post")
            {
                req = WeiXinRequest.RequestHelper(false);
                if (req.ToUserName == config.wxid)
                {
                    #region 接收信息处理
                    try
                    {
                        switch (req.MsgType)
                        {
                            case "event": WxEventHandler(req); break;//微信事件消息处理
                            case "text": WxTextHandler(req); break;//微信文字消息处理
                            default: WxTextHandler(req); break;//微信文字消息处理
                        }
                    }
                    catch (Exception e)
                    {
                        Utils.WriteTxt(e.ToString());
                    }

                    #endregion
                }
            }
            else
            {
                ValidApi.Valid(config.wxtoken);
            }
        }

        #region 事件消息处理
        void WxEventHandler(WeiXinRequest _req)
        {
            switch (_req.Event)
            {
                case "subscribe": WxSubscribeHandler(_req); break; //用户关注处理
                case "CLICK": MenuClickHandler(_req); break;//自定义菜单单击时间
                case "unsubscribe": UnSubscribHandler(_req); break;//用户取消关注
                default:
                    break;
            }
        }

        #region 用户关注处理
        void WxSubscribeHandler(WeiXinRequest __req)
        {
            #region 获取关注规则
            int positionid = 1;
            Model.wxrule model_wxrule = Model.wxrule.GetModelByPostionID(positionid);
            if (model_wxrule != null)
            {
                if (model_wxrule.materialid > 0)
                {
                    Model.wxmaterial model_materialt = Model.wxmaterial.GetModel(model_wxrule.materialid);
                    if (model_materialt != null)
                    {
                        WxRuleHandler(model_materialt);
                    }
                    else
                    {
                        WeiXinResponse.ResText(__req, "暂无相关规则匹配！");
                    }
                }
                else
                {
                    WeiXinResponse.ResText(__req, "暂无相关规则匹配！");
                }
            }
            else
            {
                #region 无关键词匹配时的默认回复
                int wxmorenpositionid = 2;
                Model.wxrule model_wxrulet = Model.wxrule.GetModelByPostionID(wxmorenpositionid);
                if (model_wxrulet != null)
                {
                    if (model_wxrulet.materialid > 0)
                    {
                        Model.wxmaterial model_material = Model.wxmaterial.GetModel(model_wxrulet.materialid);
                        if (model_material != null)
                        {
                            WxRuleHandler(model_material);
                        }
                        else
                        {
                            WeiXinResponse.ResText(__req, "暂无相关规则匹配！");
                        }
                    }
                    else
                    {
                        WeiXinResponse.ResText(__req, "暂无相关规则匹配！");
                    }
                }
                else
                {
                    WeiXinResponse.ResText(__req, "暂无相关关键词匹配！");
                }
                #endregion
            }
            #endregion
        }
        #endregion
        #region 自定义菜单单击事件
        void MenuClickHandler(WeiXinRequest __req)
        {
            if (!string.IsNullOrEmpty(__req.EventKey))
            {
                try
                {
                    #region 自定义图文回复的操作
                    List<Articles> list = new List<Articles>();
                    List<Model.wxmaterial> list_wxmaterials = Model.wxmaterial.GetWXRules(__req.EventKey);
                    if (list_wxmaterials != null && list_wxmaterials.Count > 0)
                    {
                        WxRuleHandler(list_wxmaterials[0]);
                    }
                    else
                    {
                        #region 无关键词匹配时的默认回复
                        int wxmorenpositionid = 2;
                        Model.wxrule model_wxrule = Model.wxrule.GetModelByPostionID(wxmorenpositionid);
                        if (model_wxrule != null)
                        {
                            if (model_wxrule.materialid > 0)
                            {
                                Model.wxmaterial model_materialt = Model.wxmaterial.GetModel(model_wxrule.materialid);
                                if (model_materialt != null)
                                {
                                    WxRuleHandler(model_materialt);
                                }
                                else
                                {
                                    WeiXinResponse.ResText(__req, "暂无相关规则匹配！");
                                }
                            }
                            else
                            {
                                WeiXinResponse.ResText(__req, "暂无相关规则匹配！");
                            }
                        }
                        else
                        {
                            WeiXinResponse.ResText(__req, "暂无相关关键词匹配！");
                        }
                        #endregion
                    }
                    #endregion
                }
                catch (System.Exception ex)
                {
                    Utils.WriteTxt(ex.ToString());
                }
            }
            else
            {
                #region 无关键词匹配时的默认回复
                int positionid = 2;
                Model.wxrule model_wxrule = Model.wxrule.GetModelByPostionID(positionid);
                if (model_wxrule != null)
                {
                    if (model_wxrule.materialid > 0)
                    {
                        Model.wxmaterial model_material = Model.wxmaterial.GetModel(model_wxrule.materialid);
                        if (model_material != null)
                        {
                            WxRuleHandler(model_material);
                        }
                        else
                        {
                            WeiXinResponse.ResText(__req, "暂无相关规则匹配！");
                        }
                    }
                    else
                    {
                        WeiXinResponse.ResText(__req, "暂无相关规则匹配！");
                    }
                }
                else
                {
                    WeiXinResponse.ResText(__req, "暂无相关关键词匹配！");
                }
                #endregion
            }
        }
        #endregion
        #endregion

        #region 文字消息处理
        void WxTextHandler(WeiXinRequest _req)
        {
            if (!string.IsNullOrEmpty(_req.Content))
            {
                try
                {
                    #region 自定义图文回复的操作
                    List<Articles> list = new List<Articles>();
                    List<Model.wxmaterial> list_wxmaterials = Model.wxmaterial.GetWXRules(_req.Content);
                    if (list_wxmaterials != null && list_wxmaterials.Count > 0)
                    {
                        WxRuleHandler(list_wxmaterials[0]);
                    }
                    else
                    {
                        #region 无关键词匹配时的默认回复
                        int positionid = 2;
                        Model.wxrule model_wxrule = Model.wxrule.GetModelByPostionID(positionid);
                        if (model_wxrule != null)
                        {
                            if (model_wxrule.materialid > 0)
                            {
                                Model.wxmaterial model_material = Model.wxmaterial.GetModel(model_wxrule.materialid);
                                if (model_material != null)
                                {
                                    WxRuleHandler(model_material);
                                }
                                else
                                {
                                    WeiXinResponse.ResText(_req, "暂无相关规则匹配！");
                                }
                            }
                            else
                            {
                                WeiXinResponse.ResText(_req, "暂无相关规则匹配！");
                            }
                        }
                        else
                        {
                            WeiXinResponse.ResText(_req, "暂无相关关键词匹配！");
                        }
                        #endregion
                    }
                    #endregion
                }
                catch (System.Exception ex)
                {
                    Utils.WriteTxt(ex.ToString());
                }
            }
        }
        #endregion

        #region 微信回复操作
        void WxRuleHandler(Model.wxmaterial model_material)
        {
            try
            {
                Web.UI.BasePage bp = new BasePage();
                List<Articles> list = new List<Articles>();//微信图文对象
                if (model_material != null)
                {
                    if (model_material.type == 2)//文字
                    {
                        WeiXinResponse.ResText(req, Utils.DropHTML(model_material.words));
                    }
                    else if (model_material.type == 3)//图片
                    {
                        Picture pic = new Picture();
                        pic.PictureUrl = model_material.imgurl;
                        //Utils.WriteTxt("pic.PictureUrl:" + pic.PictureUrl);
                        Model.siteconfig config = Model.siteconfig.loadConfig();
                        WeiXinResponse.ResPicture(req, pic, config.weburl);
                    }
                    else if (model_material.type == 4)//多图文
                    {
                        List<Model.wxmaterial_sub> list_submaterial = Model.wxmaterial_sub.GetObjectListO(10, "wxmaterialid=" + model_material.id, "sortid asc,id desc");
                        if (list_submaterial != null && list_submaterial.Count > 0)
                        {
                            for (int i = 0; i < list_submaterial.Count; i++)
                            {
                                if (list != null && list.Count >= 10)
                                {
                                    continue;
                                }
                                string returnurl = System.Web.HttpUtility.UrlEncode(bp.linkurl("mwxdetails404"));
                                if (!string.IsNullOrEmpty(list_submaterial[i].reurl))
                                {
                                    returnurl = (model_material.isdetails == 2 ? System.Web.HttpUtility.UrlEncode(list_submaterial[i].reurl) : System.Web.HttpUtility.UrlEncode(bp.linkurl("wxsubdetails", model_material.id, list_submaterial[i].id)));
                                }
                                list.Add(new Articles { Description = Utils.ToTxt(list_submaterial[i].brief), PicUrl = list_submaterial[i].imgurl, Title = list_submaterial[i].title, Url = GetWebDomain(config.weburl) + "/aspx/user/checkstatus.aspx?wxid=" + req.FromUserName + "&returnurl=" + returnurl });
                            }
                        }
                        WeiXinResponse.ResArticles(req, list);
                    }
                    else//单图文
                    {
                        string returnurl = System.Web.HttpUtility.UrlEncode(bp.linkurl("mwxdetails404"));
                        if (!string.IsNullOrEmpty(model_material.reurl))
                        {
                            returnurl = (model_material.isdetails == 2 ? System.Web.HttpUtility.UrlEncode(model_material.reurl) : System.Web.HttpUtility.UrlEncode(bp.linkurl("wxdetails", model_material.id)));
                        }
                        list.Add(new Articles { Description = Utils.ToTxt(model_material.brief), PicUrl = model_material.imgurl, Title = model_material.title, Url = GetWebDomain(config.weburl) + "/aspx/user/checkstatus.aspx?wxid=" + req.FromUserName + "&returnurl=" + returnurl });
                        WeiXinResponse.ResArticles(req, list);
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.WriteTxt("微信自定义素材处理出错，错误是：" + ex);
            }
        }
        #endregion
        #region 用户取消关注
        void UnSubscribHandler(WeiXinRequest req)
        {
            #region 取消关注 

            #endregion
        }
        #endregion

        #region 私有函数
        string GetWebDomain(string _originalurl)
        {
            string result = _originalurl;
            #region 网站域名
            if (!string.IsNullOrEmpty(_originalurl))
            {
                if (_originalurl.Contains("http://") || _originalurl.Contains("https://"))
                {

                }
                else
                {
                    result = "http://" + _originalurl;
                }
            }
            #endregion
            return result;
        }
        #endregion

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        public class CodeModel
        {
            public string access_token { get; set; }
            public int expires_in { get; set; }
            public string refresh_token { get; set; }
            public string openid { get; set; }
            public string scope { get; set; }
        }

        public class UserInfo
        {
            public string openid { get; set; }
            public string nickname { get; set; }
            public int sex { get; set; }
            public string province { get; set; }
            public string city { get; set; }
            public string country { get; set; }
            public string headimgurl { get; set; }
            public JsonArrayAttribute privilege { get; set; }
        }
    }
}