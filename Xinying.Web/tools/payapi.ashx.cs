﻿using LitJson;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using Xinying.Common;

namespace Xinying.Web.tools
{
    /// <summary>
    /// payapi 的摘要说明
    /// </summary>
    public class payapi : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            #region 来路域名验证（已注释）
            //try
            //{
            //    HttpRequest Request = HttpContext.Current.Request;
            //    if (!Xinying.Common.Utils.IsAjaxRequest() || !IsSafeSite())//如果不是ajax请求或者不在域名白名单中进行拦截
            //    {
            //        if (!(Request.Url.Host.ToLower() == "localhost"))
            //        {
            //            //如果有A,B两个页面，在浏览器中直接请求A页面，在A页面的中Page_Load事件中导航到B 页面，则    Request.UrlReferrer返回空。因为 在Page_load事件中页面还未初始化，所以无法记录当前页的信息，导航到b页面也就无法获得上一页面的信息 
            //            //所以白名单不在此处判断
            //            string referrerHost = Request.UrlReferrer.Host.ToLower().Trim();
            //            #region 拼接json并返回
            //            ReturnData RetDataModel = new ReturnData();
            //            RetDataModel.pay_url = HttpUtility.UrlEncode(string.Format("http://{0}/api/payment/wechatpay/error.aspx?status=8", XYRequest.GetCurrentFullHost()));
            //            RetDataModel.verify_status = 1;
            //            RetDataModel.verify_msg = "请联系管理员将贵方域名加入白名单";
            //            context.Response.Write(JsonMapper.ToJson(RetDataModel));
            //            return;
            //            #endregion
            //        }
            //        else
            //        {
            //            #region 本地调试
            //            GetPayUrl(context);
            //            return;
            //            #endregion
            //        }
            //    }
            //}
            //catch (Exception ex)
            //{
            //    #region 拼接json并返回
            //    ReturnData RetDataModel = new ReturnData();
            //    RetDataModel.pay_url = HttpUtility.UrlEncode(string.Format("http://{0}/api/payment/wechatpay/error.aspx?status=1", XYRequest.GetCurrentFullHost()));
            //    RetDataModel.verify_status = 1;
            //    RetDataModel.verify_msg = "非法请求";
            //    Utils.WriteTxt($"payapi.ashx_error:{ex}");
            //    context.Response.Write(JsonMapper.ToJson(RetDataModel));
            //    return;
            //    #endregion
            //}
            #endregion
            //取得处事类型
            string action = XYRequest.GetString("action");
            switch (action)
            {
                case "getpayurl": //验证用户名
                    GetPayUrl(context);
                    break;
                #region 通过订单号查询订单支付状态
                case "CheckOrderStatusByOrderno":
                    CheckOrderStatusByOrderno(context);
                    break;
                    #endregion
            }
        }

        #region 判断是否是合规网站调用，仅在白名单中的域名允许调用
        /// <summary>
        /// 判断是否是合规网站调用
        /// </summary>
        /// <returns></returns>
        public static bool IsSafeSite()
        {
            bool reB = false;
            try
            {
                HttpRequest Request = HttpContext.Current.Request;
                string referrerHost = Request.UrlReferrer.Host.ToLower().Trim();
                if (referrerHost.EndsWith("localhost"))//本地调试
                {
                    reB = true;
                }
                else
                {
                    List<Model.domainwhitelist> DomainWhiteLists = Model.domainwhitelist.GetObjectListO("", "sort_id asc,id desc");
                    if (DomainWhiteLists != null && DomainWhiteLists.Count > 0)
                    {
                        for (int i = 0, l = DomainWhiteLists.Count; i < l; i++)
                        {
                            if (referrerHost.EndsWith(DomainWhiteLists[i].domain))
                            {
                                reB = true;
                                break;
                            }
                        }
                    }
                }
            }
            catch { }
            return reB;
        }
        #endregion

        #region 获取实际支付地址============================
        private void GetPayUrl(HttpContext context)
        {
            string username = XYRequest.GetString("param");
            #region 获取相关参数，验证是否是合法的第三方api账户，以及传输的数据是否有被修改
            string AppKey = XYRequest.GetString("appkey");
            string OutTradeNo = XYRequest.GetString("out_trade_no");
            string Sign = XYRequest.GetString("sign");
            #region 验证是否是合法的第三方api账户
            Model.apiuser ApiUserModel = Model.apiuser.GetModelByAppKey(AppKey);
            if (ApiUserModel == null)
            {
                #region 拼接json并返回
                ReturnData RetDataModel = new ReturnData();
                RetDataModel.pay_url = HttpUtility.UrlEncode(string.Format("http://{0}/api/payment/wechatpay/error.aspx?status=1", XYRequest.GetCurrentFullHost()));
                RetDataModel.verify_status = 1;
                RetDataModel.verify_msg = "非法第三方应用";
                Utils.WriteTxt($"订单{OutTradeNo}_apiuser不存在");
                context.Response.Write(JsonMapper.ToJson(RetDataModel));
                return;
                #endregion
            }
            else
            {
                #region 校验数据签名
                //var reqdata = Request.Url.Query.Trim('?');
                Stream s = System.Web.HttpContext.Current.Request.InputStream;
                byte[] b = new byte[s.Length];
                s.Read(b, 0, (int)s.Length);
                var reqdata = Encoding.UTF8.GetString(b);
                //Utils.WriteTxt("reqdata:" + reqdata);
                var arr = reqdata.Split("&".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                string data;
                if (Sign == Utils.WxPaySign(arr, ApiUserModel.appsecret, out data))
                {
                    //Utils.WriteTxt($"订单{OutTradeNo}_验签成功");
                    #region 订单相关信息
                    string PayBody = XYRequest.GetString("paybody");
                    string CallbackUrl = XYRequest.GetString("callback_url");
                    string PayMethod = XYRequest.GetString("paymethod");
                    string NotifyUrl = XYRequest.GetString("notify_url");
                    string TimeStamp = XYRequest.GetString("timestamp");
                    int TotalFee = XYRequest.GetInt("total_fee", 0);
                    string Version = XYRequest.GetString("version");
                    #region 校验数据合法性，合法的数据才返回真实微信支付地址
                    if (PayMethod != "weixin.pay")
                    {
                        #region 拼接json并返回
                        ReturnData RetDataModel = new ReturnData();
                        RetDataModel.pay_url = HttpUtility.UrlEncode(string.Format("http://{0}/api/payment/wechatpay/error.aspx?status=2", XYRequest.GetCurrentFullHost()));
                        RetDataModel.verify_status = 1;
                        RetDataModel.verify_msg = "支付方式参数有误";
                        Utils.WriteTxt($"订单{OutTradeNo}_支付方式参数有误");
                        context.Response.Write(JsonMapper.ToJson(RetDataModel));
                        return;
                        #endregion
                    }
                    if (string.IsNullOrEmpty(OutTradeNo))
                    {
                        #region 拼接json并返回
                        ReturnData RetDataModel = new ReturnData();
                        RetDataModel.pay_url = HttpUtility.UrlEncode(string.Format("http://{0}/api/payment/wechatpay/error.aspx?status=3", XYRequest.GetCurrentFullHost()));
                        RetDataModel.verify_status = 1;
                        RetDataModel.verify_msg = "订单号为空";
                        Utils.WriteTxt("订单号为空");
                        context.Response.Write(JsonMapper.ToJson(RetDataModel));
                        return;
                        #endregion
                    }
                    else
                    {
                        if (OutTradeNo.Length != 10)
                        {
                            #region 拼接json并返回
                            ReturnData RetDataModel = new ReturnData();
                            RetDataModel.pay_url = HttpUtility.UrlEncode(string.Format("http://{0}/api/payment/wechatpay/error.aspx?status=9", XYRequest.GetCurrentFullHost()));
                            RetDataModel.verify_status = 1;
                            RetDataModel.verify_msg = "订单号长度必须为10位";
                            Utils.WriteTxt($"订单{OutTradeNo}_订单号长度必须为10位");
                            context.Response.Write(JsonMapper.ToJson(RetDataModel));
                            return;
                            #endregion
                        }
                    }
                    if (string.IsNullOrEmpty(PayBody))
                    {
                        PayBody = "商品";
                    }
                    if (string.IsNullOrEmpty(CallbackUrl))
                    {
                        #region 拼接json并返回
                        ReturnData RetDataModel = new ReturnData();
                        RetDataModel.pay_url = HttpUtility.UrlEncode(string.Format("http://{0}/api/payment/wechatpay/error.aspx?status=4", XYRequest.GetCurrentFullHost()));
                        RetDataModel.verify_status = 1;
                        RetDataModel.verify_msg = "同步回调地址为空";
                        Utils.WriteTxt($"订单{OutTradeNo}_同步回调地址为空");
                        context.Response.Write(JsonMapper.ToJson(RetDataModel));
                        return;
                        #endregion
                    }
                    if (string.IsNullOrEmpty(NotifyUrl))
                    {
                        #region 拼接json并返回
                        ReturnData RetDataModel = new ReturnData();
                        RetDataModel.pay_url = HttpUtility.UrlEncode(string.Format("http://{0}/api/payment/wechatpay/error.aspx?status=5", XYRequest.GetCurrentFullHost()));
                        RetDataModel.verify_status = 1;
                        RetDataModel.verify_msg = "异步回调地址为空";
                        Utils.WriteTxt($"订单{OutTradeNo}_异步回调地址为空");
                        context.Response.Write(JsonMapper.ToJson(RetDataModel));
                        return;
                        #endregion
                    }
                    if (TotalFee <= 0)
                    {
                        #region 拼接json并返回
                        ReturnData RetDataModel = new ReturnData();
                        RetDataModel.pay_url = HttpUtility.UrlEncode(string.Format("http://{0}/api/payment/wechatpay/error.aspx?status=6", XYRequest.GetCurrentFullHost()));
                        RetDataModel.verify_status = 1;
                        RetDataModel.verify_msg = "支付金额小于等于0";
                        Utils.WriteTxt($"订单{OutTradeNo}_支付金额小于等于0");
                        context.Response.Write(JsonMapper.ToJson(RetDataModel));
                        return;
                        #endregion
                    }
                    if (Common.XYKeys.STRING_WXPAY_VERSIONS.Contains("|" + Version + "|"))
                    {

                    }
                    else
                    {
                        #region 拼接json并返回
                        ReturnData RetDataModel = new ReturnData();
                        RetDataModel.pay_url = HttpUtility.UrlEncode(string.Format("http://{0}/api/payment/wechatpay/error.aspx?status=7", XYRequest.GetCurrentFullHost(), OutTradeNo));
                        RetDataModel.verify_status = 1;
                        RetDataModel.verify_msg = "版本号有误";
                        Utils.WriteTxt($"订单{OutTradeNo}_版本号有误，错误版本号为{Version}");
                        context.Response.Write(JsonMapper.ToJson(RetDataModel));
                        return;
                        #endregion
                    }
                    #endregion
                    #region 返回真实的微信支付地址
                    #region 拼接json并返回
                    ReturnData RetDataModelS = new ReturnData();
                    RetDataModelS.pay_url = HttpUtility.UrlEncode(string.Format("http://{0}/aspx/pay/wxpay.aspx?{1}&sign={2}", XYRequest.GetCurrentFullHost(), data, Sign));
                    RetDataModelS.verify_status = 0;
                    RetDataModelS.verify_msg = "SUCCESS";
                    context.Response.Write(JsonMapper.ToJson(RetDataModelS));
                    return;
                    #endregion
                    #endregion
                    #endregion
                }
                else
                {
                    #region 拼接json并返回
                    ReturnData RetDataModel = new ReturnData();
                    RetDataModel.pay_url = HttpUtility.UrlEncode(string.Format("http://{0}/api/payment/wechatpay/error.aspx?status=1", XYRequest.GetCurrentFullHost()));
                    RetDataModel.verify_status = 1;
                    RetDataModel.verify_msg = "验签不通过";
                    Utils.WriteTxt($"订单{OutTradeNo}_验签不通过");
                    context.Response.Write(JsonMapper.ToJson(RetDataModel));
                    return;
                    #endregion
                }
                #endregion
            }
            #endregion
            #endregion
        }
        public class ReturnData
        {
            /// <summary>
            /// 校验状态 0：校验成功 1：校验异常 2：支付系统异常
            /// </summary>
            public int verify_status { get; set; }
            /// <summary>
            /// 校验详情 校验成功则返回“SUCCESS”,异常则显示详细原因
            /// </summary>
            public string verify_msg { get; set; }
            /// <summary>
            /// 支付url，如果数据验证失败，返回失败页面（其实都是/api/payment/wechatpay/index.aspx加上不同参数）展示相关信息
            /// </summary>
            public string pay_url { get; set; }
        }
        #endregion

        #region 通过订单号查询订单支付状态
        private void CheckOrderStatusByOrderno(HttpContext context)
        {
            string order_no = context.Request.Params.Get("order_no");
            if (!string.IsNullOrEmpty(order_no))
            {
                Model.wepayorder OrderModel = Model.wepayorder.GetModelBySN(order_no);
                if (OrderModel != null)
                {
                    if (OrderModel.paymentstatus == 2)
                    {
                        context.Response.Write("{ \"status\": 1}");
                        return;
                    }
                    else
                    {
                        context.Response.Write("{ \"status\": 0, \"error\": \"该订单未支付\" }");
                        return;
                    }
                }
                else
                {
                    context.Response.Write("{ \"status\": 0, \"error\": \"该订单不存在或已删除\" }");
                    return;
                }
            }
            else
            {
                context.Response.Write("{ \"status\": 0, \"error\": \"订单号不能为空\" }");
                return;
            }
        }
        #endregion

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}