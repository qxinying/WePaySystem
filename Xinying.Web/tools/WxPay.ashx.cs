﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WxPayHelper;
using Newtonsoft.Json;
using Xinying.Web.UI;
using Xinying.Model;

namespace WxPay
{
    /// <summary>
    /// WxPay 的摘要说明
    /// </summary>
    public class WxPay : IHttpHandler
    {
        protected Xinying.API.Payment.WxPay.WxPayConfig Config = new Xinying.API.Payment.WxPay.WxPayConfig();
        /// <summary>
        /// 公众账号ID
        /// </summary>
        private string appid = Xinying.API.Payment.WxPay.WxPayConfig.AppID;
        /// <summary>
        /// 商户号
        /// </summary>
        private string mch_id = Xinying.API.Payment.WxPay.WxPayConfig.MCHID;
        /// <summary>
        /// 异步通知url
        /// </summary>
        private string notify_url = "http://" + WxApi.Utils.GetCurrentFullHost() + Xinying.API.Payment.WxPay.WxPayConfig.NotifyUrl;
        /// <summary>
        /// 同步返回url
        /// </summary>
        private string return_url = "http://" + WxApi.Utils.GetCurrentFullHost() + "/api/payment/wechatpay/payReturnUrl.aspx";
        /// <summary>
        /// 密钥
        /// </summary>
        private string key = Xinying.API.Payment.WxPay.WxPayConfig.Key;

        public void ProcessRequest(HttpContext context)
        {
            #region v1.0：因不考虑分润，故直接使用第三方的异步和同步回调地址作为回调地址；v2.0：考虑分润，使用本系统回调地址，回调之后系统给第三方发通知
            string Version = Xinying.Common.XYRequest.GetQueryString("version");
            if (Version == "1.0")
            {
                string SysOutTradeNo = Xinying.Common.XYRequest.GetQueryString("out_trade_no");
                if (SysOutTradeNo.Length >= 28)
                {
                    string SysOrderNo = SysOutTradeNo.Substring(0, 28);
                    Xinying.Model.wepayorder WePayOrderModel = Xinying.Model.wepayorder.GetModelBySysOrderNo(SysOrderNo);
                    if (WePayOrderModel != null)
                    {
                        notify_url = WePayOrderModel.apiuser_notifyurl;
                        return_url = WePayOrderModel.apiuser_returnurl;
                    }
                }
            }
            #region 容错
            if (string.IsNullOrEmpty(notify_url))
            {
                notify_url = "http://" + WxApi.Utils.GetCurrentFullHost() + Xinying.API.Payment.WxPay.WxPayConfig.NotifyUrl;
            }
            if (string.IsNullOrEmpty(return_url))
            {
                return_url = "http://" + WxApi.Utils.GetCurrentFullHost() + "/api/payment/wechatpay/payReturnUrl.aspx";
            }
            #endregion
            #endregion
            string action = Xinying.Common.XYRequest.GetQueryString("action");
            switch (action)
            {
                case "unifysign": GetUnifySign(context); break;
                case "jspayparam": GetJsPayParam(context); break;
                case "nativedynamic": GetPayQr(context); break;
                case "jsordercancel": GetJsOrderCancel(context); break;
            }
        }

        #region 获取js支付参数
        void GetJsPayParam(HttpContext context)
        {
            //Utils.WriteTxt("GetJsPayParam start");
            try
            {
                JsEntities jsEntities = new JsEntities()
                {
                    appId = appid,
                    nonceStr = WxPayHelper.Utils.GetRandom(),
                    package = string.Format("prepay_id={0}", GetPrepayId(context)),
                    signType = "MD5",
                    timeStamp = WxPayHelper.Utils.ConvertDateTimeInt(DateTime.Now).ToString()
                };
                //Utils.WriteTxt("package ex:" + jsEntities.package);
                string url, sign;
                WxPayHelper.Utils.GetUnifyUrlXml<JsEntities>(jsEntities, key, out url, out sign);
                jsEntities.paySign = sign;
                //Utils.WriteTxt(JsonConvert.SerializeObject(jsEntities));
                context.Response.Write("{ \"status\": 0, \"jsEntities\": " + JsonConvert.SerializeObject(jsEntities) + ", \"return_uri\": \"" + return_url + "\" }");
            }
            catch (Exception ex)
            {
                Utils.WriteTxt("{ \"status\": 1, \"error\": \"" + ex + "\" }");
            }
        }
        #endregion

        #region 订单关闭
        void GetJsOrderCancel(HttpContext context)
        {
            try
            {
                //WxPayHelper.Utils.WriteTxt("关闭进来了");
                //JsCancelEntities jscancelEntities = new JsCancelEntities(){
                //    appId=appid,
                //    mch_id=mch_id1,
                //    out_trade_no=""
                //}
                string xml;
                GetUnifySign1(context, out xml);
                //WxPayHelper.Utils.WriteTxt(xml);
                UnifyReceive unifyReceive = new UnifyReceive(Utils.HttpPost("https://api.mch.weixin.qq.com/pay/closeorder", xml));
                // Utils.WriteTxt(unifyReceive.return_code);
            }
            catch (Exception ex)
            {
                Utils.WriteTxt("GetJsOrderCancel ex:" + ex);
            }
        }
        #endregion

        #region 获取预支付ID
        string GetPrepayId(HttpContext context)
        {
            string xml;
            GetUnifySign(context, out xml);
            //WxPayHelper.Utils.WriteTxt(xml);
            UnifyReceive unifyReceive = new UnifyReceive(Utils.HttpPost("https://api.mch.weixin.qq.com/pay/unifiedorder", xml));
            //Utils.WriteTxt("unifyReceive.return_msg:" + unifyReceive.return_msg);
            //Utils.WriteTxt("unifyReceive.prepay_id:" + unifyReceive.prepay_id);
            return unifyReceive.prepay_id;
        }
        #endregion

        #region 获取统一签名
        void GetUnifySign(HttpContext context)
        {
            string xml;
            context.Response.Write(GetUnifySign(context, out xml));
        }
        #endregion

        #region 获取统一签名
        string GetUnifySign(HttpContext context, out string xml)
        {
            string url, sign;
            xml = WxPayHelper.Utils.GetUnifyUrlXml<UnifyEntities>(GetUnifyEntities(context), key, out url, out sign);
            return sign;
        }
        #endregion

        #region 获取统一签名1
        string GetUnifySign1(HttpContext context, out string xml)
        {
            string url, sign;
            xml = WxPayHelper.Utils.GetUnifyUrlXml1<JsCancelEntities>(GetUnifyEntities1(context), key, out url, out sign);
            return sign;
        }
        #endregion

        #region 获取二维码
        void GetPayQr(HttpContext context)
        {
            string url = GetPayUrl(context);
            WxPayHelper.Utils.GetQrCode(url);
        }
        #endregion

        #region 获取预支付ID
        string GetPayUrl(HttpContext context)
        {
            string xml;
            GetUnifySign(context, out xml);
            // WxPayHelper.Utils.WriteTxt(xml);
            UnifyReceive unifyReceive = new UnifyReceive(Utils.HttpPost("https://api.mch.weixin.qq.com/pay/unifiedorder", xml));
            return unifyReceive.code_url;
        }
        #endregion

        #region 获取统一支付接口参数对象
        UnifyEntities GetUnifyEntities(HttpContext context)
        {
            string msgid = Xinying.Common.XYRequest.GetQueryString("msgid");
            //Utils.WriteTxt("msgid:" + msgid);
            UnifyEntities unify = new UnifyEntities
            {
                appid = appid,
                body = Xinying.Common.XYRequest.GetQueryString("body"),
                mch_id = mch_id,
                nonce_str = WxPayHelper.Utils.GetRandom(),
                out_trade_no = Xinying.Common.XYRequest.GetQueryString("out_trade_no"),
                notify_url = notify_url,
                spbill_create_ip = Xinying.Common.XYRequest.GetIP(),
                trade_type = Xinying.Common.XYRequest.GetQueryString("trade_type"),
                total_fee = Xinying.Common.XYRequest.GetQueryString("total_fee")
            };
            if (unify.trade_type == "NATIVE")
            {
                unify.product_id = msgid;
            }
            else
            {
                unify.openid = msgid;
            }
            //Utils.WriteTxt("appid:" + unify.appid + ";body:" + unify.body + ";mch_id:" + unify.mch_id + ";nonce_str:" + unify.nonce_str + ";out_trade_no:" + unify.out_trade_no + ";notify_url:" + unify.notify_url + ";spbill_create_ip:" + unify.spbill_create_ip + ";trade_type:" + unify.trade_type + ";total_fee:" + unify.total_fee);
            return unify;
        }
        #endregion

        #region 获取取消订单接口参数对象
        JsCancelEntities GetUnifyEntities1(HttpContext context)
        {
            string msgid = Xinying.Common.XYRequest.GetQueryString("msgid");
            //Utils.WriteTxt("msgid:" + msgid);
            JsCancelEntities unify = new JsCancelEntities
            {
                appid = appid,
                mch_id = mch_id,
                nonce_str = WxPayHelper.Utils.GetRandom(),
                out_trade_no = Xinying.Common.XYRequest.GetQueryString("out_trade_no")
            };

            //Utils.WriteTxt("appid:" + unify.appid + ";body:" + unify.body + ";mch_id:" + unify.mch_id + ";nonce_str:" + unify.nonce_str + ";out_trade_no:" + unify.out_trade_no + ";notify_url:" + unify.notify_url + ";spbill_create_ip:" + unify.spbill_create_ip + ";trade_type:" + unify.trade_type + ";total_fee:" + unify.total_fee);
            return unify;
        }
        #endregion

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}