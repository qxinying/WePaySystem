﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Xinying.Common;

namespace Xinying.Web.admin.domainwhitelist
{
    public partial class edit : Web.UI.ManagePage
    {
        private string action = XYEnums.ActionEnum.Add.ToString(); //操作类型
        private int id = 0;

        protected string channel_name = "domainwhitelist";
        protected string log_name = "白名单";

        protected void Page_Load(object sender, EventArgs e)
        {
            string _action = XYRequest.GetQueryString("action");
            if (!string.IsNullOrEmpty(_action) && _action == XYEnums.ActionEnum.Edit.ToString())
            {
                this.action = XYEnums.ActionEnum.Edit.ToString();//修改类型
                if (!int.TryParse(Request.QueryString["id"] as string, out this.id))
                {
                    JscriptMsg("传输参数不正确！", "back");
                    return;
                }
                if (!Model.domainwhitelist.Exists(this.id))
                {
                    JscriptMsg("记录不存在或已被删除！", "back");
                    return;
                }
            }
            if (!Page.IsPostBack)
            {
                ChkAdminLevel(channel_name, XYEnums.ActionEnum.View.ToString()); //检查权限
                ddlbind();
                if (action == XYEnums.ActionEnum.Edit.ToString()) //修改
                {
                    ShowInfo(this.id);
                }
            }
        }

        #region ddlbind
        protected void ddlbind()
        {
            #region 绑定第三方调用者
            ddlApiUserId.DataSource = Model.apiuser.GetObjectListO("", "sort_id asc,id desc");
            ddlApiUserId.DataTextField = "apiusername";
            ddlApiUserId.DataValueField = "id";
            ddlApiUserId.DataBind();
            ddlApiUserId.Items.Insert(0, new ListItem("无关联开发者", "0"));
            #endregion
        }
        #endregion

        #region 赋值操作=================================
        private void ShowInfo(int _id)
        {
            Model.domainwhitelist model = Model.domainwhitelist.GetModel(_id);
            if (model != null)
            {
                txtDomain.Text = model.domain;
                ddlApiUserId.SelectedValue = model.apiuserid.ToStr();
                txtSortId.Text = model.sort_id.ToStr();
            }
        }
        #endregion

        #region 增加操作=================================
        private bool DoAdd()
        {
            Model.domainwhitelist model = new Model.domainwhitelist();
            model.domain = txtDomain.Text;
            model.apiuserid = ddlApiUserId.SelectedValue.ToInt();
            model.sort_id = txtSortId.Text.ToInt();

            if (Model.domainwhitelist.Add(model) > 0)
            {
                AddAdminLog(XYEnums.ActionEnum.Add.ToString(), "添加" + log_name + ":" + model.domain); //记录日志
                return true;
            }
            return false;
        }
        #endregion

        #region 修改操作=================================
        private bool DoEdit(int _id)
        {
            bool result = false;
            Model.domainwhitelist model = Model.domainwhitelist.GetModel(_id);

            model.domain = txtDomain.Text;
            model.apiuserid = ddlApiUserId.SelectedValue.ToInt();
            model.sort_id = txtSortId.Text.ToInt();

            if (Model.domainwhitelist.Update(model))
            {
                AddAdminLog(XYEnums.ActionEnum.Edit.ToString(), "修改" + log_name + ":" + model.domain); //记录日志
                result = true;
            }

            return result;
        }
        #endregion

        //保存
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (action == XYEnums.ActionEnum.Edit.ToString()) //修改
            {
                ChkAdminLevel(channel_name, XYEnums.ActionEnum.Edit.ToString()); //检查权限
                if (!DoEdit(this.id))
                {
                    JscriptMsg("保存过程中发生错误！", "");
                    return;
                }
                JscriptMsg("修改成功！", "list.aspx");
            }
            else //添加
            {
                ChkAdminLevel(channel_name, XYEnums.ActionEnum.Add.ToString()); //检查权限
                if (!DoAdd())
                {
                    JscriptMsg("保存过程中发生错误！", "");
                    return;
                }
                JscriptMsg("添加成功！", "list.aspx");
            }
        }
    }
}