﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="Xinying.Web.admin.advert.index" %>

<%@ Import Namespace="Xinying.Common" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>广告管理</title>
    <link href="../../scripts/artdialog/ui-dialog.css" rel="stylesheet" type="text/css" />
    <link href="../skin/default/style.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" charset="utf-8" src="../../scripts/jquery/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../scripts/jquery/Validform_v5.3.2_min.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../scripts/datepicker/WdatePicker.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../scripts/artdialog/dialog-plus-min.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../scripts/webuploader/webuploader.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../editor/kindeditor-min.js"></script>
    <script type="text/javascript" charset="utf-8" src="../js/uploader.js"></script>
    <script type="text/javascript" charset="utf-8" src="../js/laymain.js"></script>
    <script type="text/javascript" charset="utf-8" src="../js/common.js"></script>
</head>
<body class="mainbody">
    <form id="form1" runat="server">
        <!--导航栏-->
        <div class="location">
            <a href="javascript:history.back(-1);" class="back"><i></i><span>返回上一页</span></a>
            <a href="../center.aspx" class="home"><i></i><span>首页</span></a> <i class="arrow"></i><span>广告位管理</span> <i class="arrow"></i>
            <span>广告位</span>
        </div>
        <!--/导航栏-->
        <!--工具栏-->
        <div class="toolbar-wrap">
            <div id="floatHead" class="toolbar">
                <div class="l-list" style="display: block;">
                    <ul class="icon-list">
                        <li><a class="add" href="adv_edit.aspx?action=<%=XYEnums.ActionEnum.Add %>"><i></i><span>新增</span></a></li>
                        <li><a class="all" href="javascript:;" onclick="checkAll(this);"><i></i><span>全选</span></a></li>
                        <li style="display: none;">
                            <asp:LinkButton ID="btnDelete" runat="server" CssClass="del" OnClientClick="return ExePostBack('btnDelete');"
                                OnClick="btnDelete_Click"><i ></i><span>删除</span></asp:LinkButton></li>
                    </ul>
                    <div class="menu-list" style="display: none">
                        <div class="rule-single-select">
                            <asp:DropDownList ID="ddlProperty" runat="server" CssClass="select2" AutoPostBack="True"
                                OnSelectedIndexChanged="btnSearch_Click">
                                <asp:ListItem Value="">所有类别</asp:ListItem>
                                <asp:ListItem Value="1">文字</asp:ListItem>
                                <asp:ListItem Value="2">图片</asp:ListItem>
                                <asp:ListItem Value="3">幻灯片</asp:ListItem>
                                <asp:ListItem Value="4">动画</asp:ListItem>
                                <asp:ListItem Value="5">视频</asp:ListItem>
                                <asp:ListItem Value="6">代码</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="r-list">
                    <asp:TextBox ID="txtKeywords" runat="server" CssClass="keyword" />
                    <asp:LinkButton ID="btnSearch" runat="server" CssClass="btn-search" OnClick="btnSearch_Click">查询</asp:LinkButton>
                </div>
            </div>
        </div>
        <!--/工具栏-->
        <div class="table-container">
            <!--列表展示.开始-->
            <asp:Repeater ID="rptList" runat="server">
                <HeaderTemplate>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="ltable">
                        <tr id="tr_header">
                            <th width="20%" align="center">广告位名称
                            </th>
                            <th width="20%" align="center">类型
                            </th>
                            <th width="20%" align="center">数量
                            </th>
                            <th width="20%" align="center">尺寸
                            </th>
                            <th width="20%" align="center">操作
                            </th>
                        </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td align="center">
                            <a href="adv_edit.aspx?action=<%=XYEnums.ActionEnum.Edit %>&id=<%#Eval("id")%>">
                                <%#Eval("title")%></a>
                        </td>
                        <td align="center">
                            <%#GetTypeName(Eval("type").ToString())%>
                        </td>
                        <td align="center">
                            <%#Eval("view_num").ToInt()>0?Eval("view_num"):"不限"%>
                        </td>
                        <td align="center">
                            <%#Eval("view_width")%>×<%#Eval("view_height")%>
                        </td>
                        <td align="center">
                            <a href="bar_list.aspx?aid=<%#Eval("id")%>">内容</a>&nbsp;<%-- <a href="adv_view.aspx?id=<%#Eval("id")%>">
                        调用</a>&nbsp; --%><a href="adv_edit.aspx?action=<%=XYEnums.ActionEnum.Edit %>&id=<%#Eval("id")%>">
                            编辑</a>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    <%#rptList.Items.Count == 0 ? "<tr id=\"tr_footer\"></tr>" : ""%>
                </table>
                </FooterTemplate>
            </asp:Repeater>
            <!--列表展示.结束-->
            <script type="text/javascript">
                $(function () {
                    var colspanCount = 0;
                    $("#tr_header").find("th").each(function () {
                        if ($(this).attr("colspan") > 0) {
                            colspanCount = parseInt(colspanCount) + parseInt($(this).attr("colspan"));
                        }
                        else {
                            colspanCount++;
                        }
                    })
                    $("#tr_footer").html("<td id=\"td_none\" align=\"center\" colspan=\"" + colspanCount + "\">暂无记录</td>")
                })
            </script>
        </div>
        <!--内容底部-->
        <div class="line20">
        </div>
        <div class="pagelist">
            <div class="l-btns">
                <span>显示</span><asp:TextBox ID="txtPageNum" runat="server" CssClass="pagenum" onkeydown="return checkNumber(event);"
                    OnTextChanged="txtPageNum_TextChanged" AutoPostBack="True"></asp:TextBox><span>条/页</span>
            </div>
            <div id="PageContent" runat="server" class="default">
            </div>
        </div>
        <!--/内容底部-->
    </form>
</body>
</html>
