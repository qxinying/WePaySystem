﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="bar_list.aspx.cs" Inherits="Xinying.Web.admin.advert.bar_list" %>

<%@ Import Namespace="Xinying.Common" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>广告内容管理</title>
    <link href="../../scripts/artdialog/ui-dialog.css" rel="stylesheet" type="text/css" />
    <link href="../skin/default/style.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" charset="utf-8" src="../../scripts/jquery/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../scripts/jquery/Validform_v5.3.2_min.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../scripts/datepicker/WdatePicker.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../scripts/artdialog/dialog-plus-min.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../scripts/webuploader/webuploader.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../editor/kindeditor-min.js"></script>
    <script type="text/javascript" charset="utf-8" src="../js/uploader.js"></script>
    <script type="text/javascript" charset="utf-8" src="../js/laymain.js"></script>
    <script type="text/javascript" charset="utf-8" src="../js/common.js"></script>
</head>
<body class="mainbody">
    <form id="form1" runat="server">
        <!--导航栏-->
        <div class="location">
            <a href="index.aspx" class="back"><i></i><span>返回上一页</span></a>
            <a href="../center.aspx" class="home"><i></i><span>首页</span></a> <i class="arrow"></i><span>广告管理</span>
            <i class="arrow"></i><span>广告内容</span>
        </div>
        <!--/导航栏-->
        <!--工具栏-->
        <div class="toolbar-wrap">
            <div id="floatHead" class="toolbar">
                <div class="l-list">
                    <ul class="icon-list">
                        <li><a class="add" href="bar_edit.aspx?action=<%=XYEnums.ActionEnum.Add %>&aid=<%=this.aid %>">
                            <i></i><span>新增</span></a></li>
                        <li><a class="all" href="javascript:;" onclick="checkAll(this);"><i></i><span>全选</span></a></li>
                        <li>
                            <asp:LinkButton ID="btnSave" runat="server" CssClass="save" OnClick="btnSave_Click"><i></i><span>保存</span></asp:LinkButton></li>
                        <li>
                            <asp:LinkButton ID="btnDelete" runat="server" CssClass="del" OnClientClick="return ExePostBack('btnDelete');"
                                OnClick="btnDelete_Click"><i></i><span>删除</span></asp:LinkButton></li>
                    </ul>
                    <div class="menu-list">
                        <div class="rule-single-select">
                            <asp:DropDownList ID="ddlAdvertId" runat="server" AutoPostBack="True" OnSelectedIndexChanged="btnSearch_Click">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="r-list">
                    <asp:TextBox ID="txtKeywords" runat="server" CssClass="keyword" />
                    <asp:LinkButton ID="btnSearch" runat="server" CssClass="btn-search" OnClick="btnSearch_Click">查询</asp:LinkButton>
                </div>
            </div>
        </div>
        <!--/工具栏-->
        <!--列表-->
        <asp:Repeater ID="rptList" runat="server">
            <HeaderTemplate>
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="ltable">
                    <tr id="tr_header">
                        <th width="15%" align="center">选择
                        </th>
                        <th width="20%" align="center">广告名称
                        </th>
                        <%--  <th width="12%">
                        开始时间
                    </th>
                    <th width="12%">
                        到期时间
                    </th>--%>
                        <th width="15%" align="center">链接
                        </th>
                        <%--   <th width="8%">
                        状态
                    </th>--%>
                        <th width="20%" align="center">发布时间
                        </th>
                        <th width="15%" align="center">排序
                        </th>
                        <th width="15%" align="center">操作
                        </th>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td align="center">
                        <asp:CheckBox ID="chkId" CssClass="checkall" runat="server" />
                        <asp:HiddenField ID="hidId" Value='<%#Eval("id")%>' runat="server" />
                    </td>
                    <td align="center">
                        <a href="bar_edit.aspx?action=<%#XYEnums.ActionEnum.Edit %>&id=<%#Eval("id")%>">
                            <%#Eval("title")%></a>
                    </td>
                    <%--  <td align="center">
                    <%#Convert.ToDateTime(Eval("start_time")).ToString("yyyy-MM-dd")%>
                </td>
                <td align="center">
                    <%#Convert.ToDateTime(Eval("end_time")).ToString("yyyy-MM-dd")%>
                </td>--%>
                    <td align="center">
                        <a target="_blank" href="<%#Eval("link_url") %>">广告链接</a>
                    </td>
                    <%--   <td align="center">
                    <%#GetState(Eval("is_lock").ToString(), Eval("start_time").ToString(), Eval("end_time").ToString())%>
                </td>--%>
                    <td align="center">
                        <%#string.Format("{0:g}",Eval("add_time"))%>
                    </td>
                    <td align="center">
                        <asp:TextBox ID="txtSortId" runat="server" Text='<%#Eval("sort_id")%>' CssClass="sort"
                            onkeydown="return checkNumber(event);" />
                    </td>
                    <td align="center">
                        <a href="bar_edit.aspx?action=<%#XYEnums.ActionEnum.Edit %>&id=<%#Eval("id")%>">编辑</a>
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                <%#rptList.Items.Count == 0 ? "<tr id=\"tr_footer\"></tr>" : ""%>
            </table>
            </FooterTemplate>
        </asp:Repeater>
        <!--/列表-->
        <script type="text/javascript">
            $(function () {
                var colspanCount = 0;
                $("#tr_header").find("th").each(function () {
                    if ($(this).attr("colspan") > 0) {
                        colspanCount = parseInt(colspanCount) + parseInt($(this).attr("colspan"));
                    }
                    else {
                        colspanCount++;
                    }
                })
                $("#tr_footer").html("<td id=\"td_none\" align=\"center\" colspan=\"" + colspanCount + "\">暂无记录</td>")
            })
        </script>
        <!--内容底部-->
        <div class="line20">
        </div>
        <div class="pagelist">
            <div class="l-btns">
                <span>显示</span><asp:TextBox ID="txtPageNum" runat="server" CssClass="pagenum" onkeydown="return checkNumber(event);"
                    OnTextChanged="txtPageNum_TextChanged" AutoPostBack="True"></asp:TextBox><span>条/页</span>
            </div>
            <div id="PageContent" runat="server" class="default">
            </div>
        </div>
        <!--/内容底部-->
    </form>
</body>
</html>
