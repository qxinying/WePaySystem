﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="orderMissedHaddler.aspx.cs" Inherits="Xinying.Web.admin.wepayorder.orderMissedHaddler" %>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,initial-scale=1.0,user-scalable=no" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <title>掉单处理</title>
    <link href="../../scripts/artdialog/ui-dialog.css" rel="stylesheet" type="text/css" />
    <link href="../skin/default/style.css" rel="stylesheet" type="text/css" />
    <link href="../../css/pagination.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../scripts/jquery/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" src="../../scripts/artdialog/dialog-plus-min.js"></script>
    <script type="text/javascript" charset="utf-8" src="../js/laymain.js"></script>
    <script type="text/javascript" charset="utf-8" src="../js/common.js"></script>
    <script type="text/javascript" src="/scripts/Di/compatibility.js"></script>
</head>
<body class="mainbody">
    <form id="form1" runat="server">
        <!--导航栏-->
        <div class="location">
            <a href="order_list.aspx" class="back"><i></i><span>返回列表页</span></a> <a href="../center.aspx"
                class="home"><i></i><span>首页</span></a> <i class="arrow"></i><a href="orderMissedHaddler.aspx">
                    <span>掉单处理</span></a>
        </div>
        <div class="line10">
        </div>
        <!--/导航栏-->
        <!--内容-->
        <div class="content-tab-wrap">
            <div id="floatHead" class="content-tab">
                <div class="content-tab-ul-wrap">
                    <ul>
                        <li><a href="javascript:;" onclick="tabs(this);" class="selected">掉单处理</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="tab-content">
            <dl>
                <dt>订单号</dt>
                <dd>
                    <input type="text" id="txtOrderNo" runat="server" class="input normal" maxlength="50" />
                    <input type="button" id="btnShowInfo" onclick="showorderinfo()" value="查询订单" />
                    <br />
                    <span style="color: red;">请输入本系统订单号进行查询</span>
                </dd>
            </dl>
            <dl>
                <dt>订单信息</dt>
                <dd>
                    <label id="label_orderinfo"></label>
                </dd>
            </dl>
        </div>
        <script type="text/javascript">
            function showorderinfo() {
                var orderno = trim($("#<%=txtOrderNo.ClientID%>").val());
                if (orderno != "") {
                    $.get("<%=Xinying.Common.XYKeys.AJAX_ADMIN_SYS %>", { timestamp: new Date().getTime(), orderno: orderno, action: "ShowOrderInfo" }, function (data) {
                        $("#label_orderinfo").html(data);
                    });
                }
                else {
                    $("#label_orderinfo").html("");
                }
            }
            $(function () {
                var sysorderno = trim($("#<%=txtOrderNo.ClientID%>").val());
                if (trim(sysorderno) != "") {
                    showorderinfo();
                }
            })
        </script>
        <!--/内容-->
        <!--工具栏-->
        <div class="page-footer">
            <div class="btn-list">
                <asp:Button ID="btnSubmit" runat="server" Text="确认处理" CssClass="btn" OnClick="btnSubmit_Click" />
                <input name="btnReturn" type="button" value="返回上一页" class="btn yellow" onclick="javascript: history.back(-1);" />
            </div>
            <div class="clear">
            </div>
        </div>
        <!--/工具栏-->
    </form>
</body>
</html>

