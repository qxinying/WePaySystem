﻿using System;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Xinying.Common;

namespace Xinying.Web.admin.wepayorder
{
    public partial class list : Web.UI.ManagePage
    {
        protected int totalCount;
        protected int page;
        protected int pageSize;

        protected string keywords = string.Empty;
        protected int paymentstatus = XYRequest.GetQueryInt("paymentstatus");
        protected int paymentid = XYRequest.GetQueryInt("paymentid");
        protected int resstatus = XYRequest.GetQueryInt("resstatus");

        protected string channel_name = "wepayorder";
        protected string log_name = "订单";
        protected string ordiUri = "";
        protected string ordiPageUri = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            this.keywords = XYRequest.GetQueryString("keywords");
            paymentstatus = XYRequest.GetQueryInt("paymentstatus");
            paymentid = XYRequest.GetQueryInt("paymentid");
            resstatus = XYRequest.GetQueryInt("resstatus");
            #region uri路径拼接
            ordiUri = Utils.CombUrlTxt("list.aspx", "keywords={0}&paymentstatus={1}&paymentid={2}&resstatus={3}", this.keywords, paymentstatus.ToStr(), paymentid.ToStr(), resstatus.ToStr());
            ordiPageUri = Utils.CombUrlTxt("list.aspx", "keywords={0}&page={1}&paymentstatus={2}&paymentid={3}&resstatus={4}", this.keywords, "__id__", paymentstatus.ToStr(), paymentid.ToStr(), resstatus.ToStr());
            #endregion
            this.pageSize = GetPageSize(10); //每页数量
            if (!Page.IsPostBack)
            {
                ChkAdminLevel(channel_name, XYEnums.ActionEnum.View.ToString()); //检查权限
                RptBind("id>0 " + CombSqlTxt(keywords, paymentstatus, paymentid, resstatus), "addtime desc,id desc");
            }
        }

        #region 数据绑定=================================
        private void RptBind(string _strWhere, string _orderby)
        {
            this.page = XYRequest.GetQueryInt("page", 1);
            txtKeywords.Text = this.keywords;
            this.rptList.DataSource = Model.wepayorder.GetList(this.pageSize, this.page, _strWhere, _orderby, out this.totalCount);
            this.rptList.DataBind();

            //绑定页码
            txtPageNum.Text = this.pageSize.ToString();
            string pageUrl = ordiPageUri;
            PageContent.InnerHtml = Utils.OutPageList(this.pageSize, this.page, this.totalCount, pageUrl, 8);
        }
        #endregion

        #region 组合SQL查询语句==========================
        protected string CombSqlTxt(string _keywords, int _paymentstatus, int _paymentid, int _resstatus)
        {
            StringBuilder strTemp = new StringBuilder();
            if (!string.IsNullOrEmpty(_keywords))
            {
                _keywords = _keywords.Replace("'", "");
                strTemp.AppendFormat(" and instr(domain,'{0}')>0 ", _keywords);
            }
            if (_paymentstatus > 0)
            {
                strTemp.Append(" and paymentstatus=" + _paymentstatus);
            }
            if (_paymentid > 0)
            {
                strTemp.Append(" and paymentid=" + _paymentid);
            }
            if (_resstatus > 0)
            {
                switch (_resstatus)
                {
                    case 1://未响应
                        strTemp.Append(" and apiuser_resstatus=0 ");
                        break;
                    case 2://已响应
                        strTemp.Append(" and apiuser_resstatus=1 ");
                        break;
                    case 3://已过期
                        strTemp.Append(" and apiuser_resstatus=2 ");
                        break;
                }
            }

            return strTemp.ToString();
        }
        #endregion

        #region 返回每页数量=============================
        private int GetPageSize(int _default_size)
        {
            int _pagesize;
            if (int.TryParse(Utils.GetCookie("wepayorder_page_size", "XinyingPage"), out _pagesize))
            {
                if (_pagesize > 0)
                {
                    return _pagesize;
                }
            }
            return _default_size;
        }
        #endregion

        //关健字查询
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            Response.Redirect(Utils.CombUrlTxt("list.aspx", "keywords={0}&paymentstatus={1}&paymentid={2}&resstatus={3}", txtKeywords.Text, ddlPaymentStatus.SelectedValue, ddlPaymentId.SelectedValue, ddlResStauts.SelectedValue));
        }

        //设置分页数量
        protected void txtPageNum_TextChanged(object sender, EventArgs e)
        {
            int _pagesize;
            if (int.TryParse(txtPageNum.Text.Trim(), out _pagesize))
            {
                if (_pagesize > 0)
                {
                    Utils.WriteCookie("wepayorder_page_size", "XinyingPage", _pagesize.ToString(), 14400);
                }
            }
            Response.Redirect(ordiUri);
        }
    }
}