﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="list.aspx.cs" Inherits="Xinying.Web.admin.wepayorder.list" %>

<%@ Import Namespace="Xinying.Common" %>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,initial-scale=1.0,user-scalable=no" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <title>订单列表</title>
    <link href="../../scripts/artdialog/ui-dialog.css" rel="stylesheet" type="text/css" />
    <link href="../skin/default/style.css" rel="stylesheet" type="text/css" />
    <link href="../../css/pagination.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../scripts/jquery/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" src="../../scripts/artdialog/dialog-plus-min.js"></script>
    <script type="text/javascript" charset="utf-8" src="../js/laymain.js"></script>
    <script type="text/javascript" charset="utf-8" src="../js/common.js"></script>
</head>

<body class="mainbody">
    <form id="form1" runat="server">
        <!--导航栏-->
        <div class="location">
            <a href="javascript:history.back(-1);" class="back"><i></i><span>返回上一页</span></a>
            <a href="../center.aspx" class="home"><i></i><span>首页</span></a>
            <i class="arrow"></i>
            <span>订单列表</span>
        </div>
        <!--/导航栏-->

        <!--工具栏-->
        <div id="floatHead" class="toolbar-wrap">
            <div class="toolbar">
                <div class="box-wrap">
                    <a class="menu-btn"></a>
                    <div class="l-list">
                        <div class="menu-list">
                            <div class="rule-single-select">
                                <asp:DropDownList ID="ddlPaymentStatus" runat="server" AutoPostBack="True" OnSelectedIndexChanged="btnSearch_Click">
                                    <asp:ListItem Value="0" Selected="True">支付状态</asp:ListItem>
                                    <asp:ListItem Value="1">待支付</asp:ListItem>
                                    <asp:ListItem Value="2">已支付</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="rule-single-select">
                                <asp:DropDownList ID="ddlPaymentId" runat="server" AutoPostBack="True" OnSelectedIndexChanged="btnSearch_Click">
                                    <asp:ListItem Value="0" Selected="True">支付方式</asp:ListItem>
                                    <asp:ListItem Value="1">管理员确认</asp:ListItem>
                                    <asp:ListItem Value="2">微信支付</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="rule-single-select">
                                <asp:DropDownList ID="ddlResStauts" runat="server" AutoPostBack="True" OnSelectedIndexChanged="btnSearch_Click">
                                    <asp:ListItem Value="0" Selected="True">通知响应状态</asp:ListItem>
                                    <asp:ListItem Value="1">未响应</asp:ListItem>
                                    <asp:ListItem Value="2">已响应</asp:ListItem>
                                    <asp:ListItem Value="3">已过期</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="r-list">
                        <asp:TextBox ID="txtKeywords" runat="server" CssClass="keyword" placehoder="请输入域名模糊匹配" />
                        <asp:LinkButton ID="lbtnSearch" runat="server" CssClass="btn-search" OnClick="btnSearch_Click">查询</asp:LinkButton>
                    </div>
                </div>
            </div>
        </div>
        <!--/工具栏-->

        <!--列表-->
        <div class="table-container">
            <asp:Repeater ID="rptList" runat="server">
                <HeaderTemplate>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="ltable">
                        <tr id="tr_header">
                            <th width="10%" align="center">原订单号</th>
                            <th width="10%" align="center">本系统订单号</th>
                            <th width="10%" align="center">支付金额(单位：元)</th>
                            <th width="10%" align="center">创建时间</th>
                            <th width="20%" align="left">支付信息</th>
                            <th width="20%" align="left">调用方信息</th>
                            <th width="20%" align="left">回调通知信息</th>
                        </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td align="center">
                            <%#Eval("orderno") %>
                        </td>
                        <td align="left" style="word-break: break-all;">
                            <%#Eval("sys_orderno") %>
                        </td>
                        <td align="center">
                            <%#Math.Round(Eval("realamount").ToDecimal()/(decimal)100.00,2) %>
                        </td>
                        <td align="center" style="word-break: break-all;">
                            <%#Eval("addtime").ToDateTimeStr() %>
                        </td>
                        <td align="left">
                            <%#Xinying.Model.wepayorder.GetPayInfo(Eval("id")) %>
                        </td>
                        <td align="left" style="word-break: break-all;">
                            <%#Xinying.Model.wepayorder.GetApiUserInfo(Eval("id")) %>
                        </td>
                        <td align="left">
                            <%#Xinying.Model.wepayorder.GetApiUserResInfo(Eval("id")) %>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    <%#rptList.Items.Count == 0 ? "<tr id=\"tr_footer\"></tr>" : ""%>
                </table>
                </FooterTemplate>
            </asp:Repeater>
        </div>
        <!--/列表-->
        <script type="text/javascript">
            $(function () {
                var colspanCount = 0;
                $("#tr_header").find("th").each(function () {
                    if ($(this).attr("colspan") > 0) {
                        colspanCount = parseInt(colspanCount) + parseInt($(this).attr("colspan"));
                    }
                    else {
                        colspanCount++;
                    }
                })
                //var thCount = $("#tr_header").find("th").length;
                $("#tr_footer").html("<td id=\"td_none\" align=\"center\" colspan=\"" + colspanCount + "\">暂无记录</td>")
            })
        </script>
        <!--内容底部-->
        <div class="line20"></div>
        <div class="pagelist">
            <div class="l-btns">
                <span>显示</span><asp:TextBox ID="txtPageNum" runat="server" CssClass="pagenum" onkeydown="return checkNumber(event);"
                    OnTextChanged="txtPageNum_TextChanged" AutoPostBack="True"></asp:TextBox><span>条/页</span>
            </div>
            <div id="PageContent" runat="server" class="default"></div>
        </div>
        <!--/内容底部-->
    </form>
</body>
</html>
