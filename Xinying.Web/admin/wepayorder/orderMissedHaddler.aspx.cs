﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Xinying.Common;
using LitJson;

namespace Xinying.Web.admin.wepayorder
{
    public partial class orderMissedHaddler : Web.UI.ManagePage
    {
        private string action = XYEnums.ActionEnum.Add.ToString(); //操作类型
        protected string channel_name = "wepayorderMissedHaddler";
        protected string sysorder = XYRequest.GetQueryString("sysorder");
        protected void Page_Load(object sender, EventArgs e)
        {
            string _action = XYRequest.GetQueryString("action");
            sysorder = XYRequest.GetQueryString("sysorder");
            if (!Page.IsPostBack)
            {
                ChkAdminLevel(channel_name, XYEnums.ActionEnum.View.ToString()); //检查权限
                loaddata();
            }
        }

        protected void loaddata()
        {
            txtOrderNo.Value = sysorder;
        }

        //保存
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            //取得管理员登录信息
            Model.manager adminInfo = new Web.UI.ManagePage().GetAdminInfo();
            if (adminInfo == null)
            {
                JscriptMsg("未登录或已超时，请重新登录！", "");
                return;
            }
            else
            {
                ChkAdminLevel(channel_name, XYEnums.ActionEnum.Edit.ToString()); //检查权限
            }
            string orderno = txtOrderNo.Value.Replace(" ", "");
            if (!string.IsNullOrEmpty(orderno))
            {
                Model.wepayorder model_order = Model.wepayorder.GetModelBySysOrderNo(orderno);
                if (model_order != null)
                {
                    if (model_order.paymentstatus == 2)
                    {
                        JscriptMsg("该订单已支付，请勿重复操作！", "");
                        return;
                    }
                    else
                    {
                        JsonData Jd = Model.wepayorder.PaySuccessWithAdmin_DBTransaction(model_order.sys_orderno, model_order.realamount, model_order.paymentid);
                        if (Jd["state"].ToInt() == 1 || Jd["state"].ToInt() == 5)
                        {
                            AddAdminLog(XYEnums.ActionEnum.Edit.ToString(), "订单" + model_order.orderno + "掉单处理成功"); //记录日志
                            JscriptMsg("操作成功！", "/admin/wepayorder/orderMissedHaddler.aspx?sysorder=" + model_order.sys_orderno);
                            return;
                        }
                        else
                        {
                            JscriptMsg(Jd["msg"].ToStr(), "");
                            return;
                        }
                    }
                }
                else
                {
                    JscriptMsg("该订单记录不存在或已删除！", "");
                    return;
                }
            }
            else
            {
                JscriptMsg("参数有误！", "");
                return;
            }
        }
    }
}