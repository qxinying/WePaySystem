﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Xinying.Common;

namespace Xinying.Web.admin.apiuser
{
    public partial class edit : Web.UI.ManagePage
    {
        private string action = XYEnums.ActionEnum.Add.ToString(); //操作类型
        private int id = 0;

        protected string channel_name = "apiuser";
        protected string log_name = "第三方开发者";

        protected void Page_Load(object sender, EventArgs e)
        {
            string _action = XYRequest.GetQueryString("action");
            if (!string.IsNullOrEmpty(_action) && _action == XYEnums.ActionEnum.Edit.ToString())
            {
                this.action = XYEnums.ActionEnum.Edit.ToString();//修改类型
                if (!int.TryParse(Request.QueryString["id"] as string, out this.id))
                {
                    JscriptMsg("传输参数不正确！", "back");
                    return;
                }
                if (!Model.apiuser.Exists(this.id))
                {
                    JscriptMsg("记录不存在或已被删除！", "back");
                    return;
                }
            }
            if (!Page.IsPostBack)
            {
                ChkAdminLevel(channel_name, XYEnums.ActionEnum.View.ToString()); //检查权限
                if (action == XYEnums.ActionEnum.Edit.ToString()) //修改
                {
                    ShowInfo(this.id);
                }
            }
        }

        #region 赋值操作=================================
        private void ShowInfo(int _id)
        {
            Model.apiuser model = Model.apiuser.GetModel(_id);
            if (model != null)
            {
                txtApiUserName.Text = model.apiusername;
                txtAppid.Text = model.appid.ToStr();
                txtAppSecret.Text = model.appsecret.ToStr();
                txtSortId.Text = model.sort_id.ToStr();
            }
        }
        #endregion

        #region 增加操作=================================
        private bool DoAdd()
        {
            Model.apiuser model = new Model.apiuser();
            model.apiusername = txtApiUserName.Text;
            model.appid = txtAppid.Text.Trim();
            model.appsecret = txtAppSecret.Text.Trim();
            #region appid（16位，小写字母和数字的组合）和appsecret（16位，小写字母和数字的组合）长度限制
            if (model.appid.Length != 16)
            {
                JscriptMsg("Appid必须为小写字母和数字的组合，长度为16位", "");
                return false;
            }
            if (model.appsecret.Length != 32)
            {
                JscriptMsg("AppSecret必须为小写字母和数字的组合，长度为32位", "");
                return false;
            }
            #endregion
            #region appid和appsecret都保证唯一性
            if (Model.apiuser.ExistsWhere($"appid='{model.appid}'"))
            {
                JscriptMsg("Appid必须唯一，该Appid已被使用，请修改", "");
                return false;
            }
            if (Model.apiuser.ExistsWhere($"appsecret='{model.appsecret}'"))
            {
                JscriptMsg("AppSecret必须唯一，该AppSecret已被使用，请修改", "");
                return false;
            }
            #endregion
            model.sort_id = txtSortId.Text.ToInt();

            if (Model.apiuser.Add(model) > 0)
            {
                AddAdminLog(XYEnums.ActionEnum.Add.ToString(), "添加" + log_name + ":" + model.apiusername); //记录日志
                return true;
            }
            return false;
        }
        #endregion

        #region 修改操作=================================
        private bool DoEdit(int _id)
        {
            bool result = false;
            Model.apiuser model = Model.apiuser.GetModel(_id);

            model.apiusername = txtApiUserName.Text;
            model.appid = txtAppid.Text.Trim();
            model.appsecret = txtAppSecret.Text.Trim();
            #region appid（16位，小写字母和数字的组合）和appsecret（16位，小写字母和数字的组合）长度限制
            if (model.appid.Length != 16)
            {
                JscriptMsg("Appid必须为小写字母和数字的组合，长度为16位", "");
                return false;
            }
            if (model.appsecret.Length != 32)
            {
                JscriptMsg("AppSecret必须为小写字母和数字的组合，长度为32位", "");
                return false;
            }
            #endregion
            #region appid和appsecret都保证唯一性
            if (Model.apiuser.ExistsWhere($"appid='{model.appid}' and id<>{model.id}"))
            {
                JscriptMsg("Appid必须唯一，该Appid已被使用，请修改", "");
                return false;
            }
            if (Model.apiuser.ExistsWhere($"appsecret='{model.appsecret}' and id<>{model.id}"))
            {
                JscriptMsg("AppSecret必须唯一，该AppSecret已被使用，请修改", "");
                return false;
            }
            #endregion
            model.sort_id = txtSortId.Text.ToInt();

            if (Model.apiuser.Update(model))
            {
                AddAdminLog(XYEnums.ActionEnum.Edit.ToString(), "修改" + log_name + ":" + model.apiusername); //记录日志
                result = true;
            }

            return result;
        }
        #endregion

        //保存
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (action == XYEnums.ActionEnum.Edit.ToString()) //修改
            {
                ChkAdminLevel(channel_name, XYEnums.ActionEnum.Edit.ToString()); //检查权限
                if (!DoEdit(this.id))
                {
                    JscriptMsg("保存过程中发生错误！", "");
                    return;
                }
                JscriptMsg("修改成功！", "list.aspx");
            }
            else //添加
            {
                ChkAdminLevel(channel_name, XYEnums.ActionEnum.Add.ToString()); //检查权限
                if (!DoAdd())
                {
                    JscriptMsg("保存过程中发生错误！", "");
                    return;
                }
                JscriptMsg("添加成功！", "list.aspx");
            }
        }
    }
}