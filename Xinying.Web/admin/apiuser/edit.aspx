﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="edit.aspx.cs" Inherits="Xinying.Web.admin.apiuser.edit" ValidateRequest="false" %>

<%@ Import Namespace="Xinying.Common" %>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,initial-scale=1.0,user-scalable=no" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <title>编辑第三方开发者</title>
    <link href="../../scripts/artdialog/ui-dialog.css" rel="stylesheet" type="text/css" />
    <link href="../skin/default/style.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../scripts/jquery/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" src="../../scripts/jquery/Validform_v5.3.2_min.js"></script>

    <script type="text/javascript" src="../../scripts/artdialog/dialog-plus-min.js"></script>
    <script type="text/javascript" charset="utf-8" src="../js/laymain.js"></script>
    <script type="text/javascript" charset="utf-8" src="../js/common.js"></script>
    <script type="text/javascript">
        $(function () {
            //初始化表单验证
            $("#form1").initValidform();
        });
    </script>
</head>

<body class="mainbody">
    <form id="form1" runat="server">
        <!--导航栏-->
        <div class="location">
            <a href="list.aspx" class="back"><i></i><span>返回列表页</span></a>
            <a href="../center.aspx" class="home"><i></i><span>首页</span></a>
            <i class="arrow"></i>
            <a href="manager_list.aspx"><span>第三方开发者</span></a>
            <i class="arrow"></i>
            <span>编辑第三方开发者</span>
        </div>
        <div class="line10"></div>
        <!--/导航栏-->

        <!--内容-->
        <div id="floatHead" class="content-tab-wrap">
            <div class="content-tab">
                <div class="content-tab-ul-wrap">
                    <ul>
                        <li><a class="selected" href="javascript:;">第三方开发者信息</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="tab-content">
            <dl>
                <dt>名称</dt>
                <dd>
                    <asp:TextBox ID="txtApiUserName" runat="server" CssClass="input normal"></asp:TextBox></dd>
            </dl>
            <dl>
                <dt>Appid</dt>
                <dd>
                    <asp:TextBox ID="txtAppid" runat="server" CssClass="input normal"></asp:TextBox>
                    <span class="red">*16位，数字和小写字母随机组合</span>
                    <input type="button" id="btnRandomAppid" onclick="randomappid()" value="随机生成16位Appid" />
                </dd>
            </dl>
            <dl>
                <dt>AppSecret</dt>
                <dd>
                    <asp:TextBox ID="txtAppSecret" runat="server" CssClass="input normal"></asp:TextBox>
                    <span class="red">*32位，数字和小写字母随机组合</span>
                    <input type="button" id="btnRandomAppSecret" onclick="randomappsecret()" value="随机生成32位AppSecret" />
                    <script type="text/javascript">
                        function randomappid() {
                            var appid = randomWord(false, 16);
                            $("#<%=txtAppid.ClientID%>").val(appid);
                        }

                        function randomappsecret() {
                            var appsecret = randomWord(false, 32);
                            $("#<%=txtAppSecret.ClientID%>").val(appsecret);
                        }

                        /*
                        ** randomWord 产生任意长度随机字母数字组合
                        ** randomFlag-是否任意长度 
                        ** min-任意长度最小位[固定位数] 
                        ** max-任意长度最大位
                        */
                        function randomWord(randomFlag, min, max) {
                            var str = "",
                                range = min,
                                arr = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];
                            // 随机产生
                            if (randomFlag) {
                                range = Math.round(Math.random() * (max - min)) + min;
                            }
                            for (var i = 0; i < range; i++) {
                                pos = Math.round(Math.random() * (arr.length - 1));
                                str += arr[pos];
                            }
                            return str;
                        }
                    </script>

                </dd>
            </dl>
            <dl>
                <dt>排序数字</dt>
                <dd>
                    <asp:TextBox ID="txtSortId" runat="server" CssClass="input small" datatype="n" sucmsg=" ">99</asp:TextBox>
                    <span class="Validform_checktip">*数字，越小越向前</span>
                </dd>
            </dl>
        </div>
        <!--/内容-->

        <!--工具栏-->
        <div class="page-footer">
            <div class="btn-wrap">
                <asp:Button ID="btnSubmit" runat="server" Text="提交保存" CssClass="btn" OnClick="btnSubmit_Click" />
                <input name="btnReturn" type="button" value="返回上一页" class="btn yellow" onclick="javascript: history.back(-1);" />
            </div>
        </div>
        <!--/工具栏-->
    </form>
</body>
</html>

