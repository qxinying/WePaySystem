﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.Text;
using System.Timers;
using Xinying.Common;
using Xinying.Model;
using System.Text.RegularExpressions;

namespace Xinying.Web
{
    public class Global : System.Web.HttpApplication
    {
        //在应用程序启动时运行的代码
        protected void Application_Start(object sender, EventArgs e)
        {
            int timeSpan = 1000 * 60;
            Timer t = new Timer(timeSpan);//设计时间间隔，这里1分钟调用一次
            t.Elapsed += new ElapsedEventHandler(Global_ExecuteTask);//定时执行
            t.AutoReset = true;
            t.Enabled = true;

            #region 程序开始之后要马上执行的方法

            #endregion
        }

        void Global_ExecuteTask(object sender, ElapsedEventArgs e)
        {
            //每分钟执行
            try
            {
                //在这里编写需要定时执行的逻辑代码
                #region 对日期没有要求的（平时每次处理10000条；0点 1点 3点 4点 5点 6点 每次处理50000条；2点处理完剩下的）;对日期有要求的（平时每次处理10000条；0点 1点 2点 3点 4点 5点 6点 每次处理50000条；23点处理完剩下的）
                //DateTime.Now.Hour 取值范围 0-23
                int top_small = 100;
                if (DateTime.Now.Hour == 0 || DateTime.Now.Hour == 1 || DateTime.Now.Hour == 3 || DateTime.Now.Hour == 4 || DateTime.Now.Hour == 5 || DateTime.Now.Hour == 6)
                {
                    top_small = 500;
                }
                else if (DateTime.Now.Hour == 2)
                {
                    top_small = 0;
                }
                #endregion
                #region 自动设置已过期未响应订单，通知期限为24h
                List<Model.wepayorder> WePayOrderListToBeOutOfDate = Model.wepayorder.GetObjectListO(top_small, "id", $"paymentstatus=2 and apiuser_resstatus=0 and paymenttime<'{DateTime.Now.AddHours(-24)}'", "");
                if (WePayOrderListToBeOutOfDate != null && WePayOrderListToBeOutOfDate.Count > 0)
                {
                    for (int i = 0, l = WePayOrderListToBeOutOfDate.Count; i < l; i++)
                    {
                        Model.wepayorder.UpdateField(WePayOrderListToBeOutOfDate[i].id, "apiuser_resstatus=2");
                    }
                }
                #endregion
                #region 本系统已成功回调但未收到第三方响应的v2.0版本的订单发送通知，通知时间为（距离回调时间1m/2m/3m/4m/5m/10m/20m/30m/1h/2h/5h/12h/24h），一共13次，第三方返回“SUCCESS”之后不再异步通知
                List<Model.wepayorder> WePayOrderListToBeNoticed = Model.wepayorder.GetObjectListO(top_small, "id,sys_orderno,apiuser_global_notifyurl,addtime", $"paymentstatus=2 and apiuser_resstatus=0 and wx_version='2.0' and (TIMESTAMPDIFF(MINUTE, paymenttime, '{DateTime.Now}')=1 or TIMESTAMPDIFF(MINUTE, paymenttime, '{DateTime.Now}')=2 or TIMESTAMPDIFF(MINUTE, paymenttime, '{DateTime.Now}')=3 or TIMESTAMPDIFF(MINUTE, paymenttime, '{DateTime.Now}')=4 or TIMESTAMPDIFF(MINUTE, paymenttime, '{DateTime.Now}')=5 or TIMESTAMPDIFF(MINUTE, paymenttime, '{DateTime.Now}')=10 or TIMESTAMPDIFF(MINUTE, paymenttime, '{DateTime.Now}')=20 or TIMESTAMPDIFF(MINUTE, paymenttime, '{DateTime.Now}')=30 or TIMESTAMPDIFF(MINUTE, paymenttime, '{DateTime.Now}')=60 or TIMESTAMPDIFF(MINUTE, paymenttime, '{DateTime.Now}')=120 or TIMESTAMPDIFF(MINUTE, paymenttime, '{DateTime.Now}')=300 or TIMESTAMPDIFF(MINUTE, paymenttime, '{DateTime.Now}')=720 or TIMESTAMPDIFF(MINUTE, paymenttime, '{DateTime.Now}')=1440)", "");
                if (WePayOrderListToBeNoticed != null && WePayOrderListToBeNoticed.Count > 0)
                {
                    for (int i = 0, l = WePayOrderListToBeNoticed.Count; i < l; i++)
                    {
                        #region 获取第三方的同步回调地址和参数，使用微信签名方法用第三方的appsecret进行签名，第三方也使用他们自己的appsecret进行验签。同时接收其反馈信息，更新该订单对应的回调信息
                        Model.wepayorder.UpdateNoticeInfo(WePayOrderListToBeNoticed[i].sys_orderno);
                        if (!string.IsNullOrEmpty(WePayOrderListToBeNoticed[i].apiuser_global_notifyurl))
                        {
                            var res = Xinying.Common.Utils.HttpGet(WePayOrderListToBeNoticed[i].apiuser_global_notifyurl);
                            Model.wepayorder.UpdateResInfoByRes(WePayOrderListToBeNoticed[i].sys_orderno, res);
                        }
                        #endregion
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                Utils.WriteTxtGlobal(System.Web.Hosting.HostingEnvironment.MapPath("~/bugLog.txt"), "Global_ExecuteTask_ex:" + ex);
            }
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            string q = "<div style='position:fixed;top:0px;width:100%;height:100%;background-color:white;color:green;font-weight:bold;border-bottom:5px solid #999;'><br>您的提交带有不合法参数,谢谢合作!<br><br>了解更多请点击:<a href='http://webscan.360.cn'>360网站安全检测</a></div>";
            if (Request.Cookies != null)
            {
                if (safe_360.CookieData())
                {
                    Response.Write(q);
                    Response.End();
                }
            }

            if (Request.UrlReferrer != null)
            {
                if (safe_360.referer())
                {
                    Response.Write(q);
                    Response.End();
                }
            }

            if (Request.RequestType.ToUpper() == "POST")
            {
                if (safe_360.PostData())
                {

                    Response.Write(q);
                    Response.End();
                }
            }

            if (Request.RequestType.ToUpper() == "GET")
            {
                if (safe_360.GetData())
                {
                    Response.Write(q);
                    Response.End();
                }
            }
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        //在出现未处理的错误时运行的代码
        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }
        //在应用程序关闭时运行的代码
        protected void Application_End(object sender, EventArgs e)
        {

        }

        public class safe_360
        {
            private const string StrRegex = @"<[^>]+?style=[\w]+?:expression\(|\b(alert|confirm|prompt)\b|^\+/v(8|9)|<[^>]*?=[^>]*?&#[^>]*?>|\b(and|or)\b.{1,6}?(=|>|<|\bin\b|\blike\b)|/\*.+?\*/|<\s*script\b|<\s*img\b|\bEXEC\b|UNION.+?SELECT|UPDATE.+?SET|INSERT\s+INTO.+?VALUES|(SELECT|DELETE).+?FROM|(CREATE|ALTER|DROP|TRUNCATE)\s+(TABLE|DATABASE)";

            public static bool PostData()
            {
                bool result = false;

                string url = HttpContext.Current.Request.Url.ToString();
                if (Regex.IsMatch(url, @"^.*" + HttpContext.Current.Request.Url.Host + @"[^\/]*\/admin\/.*$"))
                {
                    return false;
                }

                for (int i = 0; i < HttpContext.Current.Request.Form.Count; i++)
                {
                    result = CheckData(HttpContext.Current.Request.Form[i].ToString());
                    if (result)
                    {
                        break;
                    }
                }
                return result;
            }


            public static bool GetData()
            {
                bool result = false;

                for (int i = 0; i < HttpContext.Current.Request.QueryString.Count; i++)
                {
                    result = CheckData(HttpContext.Current.Request.QueryString[i].ToString());
                    if (result)
                    {
                        break;
                    }
                }
                return result;
            }

            public static bool CookieData()
            {
                bool result = false;
                for (int i = 0; i < HttpContext.Current.Request.Cookies.Count; i++)
                {
                    result = CheckData(HttpContext.Current.Request.Cookies[i].Value.ToLower());
                    if (result)
                    {
                        break;
                    }
                }
                return result;

            }

            public static bool referer()
            {
                bool result = false;
                return result = CheckData(HttpContext.Current.Request.UrlReferrer.ToString());
            }

            public static bool CheckData(string inputData)
            {
                if (Regex.IsMatch(inputData, StrRegex))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }
}