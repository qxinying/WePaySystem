﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="error.aspx.cs" Inherits="Xinying.Web.aspx.error.error" %>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta content="telephone=no" name="format-detection">
    <link href="/css/common.css" rel="stylesheet" />
    <link href="/css/style.css" rel="stylesheet" />
    <script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
    <title>哎呀出错了T_T</title>
    <script>
        $(function () {
            $("body").css("background", "#e1e0de")
        })
    </script>
    <style type="text/css">
        .abody
        {
            background: #f0efed;
        }
        .error
        {
            background: #e1e0de;
            padding: 30px 0;
            text-align: center;
        }
    </style>
</head>
<body class="abody">
    <div class="error">
        <div class="media">
            <img src="/aspx/error/error.jpg" height="103" /></div>
        <div class="intro">
            多次授权失败T_T请退出微信或清除缓存重试一下<br />
            <div style="text-align: left; margin-left: 30px;">
                授权失败原因可能是：<br />
                1.手机禁用了cookie;<br />
                2.网络不稳定导致授权失败;<br />
                3.微信服务器繁忙.<br />
                请核对原因，更改配置，或稍后重试.
            </div>
        </div>
    </div>
</body>
</html>
