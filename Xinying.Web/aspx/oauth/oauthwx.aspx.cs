﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Xinying.Common;
using Xinying.Model;

namespace Xinying.Web.aspx.oauth
{
    public partial class oauthwx : Web.UI.BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int state = XYRequest.GetQueryInt("state", 1);//1: /aspx/mobile/recharge.aspx
            #region 检测登录状态，如果未登录，获取用户信息；如果已登录，直接跳转
            Xinying.Model.wxuserinfo model = Xinying.Model.wxuserinfo.GetLoginModel();
            if (model != null)
            {
                string reuri = "/aspx/error/error.aspx";
                switch (state)
                {
                    case 1:
                        string B2B2COrderno=Utils.GetCookie("B2B2COrderno");
                        if (!string.IsNullOrEmpty(B2B2COrderno))
                        {
                            reuri = "/aspx/pay/wxpay.aspx?order_no=" + B2B2COrderno;
                        }
                        break;                 
                    default:
                        break;
                }
                Response.Redirect(reuri);
            }
            else
            {
                #region 向腾讯服务器发起请求（微信API 获取code ）
                string uri = config.weburl + "/aspx/oauth/redictwx.aspx";
                string urigetcode = String.Format("https://open.weixin.qq.com/connect/oauth2/authorize?appid={0}&redirect_uri={1}&response_type=code&scope=snsapi_userinfo&state={2}#wechat_redirect", config.wxappid, System.Web.HttpUtility.UrlEncode(uri), state);
                Response.Redirect(urigetcode);
                #endregion
            }
            #endregion
        }
    }
}