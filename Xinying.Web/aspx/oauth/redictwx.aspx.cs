﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Xinying.Common;
using Newtonsoft.Json;
using wxHelper;
using System.Xml.Linq;

namespace Xinying.Web.aspx.oauth
{
    public partial class redictwx : Xinying.Web.UI.BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int issubscribe = 0;
            string state = Request.QueryString["state"];
            string modelstr = Utils.HttpGet(String.Format("https://api.weixin.qq.com/sns/oauth2/access_token?appid={0}&secret={1}&code={2}&grant_type=authorization_code", config.wxappid, config.wxappsecret, Request.QueryString["code"]));
            var model_code = JsonConvert.DeserializeObject<Xinying.Web.UI.BasePage.CodeModel>(modelstr);
            if (model_code != null)
            {
                string userinfostr = Utils.HttpGet(String.Format("https://api.weixin.qq.com/sns/userinfo?access_token={0}&openid={1}&lang=zh_CN", model_code.access_token, model_code.openid));
                var model_userinfo = JsonConvert.DeserializeObject<UserInfo>(userinfostr);
                #region 微信粉丝信息 存入本地或更新
                if (model_userinfo != null && (!string.IsNullOrEmpty(model_userinfo.openid)))
                {
                    UpdateWXUser(model_userinfo);
                }
                #endregion
                #region  判断是否是关注的会员 https://api.weixin.qq.com/cgi-bin/user/info?access_token=ACCESS_TOKEN&openid=OPENID&lang=zh_CN
                #region 判断全局access_token是否过期，如果过期，重新获取。
                string access_token;
                XElement root;
                string path = Request.MapPath("/xmlconfig/access_token.xml");//token的地址
                try
                {
                    root = XElement.Load(path);//加载xml文件
                    if (Utils.StrToDateTime(root.Element("expirestime").Value) < DateTime.Now)//从xml文件中获取接点expirestime的值，判断是否过期
                    {
                        string token = WeiXinRequest.GetAccess_token(config.wxappid, config.wxappsecret);//获取access_Token
                        access_token = token;
                        root.Element("token").SetValue(token);
                        root.Element("expirestime").SetValue(DateTime.Now.AddSeconds(7200));
                        root.Save(path);
                    }
                    else
                    {
                        access_token = root.Element("token").Value;
                    }
                }
                catch (Exception)//文件不存在，则新建一个xml文件
                {
                    access_token = WeiXinRequest.GetAccess_token(config.wxappid, config.wxappsecret);
                    root = new XElement("wx",
                        new XElement("token", access_token),
                        new XElement("expirestime", DateTime.Now.AddSeconds(7200)));
                    root.Save(path);
                }
                #endregion
                string userinfostrIsfocus = Utils.HttpGet(String.Format("https://api.weixin.qq.com/cgi-bin/user/info?access_token={0}&openid={1}", access_token, model_code.openid));
                var model_userinfoIsfocus = JsonConvert.DeserializeObject<UserInfoIsfocus>(userinfostrIsfocus);
                #region 微信粉丝信息 存入本地或更新
                if (model_userinfoIsfocus != null && (!string.IsNullOrEmpty(model_userinfoIsfocus.openid)) && (!userinfostrIsfocus.Contains("\"subscribe\":0,")))
                {
                    issubscribe = 1;
                    Xinying.Model.wxuserinfo.UpdateFieldByOpenID(model_userinfoIsfocus.openid, "is_subscribe=1");
                }
                else
                {
                    Xinying.Model.wxuserinfo.UpdateFieldByOpenID(model_userinfoIsfocus.openid, "is_subscribe=0");
                }
                #endregion
                #endregion
            }
            #region 根据state的值回跳
            //if (issubscribe == 1)
            //{
            string reuri = "/aspx/error/error.aspx";
            switch (state)
            {
                case "1":
                    string B2B2COrderno=Utils.GetCookie("B2B2COrderno");
                    if (!string.IsNullOrEmpty(B2B2COrderno))
                    {
                        reuri = "/aspx/pay/wxpay.aspx?order_no=" + B2B2COrderno;
                    }
                    Response.Redirect(reuri);
                    break;               
                default:
                    Response.Redirect("/aspx/mobile/recharge.aspx");
                    break;
            }
            //}
            //else
            //{
            //    Response.Redirect(config.guanzhuurl);
            //}
            #endregion
        }
    }
}