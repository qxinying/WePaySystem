﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Xinying.Common;
using Newtonsoft.Json;
using wxHelper;
using System.Xml.Linq;
using Xinying.Model;

namespace Xinying.Web.aspx.oauth
{
    public partial class redictwx_forson : System.Web.UI.Page
    {
        protected internal Model.siteconfig config = new BLL.siteconfig().loadConfig();
        protected void Page_Load(object sender, EventArgs e)
        {
            #region 用于回传给第三方的参数
            Dictionary<string, string> ParaDic = new Dictionary<string, string>();
            #endregion
            int issubscribe = 0;
            string state = Request.QueryString["state"];
            string modelstr = Utils.HttpGet(String.Format("https://api.weixin.qq.com/sns/oauth2/access_token?appid={0}&secret={1}&code={2}&grant_type=authorization_code", config.wxappid, config.wxappsecret, Request.QueryString["code"]));
            var model_code = JsonConvert.DeserializeObject<CodeModel>(modelstr);
            if (model_code != null)
            {
                string userinfostr = Utils.HttpGet(String.Format("https://api.weixin.qq.com/sns/userinfo?access_token={0}&openid={1}&lang=zh_CN", model_code.access_token, model_code.openid));
                var model_userinfo = JsonConvert.DeserializeObject<UserInfo>(userinfostr);
                #region 微信粉丝信息 存入本地或更新
                if (model_userinfo != null && (!string.IsNullOrEmpty(model_userinfo.openid)))
                {
                    UpdateWXUser(model_userinfo);
                    #region 回传给第三方的参数整理
                    ParaDic["city"] = model_userinfo.city;
                    ParaDic["country"] = model_userinfo.country;
                    ParaDic["headimgurl"] = HttpUtility.UrlEncode(model_userinfo.headimgurl);
                    ParaDic["nickname"] = model_userinfo.nickname;
                    ParaDic["openid"] = model_userinfo.openid;
                    ParaDic["province"] = model_userinfo.province;
                    ParaDic["sex"] = model_userinfo.sex.ToStr();//值为1时是男性，值为2时是女性，值为0时是未知 
                    #endregion
                }
                #endregion
                #region  判断是否是关注的会员 https://api.weixin.qq.com/cgi-bin/user/info?access_token=ACCESS_TOKEN&openid=OPENID&lang=zh_CN
                #region 判断全局access_token是否过期，如果过期，重新获取。
                string access_token;
                XElement root;
                string path = Request.MapPath("/xmlconfig/access_token.xml");//token的地址
                try
                {
                    root = XElement.Load(path);//加载xml文件
                    if (Utils.StrToDateTime(root.Element("expirestime").Value) < DateTime.Now)//从xml文件中获取接点expirestime的值，判断是否过期
                    {
                        string token = WeiXinRequest.GetAccess_token(config.wxappid, config.wxappsecret);//获取access_Token
                        access_token = token;
                        root.Element("token").SetValue(token);
                        root.Element("expirestime").SetValue(DateTime.Now.AddSeconds(7200));
                        root.Save(path);
                    }
                    else
                    {
                        access_token = root.Element("token").Value;
                    }
                }
                catch (Exception)//文件不存在，则新建一个xml文件
                {
                    access_token = WeiXinRequest.GetAccess_token(config.wxappid, config.wxappsecret);
                    root = new XElement("wx",
                        new XElement("token", access_token),
                        new XElement("expirestime", DateTime.Now.AddSeconds(7200)));
                    root.Save(path);
                }
                #endregion
                string userinfostrIsfocus = Utils.HttpGet(String.Format("https://api.weixin.qq.com/cgi-bin/user/info?access_token={0}&openid={1}", access_token, model_code.openid));
                var model_userinfoIsfocus = JsonConvert.DeserializeObject<Web.UI.BasePage.UserInfoIsfocus>(userinfostrIsfocus);
                #region 微信粉丝信息 存入本地或更新
                if (model_userinfoIsfocus != null && (!string.IsNullOrEmpty(model_userinfoIsfocus.openid)) && (!userinfostrIsfocus.Contains("\"subscribe\":0,")))
                {
                    issubscribe = 1;
                    Xinying.Model.wxuserinfo.UpdateFieldByOpenID(model_userinfoIsfocus.openid, "is_subscribe=1");
                    #region 回传给第三方的参数整理
                    ParaDic["is_subscribe"] = "1";
                    #endregion
                }
                else
                {
                    Xinying.Model.wxuserinfo.UpdateFieldByOpenID(model_userinfoIsfocus.openid, "is_subscribe=0");
                    #region 回传给第三方的参数整理
                    ParaDic["is_subscribe"] = "0";
                    #endregion
                }
                #region 回传给第三方的参数整理
                ParaDic["wxpublicid"] = Model.wxuserinfo.GetIdByOpenId(ParaDic["openid"]).ToStr();
                #endregion
                #endregion
                #endregion
            }
            #region 带上参数会跳给第三方
            string reuri = Utils.GetCookie("WxOauthForSonRedictUrl");
            #region 将回传的参数
            ParaDic["state"] = "wechatoauth";
            string FullRedictUrl = Utils.AddUrlPara(reuri, ParaDic);
            #endregion
            Response.Redirect(FullRedictUrl);
            #endregion
        }

        #region 微信粉丝信息 存入本地或更新 并且登录状态
        public static void UpdateWXUser(UserInfo model_userinfo)
        {
            if (Model.wxuserinfo.ExistsWhere("openid='" + model_userinfo.openid + "'"))
            {
                Model.wxuserinfo model_wxuserinfo = Model.wxuserinfo.GetModelByOpenid(model_userinfo.openid);
                if (model_wxuserinfo == null)
                {
                    model_wxuserinfo = new Model.wxuserinfo();
                }
                if (model_wxuserinfo.addtime == null)
                {
                    model_wxuserinfo.addtime = DateTime.Now;
                }
                model_wxuserinfo.city = model_userinfo.city;
                model_wxuserinfo.country = model_userinfo.country;
                model_wxuserinfo.headimgurl = model_userinfo.headimgurl;
                model_wxuserinfo.nickname = model_userinfo.nickname;
                model_wxuserinfo.openid = model_userinfo.openid;
                model_wxuserinfo.province = model_userinfo.province;
                model_wxuserinfo.sex = model_userinfo.sex;

                if (model_wxuserinfo.id > 0)
                {
                    Model.wxuserinfo.Update(model_wxuserinfo);
                    #region 存入session cookie 保持登录状态
                    //HttpContext.Current.Session["QxUserOpenid"] = model_wxuserinfo;
                    if (!string.IsNullOrEmpty(model_userinfo.openid))
                        Utils.WriteCookie("Xinying", "QXUOPID", Common.DESEncrypt.Encrypt(model_userinfo.openid), 120);
                    if (!string.IsNullOrEmpty(model_userinfo.nickname))
                        Utils.WriteCookie("Xinying", "QXUserName", Common.DESEncrypt.Encrypt(model_userinfo.nickname), 120);
                    #endregion
                }
                else
                {
                    Model.wxuserinfo.Add(model_wxuserinfo);
                    #region 存入session cookie 保持登录状态
                    //HttpContext.Current.Session["QxUserOpenid"] = model_wxuserinfo;
                    if (!string.IsNullOrEmpty(model_userinfo.openid))
                        Utils.WriteCookie("Xinying", "QXUOPID", Common.DESEncrypt.Encrypt(model_userinfo.openid), 120);
                    if (!string.IsNullOrEmpty(model_userinfo.nickname))
                        Utils.WriteCookie("Xinying", "QXUserName", Common.DESEncrypt.Encrypt(model_userinfo.nickname), 120);
                    #endregion
                }
            }
            else
            {
                Model.wxuserinfo model_wxuserinfo = new Model.wxuserinfo();
                model_wxuserinfo.addtime = DateTime.Now;
                model_wxuserinfo.city = model_userinfo.city;
                model_wxuserinfo.country = model_userinfo.country;
                model_wxuserinfo.headimgurl = model_userinfo.headimgurl;
                model_wxuserinfo.nickname = model_userinfo.nickname;
                model_wxuserinfo.openid = model_userinfo.openid;
                model_wxuserinfo.province = model_userinfo.province;
                model_wxuserinfo.sex = model_userinfo.sex;
                Model.wxuserinfo.Add(model_wxuserinfo);
                #region 存入session cookie 保持登录状态
                //HttpContext.Current.Session["QxUserOpenid"] = model_wxuserinfo;
                if (!string.IsNullOrEmpty(model_userinfo.openid))
                    Utils.WriteCookie("Xinying", "QXUOPID", Common.DESEncrypt.Encrypt(model_userinfo.openid), 120);
                if (!string.IsNullOrEmpty(model_userinfo.nickname))
                    Utils.WriteCookie("Xinying", "QXUserName", Common.DESEncrypt.Encrypt(model_userinfo.nickname), 120);
                #endregion
            }
        }
        #endregion

        #region 微会员相关
        public class CodeModel
        {
            public string access_token { get; set; }
            public int expires_in { get; set; }
            public string refresh_token { get; set; }
            public string openid { get; set; }
            public string scope { get; set; }
        }

        public class UserInfo
        {
            public string openid { get; set; }
            public string nickname { get; set; }
            public int sex { get; set; }//用户的性别，值为1时是男性，值为2时是女性，值为0时是未知 
            public string province { get; set; }
            public string city { get; set; }
            public string country { get; set; }
            public string headimgurl { get; set; }
        }

        public class UserInfoIsfocus
        {
            public string openid { get; set; }
            public string nickname { get; set; }
            public int sex { get; set; }//用户的性别，值为1时是男性，值为2时是女性，值为0时是未知 
            public string province { get; set; }
            public string city { get; set; }
            public string country { get; set; }
            public string headimgurl { get; set; }
            public string subscribe_time { get; set; }
        }
        #endregion
    }
}