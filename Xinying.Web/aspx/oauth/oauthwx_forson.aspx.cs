﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Xinying.Common;
using Xinying.Model;

namespace Xinying.Web.aspx.oauth
{
    public partial class oauthwx_forson : System.Web.UI.Page
    {
        protected internal Model.siteconfig config = new BLL.siteconfig().loadConfig();
        protected void Page_Load(object sender, EventArgs e)
        {
            string apiuser_appid = XYRequest.GetString("apiuser_appid");
            string apiuser_redirect_uri = HttpUtility.UrlDecode(XYRequest.GetString("apiuser_redirect_uri"));
            string state = XYRequest.GetString("state");
            #region 参数验证
            if (string.IsNullOrEmpty(apiuser_appid))
            {
                Response.Write("非法请求方");
                return;
            }
            else
            {
                bool hasapiuser = Model.apiuser.ExistsWhere($"appid='{apiuser_appid}'");
                if (!hasapiuser)
                {
                    Response.Write("非法请求方");
                    return;
                }
            }
            if (string.IsNullOrEmpty(apiuser_redirect_uri))
            {
                Response.Write("无授权回调地址");
                return;
            }
            if (state != "wechatoauth")
            {
                Response.Write("非法参数");
                return;
            }
            Utils.WriteCookie("WxOauthForSonRedictUrl", apiuser_redirect_uri);
            #endregion
            #region 向腾讯服务器发起请求（微信API 获取code）
            //Utils.WriteTxt("授权请求:" + Request.Url.ToString());
            //后期考虑扩展，安全性会更高
            //state = Guid.NewGuid().ToString("N");
            //Utils.WriteCookie("WxOauthState", state);//用于和授权回调中的state进行比对
            string uri = config.weburl + "/aspx/oauth/redictwx_forson.aspx";
            string urigetcode = String.Format("https://open.weixin.qq.com/connect/oauth2/authorize?appid={0}&redirect_uri={1}&response_type=code&scope=snsapi_userinfo&state={2}#wechat_redirect", config.wxappid, System.Web.HttpUtility.UrlEncode(uri), state);
            Response.Redirect(urigetcode);
            #endregion
        }
    }
}