﻿using Xinying.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Xinying.Web.aspx.user
{
    public partial class checkstatus : Web.UI.BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Login();
            //if (cookieuser=="")
            //{
            //    Login(wxid);
            //}
            //else
            //{
            //    if (wxid == cookieuser)
            //    {
            //        Login(wxid);
            //    }
            //    else
            //    {
            //        Response.Write("<script>location.href='/login.aspx'</script>");
            //    }
            //}

        }

        #region 用户登录
        void Login()
        {
            string url = XYRequest.GetQueryString("returnurl");
            string openid = XYRequest.GetQueryString("wxid");
            Model.wxuserinfo model = Model.wxuserinfo.GetModelByOpenid(openid);
            if (model != null)
            {
                Session["QxUserOpenid"] = model;
            }
            Utils.WriteCookie("QxUserOpenid", openid, 30000);
            Response.Write("<script>location.href='" + System.Web.HttpUtility.UrlDecode(url) + "'</script>");
        }
        #endregion
    }

}