﻿<%@ Page Language="C#" MasterPageFile="~/aspx/pay/template.Master" AutoEventWireup="true" CodeBehind="wxpay.aspx.cs" Inherits="Xinying.Web.aspx.pay.wxpay" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>微信支付发起页面 -
        <%=config.webname %></title>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="main" runat="server">
    <div id="div_loading" style="text-align: center;">
        <div style="text-align: center; width: 100%; margin-top: 30px;">微信支付正在跳转中，请稍后。。。</div>
        <div style="margin-top: 15px;">
            <img src="/aspx/pay/images/loading.gif" width="30%" />
        </div>
    </div>
    <script type="text/javascript" src="/scripts/Di/compatibility.js"></script>
    <script type="text/javascript" src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
    <script src="/scripts/jquery-1.4.4.js" type="text/javascript"></script>
    <script type="text/javascript">
        var timeStamp = parseInt("<%=this.timeStamp %>")
        $(function () {
            wxJsSDK();
        });

        function wxJsSDK() {
            wx.config({
                debug: false, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
                appId: '<%=this.appid %>', // 必填，公众号的唯一标识
                timestamp: timeStamp, // 必填，生成签名的时间戳
                nonceStr: '<%=this.nonceStr %>', // 必填，生成签名的随机串
                signature: '<%=this.signature %>', // 必填，签名，见附录1
                jsApiList: [
                    'chooseWXPay'
                ]
            });

            wx.error(function (res) {
                //config信息验证失败会执行error函数，如签名过期导致验证失败，具体错误信息可以打开config的debug模式查看，也可以在返回的res参数中查看，对于SPA可以在这里更新签名。
                //alert(res)
            });

            wx.checkJsApi({
                jsApiList: ['chooseWXPay'], // 需要检测的JS接口列表，所有JS接口列表见附录2,
                success: function (res) {
                    // 以键值对的形式返回，可用的api值true，不可用为false
                    // 如：{"checkResult":{"chooseImage":true},"errMsg":"checkJsApi:ok"}
                    //alert(res.toString());
                }
            });

            wx.ready(function () {
                // config信息验证后会执行ready方法，所有接口调用都必须在config接口获得结果之后，config是一个客户端的异步操作，所以如果需要在页面加载时就调用相关接口，则须把相关接口放在ready函数中调用来确保正确执行。对于用户触发时才调用的接口，则可以直接调用，不需要放在ready函数中。
                btnlockclick();
            });
        }

        function wxPay(appId, timeStamp, nonceStr, packages, signType, paySign, callback, errCallBack) {
            wx.chooseWXPay({
                timestamp: timeStamp, // 支付签名时间戳，注意微信jssdk中的所有使用timestamp字段均为小写。但最新版的支付后台生成签名使用的timeStamp字段名需大写其中的S字符
                nonceStr: nonceStr, // 支付签名随机串，不长于 32 位
                package: packages, // 统一支付接口返回的prepay_id参数值，提交格式如：prepay_id=***）
                signType: signType, // 签名方式，默认为'SHA1'，使用新版支付需传入'MD5'
                paySign: paySign, // 支付签名
                success: function (res) {
                    // 支付成功后的回调函数
                    callback(res);
                },
                error: function (res) {
                    // 支付失败后的回调函数，如果有的话
                    errCallBack(res);
                }
            });
        }
    </script>
    <input type="button" id="btnlock" style="display: none;" />
    <script type="text/javascript">
        ///全局设置ajax同步
        $.ajaxSetup({
            async: false
        });

        function btnlockclick() {
            if ($("#btnlock").hasClass("locked")) {
                return;
            }
            var payableflag = "<%=payableflag%>";
            if (payableflag.toString() != "1") {
                return;
            }
            //微支付逻辑
            var totalfee = parseInt("<%=TotalFee %>");
            var outtradeno = "<%=OutTradeNo %>" + (parseInt(Math.random() * 10000).toString().PadLeft(4, '0')).toString();
            //alert(outtradeno);
            //alert(totalfee);
            if (parseFloat(totalfee) > 0) {
                $("#btnlock").addClass("locked");
                $("#div_loading").css("display", "none");
                //调用JSAPI
                $.get("/tools/WxPay.ashx", {
                    body: "<%=PayBody%>",
                    total_fee: "" + totalfee.toString() + "", //单位为 分
                    out_trade_no: outtradeno,
                    trade_type: "JSAPI",
                    msgid: "<%=this.open_id %>",
                    action: "jspayparam",
                    version:"<%=Version%>"
                }, function (data) {
                    if (data.status == 0) {
                        wxPay(data.jsEntities.appId, data.jsEntities.timeStamp, data.jsEntities.nonceStr, data.jsEntities.package, data.jsEntities.signType, data.jsEntities.paySign, function () {
                            window.opener = null;
                            //window.open("/api/payment/wepay/index.aspx?re=" + outtradeno, '_self');
                            window.open("<%=ReturnUrl%>?re=" + outtradeno, '_self');//v1.0:直接返回第三方同步回调地址;v2.0:返回本系统
                            window.close();
                        }, function (e) {
                            //取消支付
                            alert(e);
                        });
                        }
                        else {
                            alert(data.error);
                            return;
                        }
                    }, "json");
            }
            else {
                //window.location.href = "/api/payment/wpay/index.aspx?re=" + outtradeno;//返回本系统回调地址
                window.location.href = "<%=ReturnUrl%>?re=" + outtradeno;//v1.0:直接返回第三方同步回调地址;v2.0:返回本系统
                return;
            }
        }

        //当支付金额为0，且未在微信浏览器环境中会执行此操作
        $(function () {
            var totalfee = parseInt("<%=TotalFee %>");
            if (parseFloat(totalfee) == 0) {
                //window.location.href = "/api/payment/wpay/index.aspx?re=" + outtradeno;
                window.location.href = "<%=ReturnUrl%>?re=" + outtradeno;//v1.0:直接返回第三方同步回调地址;v2.0:返回本系统
                return;
            }
            else {
                var st = setTimeout("btnlockclick()", 3000);
            }
        })
    </script>
</asp:Content>
