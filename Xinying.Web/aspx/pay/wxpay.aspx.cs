﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Xinying.Common;
using System.Xml.Linq;
using wxHelper;
using Xinying.Model;
using LitJson;
using System.IO;
using System.Text;

namespace Xinying.Web.aspx.pay
{
    public partial class wxpay : Web.UI.BasePage
    {
        #region 微信JSSDK配置
        protected string appid = Xinying.API.Payment.WxPay.WxPayConfig.AppID;
        protected string timeStamp = WxPayHelper.Utils.ConvertDateTimeInt(DateTime.Now).ToString();
        protected string nonceStr = WxPayHelper.Utils.GetRandom();
        protected string signature = "";
        protected string tradeno = "";
        #region 微信用户信息
        protected string open_id = string.Empty;
        #endregion
        #endregion
        #region 登陆会员的信息
        protected Web.UI.BasePage bp = new UI.BasePage();
        protected Model.wxuserinfo model_wxuser = null;
        #endregion
        protected int payableflag = 0;//0:无需微信支付 1：需要微信支付
        protected string strUserAgent = "";
        protected Model.wepayorder WePayOrderModel = null;
        protected int TotalFee = 0;
        protected string OutTradeNo = "";
        protected string PayBody = "";
        protected string ReturnUrl = "/api/payment/wechatpay/error.aspx?status=4";
        protected string Version = "2.0";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                loaddata();
            }
        }

        protected void loaddata()
        {
            #region 获取相关参数，验证是否是合法的第三方api账户，以及传输的数据是否有被修改
            string AppKey = XYRequest.GetString("appkey");
            OutTradeNo = XYRequest.GetString("out_trade_no");
            string Sign = XYRequest.GetString("sign");
            #region 验证是否是合法的第三方api账户
            Model.apiuser ApiUserModel = Model.apiuser.GetModelByAppKey(AppKey);
            if (ApiUserModel == null)
            {
                Response.Redirect(string.Format("http://{0}/api/payment/wechatpay/error.aspx?status=1", XYRequest.GetCurrentFullHost(), OutTradeNo));
            }
            else
            {
                #region 校验数据签名
                var reqdata = Request.Url.Query;
                var arr = reqdata.TrimStart('?').Split("&".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                string data;
                if (Sign == Utils.WxPaySign(arr, ApiUserModel.appsecret, out data))
                {
                    //Response.Write("wxpay验签成功");
                    #region 订单相关信息
                    PayBody = XYRequest.GetString("paybody");
                    string CallbackUrl = XYRequest.GetString("callback_url");
                    string PayMethod = XYRequest.GetString("paymethod");
                    string NotifyUrl = XYRequest.GetString("notify_url");
                    string TimeStamp = XYRequest.GetString("timestamp");
                    TotalFee = XYRequest.GetInt("total_fee", 0);
                    Version = XYRequest.GetString("version");
                    #region 校验数据合法性，合法的数据才发起微信支付
                    if (PayMethod != "weixin.pay")
                    {
                        Response.Redirect(string.Format("http://{0}/api/payment/wechatpay/error.aspx?status=2", XYRequest.GetCurrentFullHost(), OutTradeNo));
                    }
                    if (string.IsNullOrEmpty(OutTradeNo))
                    {
                        Response.Redirect(string.Format("http://{0}/api/payment/wechatpay/error.aspx?status=3", XYRequest.GetCurrentFullHost(), OutTradeNo));
                    }
                    else
                    {
                        if (OutTradeNo.Length != 10)
                        {
                            Response.Redirect(string.Format("http://{0}/api/payment/wechatpay/error.aspx?status=9", XYRequest.GetCurrentFullHost(), OutTradeNo));
                        }
                    }
                    if (string.IsNullOrEmpty(PayBody))
                    {
                        PayBody = "商品";
                    }
                    if (string.IsNullOrEmpty(CallbackUrl))
                    {
                        Response.Redirect(string.Format("http://{0}/api/payment/wechatpay/error.aspx?status=4", XYRequest.GetCurrentFullHost(), OutTradeNo));
                    }
                    if (string.IsNullOrEmpty(NotifyUrl))
                    {
                        Response.Redirect(string.Format("http://{0}/api/payment/wechatpay/error.aspx?status=5", XYRequest.GetCurrentFullHost(), OutTradeNo));
                    }
                    if (TotalFee <= 0)
                    {
                        Response.Redirect(string.Format("http://{0}/api/payment/wechatpay/error.aspx?status=6", XYRequest.GetCurrentFullHost(), OutTradeNo));
                    }
                    if (Common.XYKeys.STRING_WXPAY_VERSIONS.Contains("|" + Version + "|"))
                    {
                        ReturnUrl = CallbackUrl;
                    }
                    else
                    {
                        Response.Redirect(string.Format("http://{0}/api/payment/wechatpay/error.aspx?status=7", XYRequest.GetCurrentFullHost(), OutTradeNo));
                    }
                    #endregion
                    WePayOrderModel = new wepayorder();
                    WePayOrderModel.addtime = DateTime.Now;
                    WePayOrderModel.apiuser_id = ApiUserModel.id;
                    WePayOrderModel.apiuser_name = ApiUserModel.apiusername;
                    WePayOrderModel.orderno = OutTradeNo;
                    WePayOrderModel.paymentid = 2;
                    WePayOrderModel.realamount = TotalFee;
                    WePayOrderModel.sys_orderno = OutTradeNo + Utils.GetRamCode();
                    OutTradeNo = WePayOrderModel.sys_orderno;//将系统订单号
                    WePayOrderModel.tradeno = "";
                    WePayOrderModel.apiuser_notifyurl = NotifyUrl;
                    WePayOrderModel.apiuser_returnurl = CallbackUrl;
                    WePayOrderModel.apiuser_global_notifyurl = NotifyUrl + "?" + data + "&sign=" + Sign;
                    #region 后期与第三方进行分润统计时需要的扩展字段
                    WePayOrderModel.apiuser_resstatus = 0;
                    WePayOrderModel.apiuser_lastnoticetime = DateTime.Parse("1900-01-01");
                    WePayOrderModel.apiuser_noticecount = 0;
                    WePayOrderModel.paymentfee = 0;
                    WePayOrderModel.paymentstatus = 1;
                    WePayOrderModel.paymenttime = DateTime.Parse("1900-01-01");
                    #endregion
                    WePayOrderModel.wx_version = Version;
                    if (Model.wepayorder.Add(WePayOrderModel) > 0)
                    {
                        payableflag = 1;
                        WePayOrderModel = Model.wepayorder.GetModelBySN(OutTradeNo);
                    }
                    else
                    {
                        Response.Redirect(string.Format("http://{0}/api/payment/wechatpay/error.aspx?status=10", XYRequest.GetCurrentFullHost(), OutTradeNo));
                    }
                    #endregion

                    #region 获取微信用户信息
                    JudgePCorMobile(out strUserAgent);
                    if (strUserAgent.IndexOf("micromessenger") > -1)
                    {
                        //判断微信Openid是否获取
                        model_wxuser = bp.GetWxUserInfo();
                        if (model_wxuser != null)
                        {
                            open_id = model_wxuser.openid;
                        }
                        else
                        {
                            #region 记录order_no
                            Utils.WriteCookie("WePayOrderno", OutTradeNo);
                            #endregion
                        }
                    }
                    #endregion

                    #region 微信js分享
                    initWXShareJs();
                    #endregion
                }
                else
                {
                    Response.Redirect(string.Format("http://{0}/api/payment/wechatpay/error.aspx?status=1", XYRequest.GetCurrentFullHost(), OutTradeNo));
                }
                #endregion
            }
            #endregion
            #endregion
        }

        protected void initWXShareJs()
        {
            string uri = Request.Url.ToString();
            #region 判断access_token是否过期，如果过期，重新获取。
            Model.siteconfig config = Model.siteconfig.loadConfig();
            XElement root;
            string access_token;
            string path = Request.MapPath("/xmlconfig/access_token.xml");//token的地址
            try
            {
                root = XElement.Load(path);//加载xml文件
                if (Utils.StrToDateTime(root.Element("expirestime").Value) < DateTime.Now)//从xml文件中获取接点expirestime的值，判断是否过期
                {
                    //Utils.WriteTxt("重新获取");
                    string token = WeiXinRequest.GetAccess_token(config.wxappid, config.wxappsecret);//获取access_Token
                    access_token = token;
                    root.Element("token").SetValue(token);
                    root.Element("expirestime").SetValue(DateTime.Now.AddSeconds(7200));
                    root.Save(path);
                }
                else
                {
                    //Utils.WriteTxt("BU重新获取");
                    access_token = root.Element("token").Value;
                }
            }
            catch (Exception)//文件不存在，则新建一个xml文件
            {
                access_token = WeiXinRequest.GetAccess_token(config.wxappid, config.wxappsecret);
                root = new XElement("wx",
                    new XElement("token", access_token),
                    new XElement("expirestime", DateTime.Now.AddSeconds(7200)));
                root.Save(path);
            }
            #endregion
            #region 判断jsapiticket是否过期，如果过期，重新获取。
            XElement rootjs;
            string jsapiticket = "";
            string pathjs = Request.MapPath("/xmlconfig/jsapiticket.xml");//jsapiticket的地址
            try
            {
                rootjs = XElement.Load(pathjs);
                //Utils.WriteTxt(Utils.StrToDateTime(rootjs.Element("expirestime").Value).ToString());
                if (Utils.StrToDateTime(rootjs.Element("expirestime").Value) < DateTime.Now)//从xml文件中获取接点expirestime的值，判断是否过期
                {
                    //Utils.WriteTxt("重新获取");
                    jsapiticket = wxHelper.WeiXinRequest.GetJsapi_ticket(access_token);
                    rootjs.Element("ticket").SetValue(jsapiticket);
                    rootjs.Element("expirestime").SetValue(DateTime.Now.AddSeconds(7200));
                    rootjs.Save(pathjs);
                }
                else
                {
                    //Utils.WriteTxt("BU重新获取");
                    jsapiticket = rootjs.Element("ticket").Value;
                }
            }
            catch (Exception)//文件不存在，则新建一个xml文件
            {
                //Utils.WriteTxt("重新创建");
                jsapiticket = wxHelper.WeiXinRequest.GetJsapi_ticket(access_token);
                rootjs = new XElement("wx",
                    new XElement("ticket", jsapiticket),
                    new XElement("expirestime", DateTime.Now.AddSeconds(7200)));
                rootjs.Save(pathjs);
            }
            #endregion
            signature = WxPayHelper.Utils.GetJSSDKSign(uri, timeStamp, nonceStr, jsapiticket);
            //Utils.WriteTxt("signature:" + signature);
        }

        public class ReturnData
        {
            /// <summary>
            /// 校验状态 0：校验成功 1：校验异常 2：支付系统异常
            /// </summary>
            public int verify_status { get; set; }
            /// <summary>
            /// 校验详情 校验成功则返回“SUCCESS”,异常则显示详细原因
            /// </summary>
            public string verify_msg { get; set; }
            /// <summary>
            /// 支付url，如果数据验证失败，返回失败页面（其实都是/api/payment/wechatpay/index.aspx加上不同参数）展示相关信息
            /// </summary>
            public string pay_url { get; set; }
        }
    }
}