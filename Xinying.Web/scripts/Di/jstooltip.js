﻿// JavaScript Document
//copyright c by cwf 2012-11
//chenweifan
(function ($) {
    $.fn.mytooltip = function (msg) {
        $(this).each(function () {
            $(this).mouseover(function (e) {
                if (/.png$|.gif$|.jpg$|.bmp$|.PNG$|.GIF|.JPG|.BMP/.test($(this).val())) {
                    $("body").append("<div id='mytooltip' ><div></div><img src=" + $(this).val() + "></img></div>");
                } else {
                    if ($(this).val().match("http://wx.qlogo.cn/") == "http://wx.qlogo.cn/") {//微信头像
                        $("body").append("<div id='mytooltip' ><div></div><img src=" + $(this).val() + " width=\"300\"></img></div>");
                    }
                    else {
                        $("body").append("<div id='mytooltip' ><div></div>" + msg + "</div>");
                    }
                }

                $("#mytooltip").css({
                    position: "absolute",
                    border: "1px solid #ccc",
                    background: "#FCFBE5",
                    padding: "5px",
                    color: "#666",
                    zIndex: 1000

                });

                $("#mytooltip").css("left", (e.pageX + 20) + "px");
                $("#mytooltip").css("top", (e.pageY - 10) + "px");

            });
            $(this).mouseout(function () {
                $("#mytooltip").remove();
            });
            $(this).mousemove(function (e) {
                $("#mytooltip").css("left", (e.pageX + 20) + "px");
                $("#mytooltip").css("top", (e.pageY - 10) + "px");

            });
            $(this).keydown(function () {
                $("#mytooltip").remove();
            });
        });
    };
})(jQuery);