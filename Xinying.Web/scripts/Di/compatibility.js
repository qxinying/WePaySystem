﻿function trim(str) { //删除左右两端的空格
    return str.replace(/(^\s*)|(\s*$)/g, "");
}
function ltrim(str) { //删除左边的空格
    return str.replace(/(^\s*)/g, "");
}
function rtrim(str) { //删除右边的空格
    return str.replace(/(\s*$)/g, "");
}

//方法一扩展（C#中PadLeft、PadRight）
String.prototype.PadLeft = function (len, charStr) {
    var s = this + '';
    return new Array(len - s.length + 1).join(charStr, '') + s;
}
String.prototype.PadRight = function (len, charStr) {
    var s = this + '';
    return s + new Array(len - s.length + 1).join(charStr, '');
}
//replaceAll
String.prototype.replaceAll = function (f, e) {//把f替换成e
    var reg = new RegExp(f, "g"); //创建正则RegExp对象   
    return this.replace(reg, e);
}
