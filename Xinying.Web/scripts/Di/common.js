﻿function IsEmail(temp) {
    //对电子邮件的验证
    var myreg = /^([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+\.[a-zA-Z]{2,3}$/;
    if (!myreg.test(temp)) {
        return false;
    }
    return true;
}

//手机号码验证信息
function isMobil(s) {
    var myreg = /(^1[3|4|5|6|7|8|9][0-9]{9}$)/;
    if (!myreg.test(s)) {
        return false;
    }
    return true;
}

//电话号码验证信息
function isTel(s) {
    var myreg = /(^(\d{3,4}-)?\d{6,8}$)/;
    if (!myreg.test(s)) {
        return false;
    }
    return true;
}

//省份
function setprovince(p, v, c, vc, a, va) {
    $("[id$='" + p + "']").change(function () {
        var pid = $("[id$='" + p + "']").val();
        $.get("/tools/front.ashx", { pid: pid, action: "city" }, function (result) {
            $("[id$='" + c + "']").html(result);
            if (pid == "0") {
                $("[id$='" + c + "']").html("<option value='0'>请选择</option>");
            }
            setcity(c, vc, a, va);
        });
    });

    $.get("/tools/front.ashx", { action: "province" }, function (result) {
        $("[id$='" + p + "']").html(result);
        if (v != "") {
            $("[id$='" + p + "']").val(v);
            $("[id$='" + p + "']").trigger("change");
        }
    });
}

//城市
function setcity(c, v, a, va) {
    $("[id$='" + c + "']").change(function () {
        var cid = $("[id$='" + c + "']").val();
        $.get("/tools/front.ashx", { cid: cid, action: "area" }, function (result) {
            $("[id$='" + a + "']").html(result);
            setarea(a, va);
        });
    });

    if (v != "") {
        $("[id$='" + c + "']").val(v);
        $("[id$='" + c + "']").trigger("change");
    }
}

//地区
function setarea(a, v) {
    if (v != "") {
        $("[id$='" + a + "']").val(v)
    }
}

function trim(str) { //删除左右两端的空格
    return str.replace(/(^\s*)|(\s*$)/g, "");
}
function ltrim(str) { //删除左边的空格
    return str.replace(/(^\s*)/g, "");
}
function rtrim(str) { //删除右边的空格
    return str.replace(/(\s*$)/g, "");
}