﻿//*小贴士 弹窗
//*作者：千寻互动 - 技术陶
//*创建时间：2014年10月09日
//*版本：1.0.1

//=============================和 TipsDialog.css 文件配套======================================
//parentObj : 添加html代码的父元素
//Obj ：小贴士绑定元素
//txt ：小贴士内容
//width ：弹窗宽度
function tips_dialog(parentObj, Obj, txt, width) {
    var html = '<div class="tips_dg" style="width:' + width + 'px">'
    + '<em class="tp_em"></em><span class="tp_s"></span>'
    + '<div class="tips_dg_con">'
    + txt
    + '</div></div>';

    parentObj.append(html);
    var dg = parentObj.find(".tips_dg:last");

    var dheight = dg.height();
    var dleft = Obj.offset().left;
    var dtop = Obj.offset().top;
    var wheight = Obj.height();
    var wwidth = Obj.width();

    dg.css("left", dleft - width / 2 + wwidth / 2).css("top", dtop - dheight - wheight / 2 - 5);

    Obj.mouseover(function () {
        dg.show();
    });
    Obj.mouseleave(function () {
        dg.hide();
    });
}

function tips_dialog_quick(Obj, txt, width) {
    var html = '<div class="tips_dg" style="width:' + width + 'px">'
    + '<em class="tp_em"></em><span class="tp_s"></span>'
    + '<div class="tips_dg_con">'
    + txt
    + '</div></div>';

    $("body").append(html);
    var dg = $(".tips_dg:last");

    var dheight = dg.height();
    var dleft = $(Obj).offset().left;
    var dtop = $(Obj).offset().top;
    var wheight = $(Obj).height();
    var wwidth = $(Obj).width();

    dg.css("left", dleft - width / 2 + wwidth / 2).css("top", dtop - dheight - wheight / 2);
    dg.show();
    $(Obj).mouseleave(function () {
        dg.remove();
    });
}