(function () {
    //    var input = document.getElementById('upload-avatar');

    //    input.onchange = function () {
    //        $("#upload-avatar").parent().next().find("span").css("color", "#f39800").html("上传中...");

    //        // 也可以传入图片路径：lrz('../demo.jpg', ...
    //        lrz(this.files[0], {
    //            before: function () {
    //                console.log('压缩开始');
    //            },
    //            fail: function (err) {
    //                console.error(err);
    //            },
    //            always: function () {
    //                console.log('压缩结束');
    //            },
    //            done: function (results) {
    //                // 你需要的数据都在这里，可以以字符串的形式传送base64给服务端转存为图片。
    //                console.log(results);

    //                setTimeout(function () {
    //                    // 发送到后端
    //                    var xhr = new XMLHttpRequest();
    //                    var data = {
    //                        FileName: results.origin.name,
    //                        FileSize: results.origin.size,
    //                        Filedata: results.base64
    //                    };

    //                    xhr.open('POST', '/tools/upload_ajax.ashx?action=UpLoadMobileFile', true);
    //                    xhr.setRequestHeader('Content-Type', 'application/json; charset=utf-8');
    //                    xhr.onreadystatechange = function () {
    //                        if (xhr.readyState === 4 && xhr.status === 200) {
    //                            var result = JSON.parse(xhr.response);

    //                            if (result.status == 0) {
    //                                alert(result.msg);
    //                            }
    //                            else {
    //                                $("#upload-avatar").parent().next().find("span").css("color", "#888888").html("上传头像");
    //                                $("#upload-avatar").prev().find("img").attr("src", result.path);
    //                                $("#upload-avatar").next().val(result.path);
    //                            }
    //                        }
    //                    };

    //                    xhr.send(JSON.stringify(data)); // 发送base64
    //                }, 100);
    //            }
    //        });
    //    };

    var input = document.getElementById('upload-photo');
    if (input != undefined) {
        input.onchange = function () {
            // 也可以传入图片路径：lrz('../demo.jpg', ...
            lrz(this.files[0], {
                before: function () {
                    console.log('压缩开始');
                },
                fail: function (err) {
                    console.error(err);
                },
                always: function () {
                    console.log('压缩结束');
                },
                done: function (results) {
                    // 你需要的数据都在这里，可以以字符串的形式传送base64给服务端转存为图片。
                    console.log(results);

                    setTimeout(function () {
                        // 发送到后端
                        var xhr = new XMLHttpRequest();
                        var data = {
                            FileName: results.origin.name,
                            FileSize: results.origin.size,
                            Filedata: results.base64
                        };

                        xhr.open('POST', '/tools/upload_ajax.ashx?action=UpLoadMobilePhotoFile', true);
                        xhr.setRequestHeader('Content-Type', 'application/json; charset=utf-8');
                        xhr.onreadystatechange = function () {
                            if (xhr.readyState === 4 && xhr.status === 200) {
                                var result = JSON.parse(xhr.response);

                                if (result.status == 0) {
                                    JsAlert(result.msg);
                                }
                                else {
                                    $("#upload-photo").prev().find("#upload-box").before('<li><div class="box" name="hid_photo_name"><img src="' + result.path + '" width="100%"></div></li>');
                                    var imgObj = $("#upload-photo").prev().find("[name='hid_photo_name']");
                                    if (imgObj != undefined && imgObj.length > 2) {
                                        $("#upload-photo").prev().find("#upload-box").hide();
                                    }
                                }
                            }
                        };

                        xhr.send(JSON.stringify(data)); // 发送base64
                    }, 100);
                }
            });
        };
    }

    var input1 = document.getElementById('upload-avatar');
    if (input1 != undefined) {
        input1.onchange = function () {
            // 也可以传入图片路径：lrz('../demo.jpg', ...
            lrz(this.files[0], {
                before: function () {
                    console.log('压缩开始');
                },
                fail: function (err) {
                    console.error(err);
                },
                always: function () {
                    console.log('压缩结束');
                },
                done: function (results) {
                    // 你需要的数据都在这里，可以以字符串的形式传送base64给服务端转存为图片。
                    console.log(results);

                    setTimeout(function () {
                        // 发送到后端
                        var xhr = new XMLHttpRequest();
                        var data = {
                            FileName: results.origin.name,
                            FileSize: results.origin.size,
                            Filedata: results.base64
                        };

                        xhr.open('POST', '/tools/upload_ajax.ashx?action=UpLoadMobilePhotoFile', true);
                        xhr.setRequestHeader('Content-Type', 'application/json; charset=utf-8');
                        xhr.onreadystatechange = function () {
                            if (xhr.readyState === 4 && xhr.status === 200) {
                                var result = JSON.parse(xhr.response);

                                if (result.status == 0) {
                                    JsAlert(result.msg);
                                }
                                else {
                                    $("#upload-img").attr("src", result.path).show();
                                    $("#upload-box").css("height", "100%");
                                    JsWaiting("请等待...");
                                    window.location.href = "/aspx/mobile/usercenter_avatarcrop.aspx?img=" + result.path;
                                }
                            }
                        };

                        xhr.send(JSON.stringify(data)); // 发送base64
                    }, 100);
                }
            });
        };
    }

    var input2 = document.getElementById('upload-album');
    if (input2 != undefined) {
        input2.onchange = function () {
            var obj = this;

            // 也可以传入图片路径：lrz('../demo.jpg', ...
            lrz(this.files[0], {
                before: function () {
                    console.log('压缩开始');
                },
                fail: function (err) {
                    console.error(err);
                },
                always: function () {
                    console.log('压缩结束');
                },
                done: function (results) {
                    // 你需要的数据都在这里，可以以字符串的形式传送base64给服务端转存为图片。
                    console.log(results);

                    setTimeout(function () {
                        // 发送到后端
                        var xhr = new XMLHttpRequest();
                        var data = {
                            FileName: results.origin.name,
                            FileSize: results.origin.size,
                            Filedata: results.base64
                        };

                        xhr.open('POST', '/tools/upload_ajax.ashx?action=UpLoadMobilePhotoFile', true);
                        xhr.setRequestHeader('Content-Type', 'application/json; charset=utf-8');
                        xhr.onreadystatechange = function () {
                            if (xhr.readyState === 4 && xhr.status === 200) {
                                var result = JSON.parse(xhr.response);

                                if (result.status == 0) {
                                    JsAlert(result.msg);
                                }
                                else {
                                    var ind = $(obj).attr("ind");
                                    var albumObj = $(".album");
                                    $(albumObj[ind]).prop("src", result.path);
                                }
                            }
                        };

                        xhr.send(JSON.stringify(data)); // 发送base64
                    }, 100);
                }
            });
        };
    }
    /**
    * 演示报告
    * @param title
    * @param src
    * @param size
    */
    function demo_report(title, src, size) {
        var img = new Image(),
            li = document.createElement('li'),
            size = (size / 1024).toFixed(2) + 'KB';

        if (size === 'NaNKB') size = '';

        img.onload = function () {
            var content = '<ul>' +
                '<li>' + title + '（' + img.width + ' X ' + img.height + '）</li>' +
                '<li class="text-cyan">' + size + '</li>' +
                '</ul>';

            li.className = 'item';
            li.innerHTML = content;
            li.appendChild(img);
            document.querySelector('#report').appendChild(li);
        };

        img.src = typeof src === 'string' ? src : URL.createObjectURL(src);
    }
})();