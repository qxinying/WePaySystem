﻿//*E话宝系统手机站
//*作者：千寻互动 - 技术陶
//*创建时间：2015年10月12日
//*最初版本：1.0
//*配合JQ库

//========================提示弹窗========================
function JsAlert(c, url, t) {
    var ObjNum = arguments.length;
    if (ObjNum < 2) {
        url = "";
    }
    else if (ObjNum < 3) {
        t = "提示信息";
    }

    alertdialog(t, c, function () {
        if (url != "") {
            switch (url) {
                case "back":
                    history.go(-1);
                    break;
                case "refresh":
                    window.location.href = window.location.href;
                    break;
                default:
                    window.location.href = url;
                    break;
            }
        }
    });
}
function JsAlertAndCallBack(c, call_back, t) {
    var ObjNum = arguments.length;
    if (ObjNum < 3) {
        t = "提示信息";
    }

    alertdialog("提示信息", c, call_back);
}

//生成弹窗
function alertdialog(t, c, fn) {
    var alertHtml = '<div class="alert-float"></div>'
    + '<div class="alert-dialog">'
    + '<div class="alert-dialog-title">' + t + '</div>'
    + '<div class="alert-dialog-con">' + c + '</div>'
    + '<div class="alert-dialog-handdle"><button class="alert-dialog-btn" type="button">确定</button></div></div>';

    $("body").append(alertHtml);

    var allwid = $(window).width();
    var addleft = (allwid - 280) / 2;
    $(".alert-dialog").css({
        "left": addleft,
        "background": "#fff",
        "width": "280px",
        "position": "fixed",
        "top": "30%",
        "border-radius": "5px",
        "text-align": "center",
        "font-family": "微软雅黑",
        "padding": "15px 0 0",
        "z-index": "1000"
    });
    $(".alert-float").css({
        "background": "url(/aspx/mobile/images/bbg01.png) repeat",
        "position": "fixed",
        "left": "0",
        "top": "0",
        "width": "100%",
        "height": "100%"
    });
    $(".alert-dialog-title").css({
        "font-size": "16px",
        "font-weight": "bolder",
        "padding": "20px 0 0"
    });
    $(".alert-dialog-con").css({
        "color": "#666"
    });
    $(".alert-dialog-handdle").css({
        "height": "40px",
        "border-top": "1px #dfdfdf solid",
        "margin-top": "10px"
    });
    $(".alert-dialog-btn").css({
        "padding": "0",
        "background": "none",
        "width": "50%",
        "height": "40px",
        "color": "#00b8f1",
        "font-family": "微软雅黑",
        "font-size": "14px"
    });
    $(".alert-dialog-handdle button").bind("click", function () {
        _alertdialog($(this).parent().parent());
        return fn && fn.call(this);
    });
    $(".alert-float").bind("click", function () {
        _confirmdialog($(this).next());
    });
}
//消灭弹窗
function _alertdialog(obj) {
    $(obj).prev().remove();
    $(obj).remove();
}

//========================确认弹窗========================
function JsConfirm(t, c, yesval, noval, yes, no) {
    var ObjNum = arguments.length;
    if (ObjNum < 1 || t == "") {
        t = "确认信息";
    }
    if (ObjNum < 2 || c == "") {
        c = "确定要继续操作吗？";
    }
    if (ObjNum < 3 || yesval == "") {
        yesval = "确定";
    }
    if (ObjNum < 4 || noval == "") {
        noval = "取消";
    }

    confirmdialog(t, c, yesval, noval, yes, no);
}

//生成弹窗
function confirmdialog(t, c, yesval, noval, yes, no) {
    var alertHtml = '<div class="alert-float"></div>'
    + '<div class="alert-dialog">'
    + '<div class="alert-dialog-title">' + t + '</div>'
    + '<div class="alert-dialog-con">' + c + '</div>'
    + '<div class="alert-dialog-handdle"><button class="alert-dialog-btn yes" type="button">' + yesval + '</button>'
    + '<button class="alert-dialog-btn no" type="button">' + noval + '</button></div></div>';

    $("body").append(alertHtml);

    var allwid = $(window).width();
    var addleft = (allwid - 280) / 2;
    $(".alert-dialog").css({
        "left": addleft,
        "background": "#fff",
        "width": "280px",
        "position": "fixed",
        "top": "30%",
        "border-radius": "5px",
        "text-align": "center",
        "font-family": "微软雅黑",
        "padding": "15px 0 0",
        "z-index": "1000"
    });
    $(".alert-float").css({
        "background": "url(/aspx/mobile/images/bbg01.png) repeat",
        "position": "fixed",
        "left": "0",
        "top": "0",
        "width": "100%",
        "height": "100%"
    });
    $(".alert-dialog-title").css({
        "font-size": "16px",
        "font-weight": "bolder",
        "padding": "20px 0 0"
    });
    $(".alert-dialog-con").css({
        "color": "#666"
    });
    $(".alert-dialog-handdle").css({
        "height": "40px",
        "border-top": "1px #dfdfdf solid",
        "margin-top": "10px"
    });
    $(".alert-dialog-btn").css({
        "padding": "0",
        "background": "none",
        "width": "50%",
        "height": "40px",
        "color": "#00b8f1",
        "font-family": "微软雅黑",
        "font-size": "14px"
    });
    $(".no").css("border-right", "1px #dfdfdf solid");
    $(".alert-dialog-handdle .yes").bind("click", function () {
        _confirmdialog($(this).parent().parent());
        return yes && yes.call(this);
    });
    $(".alert-dialog-handdle .no").bind("click", function () {
        _confirmdialog($(this).parent().parent());
        return no && no.call(this);
    });
    $(".alert-float").bind("click", function () {
        _confirmdialog($(this).next());
    });
}
//消灭弹窗
function _confirmdialog(obj) {
    $(obj).prev().remove();
    $(obj).remove();
}

//========================等待弹窗（手动添加代码消除）========================
function JsWaiting(t) {
    waitingdialog(t);
}
function JsWaitingOver() {
    _waitingdialog(".alert-dialog");
}

//生成弹窗
function waitingdialog(t) {
    var alertHtml = '<div class="alert-float"></div>'
    + '<div class="alert-dialog">'
    + '<div class="alert-dialog-title">' + t + '</div></div>';

    $("body").append(alertHtml);

    var allwid = $(window).width();
    var addleft = (allwid - 280) / 2;
    $(".alert-dialog").css({
        "left": addleft,
        "background": "#fff",
        "width": "280px",
        "position": "fixed",
        "top": "30%",
        "border-radius": "5px",
        "text-align": "center",
        "font-family": "微软雅黑",
        "z-index": "1000"
    });
    $(".alert-float").css({
        "background": "url(/aspx/mobile/images/bbg01.png) repeat",
        "position": "fixed",
        "left": "0",
        "top": "0",
        "width": "100%",
        "height": "100%"
    });
    $(".alert-dialog-title").css({
        "font-size": "16px",
        "font-weight": "bolder",
        "padding": "20px 0"
    });
}
//消灭弹窗
function _waitingdialog(obj) {
    $(obj).prev().remove();
    $(obj).remove();
}

//========================小提示弹窗（无需点击确定，自动消失）========================
function JsTip(t, sec, url) {
    var ObjNum = arguments.length;
    if (ObjNum < 2) {
        sec = 1;
    }
    if (ObjNum < 3) {
        url = "";
    }

    tipdialog(t);
    setTimeout(function () {
        if (url != "") {
            switch (url) {
                case "back":
                    history.go(-1);
                    break;
                default:
                    window.location.href = url;
                    break;
            }
        }
        _tipdialog('.tip-dialog');
    }, sec * 1000);
}

function JsTipAndCallBack(t, sec, fn) {
    var ObjNum = arguments.length;
    if (ObjNum < 2) {
        sec = 1;
    }

    tipdialog(t);
    setTimeout(function () {
        _tipdialog('.tip-dialog');
        return fn && fn.call(this);
    }, sec * 1000);
}

//生成弹窗
function tipdialog(t) {
    var tipHtml = '<div class="tip-float"></div>'
    + '<div class="tip-dialog">'
    + '<div class="tip-dialog-title">' + t + '</div></div>';

    $("body").append(tipHtml);

    var allwid = $(window).width();
    var addleft = (allwid - 280) / 2;
    $(".tip-dialog").css({
        "left": addleft,
        "background": "#fff",
        "width": "280px",
        "position": "fixed",
        "top": "30%",
        "border-radius": "5px",
        "text-align": "center",
        "font-family": "微软雅黑",
        "padding": "15px 0",
        "z-index": "1000"
    });
    $(".tip-float").css({
        "background": "url(/aspx/mobile/images/bbg01.png) repeat",
        "position": "fixed",
        "left": "0",
        "top": "0",
        "width": "100%",
        "height": "100%"
    });
    $(".tip-dialog-title").css({
        "font-size": "16px",
        "font-weight": "bolder",
        "padding": "20px 0"
    });
}
//消灭弹窗
function _tipdialog(obj) {
    $(obj).prev().remove();
    $(obj).remove();
}