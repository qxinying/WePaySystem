﻿document.write("<script type=\"text/javascript\" src=\"http://api.map.baidu.com/api?v=2.0&ak=CgS5DdyDqg2EKEEylrw0WxNn\"></script>");
//obj 控件ID，location 经纬度或者地址，zoom 缩放级别
function initmap(map, x, y, zoom, objx,objy,flag,overview) {
    // 百度地图API功能
    var point = new BMap.Point(x, y)
    map.centerAndZoom(point, zoom);
    map.enableScrollWheelZoom();                            //启用滚轮放大缩小
    if( overview == 1) {
        addOverviewMap(map);
    }
    var marker = new BMap.Marker(point);  // 创建标注
    map.addOverlay(marker);              // 将标注添加到地图中
    if (flag==1) {
        //marker.enableDragging();    //可拖拽
        marker.addEventListener("dragend", function (e) {
            $("#" + objx).val(e.point.lng);
            $("#" + objy).val(e.point.lat);
        })
    }
}
//点击获取经纬度
function getlocation(map, objx, objy) {
    map.addEventListener("click", function (e) {
        $("#" + objx).val(e.point.lng);
        $("#" + objy).val(e.point.lat);
        var gc = new BMap.Geocoder();
        gc.getLocation(e.point, function (rs) {
            var addComp = rs.addressComponents;
            $("#txtAddress").val(addComp.street + addComp.streetNumber);
        });
        
    });
}
//搜索地图
function KeywordsSearch(map, key) {
    var local = new BMap.LocalSearch(map, {
        renderOptions: { map: map },
    });
    local.search(key);
}
//添加缩略图
function addOverviewMap(map) {
    map.addControl(new BMap.OverviewMapControl());              //添加默认缩略地图控件
    map.addControl(new BMap.OverviewMapControl({ isOpen: true, anchor: BMAP_ANCHOR_TOP_RIGHT }));   //右上角，打开
}
//地址解析
function Geocoder(map,key) {
    // 创建地址解析器实例
    var myGeo = new BMap.Geocoder();
    // 将地址解析结果显示在地图上,并调整地图视野
    myGeo.getPoint(key, function (point) {
        if (point) {
            map.setCenter(point);
        }
    });
}
function AddNavi(map)
{
    map.addControl(new BMap.NavigationControl());  //添加默认缩放平移控件
}