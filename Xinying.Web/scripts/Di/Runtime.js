﻿//var nowtime;
//var count = 9;
//var count1 = 7;
//function DownCount() {
//    nowtime = Number(nowtime) + 1000; //当前时间 
//    setTimeout("DownCount()", 1000)
//}
//function lxfEndtime() {
//    $(".dtime").each(function () {
//        var ptime = new Date($(this).attr("ptime")).getTime(); //取结束日期(毫秒值)
//        var youtime = ptime - nowtime; //还有多久(毫秒值)
//        var seconds = youtime / 1000;
//        var minutes = Math.floor(seconds / 60);
//        var hours = Math.floor(minutes / 60);
//        var days = Math.floor(hours / 24);
//        var CDay = days;
//        var CHour = hours % 24;
//        var CMinute = minutes % 60;
//        var CSecond = Math.floor(seconds % 60); //"%"是取余运算，可以理解为60进一后取余数，然后只要余数。
//        if (ptime <= nowtime) {
//            window.location.href = "/index.aspx";
//            return;
//        } else {
//            $(this).html("倒计时：" + "0" + CMinute + ":" + (CSecond < 10 ? "0" + CSecond : CSecond) + ":<span class=\"dhm\">99</span>");              //输出数据
//        }
//    });
//    setTimeout("lxfEndtime()", 1000);
//};

//function GetJX() {
//    var psell = "";
//    var ssell = "";
//    var lli = $(".latestbd").find("li");
//    lli.each(function (i) {
//        var p = $(lli[i]).attr("pid");
//        var s = $(lli[i]).attr("sid");
//        psell += p + ",";
//        ssell += s + ",";
//    });
//    $.get("/tools/front.ashx", { pid: psell, sid: ssell, action: "GetJX" }, function (result) {
//        if (result != "" && result != "$") {
//            $(".latestbd").html(result);

//        }
//    });
//    setTimeout("GetJX()", 1000);
//};

//function hmpast() {
//    $(".dtime").each(function () {
//        var q = parseInt(count) - 1;
//        var q1 = parseInt(count1) - 1;
//        if (q == -1) {
//            q = 9;
//        }
//        if (q1 == -1) {
//            q1 = 9;
//        }
//        count = q;
//        count1 = q1;
//        $(this).find(".dhm").html(q + "" + q1);
//    });
//    setTimeout("hmpast()",100);
//}

//$(function () {
//    var _s_today = $("#servertime").attr("btime"); //获取服务器时间（字符串）  
//    nowtime = new Date(_s_today);
//    setTimeout("DownCount()", 1000)
//    lxfEndtime();
//    setTimeout("GetJX()", 1000);
//    setTimeout("hmpast()", 100);
//});

//凯子集成

//普通
function showTimeCommon(tuanid, time_distance) {
    this.tuanid = tuanid;
    //PHP时间是秒，JS时间是微秒 
    this.time_distance = time_distance * 1000;
}
showTimeCommon.prototype.setTimeShow = function (fn) {
    var timer = $(this.tuanid);
    var str_time;
    var int_day, int_hour, int_minute, int_second;
    time_distance = this.time_distance;
    this.time_distance = this.time_distance - 1000;
    if (time_distance > 0) {
        int_day = Math.floor(time_distance / 86400000);
        time_distance -= int_day * 86400000;
        int_hour = Math.floor(time_distance / 3600000);
        time_distance -= int_hour * 3600000;
        int_minute = Math.floor(time_distance / 60000);
        time_distance -= int_minute * 60000;
        int_second = Math.floor(time_distance / 1000);
        //if (int_hour < 10)
        //    int_hour = "0" + int_hour;
        //if (int_minute < 10)
        //    int_minute = "0" + int_minute;
        //if (int_second < 10)
        //    int_second = "0" + int_second;
        //str_time = "还剩" + int_day + "天" + int_hour + "时" + int_minute + "分" + int_second + "秒";
        timer.html(int_second);
        var self = this;
        setTimeout(function () { self.setTimeShow(fn); }, 1000); //D:正确 
    }
    else {
        //业务逻辑
        timer.html(0);
        fn();
        return;
    }
}
//团购
function showTime(tuanid, time_distance) {
    this.tuanid = tuanid;
    //PHP时间是秒，JS时间是微秒 
    this.time_distance = time_distance * 1000;
}
showTime.prototype.setTimeShow = function (fn) {
    var timer = $(this.tuanid);
    var str_time;
    var int_day, int_hour, int_minute, int_second;
    time_distance = this.time_distance;
    this.time_distance = this.time_distance - 1000;
    if (time_distance > 0) {
        int_day = Math.floor(time_distance / 86400000);
        time_distance -= int_day * 86400000;
        int_hour = Math.floor(time_distance / 3600000);
        time_distance -= int_hour * 3600000;
        int_minute = Math.floor(time_distance / 60000);
        time_distance -= int_minute * 60000;
        int_second = Math.floor(time_distance / 1000);
        if (int_hour < 10)
            int_hour = "0" + int_hour;
        if (int_minute < 10)
            int_minute = "0" + int_minute;
        if (int_second < 10)
            int_second = "0" + int_second;
        //str_time = "还剩" + int_day + "天" + int_hour + "时" + int_minute + "分" + int_second + "秒";
        timer.find("span[name='leftDay']").html(int_day);
        timer.find("span[name='leftHour']").html(int_hour);
        timer.find("span[name='leftMinute']").html(int_minute);
        timer.find("span[name='leftSecond']").html(int_second);
        var self = this;
        setTimeout(function () { self.setTimeShow(); }, 1000); //D:正确 
    }
    else {
        //业务逻辑
        return fn && fn.call(this);
    }
}
//手机注册验证码
function showTime_reg(tuanid, time_distance) {
    this.tuanid = tuanid;
    //PHP时间是秒，JS时间是微秒 
    this.time_distance = time_distance * 1000;
}
showTime_reg.prototype.setTimeShow = function () {
    var timer = $(this.tuanid);
    var str_time;
    var int_day, int_hour, int_minute, int_second;
    time_distance = this.time_distance;
    this.time_distance = this.time_distance - 1000;
    if (time_distance > 0) {
        int_day = Math.floor(time_distance / 86400000);
        time_distance -= int_day * 86400000;
        int_hour = Math.floor(time_distance / 3600000);
        time_distance -= int_hour * 3600000;
        int_minute = Math.floor(time_distance / 60000);
        time_distance -= int_minute * 60000;
        int_second = Math.floor(time_distance / 1000);
        if (int_hour < 10)
            int_hour = "0" + int_hour;
        if (int_minute < 10)
            int_minute = "0" + int_minute;
        //if (int_second < 10)
        //    int_second = "0" + int_second;
        //str_time = "还剩" + int_day + "天" + int_hour + "时" + int_minute + "分" + int_second + "秒";
        timer.val(int_second + "s后可重发");
        var self = this;
        setTimeout(function () { self.setTimeShow(); }, 1000); //D:正确 
    }
    else {
        //业务逻辑
        timer.val("获取验证码");
        $("#txtMobile").blur();
        return;
    }
}
//手机注册验证码
function showTime_reg_m(tuanid, time_distance) {
    this.tuanid = tuanid;
    //PHP时间是秒，JS时间是微秒 
    this.time_distance = time_distance * 1000;
}
showTime_reg_m.prototype.setTimeShow = function () {
    var timer = $(this.tuanid);
    var str_time;
    var int_day, int_hour, int_minute, int_second;
    time_distance = this.time_distance;
    this.time_distance = this.time_distance - 1000;
    if (time_distance > 0) {
        int_day = Math.floor(time_distance / 86400000);
        time_distance -= int_day * 86400000;
        int_hour = Math.floor(time_distance / 3600000);
        time_distance -= int_hour * 3600000;
        int_minute = Math.floor(time_distance / 60000);
        time_distance -= int_minute * 60000;
        int_second = Math.floor(time_distance / 1000);
        if (int_hour < 10)
            int_hour = "0" + int_hour;
        if (int_minute < 10)
            int_minute = "0" + int_minute;
        //if (int_second < 10)
        //    int_second = "0" + int_second;
        //str_time = "还剩" + int_day + "天" + int_hour + "时" + int_minute + "分" + int_second + "秒";
        timer.html(int_second + "s后可重发");
        var self = this;
        setTimeout(function () { self.setTimeShow(); }, 1000); //D:正确 
    }
    else {
        //业务逻辑
        timer.html("获取验证码");
        return;
    }
}
//注册邮箱激活验证码
function showTime_vemail(tuanid, time_distance) {
    this.tuanid = tuanid;
    //PHP时间是秒，JS时间是微秒 
    this.time_distance = time_distance * 1000;
}
showTime_vemail.prototype.setTimeShow = function () {
    var timer = $(this.tuanid);
    var str_time;
    var int_day, int_hour, int_minute, int_second;
    time_distance = this.time_distance;
    this.time_distance = this.time_distance - 1000;
    if (time_distance > 0) {
        int_day = Math.floor(time_distance / 86400000);
        time_distance -= int_day * 86400000;
        int_hour = Math.floor(time_distance / 3600000);
        time_distance -= int_hour * 3600000;
        int_minute = Math.floor(time_distance / 60000);
        time_distance -= int_minute * 60000;
        int_second = Math.floor(time_distance / 1000);
        if (int_hour < 10)
            int_hour = "0" + int_hour;
        if (int_minute < 10)
            int_minute = "0" + int_minute;
        //if (int_second < 10)
        //    int_second = "0" + int_second;
        //str_time = "还剩" + int_day + "天" + int_hour + "时" + int_minute + "分" + int_second + "秒";
        str_time = int_second;
        timer.html("(" + str_time + "s后可重新发送)");
        $("#hidVEmailLeftCount").val(str_time);
        var self = this;
        setTimeout(function () { self.setTimeShow(); }, 1000); //D:正确 
    }
    else {
        //业务逻辑
        timer.html("重新发送");
        $("#hidVEmailLeftCount").val("0");
        $("#resend").removeClass("disabled");
        return;
    }
}
//注册邮箱激活验证码
function showTime_vemail_m(tuanid, time_distance) {
    this.tuanid = tuanid;
    //PHP时间是秒，JS时间是微秒 
    this.time_distance = time_distance * 1000;
}
showTime_vemail_m.prototype.setTimeShow = function () {
    var timer = $(this.tuanid);
    var str_time;
    var int_day, int_hour, int_minute, int_second;
    time_distance = this.time_distance;
    this.time_distance = this.time_distance - 1000;
    if (time_distance > 0) {
        int_day = Math.floor(time_distance / 86400000);
        time_distance -= int_day * 86400000;
        int_hour = Math.floor(time_distance / 3600000);
        time_distance -= int_hour * 3600000;
        int_minute = Math.floor(time_distance / 60000);
        time_distance -= int_minute * 60000;
        int_second = Math.floor(time_distance / 1000);
        if (int_hour < 10)
            int_hour = "0" + int_hour;
        if (int_minute < 10)
            int_minute = "0" + int_minute;
        //if (int_second < 10)
        //    int_second = "0" + int_second;
        //str_time = "还剩" + int_day + "天" + int_hour + "时" + int_minute + "分" + int_second + "秒";
        str_time = int_second;
        timer.html("(" + str_time + "s后可重新发送)");
        $("#hidVEmailLeftCount").val(str_time);
        var self = this;
        setTimeout(function () { self.setTimeShow(); }, 1000); //D:正确 
    }
    else {
        //业务逻辑
        timer.html("重新发送");
        $("#hidVEmailLeftCount").val("0");
        $("#reSend").removeClass("locked");
        return;
    }
}
//忘记密码验证码
function showTime_fgt(tuanid, time_distance) {
    this.tuanid = tuanid;
    //PHP时间是秒，JS时间是微秒 
    this.time_distance = time_distance * 1000;
}
showTime_fgt.prototype.setTimeShow = function (l) {
    var timer = $(this.tuanid);
    var fgtway = l;
    var way = $("#hidFgtWay").val().trim();
    var str_time;
    var int_day, int_hour, int_minute, int_second;
    time_distance = this.time_distance;
    this.time_distance = this.time_distance - 1000;
    if (time_distance > 0) {
        int_day = Math.floor(time_distance / 86400000);
        time_distance -= int_day * 86400000;
        int_hour = Math.floor(time_distance / 3600000);
        time_distance -= int_hour * 3600000;
        int_minute = Math.floor(time_distance / 60000);
        time_distance -= int_minute * 60000;
        int_second = Math.floor(time_distance / 1000);
        if (int_hour < 10)
            int_hour = "0" + int_hour;
        if (int_minute < 10)
            int_minute = "0" + int_minute;
        //if (int_second < 10)
        //    int_second = "0" + int_second;
        //str_time = "还剩" + int_day + "天" + int_hour + "时" + int_minute + "分" + int_second + "秒";
        switch (fgtway) {
            case "1":
                addCookie("fgtpwdmobilecodeleft", int_second);
                break;
            case "2":
                addCookie("fgtpwdemailcodeleft", int_second);
                break;
        }
        if (way == fgtway) {
            timer.val(int_second + "s后可重发");
        }
        var self = this;
        setTimeout(function () { self.setTimeShow(fgtway); }, 1000); //D:正确 
    }
    else {
        //业务逻辑
        timer.val("获取验证码");
        switch (fgtway) {
            case "1":
                addCookie("fgtpwdmobilecodeleft", 0);
                break;
            case "2":
                addCookie("fgtpwdemailcodeleft", 0);
                break;
        }
        if (way == fgtway) {
            $("#txtFgtWay").blur();
        }
        return;
    }
}
//忘记密码验证码
function showTime_fgtmobile(tuanid, time_distance) {
    this.tuanid = tuanid;
    //PHP时间是秒，JS时间是微秒 
    this.time_distance = time_distance * 1000;
}
showTime_fgtmobile.prototype.setTimeShow = function (l) {
    var timer = $(this.tuanid);
    var str_time;
    var int_day, int_hour, int_minute, int_second;
    time_distance = this.time_distance;
    this.time_distance = this.time_distance - 1000;
    if (time_distance > 0) {
        int_day = Math.floor(time_distance / 86400000);
        time_distance -= int_day * 86400000;
        int_hour = Math.floor(time_distance / 3600000);
        time_distance -= int_hour * 3600000;
        int_minute = Math.floor(time_distance / 60000);
        time_distance -= int_minute * 60000;
        int_second = Math.floor(time_distance / 1000);
        if (int_hour < 10)
            int_hour = "0" + int_hour;
        if (int_minute < 10)
            int_minute = "0" + int_minute;
        //if (int_second < 10)
        //    int_second = "0" + int_second;
        //str_time = "还剩" + int_day + "天" + int_hour + "时" + int_minute + "分" + int_second + "秒";
        str_time = int_second;
        timer.html("" + str_time + "s后可重发");
        $("#hidVeryMobileLeftCount").val(str_time);
        var self = this;
        setTimeout(function () { self.setTimeShow(); }, 1000); //D:正确 
    }
    else {
        //业务逻辑
        timer.html("获取验证码");
        $("#hidVeryMobileLeftCount").val("0");
        timer.removeClass("locked");
        return;
    }
}
function showTime_fgtemail(tuanid, time_distance) {
    this.tuanid = tuanid;
    //PHP时间是秒，JS时间是微秒 
    this.time_distance = time_distance * 1000;
}
showTime_fgtemail.prototype.setTimeShow = function (l) {
    var timer = $(this.tuanid);
    var str_time;
    var int_day, int_hour, int_minute, int_second;
    time_distance = this.time_distance;
    this.time_distance = this.time_distance - 1000;
    if (time_distance > 0) {
        int_day = Math.floor(time_distance / 86400000);
        time_distance -= int_day * 86400000;
        int_hour = Math.floor(time_distance / 3600000);
        time_distance -= int_hour * 3600000;
        int_minute = Math.floor(time_distance / 60000);
        time_distance -= int_minute * 60000;
        int_second = Math.floor(time_distance / 1000);
        if (int_hour < 10)
            int_hour = "0" + int_hour;
        if (int_minute < 10)
            int_minute = "0" + int_minute;
        //if (int_second < 10)
        //    int_second = "0" + int_second;
        //str_time = "还剩" + int_day + "天" + int_hour + "时" + int_minute + "分" + int_second + "秒";
        str_time = int_second;
        timer.html("" + str_time + "s后可重发");
        $("#hidVeryEmailLeftCount").val(str_time);
        var self = this;
        setTimeout(function () { self.setTimeShow(); }, 1000); //D:正确 
    }
    else {
        //业务逻辑
        timer.html("获取验证码");
        $("#hidVeryEmailLeftCount").val("0");
        timer.removeClass("locked");
        return;
    }
}

//绑定手机验证码
function showTime_bdmobile(tuanid, time_distance) {
    this.tuanid = tuanid;
    //PHP时间是秒，JS时间是微秒 
    this.time_distance = time_distance * 1000;
}
showTime_bdmobile.prototype.setTimeShow = function () {
    var timer = $(this.tuanid);
    var str_time;
    var int_day, int_hour, int_minute, int_second;
    time_distance = this.time_distance;
    this.time_distance = this.time_distance - 1000;
    if (time_distance > 0) {
        int_day = Math.floor(time_distance / 86400000);
        time_distance -= int_day * 86400000;
        int_hour = Math.floor(time_distance / 3600000);
        time_distance -= int_hour * 3600000;
        int_minute = Math.floor(time_distance / 60000);
        time_distance -= int_minute * 60000;
        int_second = Math.floor(time_distance / 1000);
        if (int_hour < 10)
            int_hour = "0" + int_hour;
        if (int_minute < 10)
            int_minute = "0" + int_minute;
        //if (int_second < 10)
        //    int_second = "0" + int_second;
        //str_time = "还剩" + int_day + "天" + int_hour + "时" + int_minute + "分" + int_second + "秒";
        addCookie("bindmobilecodeleft", int_second);
        timer.html("(" + int_second + "s后可重发)");
        timer.addClass("disabled");
        var self = this;
        setTimeout(function () { self.setTimeShow(); }, 1000); //D:正确 
    }
    else {
        //业务逻辑
        timer.html("发送验证码");
        addCookie("bindmobilecodeleft", 0);
        timer.removeClass("disabled");
        return;
    }
}
//绑定手机验证码
function showTime_bdmobile_mobile(tuanid, time_distance) {
    this.tuanid = tuanid;
    //PHP时间是秒，JS时间是微秒 
    this.time_distance = time_distance * 1000;
}
showTime_bdmobile_mobile.prototype.setTimeShow = function () {
    var timer = $(this.tuanid);
    var str_time;
    var int_day, int_hour, int_minute, int_second;
    time_distance = this.time_distance;
    this.time_distance = this.time_distance - 1000;
    if (time_distance > 0) {
        int_day = Math.floor(time_distance / 86400000);
        time_distance -= int_day * 86400000;
        int_hour = Math.floor(time_distance / 3600000);
        time_distance -= int_hour * 3600000;
        int_minute = Math.floor(time_distance / 60000);
        time_distance -= int_minute * 60000;
        int_second = Math.floor(time_distance / 1000);
        if (int_hour < 10)
            int_hour = "0" + int_hour;
        if (int_minute < 10)
            int_minute = "0" + int_minute;
        //if (int_second < 10)
        //    int_second = "0" + int_second;
        //str_time = "还剩" + int_day + "天" + int_hour + "时" + int_minute + "分" + int_second + "秒";
        timer.html(int_second + "s后可重发");
        var self = this;
        setTimeout(function () { self.setTimeShow(); }, 1000); //D:正确 
    }
    else {
        //业务逻辑
        timer.removeClass("locked").html("发送验证码");
        addCookie("bdmobilecd", "");
        return;
    }
}

//解绑手机验证码
function showTime_cbmobile(tuanid, time_distance) {
    this.tuanid = tuanid;
    //PHP时间是秒，JS时间是微秒 
    this.time_distance = time_distance * 1000;
}
showTime_cbmobile.prototype.setTimeShow = function () {
    var timer = $(this.tuanid);
    var str_time;
    var int_day, int_hour, int_minute, int_second;
    time_distance = this.time_distance;
    this.time_distance = this.time_distance - 1000;
    if (time_distance > 0) {
        int_day = Math.floor(time_distance / 86400000);
        time_distance -= int_day * 86400000;
        int_hour = Math.floor(time_distance / 3600000);
        time_distance -= int_hour * 3600000;
        int_minute = Math.floor(time_distance / 60000);
        time_distance -= int_minute * 60000;
        int_second = Math.floor(time_distance / 1000);
        if (int_hour < 10)
            int_hour = "0" + int_hour;
        if (int_minute < 10)
            int_minute = "0" + int_minute;
        //if (int_second < 10)
        //    int_second = "0" + int_second;
        //str_time = "还剩" + int_day + "天" + int_hour + "时" + int_minute + "分" + int_second + "秒";
        addCookie("changebindmobilecodeleft", int_second);
        timer.html("(" + int_second + "s后可重新获取)");
        timer.addClass("disabled");
        var self = this;
        setTimeout(function () { self.setTimeShow(); }, 1000); //D:正确 
    }
    else {
        //业务逻辑
        timer.html("免费获取验证码");
        addCookie("changebindmobilecodeleft", 0);
        timer.removeClass("disabled");
        return;
    }
}
//解绑手机验证码
function showTime_cbmobile_mobile(tuanid, time_distance) {
    this.tuanid = tuanid;
    //PHP时间是秒，JS时间是微秒 
    this.time_distance = time_distance * 1000;
}
showTime_cbmobile_mobile.prototype.setTimeShow = function () {
    var timer = $(this.tuanid);
    var str_time;
    var int_day, int_hour, int_minute, int_second;
    time_distance = this.time_distance;
    this.time_distance = this.time_distance - 1000;
    if (time_distance > 0) {
        int_day = Math.floor(time_distance / 86400000);
        time_distance -= int_day * 86400000;
        int_hour = Math.floor(time_distance / 3600000);
        time_distance -= int_hour * 3600000;
        int_minute = Math.floor(time_distance / 60000);
        time_distance -= int_minute * 60000;
        int_second = Math.floor(time_distance / 1000);
        if (int_hour < 10)
            int_hour = "0" + int_hour;
        if (int_minute < 10)
            int_minute = "0" + int_minute;
        //if (int_second < 10)
        //    int_second = "0" + int_second;
        //str_time = "还剩" + int_day + "天" + int_hour + "时" + int_minute + "分" + int_second + "秒";
        timer.html(int_second + "s后可重发");
        var self = this;
        setTimeout(function () { self.setTimeShow(); }, 1000); //D:正确 
    }
    else {
        //业务逻辑
        timer.removeClass("locked").html("发送验证码");
        addCookie("ubdmobilecd", "");
        return;
    }
}

//绑定邮箱验证码
function showTime_bdemail(tuanid, time_distance) {
    this.tuanid = tuanid;
    //PHP时间是秒，JS时间是微秒 
    this.time_distance = time_distance * 1000;
}
showTime_bdemail.prototype.setTimeShow = function () {
    var timer = $(this.tuanid);
    var str_time;
    var int_day, int_hour, int_minute, int_second;
    time_distance = this.time_distance;
    this.time_distance = this.time_distance - 1000;
    if (time_distance > 0) {
        int_day = Math.floor(time_distance / 86400000);
        time_distance -= int_day * 86400000;
        int_hour = Math.floor(time_distance / 3600000);
        time_distance -= int_hour * 3600000;
        int_minute = Math.floor(time_distance / 60000);
        time_distance -= int_minute * 60000;
        int_second = Math.floor(time_distance / 1000);
        if (int_hour < 10)
            int_hour = "0" + int_hour;
        if (int_minute < 10)
            int_minute = "0" + int_minute;
        //if (int_second < 10)
        //    int_second = "0" + int_second;
        //str_time = "还剩" + int_day + "天" + int_hour + "时" + int_minute + "分" + int_second + "秒";
        addCookie("bindemailcodeleft", int_second);
        timer.html("(" + int_second + "s后可重发)");
        timer.addClass("disabled");
        var self = this;
        setTimeout(function () { self.setTimeShow(); }, 1000); //D:正确 
    }
    else {
        //业务逻辑
        timer.html("发送验证码");
        addCookie("bindemailcodeleft", 0);
        timer.removeClass("disabled");
        return;
    }
}
//绑定邮箱验证码
function showTime_bdemail_mobile(tuanid, time_distance) {
    this.tuanid = tuanid;
    //PHP时间是秒，JS时间是微秒 
    this.time_distance = time_distance * 1000;
}
showTime_bdemail_mobile.prototype.setTimeShow = function () {
    var timer = $(this.tuanid);
    var str_time;
    var int_day, int_hour, int_minute, int_second;
    time_distance = this.time_distance;
    this.time_distance = this.time_distance - 1000;
    if (time_distance > 0) {
        int_day = Math.floor(time_distance / 86400000);
        time_distance -= int_day * 86400000;
        int_hour = Math.floor(time_distance / 3600000);
        time_distance -= int_hour * 3600000;
        int_minute = Math.floor(time_distance / 60000);
        time_distance -= int_minute * 60000;
        int_second = Math.floor(time_distance / 1000);
        if (int_hour < 10)
            int_hour = "0" + int_hour;
        if (int_minute < 10)
            int_minute = "0" + int_minute;
        //if (int_second < 10)
        //    int_second = "0" + int_second;
        //str_time = "还剩" + int_day + "天" + int_hour + "时" + int_minute + "分" + int_second + "秒";
        timer.html(int_second + "s后可重发");
        var self = this;
        setTimeout(function () { self.setTimeShow(); }, 1000); //D:正确 
    }
    else {
        //业务逻辑
        timer.removeClass("locked").html("发送验证码");
        addCookie("bdemailcd", "");
        return;
    }
}

//解绑邮箱验证码
function showTime_cbemail(tuanid, time_distance) {
    this.tuanid = tuanid;
    //PHP时间是秒，JS时间是微秒 
    this.time_distance = time_distance * 1000;
}
showTime_cbemail.prototype.setTimeShow = function () {
    var timer = $(this.tuanid);
    var str_time;
    var int_day, int_hour, int_minute, int_second;
    time_distance = this.time_distance;
    this.time_distance = this.time_distance - 1000;
    if (time_distance > 0) {
        int_day = Math.floor(time_distance / 86400000);
        time_distance -= int_day * 86400000;
        int_hour = Math.floor(time_distance / 3600000);
        time_distance -= int_hour * 3600000;
        int_minute = Math.floor(time_distance / 60000);
        time_distance -= int_minute * 60000;
        int_second = Math.floor(time_distance / 1000);
        if (int_hour < 10)
            int_hour = "0" + int_hour;
        if (int_minute < 10)
            int_minute = "0" + int_minute;
        //if (int_second < 10)
        //    int_second = "0" + int_second;
        //str_time = "还剩" + int_day + "天" + int_hour + "时" + int_minute + "分" + int_second + "秒";
        addCookie("changebindemailcodeleft", int_second);
        timer.html("(" + int_second + "s后可重新获取)");
        timer.addClass("disabled");
        var self = this;
        setTimeout(function () { self.setTimeShow(); }, 1000); //D:正确 
    }
    else {
        //业务逻辑
        timer.html("免费获取验证码");
        addCookie("changebindemailcodeleft", 0);
        timer.removeClass("disabled");
        return;
    }
}
//解绑邮箱验证码
function showTime_cbemail_mobile(tuanid, time_distance) {
    this.tuanid = tuanid;
    //PHP时间是秒，JS时间是微秒 
    this.time_distance = time_distance * 1000;
}
showTime_cbemail_mobile.prototype.setTimeShow = function () {
    var timer = $(this.tuanid);
    var str_time;
    var int_day, int_hour, int_minute, int_second;
    time_distance = this.time_distance;
    this.time_distance = this.time_distance - 1000;
    if (time_distance > 0) {
        int_day = Math.floor(time_distance / 86400000);
        time_distance -= int_day * 86400000;
        int_hour = Math.floor(time_distance / 3600000);
        time_distance -= int_hour * 3600000;
        int_minute = Math.floor(time_distance / 60000);
        time_distance -= int_minute * 60000;
        int_second = Math.floor(time_distance / 1000);
        if (int_hour < 10)
            int_hour = "0" + int_hour;
        if (int_minute < 10)
            int_minute = "0" + int_minute;
        //if (int_second < 10)
        //    int_second = "0" + int_second;
        //str_time = "还剩" + int_day + "天" + int_hour + "时" + int_minute + "分" + int_second + "秒";
        timer.html(int_second + "s后可重发");
        var self = this;
        setTimeout(function () { self.setTimeShow(); }, 1000); //D:正确 
    }
    else {
        //业务逻辑
        timer.removeClass("locked").html("发送验证码");
        addCookie("ubdemailcd", "");
        return;
    }
}

//设置/修改支付密码验证码
function showTime_changepaypwd(tuanid, time_distance) {
    this.tuanid = tuanid;
    //PHP时间是秒，JS时间是微秒 
    this.time_distance = time_distance * 1000;
}
showTime_changepaypwd.prototype.setTimeShow = function () {
    var timer = $(this.tuanid);
    var str_time;
    var int_day, int_hour, int_minute, int_second;
    time_distance = this.time_distance;
    this.time_distance = this.time_distance - 1000;
    if (time_distance > 0) {
        int_day = Math.floor(time_distance / 86400000);
        time_distance -= int_day * 86400000;
        int_hour = Math.floor(time_distance / 3600000);
        time_distance -= int_hour * 3600000;
        int_minute = Math.floor(time_distance / 60000);
        time_distance -= int_minute * 60000;
        int_second = Math.floor(time_distance / 1000);
        if (int_hour < 10)
            int_hour = "0" + int_hour;
        if (int_minute < 10)
            int_minute = "0" + int_minute;
        //if (int_second < 10)
        //    int_second = "0" + int_second;
        //str_time = "还剩" + int_day + "天" + int_hour + "时" + int_minute + "分" + int_second + "秒";
        timer.html(int_second + "s后可重发");
        var self = this;
        setTimeout(function () { self.setTimeShow(); }, 1000); //D:正确 
    }
    else {
        //业务逻辑
        timer.removeClass("locked").html("发送验证码");
        addCookie("chgppwdverify", "");
        return;
    }
}

function showTime03(tuanid, time_distance) {
    this.tuanid = tuanid;
    //PHP时间是秒，JS时间是微秒 
    this.time_distance = time_distance * 1000;
}
showTime03.prototype.setTimeShow = function () {
    var timer = $(this.tuanid);
    var str_time;
    var int_day, int_hour, int_minute, int_second;
    time_distance = this.time_distance;
    this.time_distance = this.time_distance - 1000;
    if (time_distance > 0) {
        int_day = Math.floor(time_distance / 86400000);
        time_distance -= int_day * 86400000;
        int_hour = Math.floor(time_distance / 3600000);
        time_distance -= int_hour * 3600000;
        int_minute = Math.floor(time_distance / 60000);
        time_distance -= int_minute * 60000;
        int_second = Math.floor(time_distance / 1000);
        if (int_hour < 10)
            int_hour = "0" + int_hour;
        if (int_minute < 10)
            int_minute = "0" + int_minute;
        if (int_second < 10)
            int_second = "0" + int_second;
        //str_time = "还剩" + int_day + "天" + int_hour + "时" + int_minute + "分" + int_second + "秒";
        str_time = int_second;
        timer.val(str_time + "秒后重新获取");
        $("#hidMobileCountLeft").val(str_time);
        var self = this;
        setTimeout(function () { self.setTimeShow(); }, 1000); //D:正确 
    }
    else {
        timer.val("发送验证码");
        $("#hidCountLeft").val("0");
        //业务逻辑
        $("#btnMobESend").addClass("enable");
        return;
    }
}

function showTimeEmail(tuanid, time_distance) {
    this.tuanid = tuanid;
    //PHP时间是秒，JS时间是微秒 
    this.time_distance = time_distance * 1000;
}
showTimeEmail.prototype.setTimeShow = function () {
    var timer = $(this.tuanid);
    var str_time;
    var int_day, int_hour, int_minute, int_second;
    time_distance = this.time_distance;
    this.time_distance = this.time_distance - 1000;
    if (time_distance > 0) {
        int_day = Math.floor(time_distance / 86400000);
        time_distance -= int_day * 86400000;
        int_hour = Math.floor(time_distance / 3600000);
        time_distance -= int_hour * 3600000;
        int_minute = Math.floor(time_distance / 60000);
        time_distance -= int_minute * 60000;
        int_second = Math.floor(time_distance / 1000);
        if (int_hour < 10)
            int_hour = "0" + int_hour;
        if (int_minute < 10)
            int_minute = "0" + int_minute;
        if (int_second < 10)
            int_second = "0" + int_second;
        //str_time = "还剩" + int_day + "天" + int_hour + "时" + int_minute + "分" + int_second + "秒";
        str_time = int_second;
        timer.val(str_time + "秒后重新获取");
        $("#hidEmailCountLeft").val(str_time);
        var self = this;
        setTimeout(function () { self.setTimeShow(); }, 1000); //D:正确 
    }
    else {
        timer.val("发送验证码");
        $("#hidEmailCountLeft").val("0");
        //业务逻辑
        emaila_press($("#txtEmail"));
        return;
    }
}

function showTimeEmailEdit(tuanid, time_distance) {
    this.tuanid = tuanid;
    //PHP时间是秒，JS时间是微秒 
    this.time_distance = time_distance * 1000;
}
showTimeEmailEdit.prototype.setTimeShow = function () {
    var timer = $(this.tuanid);
    var str_time;
    var int_day, int_hour, int_minute, int_second;
    time_distance = this.time_distance;
    this.time_distance = this.time_distance - 1000;
    if (time_distance > 0) {
        int_day = Math.floor(time_distance / 86400000);
        time_distance -= int_day * 86400000;
        int_hour = Math.floor(time_distance / 3600000);
        time_distance -= int_hour * 3600000;
        int_minute = Math.floor(time_distance / 60000);
        time_distance -= int_minute * 60000;
        int_second = Math.floor(time_distance / 1000);
        if (int_hour < 10)
            int_hour = "0" + int_hour;
        if (int_minute < 10)
            int_minute = "0" + int_minute;
        if (int_second < 10)
            int_second = "0" + int_second;
        //str_time = "还剩" + int_day + "天" + int_hour + "时" + int_minute + "分" + int_second + "秒";
        str_time = int_second;
        timer.val(str_time + "秒后重新获取");
        $("#hidEmailCountLeft").val(str_time);
        var self = this;
        setTimeout(function () { self.setTimeShow(); }, 1000); //D:正确 
    }
    else {
        timer.val("发送验证码");
        $("#hidEmailCountLeft").val("0");
        //业务逻辑
        $("#btnEmailESend").addClass("enable");
        return;
    }
}