﻿function bigimages(name) {
    $(name).each(function () {
        $(this).mouseover(function (e) {
            if (/.png$|.gif$|.jpg$|.bmp$|.PNG$|.GIF$|.JPG$|.BMP$/.test($(this).val())) {
                $("body").append("<div id='mytooltip' ><div></div><img src=" + $(this).val() + "></img></div>");
            }
            //            else {
            //                $("body").append("<div id='mytooltip' ><div></div>" + msg + "</div>");
            //            }

            $("#mytooltip").css({
                position: "absolute",
                border: "1px solid #ccc",
                background: "#FCFBE5",
                padding: "5px",
                color: "#666",
                zIndex: 1000

            });

            //            $("#mytooltip").css("left", (e.pageX - 500) + "px");
            //            $("#mytooltip").css("top", (e.pageY - 10) + "px");
            $("#mytooltip").css("left",$(window).width()/3+ "px");
            $("#mytooltip").css("top", $(window).height() /10 + "px");

        });
        $(this).mouseout(function () {
            $("#mytooltip").remove();
        });
        $(this).mousemove(function (e) {
            //            $("#mytooltip").css("left", (e.pageX - 500) + "px");
            //            $("#mytooltip").css("top", (e.pageY - 10) + "px");
            $("#mytooltip").css("left", $(window).width() / 3 + "px");
            $("#mytooltip").css("top", $(window).height() / 10 + "px");

        });
        $(this).keydown(function () {
            $("#mytooltip").remove();
        });
    })
}
        
