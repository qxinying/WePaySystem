﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Xinying.Common;
using System.Text;
using LitJson;

namespace Xinying.Web.demo
{
    public partial class WePay : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                loaddata();
            }
        }

        #region loaddata
        protected void loaddata()
        {

        }
        #endregion

        protected void btnWePayV1_0_Click(object sender, EventArgs e)
        {
            #region 在线微信支付
            demo.wxpay.Pay pay = new demo.wxpay.Pay();
            pay.appkey = Common.XYKeys.STRING_WXPAY_APPID;
            pay.paybody = HttpUtility.UrlEncode("第三方系统_微信支付", Encoding.GetEncoding("utf-8"));
            pay.paymethod = "weixin.pay";
            pay.callback_url = HttpUtility.UrlEncode(string.Format("http://{0}/demo/wxpay/back.aspx", XYRequest.GetCurrentFullHost()));
            pay.notify_url = HttpUtility.UrlEncode(string.Format("http://{0}/demo/wxpay/notify.ashx", XYRequest.GetCurrentFullHost()));
            pay.out_trade_no = DateTime.Now.ToString("HHmmssffff");//第三方订单号
            pay.timestamp = WxApi.Utils.GetTimeStamp().ToString();
            pay.total_fee = (int)(0.01 * 100);//单位：分
            pay.version = "1.0";
            string data;
            pay.sign = Common.Utils.WxPaySign(pay, Common.XYKeys.STRING_WXPAY_APPSECRET, out data);
            //相关参数请求服务器，返回支付地址（考虑到可扩展多种支付，所以支付地址不确定，需要向服务器请求得知）
            var str = Common.Utils.HttpPost(XYKeys.STRING_WXPAY_SERVERURL + "/tools/payapi.ashx", data + "&sign=" + pay.sign + "&action=getpayurl");
            try
            {
                JsonData jobj = JsonMapper.ToObject(str);
                string verify_status = jobj["verify_status"].ToStr();
                string url = HttpUtility.UrlDecode(jobj["pay_url"].ToStr());
                Response.Redirect(url, false);
            }
            catch (Exception ex)
            {
                Utils.WriteTxt("wxpay.aspx.cs报错：" + ex);
                Response.Redirect("/api/payment/wechatpay/error.aspx");
            }
            #endregion
        }

        protected void btnWePayV2_0_Click(object sender, EventArgs e)
        {
            #region 在线微信支付
            demo.wxpay.Pay pay = new demo.wxpay.Pay();
            pay.appkey = Common.XYKeys.STRING_WXPAY_APPID;
            pay.paybody = HttpUtility.UrlEncode("第三方系统_微信支付", Encoding.GetEncoding("utf-8"));
            pay.paymethod = "weixin.pay";
            pay.callback_url = HttpUtility.UrlEncode(string.Format("http://{0}/demo/wxpay/back.aspx", XYRequest.GetCurrentFullHost()));
            pay.notify_url = HttpUtility.UrlEncode(string.Format("http://{0}/demo/wxpay/notify.ashx", XYRequest.GetCurrentFullHost()));
            pay.out_trade_no = DateTime.Now.ToString("HHmmssffff");//第三方订单号
            pay.timestamp = WxApi.Utils.GetTimeStamp().ToString();
            pay.total_fee = (int)(0.01 * 100);//单位：分
            pay.version = "2.0";
            string data;
            pay.sign = Common.Utils.WxPaySign(pay, Common.XYKeys.STRING_WXPAY_APPSECRET, out data);
            //相关参数请求服务器，返回支付地址（考虑到可扩展多种支付，所以支付地址不确定，需要向服务器请求得知）
            var str = Common.Utils.HttpPost(XYKeys.STRING_WXPAY_SERVERURL + "/tools/payapi.ashx", data + "&sign=" + pay.sign + "&action=getpayurl");
            try
            {
                JsonData jobj = JsonMapper.ToObject(str);
                string verify_status = jobj["verify_status"].ToStr();
                string url = HttpUtility.UrlDecode(jobj["pay_url"].ToStr());
                Response.Redirect(url, false);
            }
            catch (Exception ex)
            {
                Utils.WriteTxt("wxpay.aspx.cs报错：" + ex);
                Response.Redirect("/api/payment/wechatpay/error.aspx");
            }
            #endregion
        }
    }
}