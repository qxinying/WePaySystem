﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Xinying.Common;

namespace Xinying.Web.demo.wxpay
{
    public partial class back : System.Web.UI.Page
    {
        public Model.siteconfig config = Model.siteconfig.loadConfig();
        protected string orderno = "";
        public Model.siteconfig siteconfig = Model.siteconfig.loadConfig();
        private static object lockThis = new object();
        protected void Page_Load(object sender, EventArgs e)
        {
            //Utils.WriteTxt("同步回调："); 
            var reqdata = Request.Url.Query.Trim('?');
            var arr = reqdata.Split("&".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            string data;
            orderno = Xinying.Common.XYRequest.GetQueryString("re");//提交给微信服务器的订单号
            orderno = orderno.Substring(0, 10);//本系统订单号
            if (XYRequest.GetQueryString("sign") == Utils.WxPaySign(arr, Xinying.Common.XYKeys.STRING_WXPAY_APPSECRET, out data))
            {
                lock (lockThis)
                {
                    #region 同步订单处理
                    Model.orders OrderModel = Model.orders.GetModelBySN(orderno);
                    if (OrderModel != null)
                    {
                        if (OrderModel.payment_status == 2)//已支付
                        {
                            Response.Write("SUCCESS");
                            return;
                        }
                        else
                        {
                            //第三方订单处理逻辑
                            Response.Write("SUCCESS");
                            return;
                        }
                    }
                    else
                    {
                        WxApi.Utils.WriteTxt($"订单{orderno}不存在");
                        return;
                    }
                    #endregion
                }
            }
        }
    }
}