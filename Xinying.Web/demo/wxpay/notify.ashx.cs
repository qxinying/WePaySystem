﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Xinying.Common;

namespace Xinying.Web.demo.wxpay
{
    /// <summary>
    /// notify 的摘要说明
    /// </summary>
    public class notify : IHttpHandler
    {
        protected string orderno = "";
        private static object lockThis = new object();
        public void ProcessRequest(HttpContext context)
        {
            var reqdata = HttpContext.Current.Request.Url.Query.Trim('?');
            var arr = reqdata.Split("&".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            string data;
            orderno = Xinying.Common.XYRequest.GetQueryString("out_trade_no");//提交给微信服务器的订单号
            orderno = orderno.Substring(0, 10);//本系统订单号
            if (XYRequest.GetQueryString("sign") == Utils.WxPaySign(arr, Xinying.Common.XYKeys.STRING_WXPAY_APPSECRET, out data))
            {
               
                lock (lockThis)
                {
                    #region 异步回调处理
                    Model.orders OrderModel = Model.orders.GetModelBySN(orderno);
                    if (OrderModel != null)
                    {
                        if (OrderModel.payment_status == 2)//已支付
                        {
                            context.Response.Write("SUCCESS");
                            return;
                        }
                        else
                        {
                            //第三方订单处理逻辑
                            context.Response.Write("SUCCESS");
                            return;
                        }
                    }
                    else
                    {
                        WxApi.Utils.WriteTxt($"订单{orderno}不存在");
                        return;
                    }
                    #endregion
                }
            }
            else
            {
                orderno = Xinying.Common.XYRequest.GetQueryString("no");
                Utils.WriteTxt($"订单{orderno}验证签名失败");
                Utils.WriteTxt(XYRequest.GetFormString("sign"));
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}