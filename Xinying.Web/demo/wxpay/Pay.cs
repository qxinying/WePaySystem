﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Xinying.Web.demo.wxpay
{
    public class Pay:BaseParam
    {
        public string out_trade_no { get; set; }
        public int total_fee { get; set; }
        public string notify_url { get; set; }
        public string callback_url { get; set; }
        public string paybody { get; set; }
    }
}