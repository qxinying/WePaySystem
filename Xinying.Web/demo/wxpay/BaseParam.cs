﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Xinying.Web.demo.wxpay
{
    /// <summary>
    /// 基础字段
    /// </summary>
    public class BaseParam
    {
        public string version { get; set; }
        public string paymethod { get; set; }
        public string timestamp { get; set; }
        public string appkey { get; set; }
        public string sign { get; set; }
    }


}