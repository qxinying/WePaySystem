﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Xinying.Web.demo
{
    public partial class WxOauth : Web.UI.WxuserBasePage
    {
        protected Model.wxuserinfo wxuser = new Model.wxuserinfo();
        protected void Page_Load(object sender, EventArgs e)
        {
            wxuser = GetWxUserInfo();
            if (wxuser != null)
            {
                Response.Write("已登录微信昵称:" + wxuser.nickname);
            }
        }
    }
}