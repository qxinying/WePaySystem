﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Xinying.DBUtility
{
    public class MyAttribute : Attribute
    {
        /// <summary>
        /// 主键
        /// </summary>
        public Boolean PrimaryKey = false;
        /// <summary>
        /// 非数据库字段
        /// </summary>
        public Boolean NoUse = false;
        /// <summary>
        /// 不插入字段
        /// </summary>
        public Boolean NoInsert = false;
        /// <summary>
        /// 不参与更新字段
        /// </summary>
        public Boolean NoUpdate = false;
        /// <summary>
        /// 字段描述
        /// </summary>
        public String Type = null;
        /// <summary>
        /// 是否类的list
        /// </summary>
        public Boolean IsList = false;
        /// <summary>
        /// 是否需要另外一个Model返回的ID
        /// </summary>
        public Boolean IsNeedID = false;
        /// <summary>
        /// 子表是否更新
        /// </summary>
        public Boolean ListNoUpdate = false;


    }
}
