﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace Xinying.DBUtility
{
    internal class DataTypeMysql
    {
        internal static Type GetType(MySqlDbType sqlType)
        {
            switch (sqlType)
            {
                case MySqlDbType.Decimal:
                case MySqlDbType.NewDecimal:
                    return typeof(Decimal);
                case MySqlDbType.Byte:
                case MySqlDbType.UByte:
                case MySqlDbType.Binary:
                case MySqlDbType.VarBinary:
                    return typeof(byte[]);
                case MySqlDbType.Int16:
                case MySqlDbType.UInt16:
                    return typeof(int);
                case MySqlDbType.Int24:
                case MySqlDbType.Int32:
                case MySqlDbType.UInt24:
                case MySqlDbType.UInt32:
                    return typeof(Int32);
                case MySqlDbType.Int64:
                case MySqlDbType.UInt64:
                    return typeof(Int64);
                case MySqlDbType.Float:
                case MySqlDbType.Double:
                    return typeof(double);
                case MySqlDbType.Timestamp:
                case MySqlDbType.DateTime:
                case MySqlDbType.Date:
                case MySqlDbType.Time:
                case MySqlDbType.Newdate:
                    return typeof(DateTime);
                case MySqlDbType.Year:
                case MySqlDbType.VarString:
                case MySqlDbType.TinyBlob:
                case MySqlDbType.MediumBlob:
                case MySqlDbType.LongBlob:
                case MySqlDbType.Blob:
                case MySqlDbType.VarChar:
                case MySqlDbType.String:
                case MySqlDbType.TinyText:
                case MySqlDbType.MediumText:
                case MySqlDbType.LongText:
                case MySqlDbType.Text:
                    return typeof(String);
                case MySqlDbType.Bit:
                    return typeof(Boolean);
                case MySqlDbType.Guid:
                    return typeof(Guid);
                default:
                    return typeof(object);
            }
        }
        /// <summary>
        /// 将DbType类型对应映射到MySqlDbType类型
        /// </summary>
        /// <param name="type">DbType类型</param>
        /// <returns></returns>
        public static MySqlDbType GetSqlType(Type type)
        {
            return GetSqlType(type.Name.ToString());
        }
        /// <summary>
        /// 将DbType类型字符串表达方式对应映射到MySqlDbType类型
        /// </summary>
        /// <param name="dbType">DbType类型字符串表达类型</param>
        /// <returns></returns>
        public static MySqlDbType GetSqlType(string typeName)
        {
            switch (typeName.ToLower())
            {
                case "char":
                case "ansistring":
                case "varchar":
                case "nvarchar":
                case "nvarchar2":
                    return MySqlDbType.VarChar;
                case "ansistringfixedlength":
                case "stringfixedlength":
                case "string":
                    return MySqlDbType.VarString;
                case "binary":
                case "image":
                case "varbinary":
                case "timestamp":
                    return MySqlDbType.Binary;
                case "bit":
                case "boolean":
                    return MySqlDbType.Bit;
                case "tinyint":
                case "byte":
                case "sbyte":
                    return MySqlDbType.Byte;
                case "money":
                case "currency":
                case "decimal":
                    return MySqlDbType.Decimal;
                case "datetime":
                case "smalldatetime":
                    return MySqlDbType.DateTime;
                case "real":
                case "double":
                    return MySqlDbType.Double;
                case "uniqueidentifier":
                case "guid":
                    return MySqlDbType.Guid;
                case "smallint":
                case "int16":
                case "uint16":
                    return MySqlDbType.Int16;
                case "int":
                case "int32":
                case "uint32":
                case "number":
                    return MySqlDbType.Int32;
                case "bigint":
                case "int64":
                case "uint64":
                case "varnumeric":
                    return MySqlDbType.Int64;
                case "float":
                case "single":
                    return MySqlDbType.Float;
                case "date":
                    return MySqlDbType.Date;
                case "time":
                    return MySqlDbType.Time;
                case "ntext":
                case "text":
                    return MySqlDbType.Text;
            }
            return MySqlDbType.VarChar;
        }

        /// <summary>
        /// 将DbType类型字符串表达形式对应映射到DbType类型
        /// </summary>
        /// <param name="dbType">DbType类型字符串表达类型</param>
        /// <returns></returns>
        public static DbType GetDbType(string typeName)
        {
            switch (typeName.ToLower())
            {
                case "char":
                case "ansistring":
                    return DbType.AnsiString;
                case "ansistringfixedlength":
                    return DbType.AnsiStringFixedLength;
                case "varbinary":
                case "binary":
                case "image":
                case "timestamp":
                case "byte[]":
                    return DbType.Binary;
                case "bit":
                case "boolean":
                    return DbType.Boolean;
                case "tinyint":
                case "byte":
                    return DbType.Byte;
                case "smallmoney":
                case "currency":
                    return DbType.Currency;
                case "date":
                    return DbType.Date;
                case "smalldatetime":
                case "datetime":
                    return DbType.DateTime;
                case "decimal":
                    return DbType.Decimal;
                case "money":
                case "double":
                    return DbType.Double;
                case "uniqueidentifier":
                case "guid":
                    return DbType.Guid;
                case "smallint":
                case "int16":
                    return DbType.Int16;
                case "uint16":
                    return DbType.UInt16;
                case "int":
                case "int32":
                    return DbType.Int32;
                case "uint32":
                    return DbType.UInt32;
                case "bigint":
                case "int64":
                    return DbType.Int64;
                case "uint64":
                    return DbType.UInt64;
                case "variant":
                case "object":
                    return DbType.Object;
                case "sbyte":
                    return DbType.SByte;
                case "float":
                case "single":
                    return DbType.Single;
                case "text":
                case "string":
                case "varchar":
                case "nvarchar":
                    return DbType.String;
                case "nchar":
                case "stringfixedlength":
                    return DbType.StringFixedLength;
                case "time":
                    return DbType.Time;
                case "varnumeric":
                    return DbType.VarNumeric;
                case "xml":
                    return DbType.Xml;
            }

            return DbType.Object;
        }
        /// <summary>
        /// 将MySqlDbType类型对应映射到DbType类型
        /// </summary>
        /// <param name="type">MySqlDbType类型</param>
        /// <returns></returns>
        public static DbType GetDbType(Type type)
        {
            return GetDbType(type.Name.ToString());
        }
        #region 其它方法

        internal static int GetGroupID(MySqlDbType MySqlDbType)
        {
            switch (MySqlDbType)
            {
                case MySqlDbType.Int16:
                case MySqlDbType.Int24:
                case MySqlDbType.Int32:
                case MySqlDbType.Int64:
                case MySqlDbType.Float:
                case MySqlDbType.Double:
                case MySqlDbType.Decimal:
                case MySqlDbType.NewDecimal:
                    return 1;
                case MySqlDbType.Guid:
                case MySqlDbType.VarString:
                case MySqlDbType.VarChar:
                case MySqlDbType.String:
                case MySqlDbType.Blob:
                case MySqlDbType.TinyBlob:
                case MySqlDbType.MediumBlob:
                case MySqlDbType.LongBlob:
                case MySqlDbType.Text:
                case MySqlDbType.TinyText:
                case MySqlDbType.MediumText:
                case MySqlDbType.Year:
                    return 0;
                case MySqlDbType.DateTime:
                case MySqlDbType.Timestamp:
                case MySqlDbType.Date:
                case MySqlDbType.Time:
                case MySqlDbType.Newdate:
                    return 2;
                default:
                    return 999;
            }
        }
        #endregion
    }
}
