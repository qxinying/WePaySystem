using System;
using System.Collections;
using System.Collections.Specialized;
using System.Data;
using System.Configuration;
using System.Web;
using System.Data.Common;
using MySql.Data.MySqlClient;
using System.Text;
using System.Reflection;
using System.Collections.Generic;

namespace Xinying.DBUtility
{
    /// <summary>
    /// Copyright (C) 2010 micosoft.cn
    /// 数据访问基础类(基于MySql)
    /// 可以用户可以修改满足自己项目的需要。
    /// </summary>
    public abstract class DbHelperMySql
    {
        //数据库连接字符串(web.config来配置)，可以动态更改connectionString支持多数据库.		
        public static string connectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString.ToString();
        //public static string connectionString = ConfigurationManager.ConnectionStrings["ConnectionStringMysql"].ConnectionString.ToString();//+ HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["DbPath"]) + ";";
        public DbHelperMySql()
        {
        }

        #region 公用方法  
        /// <summary>  
        /// 得到最大值  
        /// </summary>  
        /// <param name="FieldName"></param>  
        /// <param name="TableName"></param>  
        /// <returns></returns>  
        public static int GetMaxID(string FieldName, string TableName)
        {
            string strsql = "select max(" + FieldName + ")+1 from " + TableName;
            object obj = GetSingle(strsql);
            if (obj == null)
            {
                return 1;
            }
            else
            {
                return int.Parse(obj.ToString());
            }
        }
        /// <summary>  
        /// 是否存在  
        /// </summary>  
        /// <param name="strSql"></param>  
        /// <returns></returns>  
        public static bool Exists(string strSql)
        {
            object obj = GetSingle(strSql);
            int cmdresult;
            if ((Object.Equals(obj, null)) || (Object.Equals(obj, System.DBNull.Value)))
            {
                cmdresult = 0;
            }
            else
            {
                cmdresult = int.Parse(obj.ToString());
            }
            if (cmdresult == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        /// <summary>  
        /// 是否存在（基于MySqlParameter）  
        /// </summary>  
        /// <param name="strSql"></param>  
        /// <param name="cmdParms"></param>  
        /// <returns></returns>  
        public static bool Exists(string strSql, params MySqlParameter[] cmdParms)
        {
            object obj = GetSingle(strSql, cmdParms);
            int cmdresult;
            if ((Object.Equals(obj, null)) || (Object.Equals(obj, System.DBNull.Value)))
            {
                cmdresult = 0;
            }
            else
            {
                cmdresult = int.Parse(obj.ToString());
            }
            if (cmdresult == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        #endregion

        #region  执行简单SQL语句
        /// <summary>
        /// 执行SQL语句，返回影响的记录数
        /// </summary>
        /// <param name="SQLString">SQL语句</param>
        /// <returns>影响的记录数</returns>
        public static int ExecuteSql(string SQLString)
        {
            using (MySqlConnection connection = new MySqlConnection(connectionString))
            {
                using (MySqlCommand cmd = new MySqlCommand(SQLString, connection))
                {
                    try
                    {
                        connection.Open();
                        int rows = cmd.ExecuteNonQuery();
                        return rows;
                    }
                    catch (MySqlException E)
                    {
                        connection.Close();
                        throw new Exception(E.Message);
                    }
                }
            }
        }

        /// <summary>
        /// 2012-2-20日新增重载，执行SQL语句，返回影响的记录数
        /// </summary>
        /// <param name="connection">MySqlConnection对象</param>
        /// <param name="trans">MySqlTransaction对象</param>
        /// <param name="SQLString">SQL语句</param>
        /// <returns>影响的记录数</returns>
        public static int ExecuteSql(MySqlConnection connection, MySqlTransaction trans, string SQLString)
        {
            using (MySqlCommand cmd = new MySqlCommand(SQLString, connection))
            {
                try
                {
                    cmd.Connection = connection;
                    cmd.Transaction = trans;
                    int rows = cmd.ExecuteNonQuery();
                    return rows;
                }
                catch (MySqlException E)
                {
                    throw new Exception(E.Message);
                }
            }
        }

        /// <summary>
        /// 执行多条SQL语句，实现数据库事务。
        /// </summary>
        /// <param name="SQLStringList">多条SQL语句</param>		
        public static bool ExecuteSqlTran(ArrayList SQLStringList)
        {
            using (MySqlConnection conn = new MySqlConnection(connectionString))
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand();
                cmd.Connection = conn;
                MySqlTransaction tx = conn.BeginTransaction();
                cmd.Transaction = tx;
                try
                {
                    for (int n = 0; n < SQLStringList.Count; n++)
                    {
                        string strsql = SQLStringList[n].ToString();
                        if (strsql.Trim().Length > 1)
                        {
                            cmd.CommandText = strsql;
                            cmd.ExecuteNonQuery();
                        }
                    }
                    tx.Commit();
                }
                catch (MySqlException E)
                {
                    tx.Rollback();
                    //throw new Exception(E.Message);
                    return false;
                }
                return true;
            }
        }
        /// <summary>
        /// 执行带一个存储过程参数的的SQL语句。
        /// </summary>
        /// <param name="SQLString">SQL语句</param>
        /// <param name="content">参数内容,比如一个字段是格式复杂的文章，有特殊符号，可以通过这个方式添加</param>
        /// <returns>影响的记录数</returns>
        public static int ExecuteSql(string SQLString, string content)
        {
            using (MySqlConnection connection = new MySqlConnection(connectionString))
            {
                MySqlCommand cmd = new MySqlCommand(SQLString, connection);
                MySqlParameter myParameter = new MySqlParameter("@content", MySqlDbType.VarChar);
                myParameter.Value = content;
                cmd.Parameters.Add(myParameter);
                try
                {
                    connection.Open();
                    int rows = cmd.ExecuteNonQuery();
                    return rows;
                }
                catch (MySqlException E)
                {
                    throw new Exception(E.Message);
                }
                finally
                {
                    cmd.Dispose();
                    connection.Close();
                }
            }
        }
        /// <summary>
        /// 向数据库里插入图像格式的字段(和上面情况类似的另一种实例)
        /// </summary>
        /// <param name="strSQL">SQL语句</param>
        /// <param name="fs">图像字节,数据库的字段类型为image的情况</param>
        /// <returns>影响的记录数</returns>
        public static int ExecuteSqlInsertImg(string strSQL, byte[] fs)
        {
            using (MySqlConnection connection = new MySqlConnection(connectionString))
            {
                MySqlCommand cmd = new MySqlCommand(strSQL, connection);
                MySqlParameter myParameter = new MySqlParameter("@fs", MySqlDbType.Binary);
                myParameter.Value = fs;
                cmd.Parameters.Add(myParameter);
                try
                {
                    connection.Open();
                    int rows = cmd.ExecuteNonQuery();
                    return rows;
                }
                catch (MySqlException E)
                {
                    throw new Exception(E.Message);
                }
                finally
                {
                    cmd.Dispose();
                    connection.Close();
                }
            }
        }

        /// <summary>
        /// 执行一条计算查询结果语句，返回查询结果（object）。
        /// </summary>
        /// <param name="SQLString">计算查询结果语句</param>
        /// <returns>查询结果（object）</returns>
        public static object GetSingle(string SQLString)
        {
            using (MySqlConnection connection = new MySqlConnection(connectionString))
            {
                using (MySqlCommand cmd = new MySqlCommand(SQLString, connection))
                {
                    try
                    {
                        connection.Open();
                        object obj = cmd.ExecuteScalar();
                        if ((Object.Equals(obj, null)) || (Object.Equals(obj, System.DBNull.Value)))
                        {
                            return null;
                        }
                        else
                        {
                            return obj;
                        }
                    }
                    catch (MySqlException e)
                    {
                        connection.Close();
                        throw new Exception(e.Message);
                    }
                }
            }
        }

        /// <summary>
        /// 2012-2-21新增重载：执行一条计算查询结果语句，返回查询结果（object）。
        /// </summary>
        public static object GetSingle(MySqlConnection connection, MySqlTransaction trans, string SQLString)
        {
            using (MySqlCommand cmd = new MySqlCommand(SQLString, connection))
            {
                try
                {
                    cmd.Connection = connection;
                    cmd.Transaction = trans;
                    object obj = cmd.ExecuteScalar();
                    if ((Object.Equals(obj, null)) || (Object.Equals(obj, System.DBNull.Value)))
                    {
                        return null;
                    }
                    else
                    {
                        return obj;
                    }
                }
                catch (MySqlException e)
                {
                    //trans.Rollback();
                    throw new Exception(e.Message);
                }
            }
        }

        /// <summary>
        /// 2015-6-3新增重载，执行一条计算查询结果语句，返回查询结果（object）。
        /// </summary>
        /// <param name="connection">MySqlConnection对象</param>
        /// <param name="trans">MySqlTransaction事务</param>
        /// <param name="SQLString">计算查询结果语句</param>
        /// <returns>查询结果（object）</returns>
        public static object GetSingle(MySqlConnection connection, MySqlTransaction trans, string SQLString, params MySqlParameter[] cmdParms)
        {
            using (MySqlCommand cmd = new MySqlCommand())
            {
                try
                {
                    PrepareCommand(cmd, connection, trans, SQLString, cmdParms);
                    object obj = cmd.ExecuteScalar();
                    cmd.Parameters.Clear();
                    if ((Object.Equals(obj, null)) || (Object.Equals(obj, System.DBNull.Value)))
                    {
                        return null;
                    }
                    else
                    {
                        return obj;
                    }
                }
                catch (System.Data.SqlClient.SqlException e)
                {
                    throw e;
                }
            }
        }

        /// <summary>
        /// 执行查询语句，返回MySqlDataReader
        /// </summary>
        /// <param name="strSQL">查询语句</param>
        /// <returns>MySqlDataReader</returns>
        public static MySqlDataReader ExecuteReader(string strSQL)
        {
            MySqlConnection connection = new MySqlConnection(connectionString);
            MySqlCommand cmd = new MySqlCommand(strSQL, connection);
            try
            {
                connection.Open();
                MySqlDataReader myReader = cmd.ExecuteReader();
                return myReader;
            }
            catch (MySqlException e)
            {
                throw new Exception(e.Message);
            }

        }
        /// <summary>
        /// 执行查询语句，返回DataSet
        /// </summary>
        /// <param name="SQLString">查询语句</param>
        /// <returns>DataSet</returns>
        public static DataSet Query(string SQLString)
        {
            using (MySqlConnection connection = new MySqlConnection(connectionString))
            {
                DataSet ds = new DataSet();
                try
                {
                    connection.Open();
                    MySqlDataAdapter command = new MySqlDataAdapter(SQLString, connection);
                    command.Fill(ds, "ds");
                }
                catch (MySqlException ex)
                {
                    throw new Exception(ex.Message);
                }
                return ds;
            }
        }


        /// <summary>
        /// 判断指定表里是否存在指定字段(增加于2014-01-21 By 深蓝色)
        /// </summary>
        /// <param name="tablename">表名</param>
        /// <param name="column_name">字段名</param>
        /// <returns>是否存在true false</returns>
        public static bool ExitColumnName(string tablename, string column_name)
        {
            bool flag = false;
            using (MySqlConnection connection = new MySqlConnection(connectionString))
            {
                connection.Open();
                MySqlCommand cmd = new MySqlCommand();

                cmd.CommandText = "select * from " + tablename + " where 1=0";
                cmd.Connection = connection;
                MySqlDataReader dr = cmd.ExecuteReader();
                for (int i = 0; i < dr.FieldCount; i++)
                {
                    if (dr.GetName(i).ToLower().Trim() == column_name.ToLower().Trim())
                    {
                        flag = true;
                        break;
                    }
                }
            }
            return flag;
        }

        /// <summary>
        /// 2012-2-21新增,执行查询语句，返回DataSet
        /// </summary>
        public static DataSet Query(MySqlConnection connection, MySqlTransaction trans, string SQLString)
        {
            DataSet ds = new DataSet();
            try
            {
                MySqlDataAdapter command = new MySqlDataAdapter(SQLString, connection);
                command.SelectCommand.Transaction = trans;
                command.Fill(ds, "ds");
            }
            catch (MySqlException ex)
            {
                throw new Exception(ex.Message);
            }
            return ds;
        }

        #endregion

        #region 执行带参数的SQL语句

        /// <summary>
        /// 执行SQL语句，返回影响的记录数
        /// </summary>
        /// <param name="SQLString">SQL语句</param>
        /// <returns>影响的记录数</returns>
        public static int ExecuteSql(string SQLString, params MySqlParameter[] cmdParms)
        {
            using (MySqlConnection connection = new MySqlConnection(connectionString))
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    try
                    {
                        PrepareCommand(cmd, connection, null, SQLString, cmdParms);
                        int rows = cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();
                        return rows;
                    }
                    catch (MySqlException E)
                    {
                        throw new Exception(E.Message);
                    }
                }
            }
        }

        /// <summary>
        /// 2012-2-20日新增一条重载方法，方便在DAL层使用事务
        /// </summary>
        /// <param name="connection">MySqlConnection对象</param>
        /// <param name="trans">MySqlTransaction对象</param>
        /// <param name="SQLString">SQL语句</param>
        /// <param name="cmdParms">MySqlParameter对像</param>
        /// <returns>受影响的行</returns>
        public static int ExecuteSql(MySqlConnection connection, MySqlTransaction trans, string SQLString, params MySqlParameter[] cmdParms)
        {
            using (MySqlCommand cmd = new MySqlCommand())
            {
                try
                {
                    PrepareCommand(cmd, connection, trans, SQLString, cmdParms);
                    int rows = cmd.ExecuteNonQuery();
                    cmd.Parameters.Clear();
                    return rows;
                }
                catch (MySqlException E)
                {
                    throw new Exception(E.Message);
                }
            }
        }

        /// <summary>
        /// 执行多条SQL语句，实现数据库事务。
        /// </summary>
        /// <param name="SQLStringList">SQL语句的哈希表（key为sql语句，value是该语句的MySqlParameter[]）</param>
        public static bool ExecuteSqlTran(Hashtable SQLStringList)
        {
            using (MySqlConnection conn = new MySqlConnection(connectionString))
            {
                conn.Open();
                using (MySqlTransaction trans = conn.BeginTransaction())
                {
                    MySqlCommand cmd = new MySqlCommand();
                    try
                    {
                        //循环
                        foreach (DictionaryEntry myDE in SQLStringList)
                        {
                            string cmdText = myDE.Key.ToString();
                            MySqlParameter[] cmdParms = (MySqlParameter[])myDE.Value;
                            PrepareCommand(cmd, conn, trans, cmdText, cmdParms);
                            int val = cmd.ExecuteNonQuery();
                            cmd.Parameters.Clear();
                        }
                        trans.Commit();
                    }
                    catch
                    {
                        trans.Rollback();
                        return false;
                    }
                }
            }
            return true;
        }


        /// <summary>
        /// 执行一条计算查询结果语句，返回查询结果（object）。
        /// </summary>
        /// <param name="SQLString">计算查询结果语句</param>
        /// <returns>查询结果（object）</returns>
        public static object GetSingle(string SQLString, params MySqlParameter[] cmdParms)
        {
            using (MySqlConnection connection = new MySqlConnection(connectionString))
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    try
                    {
                        PrepareCommand(cmd, connection, null, SQLString, cmdParms);
                        object obj = cmd.ExecuteScalar();
                        cmd.Parameters.Clear();
                        if ((Object.Equals(obj, null)) || (Object.Equals(obj, System.DBNull.Value)))
                        {
                            return null;
                        }
                        else
                        {
                            return obj;
                        }
                    }
                    catch (MySqlException e)
                    {
                        throw new Exception(e.Message);
                    }
                }
            }
        }

        /// <summary>
        /// 执行查询语句，返回MySqlDataReader
        /// </summary>
        /// <param name="strSQL">查询语句</param>
        /// <returns>MySqlDataReader</returns>
        public static MySqlDataReader ExecuteReader(string SQLString, params MySqlParameter[] cmdParms)
        {
            MySqlConnection connection = new MySqlConnection(connectionString);
            MySqlCommand cmd = new MySqlCommand();
            try
            {
                PrepareCommand(cmd, connection, null, SQLString, cmdParms);
                MySqlDataReader myReader = cmd.ExecuteReader();
                cmd.Parameters.Clear();
                return myReader;
            }
            catch (MySqlException e)
            {
                throw new Exception(e.Message);
            }

        }

        /// <summary>
        /// 执行查询语句，返回DataSet
        /// </summary>
        /// <param name="SQLString">查询语句</param>
        /// <returns>DataSet</returns>
        public static DataSet Query(string SQLString, params MySqlParameter[] cmdParms)
        {
            using (MySqlConnection connection = new MySqlConnection(connectionString))
            {
                MySqlCommand cmd = new MySqlCommand();
                PrepareCommand(cmd, connection, null, SQLString, cmdParms);
                using (MySqlDataAdapter da = new MySqlDataAdapter(cmd))
                {
                    DataSet ds = new DataSet();
                    try
                    {
                        da.Fill(ds, "ds");
                        cmd.Parameters.Clear();
                    }
                    catch (MySqlException ex)
                    {
                        throw new Exception(ex.Message);
                    }
                    return ds;
                }
            }
        }

        /// <summary>
        /// 2012-2-21新增结合事务
        /// </summary>
        public static DataSet Query(MySqlConnection connection, MySqlTransaction trans, string SQLString, params MySqlParameter[] cmdParms)
        {
            MySqlCommand cmd = new MySqlCommand();
            PrepareCommand(cmd, connection, trans, SQLString, cmdParms);
            using (MySqlDataAdapter da = new MySqlDataAdapter(cmd))
            {
                DataSet ds = new DataSet();
                try
                {
                    da.Fill(ds, "ds");
                    cmd.Parameters.Clear();
                }
                catch (MySqlException ex)
                {
                    throw new Exception(ex.Message);
                }
                return ds;
            }
        }

        private static void PrepareCommand(MySqlCommand cmd, MySqlConnection conn, MySqlTransaction trans, string cmdText, MySqlParameter[] cmdParms)
        {
            if (conn.State != ConnectionState.Open)
                conn.Open();
            cmd.Connection = conn;
            cmd.CommandText = cmdText;
            if (trans != null)
                cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;//cmdType;
            if (cmdParms != null)
            {
                foreach (MySqlParameter parm in cmdParms)
                    cmd.Parameters.Add(parm);
            }
        }

        #endregion

        #region 存储过程操作

        /// <summary>
        /// 执行存储过程
        /// </summary>
        /// <param name="storedProcName">存储过程名</param>
        /// <param name="parameters">存储过程参数</param>
        /// <returns>MySqlDataReader</returns>
        public static MySqlDataReader RunProcedure(string storedProcName, IDataParameter[] parameters)
        {
            MySqlConnection connection = new MySqlConnection(connectionString);
            MySqlDataReader returnReader;
            connection.Open();
            MySqlCommand command = BuildQueryCommand(connection, storedProcName, parameters);
            command.CommandType = CommandType.StoredProcedure;
            returnReader = command.ExecuteReader();
            return returnReader;
        }


        /// <summary>
        /// 执行存储过程
        /// </summary>
        /// <param name="storedProcName">存储过程名</param>
        /// <param name="parameters">存储过程参数</param>
        /// <param name="tableName">DataSet结果中的表名</param>
        /// <returns>DataSet</returns>
        public static DataSet RunProcedure(string storedProcName, IDataParameter[] parameters, string tableName)
        {
            using (MySqlConnection connection = new MySqlConnection(connectionString))
            {
                DataSet dataSet = new DataSet();
                connection.Open();
                MySqlDataAdapter sqlDA = new MySqlDataAdapter();
                sqlDA.SelectCommand = BuildQueryCommand(connection, storedProcName, parameters);
                sqlDA.Fill(dataSet, tableName);
                connection.Close();
                return dataSet;
            }
        }


        /// <summary>
        /// 构建 MySqlCommand 对象(用来返回一个结果集，而不是一个整数值)
        /// </summary>
        /// <param name="connection">数据库连接</param>
        /// <param name="storedProcName">存储过程名</param>
        /// <param name="parameters">存储过程参数</param>
        /// <returns>MySqlCommand</returns>
        private static MySqlCommand BuildQueryCommand(MySqlConnection connection, string storedProcName, IDataParameter[] parameters)
        {
            MySqlCommand command = new MySqlCommand(storedProcName, connection);
            command.CommandType = CommandType.StoredProcedure;
            foreach (MySqlParameter parameter in parameters)
            {
                command.Parameters.Add(parameter);
            }
            return command;
        }

        /// <summary>
        /// 执行存储过程，返回影响的行数		
        /// </summary>
        /// <param name="storedProcName">存储过程名</param>
        /// <param name="parameters">存储过程参数</param>
        /// <param name="rowsAffected">影响的行数</param>
        /// <returns></returns>
        public static int RunProcedure(string storedProcName, IDataParameter[] parameters, out int rowsAffected)
        {
            using (MySqlConnection connection = new MySqlConnection(connectionString))
            {
                int result;
                connection.Open();
                MySqlCommand command = BuildIntCommand(connection, storedProcName, parameters);
                rowsAffected = command.ExecuteNonQuery();
                result = (int)command.Parameters["ReturnValue"].Value;
                //Connection.Close();
                return result;
            }
        }

        /// <summary>
        /// 创建 MySqlCommand 对象实例(用来返回一个整数值)	
        /// </summary>
        /// <param name="storedProcName">存储过程名</param>
        /// <param name="parameters">存储过程参数</param>
        /// <returns>MySqlCommand 对象实例</returns>
        private static MySqlCommand BuildIntCommand(MySqlConnection connection, string storedProcName, IDataParameter[] parameters)
        {
            MySqlCommand command = BuildQueryCommand(connection, storedProcName, parameters);
            command.Parameters.Add(new MySqlParameter("ReturnValue",
                MySqlDbType.Int32, 4, ParameterDirection.ReturnValue,
                false, 0, 0, string.Empty, DataRowVersion.Default, null));
            return command;
        }
        #endregion

        #region BaseModel开发需要
        private static IDbCommand GetCommand()
        {
            return new MySql.Data.MySqlClient.MySqlCommand();
        }

        /// <summary>
        /// 添加参数
        /// </summary>
        /// <param name="parameterName"></param>
        /// <param name="value"></param>
        /// <param name="dbType"></param>
        /// <param name="size"></param>
        /// <param name="direction"></param>
        /// <returns></returns>
        public static DbParameter AddParameters(string parameterName, object value, DbType dbType, int size, ParameterDirection direction)
        {
            parameterName = parameterName.Substring(0, 1) == "@" ? parameterName : "@" + parameterName;
            DbParameter para = null;

            para = new MySqlParameter();
            para.ParameterName = parameterName;
            para.Value = value;
            para.DbType = dbType;
            para.Direction = direction;
            if (dbType == DbType.DateTime)
            {
                //para.DbType = DbType.Date;
                para.DbType = DbType.String;
            }
            return para;
        }

        private static IDbConnection GetConnection()
        {
            return new MySqlConnection(connectionString);
        }

        private static IDataAdapter GetAdapater(string Sql, IDbConnection iConn)
        {
            return new MySqlDataAdapter(Sql, (MySqlConnection)iConn);
        }

        /// <summary>
        /// 根据table获取对象
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static List<T> ToList<T>(DataTable dt)
        {
            List<T> list = new List<T>();
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    T obj = (T)Activator.CreateInstance(typeof(T));
                    PropertyInfo[] pis = obj.GetType().GetProperties();
                    object propValue = null;
                    for (int i = 0; i < pis.Length; i++)
                    {
                        if (!dt.Columns.Contains(pis[i].Name))
                        {
                            continue;
                        }
                        Object[] attr = pis[i].GetCustomAttributes(false);
                        if (attr.Length > 0)
                        {
                            MyAttribute myattr = pis[i].GetCustomAttributes(false)[0] as MyAttribute;
                            if (myattr.NoUse == true || myattr.IsList == true)
                            {
                                continue;
                            }

                        }

                        propValue = row[pis[i].Name];
                        if (propValue == null || propValue == DBNull.Value)
                        {
                            continue;
                        }
                        pis[i].SetValue(obj, propValue, null);
                    }
                    list.Add(obj);
                }
            }
            return list;
        }

        public static List<object> ToList(DataTable dt, Type t)
        {
            List<object> list = new List<object>();
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    Object obj = t.Assembly.CreateInstance(t.FullName);
                    PropertyInfo[] pis = obj.GetType().GetProperties();
                    object propValue = null;
                    for (int i = 0; i < pis.Length; i++)
                    {
                        if (!dt.Columns.Contains(pis[i].Name))
                        {
                            continue;
                        }
                        Object[] attr = pis[i].GetCustomAttributes(false);
                        if (attr.Length > 0)
                        {
                            MyAttribute myattr = pis[i].GetCustomAttributes(false)[0] as MyAttribute;
                            if (myattr.NoUse == true)
                            {
                                continue;
                            }
                        }
                        propValue = row[pis[i].Name];
                        if (propValue == null || propValue == DBNull.Value)
                        {
                            continue;
                        }
                        pis[i].SetValue(obj, propValue, null);
                    }
                    list.Add(obj);
                }
            }
            return list;
        }

        /// <summary>
        /// 获取Insert语句
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="parentid"></param>
        /// <returns></returns>
        public static System.Data.IDbCommand GetInsertSql(object obj, int parentid, IDbConnection iConn, System.Data.IDbTransaction iTrans)
        {
            System.Data.IDbCommand iCmd = GetCommand();
            StringBuilder _TempSql = new StringBuilder();
            StringBuilder _TempSql2 = new StringBuilder();
            int i = 0;
            _TempSql.Append("insert into " + obj.GetType().Name + "(");
            _TempSql2.Append(") Values(");
            PropertyInfo[] pis = obj.GetType().GetProperties();
            for (; i < pis.Length; i++)
            {
                Object[] attr = pis[i].GetCustomAttributes(false);
                if (attr.Length > 0)
                {
                    MyAttribute myattr = pis[i].GetCustomAttributes(false)[0] as MyAttribute;
                    if (myattr.IsList == true || myattr.NoUse == true || myattr.NoInsert == true || (myattr.PrimaryKey == true && myattr.NoInsert == true))
                    {
                        continue;
                    }
                    if (myattr.IsNeedID)
                    {
                        _TempSql.Append("`" + pis[i].Name + "`,");
                        _TempSql2.Append("@" + pis[i].Name + ",");
                        iCmd.Parameters.Add(AddParameters("@" + pis[i].Name, parentid, DataType.GetDbType(pis[i].PropertyType), 0, System.Data.ParameterDirection.Input));
                        continue;
                    }
                }
                if (pis[i].GetValue(obj, null) == null)
                {
                    continue;
                }
                _TempSql.Append("`" + pis[i].Name + "`,");
                _TempSql2.Append("@" + pis[i].Name + ",");
                iCmd.Parameters.Add(AddParameters("@" + pis[i].Name, pis[i].GetValue(obj, null), DataType.GetDbType(pis[i].PropertyType), 0, System.Data.ParameterDirection.Input));
            }

            iCmd.CommandText = _TempSql.ToString().TrimEnd(',') + _TempSql2.ToString().TrimEnd(',') + ")";
            iCmd.Connection = iConn;
            iCmd.Transaction = iTrans;
            return iCmd;
        }

        /// <summary>
        /// 添加
        /// </summary>
        /// <typeparam name="T">类</typeparam>
        /// <param name="obj">对象</param>
        /// <param name="rowsAffected">影响行数</param>
        /// <returns></returns>
        public static int InsertSql<T>(T obj, out int rowsAffected)
        {
            System.Data.IDbCommand command = GetCommand();
            StringBuilder _TempSql = new StringBuilder();
            StringBuilder _TempSql2 = new StringBuilder();
            List<IDbCommand> sqlList = new List<IDbCommand>();
            List<object> templist = new List<Object>();

            #region 获取sql
            _TempSql.Append("insert into " + obj.GetType().Name + "(");
            _TempSql2.Append(") Values(");
            int i = 0;
            PropertyInfo[] pis = obj.GetType().GetProperties();
            for (; i < pis.Length; i++)
            {
                Object[] attr = pis[i].GetCustomAttributes(false);
                if (attr.Length > 0)
                {
                    MyAttribute myattr = pis[i].GetCustomAttributes(false)[0] as MyAttribute;
                    if (myattr.NoUse == true || myattr.NoInsert == true || (myattr.PrimaryKey == true && myattr.NoInsert == true))
                    {
                        continue;
                    }
                    if (myattr.IsList)
                    {
                        templist.Add(pis[i].GetValue(obj, null));
                        continue;
                    }
                }
                if (pis[i].GetValue(obj, null) == null)
                {
                    continue;
                }
                _TempSql.Append("`" + pis[i].Name + "`,");
                _TempSql2.Append("@" + pis[i].Name + ",");
                command.Parameters.Add(AddParameters("@" + pis[i].Name, pis[i].GetValue(obj, null), DataType.GetDbType(pis[i].PropertyType), 0, System.Data.ParameterDirection.Input));

            }
            string sql = _TempSql.ToString().TrimEnd(',') + _TempSql2.ToString().TrimEnd(',') + ")";
            #endregion

            using (System.Data.IDbConnection iConn = GetConnection())
            {
                int result = 0;
                iConn.Open();
                command.Connection = iConn;
                using (System.Data.IDbTransaction iDbTran = iConn.BeginTransaction())
                {
                    command.Transaction = iDbTran;
                    try
                    {
                        sql += ";select @@IDENTITY as returnName";
                        command.CommandText = sql;
                        rowsAffected = Convert.ToInt32(command.ExecuteScalar());
                        result = rowsAffected;

                        #region 获取列表的sql
                        if (templist != null && templist.Count > 0)
                        {
                            for (int o = 0; o < templist.Count; o++)
                            {
                                Object tobj = templist[o]; //tobj 得到的是List<class> 这种类型的
                                if (tobj != null)
                                {
                                    int count = (int)tobj.GetType().GetProperty("Count").GetValue(tobj, null);

                                    for (int j = 0; j < count; j++)
                                    {
                                        object ol = tobj.GetType().GetMethod("get_Item").Invoke(tobj, new object[] { j });
                                        sqlList.Add(GetInsertSql(ol, result, iConn, iDbTran));
                                    }

                                }
                            }
                        }
                        #endregion

                        #region 执行子sql
                        foreach (var v in sqlList)
                        {
                            if (v != null)
                            {
                                v.ExecuteNonQuery();
                            }
                        }
                        #endregion

                        iDbTran.Commit();
                        return result;
                    }
                    catch (Exception ex)
                    {
                        iDbTran.Rollback();
                        throw;
                    }

                }
            }
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="where"></param>
        /// <returns></returns>
        public static int DeleteSql(string name, string where)
        {
            string sql = "delete  from " + name + " Where " + where;
            using (System.Data.IDbConnection iConn = GetConnection())
            {
                iConn.Open();
                System.Data.IDbCommand command = GetCommand();
                command.Connection = iConn;
                command.CommandText = sql;
                int rowsAffected = command.ExecuteNonQuery();
                iConn.Close();
                command.Dispose();
                return rowsAffected;
            }
        }

        /// <summary>
        /// 修改
        /// </summary>
        /// <typeparam name="T">类</typeparam>
        /// <param name="obj">对象</param>
        /// <param name="rowsAffected">影响行数</param>
        /// <returns></returns>
        public static int UpdateSql<T>(T obj)
        {
            System.Data.IDbCommand command = GetCommand();
            StringBuilder _TempSql = new StringBuilder();
            StringBuilder _TempSql2 = new StringBuilder();
            List<IDbCommand> sqlList = new List<IDbCommand>();
            List<object> templist = new List<Object>();
            List<string> tempdeletesql = new List<string>();
            int parentid = 0;
            int result = 0;
            #region 获取sql
            _TempSql.Append("Update " + obj.GetType().Name + " set ");

            string where = "1=1 ";
            int i = 0;

            DbParameter idparameter = null;
            PropertyInfo[] pis = obj.GetType().GetProperties();
            for (; i < pis.Length; i++)
            {
                Object[] attr = pis[i].GetCustomAttributes(false);
                if (attr.Length > 0)
                {
                    MyAttribute myattr = pis[i].GetCustomAttributes(false)[0] as MyAttribute;
                    if (myattr.NoUse == true || myattr.NoUpdate == true || myattr.ListNoUpdate == true)
                    {
                        continue;
                    }
                    else if (myattr.PrimaryKey == true)
                    {
                        idparameter = AddParameters("@" + pis[i].Name, pis[i].GetValue(obj, null), DataType.GetDbType(pis[i].PropertyType), 4, System.Data.ParameterDirection.Input);
                        // command.Parameters.Add(AddParameters("@" + pis[i].Name, pis[i].GetValue(obj, null), DataType.GetDbType(pis[i].PropertyType), 4, System.Data.ParameterDirection.Input));
                        where += " and " + pis[i].Name + "=" + "@" + pis[i].Name;
                        parentid = (int)pis[i].GetValue(obj, null);
                        continue;
                    }
                    if (myattr.IsList)
                    {
                        #region 删除原有的
                        Type objLt = pis[i].PropertyType;
                        Type ot = Activator.CreateInstance(objLt, true).GetType().GetGenericArguments()[0];
                        Object ob2 = ot.Assembly.CreateInstance(ot.FullName);
                        PropertyInfo[] pis2 = ob2.GetType().GetProperties();
                        string sql = "delete  from " + ot.Name + " where ";
                        for (int j = 0; j < pis2.Length; j++)
                        {
                            if (pis2[j].GetCustomAttributes(false) != null && pis2[j].GetCustomAttributes(false).Length > 0)
                            {
                                MyAttribute myattr2 = pis2[j].GetCustomAttributes(false)[0] as MyAttribute;
                                if (myattr2.IsNeedID == true)
                                {
                                    sql += pis2[j].Name + "=" + parentid.ToString();
                                    tempdeletesql.Add(sql);
                                    break;
                                }
                            }
                        }
                        #endregion

                        templist.Add(pis[i].GetValue(obj, null));
                        continue;
                    }
                }
                //if (pis[i].GetValue(obj, null) == null)
                if (pis[i].GetValue(obj, null) == null || (pis[i].PropertyType.Name == "DateTime" && pis[i].GetValue(obj, null).ToString() == "0001/1/1 0:00:00"))
                {
                    continue;
                }
                command.Parameters.Add(AddParameters("@" + pis[i].Name, pis[i].GetValue(obj, null), DataType.GetDbType(pis[i].PropertyType), 0, System.Data.ParameterDirection.Input));
                _TempSql.Append("`" + pis[i].Name + "`=" + "@" + pis[i].Name + ",");

            }
            command.Parameters.Add(idparameter);
            _TempSql = _TempSql.Remove(_TempSql.Length - 1, 1);
            _TempSql.Append(" where " + where);
            #endregion

            using (System.Data.IDbConnection iConn = GetConnection())
            {

                iConn.Open();
                command.Connection = iConn;
                using (System.Data.IDbTransaction iDbTran = iConn.BeginTransaction())
                {
                    command.Transaction = iDbTran;
                    try
                    {
                        command.CommandText = _TempSql.ToString();
                        result = command.ExecuteNonQuery();
                        foreach (var v in tempdeletesql)
                        {
                            command.CommandText = v.ToString();
                            command.ExecuteNonQuery();
                        }

                        #region 获取列表的sql
                        if (templist != null && templist.Count > 0)
                        {
                            for (int o = 0; o < templist.Count; o++)
                            {
                                Object tobj = templist[o]; //tobj 得到的是List<class> 这种类型的
                                if (tobj != null)
                                {
                                    int count = (int)tobj.GetType().GetProperty("Count").GetValue(tobj, null);

                                    for (int j = 0; j < count; j++)
                                    {
                                        object ol = tobj.GetType().GetMethod("get_Item").Invoke(tobj, new object[] { j });
                                        sqlList.Add(GetInsertSql(ol, parentid, iConn, iDbTran));
                                    }

                                }
                            }
                        }
                        #endregion

                        #region 执行子sql
                        foreach (var v in sqlList)
                        {
                            if (v != null)
                            {
                                v.ExecuteNonQuery();
                            }
                        }
                        #endregion

                        iDbTran.Commit();
                        return result;
                    }
                    catch (Exception)
                    {
                        iDbTran.Rollback();
                        result = 0;
                        throw;
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// 获取对象
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="where">查询条件，可为NUll</param>
        /// <returns></returns>
        public static List<T> GetModelOrAllSql<T>(string where, string Order)
        {
            // T obj = default(T);
            List<T> obj = new List<T>();
            if (string.IsNullOrEmpty(where))
            {
                where = " 1=1 ";
            }
            string sql = "select * from " + typeof(T).Name + " where " + where;
            if (!string.IsNullOrEmpty(Order))
            {
                sql += " " + Order;
            }
            System.Data.IDbConnection iConn = GetConnection();
            IDataAdapter adapter = GetAdapater(sql, iConn);
            DataSet ds = new DataSet();
            adapter.Fill(ds);
            if (ds != null && ds.Tables.Count > 0)
            {
                obj = ToList<T>(ds.Tables[0]);
            }
            return obj;
        }


        #endregion

        #region Bulk相关
        /// <summary>
        /// 创建表结构(表结构已过时，请使用该方法下的方法GetTableSchema_ImportAddFlowcard)
        /// </summary>
        /// <returns></returns>
        public static DataTable GetTableSchema_Flowcard()
        {
            DataTable dt = new DataTable();
            dt.Columns.AddRange(new DataColumn[]{
                new DataColumn("id",typeof(String)),
                new DataColumn("cardnumber",typeof(String)),
                new DataColumn("deadline",typeof(DateTime)),
                new DataColumn("allflow",typeof(int)),
                new DataColumn("usedflow",typeof(int)),
                new DataColumn("leftflow",typeof(int)),
                new DataColumn("price",typeof(Decimal)),
                new DataColumn("state",typeof(int)),
                new DataColumn("addtime",typeof(DateTime)),
                new DataColumn("customerid",typeof(Int32)),
                new DataColumn("userid",typeof(Int32)),
                new DataColumn("isdel",typeof(Int32)),
                new DataColumn("initflow",typeof(int)),
                new DataColumn("imsi",typeof(String)),
                new DataColumn("iccid",typeof(String)),
                new DataColumn("activedeadline",typeof(DateTime)),
                new DataColumn("stoptime",typeof(DateTime)),
                new DataColumn("origin",typeof(String)),
                new DataColumn("contactname",typeof(String)),
                new DataColumn("contactmobile",typeof(String)),
                new DataColumn("contactplatform",typeof(String)),
                new DataColumn("contactcustomername",typeof(String)),
                new DataColumn("contactremark",typeof(String)),
                new DataColumn("dailyImportTypeid",typeof(Int32)),
                new DataColumn("rechargeTypePriceTemplateID",typeof(Int32))
                }
            );
            return dt;
        }

        /// <summary>
        /// 总表导入时使用的表结构
        /// </summary>
        /// <returns></returns>
        public static DataTable GetTableSchema_ImportAddFlowcard()
        {
            DataTable dt = new DataTable();
            dt.Columns.AddRange(new DataColumn[]{
                new DataColumn("id",typeof(String)),
                new DataColumn("cardnumber",typeof(String)),
                new DataColumn("deadline",typeof(DateTime)),
                new DataColumn("allflow",typeof(int)),
                new DataColumn("usedflow",typeof(int)),
                new DataColumn("leftflow",typeof(int)),
                new DataColumn("price",typeof(Decimal)),
                new DataColumn("state",typeof(int)),
                new DataColumn("addtime",typeof(DateTime)),
                new DataColumn("customerid",typeof(Int32)),
                new DataColumn("userid",typeof(Int32)),
                new DataColumn("isdel",typeof(Int32)),
                new DataColumn("initflow",typeof(int)),
                new DataColumn("imsi",typeof(String)),
                new DataColumn("iccid",typeof(String)),
                new DataColumn("activedeadline",typeof(DateTime)),
                new DataColumn("stoptime",typeof(DateTime)),
                new DataColumn("origin",typeof(String)),
                new DataColumn("contactname",typeof(String)),
                new DataColumn("contactmobile",typeof(String)),
                new DataColumn("contactplatform",typeof(String)),
                new DataColumn("contactcustomername",typeof(String)),
                new DataColumn("contactremark",typeof(String)),
                new DataColumn("dailyImportTypeid",typeof(Int32)),
                new DataColumn("rechargeTypePriceTemplateID",typeof(Int32)),
                new DataColumn("serialnumber",typeof(String)),
                new DataColumn("opentime",typeof(DateTime)),
                new DataColumn("totalPayment",typeof(Decimal)),
                new DataColumn("balance",typeof(Decimal)),
                new DataColumn("testPeriodDeadline",typeof(DateTime)),
                new DataColumn("quitePeriodDeadline",typeof(DateTime)),
                new DataColumn("totalCost",typeof(Decimal)),
                new DataColumn("activeState",typeof(Int32)),
                new DataColumn("balanceUpdateTime",typeof(DateTime))
                }
            );
            return dt;
        }

        /// <summary>
        /// bulk方法主要思想是通过在客户端把数据都缓存在Table中，然后利用SqlBulkCopy一次性把Table中的数据插入到数据库 
        /// </summary>
        /// <param name="dt"></param>
        public static void BulkToDB_Flowcard(DataTable dt)
        {
            using (MySqlConnection conn = new MySqlConnection(connectionString))
            {
                try
                {
                    conn.Open();
                    //tran = conn.BeginTransaction();  
                    MySqlBulkLoader bulk = new MySqlBulkLoader(conn)
                    {
                        //FieldTerminator = ",",
                        //FieldQuotationCharacter = '"',
                        //EscapeCharacter = '"',
                        //LineTerminator = @"\n",
                        //FileName = filePath,
                        //NumberOfLinesToSkip = 1,
                        //TableName = "ips"
                    };
                    //bulk.Columns.AddRange(table.Columns.Cast<DataColumn>().Select(colum => colum.ColumnName).ToArray());  
                    int count = bulk.Load();
                    // tran.Commit();  
                }
                catch (MySql.Data.MySqlClient.MySqlException ex)
                {
                    throw new Exception(ex.Message);
                }
            }

            //MySqlConnection sqlConn = new MySqlConnection(
            //ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
            //SqlBulkCopy bulkCopy = new SqlBulkCopy(sqlConn);
            //bulkCopy.DestinationTableName = "flowcard";
            //bulkCopy.BatchSize = dt.Rows.Count;

            //try
            //{
            //    sqlConn.Open();
            //    if (dt != null && dt.Rows.Count != 0)
            //        bulkCopy.WriteToServer(dt);
            //}
            //catch (Exception ex)
            //{
            //    throw ex;
            //}
            //finally
            //{
            //    sqlConn.Close();
            //    if (bulkCopy != null)
            //        bulkCopy.Close();
            //}
        }

        //public static void BulkToDB_Flowcard(DataTable dt, string SQLCStrType)
        //{
        //    string SQLCStr = GetConnectionStringByType(SQLCStrType);
        //    MySqlConnection sqlConn = new MySqlConnection(SQLCStr);
        //    SqlBulkCopy bulkCopy = new SqlBulkCopy(sqlConn);
        //    bulkCopy.DestinationTableName = "flowcard";
        //    bulkCopy.BatchSize = dt.Rows.Count;

        //    try
        //    {
        //        sqlConn.Open();
        //        if (dt != null && dt.Rows.Count != 0)
        //            bulkCopy.WriteToServer(dt);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {
        //        sqlConn.Close();
        //        if (bulkCopy != null)
        //            bulkCopy.Close();
        //    }
        //}
        #endregion

    }
}
