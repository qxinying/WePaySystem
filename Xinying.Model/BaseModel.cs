﻿using System;
using System.Collections.Generic;
using System.Text;
using Xinying.DBUtility;
using System.Data;
using Xinying.Common;
using MySql.Data.MySqlClient;

namespace Xinying.Model
{
    public class BaseModel<T>
    {
        public static string DefaultDbType = "Mysql";
        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static int Add(object obj)
        {
            int rowaffect = 0;
            #region 不同数据库选择不同的库支持操作
            if (DefaultDbType == "Mysql")
            {
                return DbHelperMySql.InsertSql(obj, out rowaffect);
            }
            else
            {
                return DbHelperSQL.InsertSql(obj, out rowaffect);
            }
            #endregion
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="id">主键，ID</param>
        /// <returns></returns>
        public static bool Delete(int id)
        {
            string name = typeof(T).Name;
            int rows;
            #region 不同数据库选择不同的库支持操作
            if (DefaultDbType == "Mysql")
            {
                rows = DbHelperMySql.DeleteSql(name, " ID=" + id);
            }
            else
            {
                rows = DbHelperSQL.DeleteSql(name, " ID=" + id);
            }
            #endregion
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static bool Update(object obj)
        {
            int result;
            #region 不同数据库选择不同的库支持操作
            if (DefaultDbType == "Mysql")
            {
                result = DbHelperMySql.UpdateSql(obj);
            }
            else
            {
                result = DbHelperSQL.UpdateSql(obj);
            }
            #endregion
            if (result > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 获取单个对象
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="id"></param>
        /// <returns></returns>
        public static T GetModel(int id)
        {
            string where = string.Empty;
            if (id > 0)
            {
                where = " ID=" + id;
            }
            else
            {
                return default(T);
            }
            List<T> list;
            #region 不同数据库选择不同的库支持操作
            if (DefaultDbType == "Mysql")
            {
                list = DbHelperMySql.GetModelOrAllSql<T>(where, null);
            }
            else
            {
                list = DbHelperSQL.GetModelOrAllSql<T>(where, null);
            }
            #endregion

            if (list != null && list.Count > 0)
            {
                return list[0];
            }
            else
            {
                return default(T);
            }
        }
        //获取活动对象
        public static T GetActivity(string _where)
        {
            string where = string.Empty;
            where = _where;
            List<T> list;
            #region 不同数据库选择不同的库支持操作
            if (DefaultDbType == "Mysql")
            {
                list = DbHelperMySql.GetModelOrAllSql<T>(where, null);
            }
            else
            {
                list = DbHelperSQL.GetModelOrAllSql<T>(where, null);
            }
            #endregion
            if (list != null && list.Count > 0)
            {
                return list[0];
            }
            else
            {
                return default(T);
            }
        }

        /// <summary>
        /// 获取全部对象
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static List<T> GetALLModel()
        {
            List<T> list;
            #region 不同数据库选择不同的库支持操作
            if (DefaultDbType == "Mysql")
            {
                list = DbHelperMySql.GetModelOrAllSql<T>("", null);
            }
            else
            {
                list = DbHelperSQL.GetModelOrAllSql<T>("", null);
            }
            #endregion
            if (list != null && list.Count > 0)
            {
                return list;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 获取单个对象
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="id"></param>
        /// <returns></returns>
        public static T GetSelectModel(string selectwhere)
        {
            string where = string.Empty;
            if (!string.IsNullOrEmpty(selectwhere))
            {
                where = selectwhere;
            }
            else
            {
                return default(T);
            }
            List<T> list;
            #region 不同数据库选择不同的库支持操作
            if (DefaultDbType == "Mysql")
            {
                list = DbHelperMySql.GetModelOrAllSql<T>(where, null);
            }
            else
            {
                list = DbHelperSQL.GetModelOrAllSql<T>(where, null);
            }
            #endregion
            if (list != null && list.Count > 0)
            {
                return list[0];
            }
            else
            {
                return default(T);
            }
        }

        public static T GetSelectModel(string selectwhere, string orderstr)
        {
            string where = string.Empty;
            if (!string.IsNullOrEmpty(selectwhere))
            {
                where = selectwhere;
            }
            else
            {
                return default(T);
            }
            List<T> list;
            #region 不同数据库选择不同的库支持操作
            if (DefaultDbType == "Mysql")
            {
                list = DbHelperMySql.GetModelOrAllSql<T>(where, orderstr);
            }
            else
            {
                list = DbHelperSQL.GetModelOrAllSql<T>(where, orderstr);
            }
            #endregion
            if (list != null && list.Count > 0)
            {
                return list[0];
            }
            else
            {
                return default(T);
            }
        }

        /// <summary>
        /// 获取最大值
        /// </summary>
        /// <returns></returns>
        public static int GetMaxId()
        {
            string name = typeof(T).Name;
            #region 不同数据库选择不同的库支持操作
            if (DefaultDbType == "Mysql")
            {
                return DbHelperMySql.GetMaxID("id", name);
            }
            else
            {
                return DbHelperSQL.GetMaxID("id", name);
            }
            #endregion
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public static bool Exists(int id)
        {
            string name = typeof(T).Name;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from " + name);
            strSql.Append(" where id=" + id);
            #region 不同数据库选择不同的库支持操作
            if (DefaultDbType == "Mysql")
            {
                return DbHelperMySql.Exists(strSql.ToString());
            }
            else
            {
                return DbHelperSQL.Exists(strSql.ToString());
            }
            #endregion
        }

        public static bool Exists(string where)
        {
            string name = typeof(T).Name;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from " + name);
            if (!string.IsNullOrEmpty(where))
            {
                strSql.Append(" where " + where);
            }
            #region 不同数据库选择不同的库支持操作
            if (DefaultDbType == "Mysql")
            {
                return DbHelperMySql.Exists(strSql.ToString());
            }
            else
            {
                return DbHelperSQL.Exists(strSql.ToString());
            }
            #endregion
        }

        public static bool ExistsWhere(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            string name = typeof(T).Name;
            strSql.Append("select count(1) from " + name);
            if (!string.IsNullOrEmpty(strWhere))
            {
                strSql.Append(" where " + strWhere);
            }
            #region 不同数据库选择不同的库支持操作
            if (DefaultDbType == "Mysql")
            {
                return DbHelperMySql.Exists(strSql.ToString());
            }
            else
            {
                return DbHelperSQL.Exists(strSql.ToString());
            }
            #endregion
        }

        /// <summary>
        /// 返回数据总数(分页用到)
        /// </summary>
        public static int GetCount(string strWhere)
        {
            string name = typeof(T).Name;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(*) as H ");
            strSql.Append(" from  " + name);
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            #region 不同数据库选择不同的库支持操作
            if (DefaultDbType == "Mysql")
            {
                return Convert.ToInt32(DbHelperMySql.GetSingle(strSql.ToString()));
            }
            else
            {
                return Convert.ToInt32(DbHelperSQL.GetSingle(strSql.ToString()));
            }
            #endregion
        }

        /// <summary>
        /// 删除多条数据
        /// </summary>
        public static bool DeleteList(string idlist)
        {
            string name = typeof(T).Name;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from  " + name);
            strSql.Append(" where id in (" + idlist + ")  ");
            int rows;
            #region 不同数据库选择不同的库支持操作
            if (DefaultDbType == "Mysql")
            {
                rows = DbHelperMySql.ExecuteSql(strSql.ToString());
            }
            else
            {
                rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            }
            #endregion
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public static DataSet GetList(string strWhere)
        {
            string name = typeof(T).Name;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select *");
            strSql.Append(" FROM  " + name);
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            #region 不同数据库选择不同的库支持操作
            if (DefaultDbType == "Mysql")
            {
                return DbHelperMySql.Query(strSql.ToString());
            }
            else
            {
                return DbHelperSQL.Query(strSql.ToString());
            }
            #endregion
        }

        /// <summary>
        /// 获取对象列表
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public static List<T> GetObjectList(string strWhere)
        {
            string name = typeof(T).Name;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select *");
            strSql.Append(" FROM  " + name);
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            DataSet ds;
            #region 不同数据库选择不同的库支持操作
            if (DefaultDbType == "Mysql")
            {
                ds = DbHelperMySql.Query(strSql.ToString());
            }
            else
            {
                ds = DbHelperSQL.Query(strSql.ToString());
            }
            #endregion
            if (ds != null && ds.Tables.Count > 0)
            {
                #region 不同数据库选择不同的库支持操作
                if (DefaultDbType == "Mysql")
                {
                    return DbHelperMySql.ToList<T>(ds.Tables[0]);
                }
                else
                {
                    return DbHelperSQL.ToList<T>(ds.Tables[0]);
                }
                #endregion
            }
            else
            {
                return null;
            }
        }


        /// <summary>
        /// 获取对象列表
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public static List<T> GetObjectListO(string strWhere, string strOrder)
        {
            string name = typeof(T).Name;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select *");
            strSql.Append(" FROM  " + name);
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            if (strOrder.Trim() != "")
            {
                strSql.Append(" order by " + strOrder);
            }

            DataSet ds;
            #region 不同数据库选择不同的库支持操作
            if (DefaultDbType == "Mysql")
            {
                ds = DbHelperMySql.Query(strSql.ToString());
            }
            else
            {
                ds = DbHelperSQL.Query(strSql.ToString());
            }
            #endregion
            if (ds != null && ds.Tables.Count > 0)
            {
                #region 不同数据库选择不同的库支持操作
                if (DefaultDbType == "Mysql")
                {
                    return DbHelperMySql.ToList<T>(ds.Tables[0]);
                }
                else
                {
                    return DbHelperSQL.ToList<T>(ds.Tables[0]);
                }
                #endregion
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 获取对象列表
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public static List<T> GetObjectListO(int Top, string strWhere, string strOrder)
        {
            string name = typeof(T).Name;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            #region 不同数据库选择不同的库支持操作
            if (DefaultDbType == "Mysql")
            {

            }
            else
            {
                if (Top > 0)
                {
                    strSql.Append(" top " + Top.ToString());
                }
            }
            #endregion
            strSql.Append(" *");
            strSql.Append(" FROM  " + name);
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            if (strOrder.Trim() != "")
            {
                strSql.Append(" order by " + strOrder);
            }
            #region 不同数据库选择不同的库支持操作
            if (DefaultDbType == "Mysql")
            {
                if (Top > 0)
                {
                    strSql.Append(" limit " + Top.ToString());
                }
            }
            #endregion

            DataSet ds;
            #region 不同数据库选择不同的库支持操作
            if (DefaultDbType == "Mysql")
            {
                ds = DbHelperMySql.Query(strSql.ToString());
            }
            else
            {
                ds = DbHelperSQL.Query(strSql.ToString());
            }
            #endregion
            if (ds != null && ds.Tables.Count > 0)
            {
                #region 不同数据库选择不同的库支持操作
                if (DefaultDbType == "Mysql")
                {
                    return DbHelperMySql.ToList<T>(ds.Tables[0]);
                }
                else
                {
                    return DbHelperSQL.ToList<T>(ds.Tables[0]);
                }
                #endregion
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 获取对象列表
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public static List<T> GetObjectListO(int Top,string strFiled,string strWhere, string strOrder)
        {
            string name = typeof(T).Name;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            #region 不同数据库选择不同的库支持操作
            if (DefaultDbType == "Mysql")
            {

            }
            else
            {
                if (Top > 0)
                {
                    strSql.Append(" top " + Top.ToString());
                }
            }
            #endregion
            if (string.IsNullOrEmpty(strFiled))
            {
                strSql.Append(" * ");
            }
            else
            {
                strSql.Append(strFiled);
            }
            strSql.Append(" FROM  " + name);
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            if (strOrder.Trim() != "")
            {
                strSql.Append(" order by " + strOrder);
            }
            #region 不同数据库选择不同的库支持操作
            if (DefaultDbType == "Mysql")
            {
                if (Top > 0)
                {
                    strSql.Append(" limit " + Top.ToString());
                }
            }
            #endregion

            DataSet ds;
            #region 不同数据库选择不同的库支持操作
            if (DefaultDbType == "Mysql")
            {
                ds = DbHelperMySql.Query(strSql.ToString());
            }
            else
            {
                ds = DbHelperSQL.Query(strSql.ToString());
            }
            #endregion
            if (ds != null && ds.Tables.Count > 0)
            {
                #region 不同数据库选择不同的库支持操作
                if (DefaultDbType == "Mysql")
                {
                    return DbHelperMySql.ToList<T>(ds.Tables[0]);
                }
                else
                {
                    return DbHelperSQL.ToList<T>(ds.Tables[0]);
                }
                #endregion
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public static DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            string name = typeof(T).Name;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            #region 不同数据库选择不同的库支持操作
            if (DefaultDbType == "Mysql")
            {

            }
            else
            {
                if (Top > 0)
                {
                    strSql.Append(" top " + Top.ToString());
                }
            }
            #endregion
            strSql.Append(" * ");
            strSql.Append(" FROM  " + name);
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            #region 不同数据库选择不同的库支持操作
            if (DefaultDbType == "Mysql")
            {
                if (Top > 0)
                {
                    strSql.Append(" limit " + Top.ToString());
                }
            }
            #endregion
            #region 不同数据库选择不同的库支持操作
            if (DefaultDbType == "Mysql")
            {
                return DbHelperMySql.Query(strSql.ToString());
            }
            else
            {
                return DbHelperSQL.Query(strSql.ToString());
            }
            #endregion
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public static List<T> GetObjectList(int Top, string strWhere, string filedOrder)
        {
            string name = typeof(T).Name;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            #region 不同数据库选择不同的库支持操作
            if (DefaultDbType == "Mysql")
            {

            }
            else
            {
                if (Top > 0)
                {
                    strSql.Append(" top " + Top.ToString());
                }
            }
            #endregion
            strSql.Append(" * ");
            strSql.Append(" FROM  " + name);
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            #region 不同数据库选择不同的库支持操作
            if (DefaultDbType == "Mysql")
            {
                if (Top > 0)
                {
                    strSql.Append(" limit " + Top.ToString());
                }
            }
            #endregion
            DataSet ds;
            #region 不同数据库选择不同的库支持操作
            if (DefaultDbType == "Mysql")
            {
                ds = DbHelperMySql.Query(strSql.ToString());
            }
            else
            {
                ds = DbHelperSQL.Query(strSql.ToString());
            }
            #endregion
            if (ds != null && ds.Tables.Count > 0)
            {
                #region 不同数据库选择不同的库支持操作
                if (DefaultDbType == "Mysql")
                {
                    return DbHelperMySql.ToList<T>(ds.Tables[0]);
                }
                else
                {
                    return DbHelperSQL.ToList<T>(ds.Tables[0]);
                }
                #endregion
            }
            else
            {
                return null;
            }
        }
        /// <summary>
        /// 获得查询分页数据
        /// </summary>
        public static DataSet GetList(int pageSize, int pageIndex, string strWhere, string filedOrder, out int recordCount)
        {
            string name = typeof(T).Name;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * FROM " + name);
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            #region 不同数据库选择不同的库支持操作
            if (DefaultDbType == "Mysql")
            {
                recordCount = Convert.ToInt32(DbHelperMySql.GetSingle(PagingHelper.CreateCountingSql(strSql.ToString())));
            }
            else
            {
                recordCount = Convert.ToInt32(DbHelperSQL.GetSingle(PagingHelper.CreateCountingSql(strSql.ToString())));
            }
            #endregion
            #region 不同数据库选择不同的库支持操作
            if (DefaultDbType == "Mysql")
            {
                return DbHelperMySql.Query(PagingHelper.CreatePagingSql(recordCount, pageSize, pageIndex, strSql.ToString(), filedOrder));
            }
            else
            {
                return DbHelperSQL.Query(PagingHelper.CreatePagingSql(recordCount, pageSize, pageIndex, strSql.ToString(), filedOrder));
            }
            #endregion
        }

        public static List<T> GetPageList(int pageSize, int pageIndex, string strWhere, string filedOrder, out int recordCount)
        {
            string name = typeof(T).Name;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select * FROM " + name);
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            DataSet ds;
            #region 不同数据库选择不同的库支持操作
            if (DefaultDbType == "Mysql")
            {
                recordCount = Convert.ToInt32(DbHelperMySql.GetSingle(PagingHelper.CreateCountingSql(strSql.ToString())));
                ds = DbHelperMySql.Query(PagingHelper.CreatePagingSql(recordCount, pageSize, pageIndex, strSql.ToString(), filedOrder));
            }
            else
            {
                recordCount = Convert.ToInt32(DbHelperSQL.GetSingle(PagingHelper.CreateCountingSql(strSql.ToString())));
                ds = DbHelperSQL.Query(PagingHelper.CreatePagingSql(recordCount, pageSize, pageIndex, strSql.ToString(), filedOrder));
            }
            #endregion
            DataTable dt = ds.Tables[0];
            List<T> list = new List<T>();
            if (dt != null && dt.Rows.Count > 0)
            {
                #region 不同数据库选择不同的库支持操作
                if (DefaultDbType == "Mysql")
                {
                    list = DbHelperMySql.ToList<T>(dt);
                }
                else
                {
                    list = DbHelperSQL.ToList<T>(dt);
                }
                #endregion
            }
            if (list != null && list.Count > 0)
            {
                return list;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 修改一列数据
        /// </summary>
        public static int UpdateField(int Id, string strValue)
        {
            string name = typeof(T).Name;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update " + name + " set " + strValue);
            strSql.Append(" where Id=" + Id);
            #region 不同数据库选择不同的库支持操作
            if (DefaultDbType == "Mysql")
            {
                return DbHelperMySql.ExecuteSql(strSql.ToString());
            }
            else
            {
                return DbHelperSQL.ExecuteSql(strSql.ToString());
            }
            #endregion
        }

        /// <summary>
        /// 获取包括扩展字段的table
        /// </summary>
        /// <param name="fieldname"></param>
        /// <param name="Top"></param>
        /// <param name="strWhere"></param>
        /// <param name="filedOrder"></param>
        /// <returns></returns>
        public static DataTable GetListEf(string fieldname, int Top, string strWhere, string filedOrder)
        {
            string tablename = typeof(T).Name;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            #region 不同数据库选择不同的库支持操作
            if (DefaultDbType == "Mysql")
            {

            }
            else
            {
                if (Top > 0)
                {
                    strSql.Append(" top " + Top.ToString());
                }
            }
            #endregion
            strSql.AppendFormat(" * from {0},{1}", tablename, fieldname);
            strSql.AppendFormat(" where {0}.id={1}.pid", tablename, fieldname);
            if (strWhere.Trim() != "")
            {
                strSql.Append(" and " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            #region 不同数据库选择不同的库支持操作
            if (DefaultDbType == "Mysql")
            {
                if (Top > 0)
                {
                    strSql.Append(" limit " + Top.ToString());
                }
            }
            #endregion
            DataSet ds;
            #region 不同数据库选择不同的库支持操作
            if (DefaultDbType == "Mysql")
            {
                ds = DbHelperMySql.Query(strSql.ToString());
            }
            else
            {
                ds = DbHelperSQL.Query(strSql.ToString());
            }
            #endregion
            if (ds != null && ds.Tables.Count > 0)
            {
                return ds.Tables[0];
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 根据ID获取包括扩展字段行
        /// </summary>
        /// <param name="id"></param>
        /// <param name="fieldname"></param>
        /// <returns></returns>
        public static DataRow GetRowEf(int id, string fieldname)
        {
            string tablename = typeof(T).Name;
            StringBuilder strSql = new StringBuilder();
            strSql.AppendFormat("select  * from {0},{1}", tablename, fieldname);
            strSql.AppendFormat(" where {0}.id={1}.pid", tablename, fieldname);
            strSql.AppendFormat(" and {0}.id={1}", tablename, id);
            DataSet ds;
            #region 不同数据库选择不同的库支持操作
            if (DefaultDbType == "Mysql")
            {
                ds = DbHelperMySql.Query(strSql.ToString());
            }
            else
            {
                ds = DbHelperSQL.Query(strSql.ToString());
            }
            #endregion
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                return ds.Tables[0].Rows[0];
            }
            else
            {
                return null;
            }
        }


        /// <summary>
        /// 获得查询分页数据包括扩展字段
        /// </summary>
        public static DataSet GetListEf(string fieldname, int pageSize, int pageIndex, string strWhere, string filedOrder, out int recordCount)
        {
            string tablename = typeof(T).Name;
            StringBuilder strSql = new StringBuilder();
            strSql.AppendFormat("select  * from {0},{1}", tablename, fieldname);
            strSql.AppendFormat(" where {0}.id={1}.pid", tablename, fieldname);
            if (strWhere.Trim() != "")
            {
                strSql.Append(" and " + strWhere);
            }
            #region 不同数据库选择不同的库支持操作
            if (DefaultDbType == "Mysql")
            {
                recordCount = Convert.ToInt32(DbHelperMySql.GetSingle(PagingHelper.CreateCountingSql(strSql.ToString())));
                return DbHelperMySql.Query(PagingHelper.CreatePagingSql(recordCount, pageSize, pageIndex, strSql.ToString(), filedOrder));
            }
            else
            {
                recordCount = Convert.ToInt32(DbHelperSQL.GetSingle(PagingHelper.CreateCountingSql(strSql.ToString())));
                return DbHelperSQL.Query(PagingHelper.CreatePagingSql(recordCount, pageSize, pageIndex, strSql.ToString(), filedOrder));
            }
            #endregion
        }

        /// <summary>
        /// 获得查询分页数据 空白模型
        /// </summary>
        public static DataSet GetListBlank(string fieldname, int pageSize, int pageIndex, string strWhere, string filedOrder, out int recordCount)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.AppendFormat("select  * from {0}", fieldname);
            strSql.AppendFormat(" where 1=1 ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" and " + strWhere);
            }
            #region 不同数据库选择不同的库支持操作
            if (DefaultDbType == "Mysql")
            {
                recordCount = Convert.ToInt32(DbHelperMySql.GetSingle(PagingHelper.CreateCountingSql(strSql.ToString())));
                return DbHelperMySql.Query(PagingHelper.CreatePagingSql(recordCount, pageSize, pageIndex, strSql.ToString(), filedOrder));
            }
            else
            {
                recordCount = Convert.ToInt32(DbHelperSQL.GetSingle(PagingHelper.CreateCountingSql(strSql.ToString())));
                return DbHelperSQL.Query(PagingHelper.CreatePagingSql(recordCount, pageSize, pageIndex, strSql.ToString(), filedOrder));
            }
            #endregion
        }

        /// <summary>
        /// 获取空白模板
        /// </summary>
        /// <param name="id"></param>
        /// <param name="fieldname"></param>
        /// <returns></returns>
        public static DataRow GetRowblank(int id, string tablename)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.AppendFormat("select  * from {0}", tablename);
            strSql.AppendFormat(" where fid={0}", id);
            DataSet ds;
            #region 不同数据库选择不同的库支持操作
            if (DefaultDbType == "Mysql")
            {
                ds = DbHelperMySql.Query(strSql.ToString());
            }
            else
            {
                ds = DbHelperSQL.Query(strSql.ToString());
            }
            #endregion
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                return ds.Tables[0].Rows[0];
            }
            else
            {
                return null;
            }
        }


        /// <summary>
        /// 获取总计SUM
        /// </summary>
        /// <param name="fieldname"></param>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public static object GetSum(string fieldname, string strWhere)
        {
            string tablename = typeof(T).Name;
            string sql = "select sum(" + fieldname + ") from " + tablename;
            if (!string.IsNullOrEmpty(strWhere))
            {
                sql += " where " + strWhere;
            }
            object obj;
            #region 不同数据库选择不同的库支持操作
            if (DefaultDbType == "Mysql")
            {
                obj = DbHelperMySql.GetSingle(sql);
            }
            else
            {
                obj = DbHelperSQL.GetSingle(sql);
            }
            #endregion
            if (obj == null)
            {
                obj = 0;
            }
            return obj;
        }

        /// <summary>
        /// 获取平均值AVG
        /// </summary>
        /// <param name="fieldname"></param>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public static object GetAvg(string fieldname, string strWhere)
        {
            string tablename = typeof(T).Name;
            string sql = "select AVG(" + fieldname + ") from " + tablename;
            if (!string.IsNullOrEmpty(strWhere))
            {
                sql += " where " + strWhere;
            }
            object obj;
            #region 不同数据库选择不同的库支持操作
            if (DefaultDbType == "Mysql")
            {
                obj = DbHelperMySql.GetSingle(sql);
            }
            else
            {
                obj = DbHelperSQL.GetSingle(sql);
            }
            #endregion
            if (obj == null)
            {
                obj = 0;
            }
            return obj;
        }

        /// <summary>
        /// 生成唯一的编号
        /// </summary>
        /// <param name="user_id"></param>
        /// <param name="content"></param>
        /// <returns></returns>
        public static string CreateOrderNo(int num, string para_title)
        {
            string name = typeof(T).Name;
            string result = string.Empty;
            int count = 0;

            do
            {
                result = Utils.GetCheckCode(num);
                StringBuilder strSql = new StringBuilder();
                strSql.Append("select count(*) as H ");
                strSql.Append(" from  " + name);
                strSql.Append(" where " + para_title + "='" + result + "'");
                #region 不同数据库选择不同的库支持操作
                if (DefaultDbType == "Mysql")
                {
                    count = Convert.ToInt32(DbHelperMySql.GetSingle(strSql.ToString()));
                }
                else
                {
                    count = Convert.ToInt32(DbHelperSQL.GetSingle(strSql.ToString()));
                }
                #endregion
            }
            while (count > 0);

            return result;
        }
    }
}
