﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Xinying.DBUtility;
using Xinying.Common;
namespace Xinying.Model
{
    //第三方调用者
    [Serializable]
    public class apiuser : BaseModel<apiuser>
    {
        #region  Model
        /// <summary>
        /// id
        /// </summary>
        [MyAttribute(PrimaryKey = true, NoInsert = true)]
        public int id { get; set; }
        /// <summary>
        /// 应用id（16位，小写字母和数字的组合）
        /// </summary>
        public string appid { get; set; }
        /// <summary>
        /// 应用秘钥（32位，小写字母和数字的组合）
        /// </summary>
        public string appsecret { get; set; }
        /// <summary>
        /// 第三方开发者名称
        /// </summary>
        public string apiusername { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        public int sort_id { get; set; }
        #endregion

        #region Method
        /// <summary>
        /// 根据appkey获取对应的model
        /// </summary>
        /// <param name="_AppKey"></param>
        /// <returns></returns>
        public static Model.apiuser GetModelByAppKey(object _AppKey)
        {
            string AppKey = _AppKey.ToStr();
            if (!string.IsNullOrEmpty(AppKey))
            {
                Model.apiuser ApiUserModel = Model.apiuser.GetSelectModel($"appid='{AppKey}'", "order by Id asc");
                return ApiUserModel;
            }
            return null;
        }

        /// <summary>
        /// 根据id获取对应的第三方调用者名称
        /// </summary>
        /// <param name="_id"></param>
        /// <returns></returns>
        public static string GetApiUserName(object _id)
        {
            string result = "";
            int id = _id.ToInt();
            if (id > 0)
            {
                Model.apiuser model = Model.apiuser.GetModel(id);
                if (model != null)
                {
                    result = model.apiusername;
                }
            }
            return result;
        }

        /// <summary>
        /// 根据id获取对应的第三方调用者appsecret
        /// </summary>
        /// <param name="_id"></param>
        /// <returns></returns>
        public static string GetApiAppSecret(object _id)
        {
            string result = "";
            int id = _id.ToInt();
            if (id > 0)
            {
                Model.apiuser model = Model.apiuser.GetModel(id);
                if (model != null)
                {
                    result = model.appsecret;
                }
            }
            return result;
        }
        #endregion
    }
}