﻿using System;
using System.Collections.Generic;
using System.Text;
using Xinying.DBUtility;
using System.Data;
using Xinying.Common;
using Xinying.Model;
using LitJson;
using System.Collections;


namespace Xinying.Model
{
    public class wepayorder : BaseModel<wepayorder>
    {
        #region 属性
        /// <summary>
        /// 自增id
        /// </summary>
        public int id { get; set; }
        /// <summary>
        /// 第三方系统订单号（由api调用方传入）
        /// 因为SysOrderNo要求28位，所以OrderNo要求10位
        /// </summary>
        public string orderno { get; set; }
        /// <summary>
        /// 本系统订单号=第三方系统订单号+时间戳（yyyyMMddHHmmssffff）
        /// 微信商户订单号（不超过32位）=本系统订单号+(parseInt(Math.random() * 10000)).toString()，故此参数不能超过28位
        /// 为了回调逻辑顺畅，故确定此参数为28位
        /// </summary>
        public string sys_orderno { get; set; }
        /// <summary>
        /// 微信订单号
        /// </summary>
        public string tradeno { get; set; }
        /// <summary>
        /// 支付方式 0：未知 1.管理员支付 2.微信支付 
        /// </summary>
        public int paymentid { get; set; }
        /// <summary>
        /// 实际支付金额(单位：分)
        /// </summary>
        public int realamount { get; set; }
        /// <summary>
        /// 添加时间
        /// </summary>
        public DateTime addtime { get; set; }
        #region api调用方相关信息
        /// <summary>
        /// 调用方id
        /// </summary>
        public int apiuser_id { get; set; }
        /// <summary>
        /// 调用方名称
        /// </summary>
        public string apiuser_name { get; set; }
        /// <summary>
        /// 调用方异步回调地址
        /// </summary>
        public string apiuser_notifyurl { get; set; }
        /// <summary>
        /// 调用方同步回调地址
        /// </summary>
        public string apiuser_returnurl { get; set; }
        #endregion
        #region 后期与第三方进行分润统计时需要的扩展字段
        /// <summary>
        /// 支付手续费(单位：元)
        /// </summary>
        public decimal paymentfee { get; set; }
        /// <summary>
        /// 支付状态1未支付2已支付
        /// </summary>
        public int paymentstatus { get; set; }
        /// <summary>
        /// 支付时间
        /// </summary>
        public DateTime paymenttime { get; set; }
        /// <summary>
        /// 请求的微信支付版本
        /// </summary>
        public string wx_version { get; set; }
        #region 支付回调通知
        //同步回调==》第三方同步回调地址
        //异步回调==》如果没有响应过的第三方跳转到第三方异步回调地址，已响应的忽略
        //异步通知次数：总共会发起10次通知（Global通知），通知频率为15s/15s/30s/3m/10m/20m/30m/30m/30m/60m/3h/3h/3h/6h/6h - 总计 24h4m，和微信一样，第三方返回“SUCCESS”之后不再异步通知
        /// <summary>
        /// 调用方响应状态 0：未响应 1：已响应 2：已过期（多次发送后仍未响应的，不再发送）
        /// </summary>
        public int apiuser_resstatus { get; set; }
        /// <summary>
        /// 上次通知时间
        /// </summary>
        public DateTime apiuser_lastnoticetime { get; set; }
        /// <summary>
        /// 通知次数
        /// </summary>
        public int apiuser_noticecount { get; set; }
        /// <summary>
        /// global中用于异步通知的完整地址，于异步返回操作中进行更新
        /// </summary>
        public string apiuser_global_notifyurl { get; set; }
        #endregion
        #endregion
        #endregion

        #region Method
        /// <summary>
        /// 根据id获取对应的回调通知信息
        /// </summary>
        /// <param name="_id"></param>
        /// <returns></returns>
        public static string GetApiUserResInfo(object _id)
        {
            string result = "";
            int id = _id.ToInt();
            if (id > 0)
            {
                Model.wepayorder model = Model.wepayorder.GetModel(id);
                if (model != null)
                {
                    if (model.paymentstatus == 2)//已支付
                    {
                        if (model.apiuser_resstatus == 1)//已响应
                        {
                            result = "已响应<br />";
                        }
                        else if (model.apiuser_resstatus == 2)//已过期
                        {
                            result = "未响应已过期(通知期限为支付起24小时)<br />";
                        }
                        else//未响应
                        {
                            result = "暂未响应<br />";
                        }
                        if (model.apiuser_noticecount > 0)
                        {
                            result += "通知次数：" + model.apiuser_noticecount + "<br />";
                            result += "上次通知时间：" + model.apiuser_lastnoticetime.ToDateTimeStrExcept1900();
                        }
                        else
                        {
                            result += "暂未通知";
                        }
                    }
                    else
                    {
                        result = "未支付";
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// 根据id获取对应的调用方信息
        /// </summary>
        /// <param name="_id"></param>
        /// <returns></returns>
        public static string GetApiUserInfo(object _id)
        {
            string result = "";
            int id = _id.ToInt();
            if (id > 0)
            {
                Model.wepayorder model = Model.wepayorder.GetModel(id);
                if (model != null)
                {
                    if (model.apiuser_id > 0)
                    {
                        result = "调用方：";
                        if (string.IsNullOrEmpty(model.apiuser_name))
                        {
                            result += Model.apiuser.GetApiUserName(model.apiuser_id) + "<br />";
                        }
                        else
                        {
                            result += model.apiuser_name + "<br />";
                        }
                        result += "异步回调地址：" + model.apiuser_notifyurl + "<br />";
                        result += "同步回调地址：" + model.apiuser_returnurl;
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// 根据id获取对应的支付信息
        /// </summary>
        /// <param name="_id"></param>
        /// <returns></returns>
        public static string GetPayInfo(object _id)
        {
            string result = "";
            int id = _id.ToInt();
            if (id > 0)
            {
                Model.wepayorder model = Model.wepayorder.GetModel(id);
                if (model != null)
                {
                    if (model.paymentstatus == 2)//已支付
                    {
                        result = "已支付<br />";
                        if (model.paymentfee > 0)
                        {
                            result += "支付手续费：" + model.paymentfee + "元<br />";
                        }
                        result += "支付方式：" + GetPaymentWay(model.paymentid) + "<br />";
                        if (model.paymentid == 2)//微信支付
                        {
                            result += "微信订单号：" + model.tradeno + "<br />";
                        }
                        result += "支付时间：" + model.paymenttime.ToDateTimeStr();
                    }
                    else//未支付
                    {
                        result = "未支付";
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// 根据apiuser_resstatus获取回调通知状态
        /// </summary>
        /// <param name="_payment_id"></param>
        /// <returns></returns>
        public static string GetResDesc(object _apiuser_resstatus)
        {
            string result = "未响应";
            int apiuser_resstatus = _apiuser_resstatus.ToInt();
            switch (apiuser_resstatus)
            {
                case 1:
                    result = "已响应";
                    break;
                case 2:
                    result = "已过期";
                    break;
            }
            return result;
        }

        /// <summary>
        /// 更新通知信息
        /// </summary>
        /// <param name="_SysOrderNo"></param>
        public static void UpdateNoticeInfo(object _SysOrderNo)
        {
            string SysOrderNo = _SysOrderNo.ToStr();
            Model.wepayorder WePayOrderModel = Model.wepayorder.GetModelBySysOrderNo(SysOrderNo);
            if (WePayOrderModel != null)
            {
                WePayOrderModel.apiuser_noticecount++;
                WePayOrderModel.apiuser_lastnoticetime = DateTime.Now;
                Model.wepayorder.UpdateField(WePayOrderModel.id, $"apiuser_noticecount={WePayOrderModel.apiuser_noticecount},apiuser_lastnoticetime='{WePayOrderModel.apiuser_lastnoticetime}'");
            }
        }

        /// <summary>
        /// 根据响应信息更新响应状态信息
        /// </summary>
        /// <param name="_SysOrderNo"></param>
        /// <param name="_Res"></param>
        public static void UpdateResInfoByRes(object _SysOrderNo, object _Res)
        {
            string SysOrderNo = _SysOrderNo.ToStr();
            string Res = _Res.ToStr();
            Model.wepayorder WePayOrderModel = Model.wepayorder.GetModelBySysOrderNo(SysOrderNo);
            if (WePayOrderModel != null)
            {
                if (Res == "SUCCESS")//成功响应，更改响应状态
                {
                    WePayOrderModel.apiuser_resstatus = 1;
                    Model.wepayorder.UpdateField(WePayOrderModel.id, "apiuser_resstatus=1");
                }
            }
        }

        /// <summary>
        /// 根据id获取对应的订单号
        /// </summary>
        /// <param name="_id"></param>
        /// <returns></returns>
        public static string GetOrderNo(Object _Id)
        {
            string result = "";
            int Id = _Id.ToInt();
            if (Id > 0)
            {
                Model.wepayorder model = Model.wepayorder.GetModel(Id);
                if (model != null)
                {
                    result = model.orderno;
                }
            }
            return result;
        }

        /// <summary>
        /// 根据payment_id获取支付方式中文表述
        /// </summary>
        /// <param name="_payment_id"></param>
        /// <returns></returns>
        public static string GetPaymentWay(object _payment_id)
        {
            string result = "未知";
            int payment_id = _payment_id.ToInt();
            switch (payment_id)
            {
                case 1:
                    result = "管理员确认";
                    break;
                case 2:
                    result = "微信支付";
                    break;
            }
            return result;
        }

        /// <summary>
        /// 根据OrderNo获取model
        /// </summary>
        /// <param name="_orderno"></param>
        /// <returns></returns>
        public static Model.wepayorder GetModelBySN(object _orderno)
        {
            if (_orderno != null)
            {
                Model.wepayorder model = Model.wepayorder.GetSelectModel(" orderno='" + _orderno.ToStr() + "'", "order by id asc");
                return model;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 根据sys_orderno获取model
        /// </summary>
        /// <param name="_sys_orderno"></param>
        /// <returns></returns>
        public static Model.wepayorder GetModelBySysOrderNo(object _sys_orderno)
        {
            if (_sys_orderno != null)
            {
                Model.wepayorder model = Model.wepayorder.GetSelectModel(" sys_orderno='" + _sys_orderno.ToStr() + "'", "order by id asc");
                return model;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 管理后台订单掉单处理时获取的订单信息
        /// </summary>
        /// <param name="model_order"></param>
        /// <returns></returns>
        public static string GetOrderInfo(Model.wepayorder model_order)
        {
            string result = "";
            if (model_order != null)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendFormat("基础信息：<br />原订单号：{0}<br />本系统订单号：{1}<br />支付金额(单位：元)：{2}<br />创建时间：{3}<br /><br />", model_order.orderno, model_order.sys_orderno, Math.Round(model_order.realamount / (decimal)100.00, 2), model_order.addtime.ToDateTimeStr());
                sb.AppendFormat("支付信息：{0}<br /><br />", GetPayInfo(model_order.id));
                sb.AppendFormat("调用方信息：{0}<br /><br />", GetApiUserInfo(model_order.id));
                sb.AppendFormat("回调通知信息：{0}", GetApiUserResInfo(model_order.id));
                result = sb.ToString();
            }
            return result;
        }

        #region 在线支付完成后的逻辑
        #region 微信支付
        /// <summary>
        /// 需要将log文件夹的写入权限打开！！
        /// 1.检测订单状态 2.核对订单信息 3.更改订单状态（包含支付方式、微信订单号）
        /// <param name="_OrderNo">系统订单号</param>
        /// <param name="_Money">以分为单位！！！</param>
        /// <param name="_TradeNo">微信订单号</param>
        /// <returns>返回值：0.默认未知状态 1.该订单已支付 2.和实际支付的金额不相符 3.订单不存在或已删除 4.微信支付事务执行失败 5.成功</returns>
        /// </summary>
        private static object LockThisDBTansaction_WxPay = new object();
        public static JsonData PaySuccessWithWxPay_DBTransaction(object _OrderNo, int _Money, string _TradeNo)
        {
            string OrderNo = _OrderNo.ToStr();
            JsonData JD;
            Model.wepayorder OrderModel = Model.wepayorder.GetModelBySysOrderNo(OrderNo);
            if (OrderModel != null)
            {
                Model.siteconfig siteconfig = Model.siteconfig.loadConfig();
                #region 加锁 防止重复操作
                lock (LockThisDBTansaction_WxPay)
                {
                    if (OrderModel.paymentstatus == 2)
                    {
                        #region 构建json对象并返回
                        JD = new JsonData();
                        JD["state"] = 1;
                        JD["msg"] = "该订单已支付";
                        JD["msgen"] = "The order has been paid";
                        return JD;
                        #endregion
                    }
                    else
                    {
                        if (OrderModel.realamount != _Money)
                        {
                            //写调试日志
                            System.IO.File.AppendAllText(Xinying.Common.Utils.GetMapPath("~/log/paylog/wepay/wepaylogF_" + DateTime.Now.ToString("yyyyMMdd") + ".txt"), "网站订单：" + OrderNo + "，金额：" + Math.Round((decimal)_Money / (decimal)100.00, 2) + "，错误信息：订单支付金额异常\r\n", System.Text.Encoding.UTF8);
                            #region 构建json对象并返回
                            JD = new JsonData();
                            JD["state"] = 2;
                            JD["msg"] = "和实际支付的金额不相符";
                            JD["msgen"] = "It is not in accordance with the amount of the actual payment";
                            return JD;
                            #endregion
                        }
                        #region 更新订单状态（包含支付方式、微信订单号）
                        string NowStr = DateTime.Now.ToDateTimeStr();
                        ArrayList sqllist = new ArrayList();
                        sqllist.Add($"update wepayorder set paymentstatus=2,paymenttime='{NowStr}',paymentid=2,tradeno='{_TradeNo}' where id={OrderModel.id}");

                        bool Re = DbHelperMySql.ExecuteSqlTran(sqllist);
                        if (Re)
                        {
                            #region 构建json对象并返回
                            JD = new JsonData();
                            JD["state"] = 5;
                            return JD;
                            #endregion
                        }
                        else
                        {
                            //写调试日志
                            System.IO.File.AppendAllText(Xinying.Common.Utils.GetMapPath("~/log/paylog/wepay/wepaylogF_" + DateTime.Now.ToString("yyyyMMdd") + ".txt"), "网站订单：" + OrderNo + "，金额：" + Math.Round((decimal)_Money / (decimal)100.00, 2) + "，错误信息：微信支付回调事务执行失败\r\n", System.Text.Encoding.UTF8);
                            #region 构建json对象并返回
                            JD = new JsonData();
                            JD["state"] = 4;
                            JD["msg"] = "微信支付回调事务执行失败";
                            JD["msgen"] = "Failure to execute the WePay payment transaction";
                            return JD;
                            #endregion
                        }
                        #endregion
                    }
                }
                #endregion
            }
            else
            {
                //写调试日志
                System.IO.File.AppendAllText(Xinying.Common.Utils.GetMapPath("~/log/paylog/wepay/wepaylogF_" + DateTime.Now.ToString("yyyyMMdd") + ".txt"), "网站订单：" + OrderNo + "，金额：" + Math.Round((decimal)_Money / (decimal)100.00, 2) + "，错误信息：订单不存在或已删除\r\n", System.Text.Encoding.UTF8);
                #region 构建json对象并返回
                JD = new JsonData();
                JD["state"] = 3;
                JD["msg"] = "订单不存在或已删除";
                JD["msgen"] = "The order does not exist or has been deleted";
                return JD;
                #endregion
            }
        }
        #endregion

        #region 管理员操作
        /// <summary>
        /// 需要将log文件夹的写入权限打开！！
        /// 1.检测订单状态 2.核对订单信息 3.更改订单状态（包含支付方式）
        /// <param name="_OrderNo"></param>
        /// <returns>返回值：0.默认未知状态 1.该订单已支付 2.和实际支付的金额不相符 3.订单不存在或已删除 4.管理员确认支付事务执行失败 5.成功</returns>
        /// </summary>
        private static object LockThisDBTansaction_Admin = new object();
        public static JsonData PaySuccessWithAdmin_DBTransaction(object _OrderNo, int _Money, int _OnlinePayway)
        {
            string OrderNo = _OrderNo.ToStr();
            JsonData JD;
            Model.wepayorder OrderModel = Model.wepayorder.GetModelBySysOrderNo(OrderNo);
            if (OrderModel != null)
            {
                Model.siteconfig siteconfig = Model.siteconfig.loadConfig();
                #region 加锁 防止重复操作
                lock (LockThisDBTansaction_Admin)
                {
                    if (OrderModel.paymentstatus == 2)
                    {
                        #region 构建json对象并返回
                        JD = new JsonData();
                        JD["state"] = 1;
                        JD["msg"] = "该订单已支付";
                        JD["msgen"] = "The order has been paid";
                        return JD;
                        #endregion
                    }
                    else
                    {
                        if (OrderModel.realamount != _Money)
                        {
                            //写调试日志
                            System.IO.File.AppendAllText(Xinying.Common.Utils.GetMapPath("~/log/paylog/wepay/wepaylogF_" + DateTime.Now.ToString("yyyyMMdd") + ".txt"), "网站订单：" + OrderNo + "，金额：" + Math.Round((decimal)_Money / (decimal)100.00, 2) + "，错误信息：订单支付金额异常\r\n", System.Text.Encoding.UTF8);
                            #region 构建json对象并返回
                            JD = new JsonData();
                            JD["state"] = 2;
                            JD["msg"] = "和实际支付的金额不相符";
                            JD["msgen"] = "It is not in accordance with the amount of the actual payment";
                            return JD;
                            #endregion
                        }
                        #region 更新订单状态（包含支付方式）
                        #region 相关数据计算
                        string NowStr = DateTime.Now.ToDateTimeStr();
                        #endregion
                        ArrayList sqllist = new ArrayList();
                        #region 订单产品相关变化
                        #region 更新订单状态（包含支付方式）
                        sqllist.Add($"update wepayorder set paymentstatus=2,paymenttime='{NowStr}',paymentid={_OnlinePayway} where id={OrderModel.id}");
                        #endregion
                        #endregion
                        bool rowsAffected = DbHelperMySql.ExecuteSqlTran(sqllist);
                        if (rowsAffected)
                        {
                            #region 构建json对象并返回
                            JD = new JsonData();
                            JD["state"] = 5;
                            return JD;
                            #endregion
                        }
                        else
                        {
                            //写调试日志
                            System.IO.File.AppendAllText(Xinying.Common.Utils.GetMapPath("~/log/paylog/wepay/wepaylogF_" + DateTime.Now.ToString("yyyyMMdd") + ".txt"), "网站订单：" + OrderNo + "，金额：" + Math.Round((decimal)_Money / (decimal)100.00, 2) + "，错误信息：管理员确认支付事务执行失败\r\n", System.Text.Encoding.UTF8);
                            #region 构建json对象并返回
                            JD = new JsonData();
                            JD["state"] = 4;
                            JD["msg"] = "管理员确认支付事务执行失败";
                            JD["msgen"] = "Failure to execute the Admin's payment transaction";
                            return JD;
                            #endregion
                        }
                        #endregion
                    }
                }
                #endregion
            }
            else
            {
                //写调试日志
                System.IO.File.AppendAllText(Xinying.Common.Utils.GetMapPath("~/log/paylog/paylogF_" + DateTime.Now.ToString("yyyyMMdd") + ".txt"), "网站订单：" + OrderNo + "，金额：" + Math.Round((decimal)_Money / (decimal)100.00, 2) + "，错误信息：订单不存在或已删除\r\n", System.Text.Encoding.UTF8);
                #region 构建json对象并返回
                JD = new JsonData();
                JD["state"] = 3;
                JD["msg"] = "订单不存在或已删除";
                JD["msgen"] = "The order does not exist or has been deleted";
                return JD;
                #endregion
            }
        }
        #endregion
        #endregion
        #endregion
    }
}
