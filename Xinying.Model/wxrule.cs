﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Xinying.DBUtility;
using Xinying.Common;
namespace Xinying.Model
{
    //微信规则
    [Serializable]
    public class wxrule : BaseModel<wxrule>
    {
        #region  Model
        /// <summary>
        /// id
        /// </summary>
        [MyAttribute(PrimaryKey = true, NoInsert = true)]
        public int id { get; set; }
        /// <summary>
        /// 标题
        /// </summary>
        public string title { get; set; }
        /// <summary>
        /// 关键词，多个用“,”分隔(要前后“,”)
        /// </summary>
        public string keystr { get; set; }
        /// <summary>
        /// 回复素材id拼接，用“,”分隔(要前后“,”)(多图文)
        /// </summary>
        public string replystr { get; set; }
        /// <summary>
        /// 素材id （文字 及 单图文）
        /// </summary>
        public int materialid { get; set; }
        /// <summary>
        /// 规则类别 1：单图文 2：文字 3：多图文
        /// </summary>
        public int ruletype { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        public int sortid { get; set; }
        /// <summary>
        /// 0.关键词匹配 1.默认回复 2.关注事件 
        /// </summary>
        public int position { get; set; }
        #endregion

        #region Method
        public static List<Model.wxrule> GetWXRules(string _keys)
        {
            List<Model.wxrule> list = new List<wxrule>();
            if (!string.IsNullOrEmpty(_keys))
            {
                list = Model.wxrule.GetObjectListO("keystr like '%," + _keys + ",%'", "sortid asc,id desc");
            }
            return list;
        }

        public static string GetRuleType(object _id)
        {
            string result = "单图文";
            int id = Utils.StrToInt(_id.ToString(), 0);
            if (id > 0)
            {
                Model.wxrule model = Model.wxrule.GetModel(id);
                if (model != null)
                {
                    switch (model.ruletype)
                    {
                        case 2: result = "文字";
                            break;
                        case 3: result = "多图文";
                            break;
                    }
                }
            }
            return result;
        }

        public static Model.wxrule GetModelByPostionID(int _postionid)
        {
            string where = (_postionid > 0 ? "position=" + _postionid : "");
            List<Model.wxrule> list = Model.wxrule.GetObjectListO(1, where, "sortid asc,id desc");
            if (list != null && list.Count > 0)
            {
                return list[0];
            }
            else
            {
                return null;
            }
        }
        #endregion
    }
}