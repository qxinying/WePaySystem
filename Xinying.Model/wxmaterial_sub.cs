﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Xinying.DBUtility;
using Xinying.Common;
namespace Xinying.Model
{
    //微信素材
    [Serializable]
    public class wxmaterial_sub : BaseModel<wxmaterial_sub>
    {
        #region  Model
        /// <summary>
        /// id
        /// </summary>
        [MyAttribute(PrimaryKey = true, NoInsert = true)]
        public int id { get; set; }
        /// <summary>
        /// 所属素材id
        /// </summary>
        public int wxmaterialid { get; set; }
        /// <summary>
        /// 标题
        /// </summary>
        public string title { get; set; }
        /// <summary>
        /// 图片
        /// </summary>
        public string imgurl { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        public int sortid { get; set; }
        /// <summary>
        /// 摘要
        /// </summary>
        public string brief { get; set; }
        /// <summary>
        /// 正文
        /// </summary>
        public string details { get; set; }
        /// <summary>
        /// 原文链接
        /// </summary>
        public string reurl { get; set; }
        /// <summary>
        /// 添加时间
        /// </summary>
        public DateTime addtime { get; set; }
        #endregion

        #region	method

        #endregion
    }
}