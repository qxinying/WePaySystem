﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Xinying.DBUtility;
using Xinying.Common;
namespace Xinying.Model
{
    //微信粉丝
    [Serializable]
    public class wxuserinfo : BaseModel<wxuserinfo>
    {
        #region  Model
        /// <summary>
        /// id
        /// </summary>
        [MyAttribute(PrimaryKey = true, NoInsert = true)]
        public int id { get; set; }
        public string openid { get; set; }
        public string nickname { get; set; }
        public int sex { get; set; }
        public string province { get; set; }
        public string city { get; set; }
        public string country { get; set; }
        public string headimgurl { get; set; }
        /// <summary>
        /// 对应表users的id
        /// </summary>
        public int user_id { get; set; }

        public DateTime addtime { get; set; }
        /// <summary>
        /// 当前子公众号在母公众号数据库里的记录id
        /// </summary>
        public int wxpublicid { get; set; }
        public string name { get; set; }
        public string mobile { get; set; }
        /// <summary>
        /// 是否关注 1：关注 0：未关注
        /// </summary>
        public int is_subscribe { get; set; }
        #endregion

        #region Method
        /// <summary>
        /// 根据openid获取数据库记录id
        /// </summary>
        /// <param name="_openid"></param>
        /// <returns></returns>
        public static int GetIdByOpenId(string _openid)
        {
            int result = 0;
            if (!string.IsNullOrEmpty(_openid))
            {
                Model.wxuserinfo model = Model.wxuserinfo.GetModelByOpenid(_openid);
                if (model != null)
                {
                    result = model.id;
                }
            }
            return result;
        }

        /// <summary>
        /// 根据id获取是否关注的标记
        /// </summary>
        /// <param name="_id"></param>
        /// <returns></returns>
        public static int GetIsSubscribe(object _id)
        {
            int result = 0;
            int id = _id.ToInt();
            if (id > 0)
            {
                Model.wxuserinfo model = Model.wxuserinfo.GetModel(id);
                if (model != null)
                {
                    result = model.is_subscribe;
                }
            }
            return result;
        }

        /// <summary>
        /// 根据wxuserinfo的信息更新对应users表的信息，如果没有对应的users表的记录，则新增记录
        /// </summary>
        /// <param name="_openid"></param>
        /// <returns></returns>
        public static bool UpdateUsersByOpenid(object _openid)
        {
            string openid = _openid.ToStr();
            if (!string.IsNullOrEmpty(openid))
            {
                Model.wxuserinfo WxUserModel = Model.wxuserinfo.GetModelByOpenid(openid);
                if (WxUserModel != null && WxUserModel.user_id > 0)
                {
                    Model.users UserModel = Model.users.GetModel(WxUserModel.user_id);
                    if (UserModel != null)
                    {
                        //更新
                        UserModel.avatar = WxUserModel.headimgurl;
                        UserModel.nick_name = WxUserModel.nickname;
                        UserModel.user_name = WxUserModel.nickname;
                        #region 默认设置
                        if (UserModel.birthday == null)
                        {
                            UserModel.birthday = DateTime.Parse("1900-01-01");
                        }
                        if (UserModel.reg_time == null)
                        {
                            UserModel.reg_time = DateTime.Now;
                        }
                        if (string.IsNullOrEmpty(UserModel.user_name))
                        {
                            UserModel.user_name = WxUserModel.nickname;
                        }
                        if (string.IsNullOrEmpty(UserModel.nick_name))
                        {
                            UserModel.nick_name = WxUserModel.nickname;
                        }
                        if (string.IsNullOrEmpty(UserModel.avatar))
                        {
                            UserModel.avatar = WxUserModel.headimgurl;
                        }
                        #endregion
                        if (Model.users.UpdateField(UserModel.id, "avatar='" + UserModel.avatar + "',nick_name='" + UserModel.nick_name + "',user_name='" + UserModel.user_name + "'") > 0)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        //新增
                        UserModel = new users();
                        UserModel.avatar = WxUserModel.headimgurl;
                        UserModel.nick_name = WxUserModel.nickname;
                        UserModel.user_name = WxUserModel.nickname;
                        #region 默认设置
                        UserModel.birthday = DateTime.Parse("1900-01-01");
                        UserModel.reg_time = DateTime.Now;
                        UserModel.user_name = WxUserModel.nickname;
                        UserModel.nick_name = WxUserModel.nickname;
                        UserModel.avatar = WxUserModel.headimgurl;
                        if (Model.users.Add(UserModel) > 0)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                        #endregion
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// 根据openid获取model
        /// </summary>
        /// <param name="_openid"></param>
        /// <returns></returns>
        public static Model.wxuserinfo GetModelByOpenid(string _openid)
        {
            if (string.IsNullOrEmpty(_openid))
            {
                return null;
            }
            else
            {
                string where = " openid='" + _openid + "'";
                List<Model.wxuserinfo> list = DbHelperMySql.GetModelOrAllSql<Model.wxuserinfo>(where, " order by id asc");
                if (list != null && list.Count > 0)
                {
                    return list[0];
                }
                else
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// 根据id获取昵称
        /// </summary>
        public static string GetNickname(object _id)
        {
            string result = "";
            if (_id != null)
            {
                int id = Utils.StrToInt(_id.ToString(), 0);
                if (id > 0)
                {
                    Model.wxuserinfo model = Model.wxuserinfo.GetModel(id);
                    if (model != null)
                    {
                        result = model.nickname;
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// 更新微会员信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static bool UpdateWXUInfo(Model.wxuserinfo model)
        {
            bool result = true;
            if (model != null)
            {
                if (!string.IsNullOrEmpty(model.openid))
                {
                    Model.wxuserinfo model_wxut = Model.wxuserinfo.GetModelByOpenid(model.openid);
                    if (model_wxut != null)//UPDATE
                    {
                        if (model_wxut.addtime == null)
                        {
                            model_wxut.addtime = DateTime.Now;
                        }
                        model_wxut.city = model.city;
                        model_wxut.country = model.country;
                        model_wxut.headimgurl = model.headimgurl;
                        model_wxut.nickname = model.nickname;
                        model_wxut.province = model.province;
                        model_wxut.sex = model.sex;
                        model_wxut.wxpublicid = model.wxpublicid;
                        if (Model.wxuserinfo.Update(model_wxut))
                        {
                            result = true;
                            return result;
                        }
                        else
                        {
                            result = false;
                            return result;
                        }
                    }
                    else//ADD
                    {
                        model_wxut = new wxuserinfo();
                        model_wxut.addtime = DateTime.Now;
                        model_wxut.city = model.city;
                        model_wxut.country = model.country;
                        model_wxut.headimgurl = model.headimgurl;
                        model_wxut.nickname = model.nickname;
                        model_wxut.province = model.province;
                        model_wxut.sex = model.sex;
                        model_wxut.wxpublicid = model.wxpublicid;
                        model_wxut.openid = model.openid;
                        if (Model.wxuserinfo.Add(model_wxut) > 0)
                        {
                            result = true;
                            return result;
                        }
                        else
                        {
                            result = false;
                            return result;
                        }
                    }
                }
            }
            return result;
        }

        public static Model.wxuserinfo GetLoginModel()
        {
            //检查Cookies
            string useropenidstr = Utils.GetCookie("Xinying", "QXUOPID");
            string usernamestr = Utils.GetCookie("Xinying", "QXUserName");
            if (!string.IsNullOrEmpty(useropenidstr) && (!string.IsNullOrEmpty(usernamestr)))
            {
                string wxuopenid = Common.DESEncrypt.Decrypt(useropenidstr);
                string username = Common.DESEncrypt.Decrypt(usernamestr);
                Model.wxuserinfo model_users = Model.wxuserinfo.GetModelByOpenid(wxuopenid);
                if (model_users != null)
                {
                    return model_users;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        public static int UpdateFieldByOpenID(string OpenId, string strValue)
        {
            string name = typeof(wxuserinfo).Name;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update " + name + " set " + strValue);
            strSql.Append(" where openid='" + OpenId + "'");
            return DbHelperMySql.ExecuteSql(strSql.ToString());
        }
        #endregion
    }
}