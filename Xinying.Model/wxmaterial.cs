﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Xinying.DBUtility;
using Xinying.Common;
namespace Xinying.Model
{
    //微信素材
    [Serializable]
    public class wxmaterial : BaseModel<wxmaterial>
    {
        #region  Model
        /// <summary>
        /// id
        /// </summary>
        [MyAttribute(PrimaryKey = true, NoInsert = true)]
        public int id { get; set; }
        /// <summary>
        /// 类型 1：单图文 2：文字 3：图片 4：多图文 默认：1
        /// </summary>
        public int type { get; set; }
        /// <summary>
        /// 标题
        /// </summary>
        public string title { get; set; }
        /// <summary>
        /// 关键词，多个用“,”分隔(要前后“,”)
        /// </summary>
        public string keystr { get; set; }
        /// <summary>
        /// 文字
        /// </summary>
        public string words { get; set; }
        /// <summary>
        /// 图片
        /// </summary>
        public string imgurl { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        public int sortid { get; set; }
        /// <summary>
        /// 状态 1：正常 2：禁用 默认:正常
        /// </summary>
        public int state { get; set; }
        /// <summary>
        /// 摘要
        /// </summary>
        public string brief { get; set; }
        /// <summary>
        /// 正文
        /// </summary>
        public string details { get; set; }
        /// <summary>
        /// 原文链接
        /// </summary>
        public string reurl { get; set; }
        /// <summary>
        /// 添加时间
        /// </summary>
        public DateTime addtime { get; set; }
        /// <summary>
        /// 是否开启正文链接 1：开启正文链接 2：直接跳转原文链接
        /// </summary>
        public int isdetails { get; set; }
        #endregion

        #region	method
        public static string GetType(object _id)
        {
            string result = "单图文";
            int id = Utils.StrToInt(_id.ToString(), 0);
            if (id > 0)
            {
                Model.wxmaterial model = Model.wxmaterial.GetModel(id);
                if (model != null)
                {
                    switch (model.type)
                    {
                        case 2: result = "文字";
                            break;
                        case 3: result = "图片";
                            break;
                        case 4: result = "多图文";
                            break;
                    }
                }
            }
            return result;
        }

        public static List<Model.wxmaterial> GetWXRules(string _keys)
        {
            List<Model.wxmaterial> list = new List<wxmaterial>();
            if (!string.IsNullOrEmpty(_keys))
            {
                list = Model.wxmaterial.GetObjectListO("state <>2 and keystr like '%," + _keys + ",%' ", "sortid asc,id desc");
            }
            return list;
        }
        #endregion
    }
}