using System;
using System.Text;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Xinying.DBUtility;
using Xinying.Common;
using System.Linq;

namespace Xinying.Model
{
    [Serializable]
    public class menu : BaseModel<menu>
    {
        #region  Model
        /// <summary>
        /// 自增ID
        /// </summary>
        [MyAttribute(PrimaryKey = true, NoInsert = true)]
        public int id { get; set; }
        /// <summary>
        /// 类型。0、click。1、view
        /// </summary>
        public int type { get; set; }
        /// <summary>
        /// 按钮名字
        /// </summary>
        public string title { get; set; }
        /// <summary>
        /// 内容。当type=0时，此内容为按钮的key值。否则为按钮值
        /// </summary>
        public string content { get; set; }
        /// <summary>
        /// 父节点ID
        /// </summary>
        public int parentid { get; set; }
        /// <summary>
        /// 排序id
        /// </summary>
        public int sortid { get; set; }
        /// <summary>
        /// 是否隐藏
        /// </summary>
        public int islock { get; set; }
        #endregion

        #region	method
        /// <summary>
        /// 根据用户ID，获取列表对象
        /// </summary>
        /// <param name="userid"></param>
        /// <returns></returns>
        public static List<menu> GetModelList()
        {
            string sql = "select * from menu where islock=0 order by parentid asc, sortid asc";
            DataTable dt = DbHelperMySql.Query(sql).Tables[0];
            if (dt.Rows.Count > 0)
            {
                return DbHelperMySql.ToList<menu>(dt);
            }
            else
                return null;
        }

        public static string GetMenuStr()
        {
            string temp = " {\"button\":[_node_]}";
            StringBuilder str = new StringBuilder();
            List<menu> list = GetModelList();
            if (list != null)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    var value = from u in list
                                where u.parentid == list[i].id
                                select u;
                    if (list[i].parentid == 0)
                    {
                        if (value.ToList().Count == 0)
                        {
                            str.Append(GetMenuStr(list[i]) + ",");
                        }
                        else
                        {
                            string temp1 = "{\"name\":\"" + list[i].title + "\",\"sub_button\":[_node_]}";
                            StringBuilder child = new StringBuilder();
                            foreach (var a in value)
                            {
                                child.Append(GetMenuStr(a) + ",");
                            }
                            str.Append(temp1.Replace("_node_", child.ToString().TrimEnd(',')) + ",");
                        }
                    }

                }

                return temp.Replace("_node_", str.ToString().TrimEnd(','));
            }
            else
            {
                return "";
            }
        }

        public static string GetParentTitle(int _par)
        {
            string sql = string.Format("select title from menu where id={0}", _par);
            object obj = DbHelperMySql.GetSingle(sql);
            if (obj != null)
            {
                return obj.ToString();
            }
            else
            {
                return "";
            }
        }
        /// <summary>
        /// 删除指定条件的数据
        /// </summary>
        /// <param name="str"></param>
        public static void DeleteByWhere(string str)
        {
            string sql = string.Format("delete from menu where {0}", str);
            DbHelperMySql.ExecuteSql(sql);
        }

        public static string GetKey(string name)
        {
            string sql = string.Format("select content from menu where mname='{0}'", name);
            object obj = DbHelperMySql.GetSingle(sql);
            return obj != null ? obj.ToString() : "";
        }
        private static string GetMenuStr(menu m)
        {
            StringBuilder str = new StringBuilder();
            if (m != null)
            {
                str.Append("{\"type\":\"" + (m.type == 0 ? "click" : "view") + "\",\"name\":\"" + m.title + "\", \"" + (m.type == 0 ? "key" : "url") + "\":\"" + m.content + "\"}");
                return str.ToString();
            }
            else
            {
                return "";
            }
        }

        #endregion

    }
}