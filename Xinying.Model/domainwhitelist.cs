﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Xinying.DBUtility;
using Xinying.Common;
namespace Xinying.Model
{
    //域名白名单（已弃用）
    [Serializable]
    public class domainwhitelist : BaseModel<domainwhitelist>
    {
        #region  Model
        /// <summary>
        /// id
        /// </summary>
        [MyAttribute(PrimaryKey = true, NoInsert = true)]
        public int id { get; set; }
        /// <summary>
        /// 关联的第三方开发者id
        /// </summary>
        public int apiuserid { get; set; }
        /// <summary>
        /// 域名
        /// </summary>
        public string domain { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        public int sort_id { get; set; }
        #endregion

        #region Method
       
        #endregion
    }
}