﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xinying.Common;
using LitJson;
namespace BaiduMapHelper
{

	public class SaveMap
	{
		/// <summary>
		/// ak
		/// </summary>
		private static string ak = "s7FAnhXHjNznXLumUjFBhNCV";
		/// <summary>
		/// 创建表（create geotable）接口   POST请求
		/// </summary>
		private static string createtableurl = "http://api.map.baidu.com/geodata/v2/geotable/create"; 
		/// <summary>
		/// 查询表（list geotable）接口 GET请求
		/// </summary>
		private static string querytableurl = "http://api.map.baidu.com/geodata/v2/geotable/list";
		/// <summary>
		/// 查询指定id表（detail geotable）接口 get请求
		/// </summary>
		private static string querytablebyid = "http://api.map.baidu.com/geodata/v2/geotable/detail";
		/// <summary>
		/// 查询指定id表（detail geotable）接口 get请求
		/// </summary>
		private static string updatetableurl = "http://api.map.baidu.com/geodata/v2/geotable/update";
		/// <summary>
		/// 删除表 post请求 注：当geotable里面没有有效数据时，才能删除geotable
		/// </summary>
		private static string deletetableurl = "http://api.map.baidu.com/geodata/v2/geotable/delete";
		/// <summary>
		/// 创建列 post请求 
		/// </summary>
		private static string createcolumnurl = "http://api.map.baidu.com/geodata/v2/column/create";
        /// <summary>
        /// 创建数据 post请求
        /// </summary>
        private static string createpoiurl = "http://api.map.baidu.com/geodata/v3/poi/create";
		/// <summary>
		/// 更新数据 post请求
		/// </summary>
		private static string updatepoiurl = "http://api.map.baidu.com/geodata/v3/poi/update";
		/// <summary>
		/// 创建表
		/// </summary>
		/// <returns>状态</returns>
		public static int CreateTable(string[] key,string[] value)
		{
			string returndata = Utils.HttpPost(createtableurl, GetStr(key,value));
			JsonData jd = (JsonData)JsonMapper.ToObject(returndata);
            if ((int)jd["status"]!=0)
            {
                return 0;
            }
			return (int)jd["id"];
		}
		/// <summary>
		/// 查询表
		/// </summary>
		/// <param name="data">参数</param>
		/// <returns>响应参数对象</returns>
		public static QueryTableParam QueryTableModel(string data)
		{
			string returndata = Utils.HttpGet(querytableurl+"?"+data);
			JsonData jd = (JsonData)JsonMapper.ToObject(returndata);
			QueryTableParam model = new QueryTableParam();
			model.status = (int)jd["status"];
			model.message = (string)jd["message"];
			if (model.status==0)
			{
				model.size = (int)jd["size"];
				JsonData geotables = (JsonData)jd["geotables"];
				List<geotables> list = new List<geotables>();
				foreach (JsonData item in geotables)
				{
					list.Add(
						new geotables
						{
							create_time = (string)item["create_time"],
							geotype = (int)item["geotype"],
							id = (int)item["id"],
							is_published = (int)item["is_published"],
							modify_time = (string)item["modify_time"],
							name = (string)item["name"]
						});
				}
				model.geotables = list;
			}
			return model;

		}
		/// <summary>
		/// 查询指定id表
		/// </summary>
		/// <param name="id">表id</param>
		/// <param name="ak">开发者ak</param>
		/// <returns>响应参数对象</returns>
		public static QueryTableParam QueryTableByID(int id,string ak)
		{
			string returndata = Utils.HttpGet(querytablebyid+"?"+string.Format("ak={0}&id={1}",ak,id));
			JsonData jd = (JsonData)JsonMapper.ToObject(returndata);
			QueryTableParam model = new QueryTableParam();
			model.status = (int)jd["status"];
			model.message = (string)jd["message"];
			if (model.status==0)
			{
				JsonData geotables = (JsonData)jd["geotable"];
				List<geotables> list = new List<geotables>();
				foreach (JsonData item in geotables)
				{
					list.Add(
						new geotables
						{
							create_time = (string)item["create_time"],
							geotype = (int)item["geotype"],
							id = (int)item["id"],
							is_published = (int)item["is_published"],
							modify_time = (string)item["modify_time"],
							name = (string)item["name"]
						});
				}
				model.geotables = list;
			}
			
			return model;
		}
		public static int UpdateTable(string data)
		{
			string returndata = Utils.HttpGet(updatetableurl+"?"+data);
			JsonData jd = (JsonData)JsonMapper.ToObject(returndata);
			return (int)jd["status"];
		}
		/// <summary>
		/// 创建列
		/// </summary>
		/// <param name="key"></param>
		/// <param name="value"></param>
		/// <returns></returns>
		public static int CreateColumn(string[] key, string[] value)
		{
			string returndata = Utils.HttpPost(createcolumnurl, GetStr(key, value));
			JsonData jd = (JsonData)JsonMapper.ToObject(returndata);
			if ((int)jd["status"] != 0)
			{
				return 0;
			} 
			return (int)jd["id"];
        }
        #region 位置数据（poi）管理
        /// <summary>
        /// 创建数据点
        /// </summary>
        /// <param name="key">参数</param>
        /// <param name="value">参数值（和参数一一对应）</param>
        /// <returns>状态，0表示成功</returns>
        public static int CreatePoi(string[] key,string[] value)
        {
            string returndata = Utils.HttpPost(createpoiurl, GetStr(key, value));
            JsonData jd = (JsonData)JsonMapper.ToObject(returndata);
            if ((int)jd["status"] != 0)
            {
                return 0;
            }
            return (int)jd["id"];
        }

		public static bool UpdatePoi(string[] key, string[] value)
		{
			string returndata = Utils.HttpPost(updatepoiurl, GetStr(key, value));
			JsonData jd = (JsonData)JsonMapper.ToObject(returndata);
			if ((int)jd["status"] == 0)
			{
				return true;
			}
			return false;
		}
        #endregion
        #region 私有方法
        private static string GetStr(string[] key,string[] value)
        {
            StringBuilder str = new StringBuilder();
            for (int i = 0; i < key.Length; i++)
            {
                str.AppendFormat("{0}={1}&",key[i],value[i]);
            }
            return str.ToString().TrimEnd('&');
        }
        #endregion

    }
}
