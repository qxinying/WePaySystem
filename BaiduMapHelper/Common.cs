﻿using Xinying.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LitJson;

namespace BaiduMapHelper
{
	public class Common
	{
		/// <summary>
		/// 转换为百度坐标
		/// </summary>
		public static string ConvertToBD(string x, string y)
		{
			string returnstr = Utils.HttpGet(string.Format("http://api.map.baidu.com/ag/coord/convert?from=2&to=4&x={0}&y={1}", x, y));
			JsonData jd = JsonMapper.ToObject(returnstr);
			if (((int)jd["error"]) == 0)
			{
				return Utils.GetDecding((string)jd["y"]) + "," + Utils.GetDecding((string)jd["x"]);
			}
			else
			{
				return "";
			}
		}
		public static string staticimage(string center, List<string> location, int height, int width, int zoom)
		{
			string str = "http://api.map.baidu.com/staticimage?center={0}&width={1}&height={2}&zoom={3}&markers={4}&markerStyles={5}";
			StringBuilder style = new StringBuilder();
			StringBuilder locations = new StringBuilder();
			for (int i = 0; i < location.Count; i++)
			{
				locations.AppendFormat("{0}|", location[i]);
				style.AppendFormat("l,{0}|", i.ToString());

			}
			return string.Format(str, center, width, height, zoom, Utils.UrlEncode(locations.ToString().TrimEnd('|')), Utils.UrlEncode(style.ToString().TrimEnd('|')));

		}
		public static List<illegal> GetModelList(string[] keys, string[] values)
		{
			string returndata = Utils.HttpPost("http://m.46644.com/tool/illegal/?act=query", GetStr(keys, values));
			JsonData jd = (JsonData)JsonMapper.ToObject(returndata);
			if ((string)jd["error"]=="0")
			{
				JsonData jdata = (JsonData)jd["data"];
				List<illegal> ill = new List<illegal>();
				foreach (JsonData item in jdata)
				{
					ill.Add(new illegal()
					{
						address = (string)item["address"],
						time = (string)item["time"],
						content = (string)item["content"],
						price = (string)item["price"],
						score = (string)item["score"]
					});
				}
				return ill;
			}
			else
			{
				return null;
			}
		}
		#region 私有方法
		private static string GetStr(string[] key, string[] value)
		{
			StringBuilder str = new StringBuilder();
			for (int i = 0; i < key.Length; i++)
			{
				str.AppendFormat("{0}={1}&", key[i], value[i]);
			}
			return str.ToString().TrimEnd('&');
		}
		#endregion
        /// <summary>
        /// 根据IP定位
        /// </summary>
        /// <returns>json数据</returns>
        public static string GetJsonIp(string ak)
        {
            return Utils.HttpGet("http://api.map.baidu.com/location/ip?coor=bd09ll&ak=" + ak);
        }
        //获取城市名
        public static string GetCityByIp(string ak)
        {
            JsonData jd = (JsonData)JsonMapper.ToObject(GetJsonIp(ak));
            if ((int)jd["status"]==0)
            {
                string address = (string)jd["address"];
                if (address.Split('|').Length > 2)
                {
                    return address.Split('|')[2];
                }
            }
            return "";
            
        }
        public static JsonData getlocation(string ak)
        {
            JsonData jd = (JsonData)JsonMapper.ToObject(GetJsonIp(ak));
            if ((int)jd["status"]==0)
            {
                return jd["content"]["point"];
            }
            return "";
            
        }

	}
}
