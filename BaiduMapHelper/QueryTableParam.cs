﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BaiduMapHelper
{
	public class QueryTableParam
	{
		/// <summary>
		/// 状态码
		/// </summary>
		public int status { get; set; }
		/// <summary>
		/// 响应的信息
		/// </summary>
		public string message { get; set; }
		/// <summary>
		/// 数据个数
		/// </summary>
		public int size { get; set; }
		/// <summary>
		/// geotable列表
		/// </summary>
		public List<geotables> geotables { get; set; }

	}
	public class geotables
	{
		/// <summary>
		/// geotable的主键
		/// </summary>
		public int id { get; set; }
		/// <summary>
		/// 1：点；2：线；3：面。默认为1（当前只支持点）。
		/// </summary>
		public int geotype { get; set; }
		/// <summary>
		/// geotable的名称
		/// </summary>
		public string name { get; set; }
		/// <summary>
		/// 否发布到检索
		/// </summary>
		public int is_published { get; set; }
		/// <summary>
		/// 创建日期
		/// </summary>
		public string create_time { get; set; }
		/// <summary>
		/// 最近一次修改时间
		/// </summary>
		public string modify_time { get; set; }
	}
}
