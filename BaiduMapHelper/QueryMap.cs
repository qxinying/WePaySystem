﻿using LitJson;
using Xinying.Common;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BaiduMapHelper
{
    public class QueryMap
    {
        #region 字段
        /// <summary>
        /// poi周边搜索接口   GET请求
        /// </summary>
        private static string poinaereurl = "http://api.map.baidu.com/geosearch/v3/nearby";
		/// <summary>
		/// 查询指定条件的数据（poi）列表接口
		/// </summary>
		private static string poiwhereurl = "http://api.map.baidu.com/geodata/v3/poi/list";
		/// <summary>
		/// 查询指定id的数据（poi）详情接口
		/// </summary>
		private static string poibyidurl = "http://api.map.baidu.com/geodata/v3/poi/detail";
        /// <summary>
        /// 本地检索是指可检索指定区域范围内的poi信息，区域通过region参数来设定，可以是全国范围也可以是小范围的如海淀区。
        /// </summary>
        private static string poibylocal = "http://api.map.baidu.com/geosearch/v3/local";
        /// <summary>
        /// Route Matrix API v1.0
        /// </summary>
        private static string routematrixurl = "http://api.map.baidu.com/direction/v1/routematrix";

        #endregion

        #region 方法
		/// <summary>
		///  poi周边搜索接口   GET请求
		/// </summary>
		/// <param name="keys"></param>
		/// <param name="values"></param>
		/// <returns></returns>
        public static QueryPoiParam QueryPoiModelNaer(string[] keys,string[] values)
        {
			string returndata = Utils.HttpGet(poinaereurl + "?" + GetStr(keys, values));
            JsonData jd = (JsonData)JsonMapper.ToObject(returndata);
            QueryPoiParam model = new QueryPoiParam();
            model.status = (int)jd["status"];
            if (model.status == 0)
            {
                model.total = (int)jd["total"];
                model.size = (int)jd["size"];
                JsonData contents = (JsonData)jd["contents"];
                List<content> list = new List<content>();
                foreach (JsonData item in contents)
                {
                    JsonData jsloc = (JsonData)item["location"];
                    Double js1 = (Double)jsloc[0];
                    Double js2 = (Double)jsloc[1];
                    list.Add(
                        new content
                        {
                            title = (string)item["title"],
                            city = (string)item["city"],
                            geotable_id = (int)item["geotable_id"],
                            address = (string)item["address"],
                            province = (string)item["province"],
                            district = (string)item["district"],
                            uid = (int)item["uid"],
                            distance = (int)item["distance"],
                            weight = (int)item["weight"],
                            coord_type = (int)item["coord_type"],
                            location=js1.ToString()+","+js2.ToString()

                        });
                }
                model.contents = list;
            }
            return model;
            
        }

        /// <summary>
        ///  poi本地搜索接口   GET请求
        /// </summary>
        /// <param name="keys"></param>
        /// <param name="values"></param>
        /// <returns></returns>
        public static QueryPoiParam QueryPoiModelLocal(string[] keys, string[] values)
        {
            string returndata = Utils.HttpGet(poibylocal + "?" + GetStr(keys, values));
            JsonData jd = (JsonData)JsonMapper.ToObject(returndata);
            QueryPoiParam model = new QueryPoiParam();
            model.status = (int)jd["status"];
            if (model.status == 0)
            {
                model.total = (int)jd["total"];
                model.size = (int)jd["size"];
                JsonData contents = (JsonData)jd["contents"];
                List<content> list = new List<content>();
                foreach (JsonData item in contents)
                {
                    JsonData jsloc = (JsonData)item["location"];
                    Double js1 = (Double)jsloc[0];
                    Double js2 = (Double)jsloc[1];
                    list.Add(
                        new content
                        {
                            title = (string)item["title"],
                            city = (string)item["city"],
                            geotable_id = (int)item["geotable_id"],
                            address = (string)item["address"],
                            province = (string)item["province"],
                            district = (string)item["district"],
                            uid = (int)item["uid"],
                            distance = (int)item["distance"],
                            weight = (int)item["weight"],
                            coord_type = (int)item["coord_type"],
                            location = js1.ToString() + "," + js2.ToString()

                        });
                }
                model.contents = list;
            }
            return model;

        }
		/// <summary>
		///  查询指定条件的数据（poi）列表接口
		/// </summary>
        public static Poi QueryPoiWhere(ArrayList keys, ArrayList values)
		{
			string returndata = Utils.HttpGet(poiwhereurl + "?" + GetStr(keys, values));
			JsonData jd = (JsonData)JsonMapper.ToObject(returndata);
			Poi model = new Poi();
			model.status = (int)jd["status"];
			if (model.status == 0)
			{
				model.total = (int)jd["total"];
				model.size = (int)jd["size"];
				JsonData contents = (JsonData)jd["pois"];
				List<poi> list = new List<poi>();
				foreach (JsonData item in contents)
				{
					JsonData jsloc = (JsonData)item["location"];
					Double js1 = (Double)jsloc[0];
					Double js2 = (Double)jsloc[1];
					list.Add(
						new poi
						{
							title = (string)item["title"],
							city = (string)item["city"],
							geotable_id = (int)item["geotable_id"],
							address = (string)item["address"],
							province = (string)item["province"],
							district = (string)item["district"],
							id = (int)item["id"],
							location = js1.ToString() + "," + js2.ToString(),
							create_time=(string)item["create_time"],
							tel =(string)item["tel"]

						});
				}
				model.pois = list;
			}
			return model;
		}
		/// <summary>
		/// 查询指定id的数据（poi）详情接口
		/// </summary>
		public static poi QueryByID(string[] keys, string[] values)
		{
			string returndata = Utils.HttpGet(poibyidurl + "?" + GetStr(keys, values));
			JsonData jd = (JsonData)JsonMapper.ToObject(returndata);
			if (((int)jd["status"])==0)
			{
				jd = (JsonData)jd["poi"];
				poi model = new poi();
				model.title = (string)jd["title"];
				model.city = (string)jd["city"];
				model.address = (string)jd["address"];
				model.province = (string)jd["province"];
				model.district = (string)jd["district"];
				model.id = (int)jd["id"];
				JsonData jsloc = (JsonData)jd["location"];
				Double js1 = (Double)jsloc[0];
				Double js2 = (Double)jsloc[1];
				model.tel = (string)jd["tel"];
				model.location = js1.ToString() + "," + js2.ToString();
				return model;
			}
			else
			{
				return null;
			}
		}
        /// <summary>
        /// 返回坐标间的距离
        /// </summary>
        /// <param name="keys"></param>
        /// <param name="values"></param>
        /// <returns></returns>
        public static List<int> routematrix(string[] keys,string[] values)
        {
            List<int> list = new List<int>();
            string returndata = Utils.HttpGet(routematrixurl + "?" + GetStr(keys, values));
            JsonData jd = (JsonData)JsonMapper.ToObject(returndata);
            JsonData jd1 = (JsonData)jd["result"]["elements"];
            foreach (JsonData item in jd1)
            {
                list.Add((int)item["distance"]["value"]);
            }
            return list;
        }
        #endregion
        #region 私有方法
        private static string GetStr(string[] key, string[] value)
        {
            StringBuilder str = new StringBuilder();
            for (int i = 0; i < key.Length; i++)
            {
                if (key[i]!="")
                {
                    str.AppendFormat("{0}={1}&", key[i], value[i]);
                }
                
            }
            return str.ToString().TrimEnd('&');
        }
        private static string GetStr(ArrayList key, ArrayList value)
        {
            StringBuilder str = new StringBuilder();
            for (int i = 0; i < key.Count; i++)
            {
                if (key[i] != "")
                {
                    str.AppendFormat("{0}={1}&", key[i], value[i]);
                }

            }
            return str.ToString().TrimEnd('&');
        }
        #endregion

    }
}
