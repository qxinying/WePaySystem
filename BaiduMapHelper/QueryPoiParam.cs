﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BaiduMapHelper
{
    public class QueryPoiParam
	{
		/// <summary>
		/// 状态码
		/// </summary>
		public int status { get; set; }
		/// <summary>
        /// 分页参数，所有召回数量
		/// </summary>
        public int total { get; set; }
		/// <summary>
        /// 分页参数，当前页返回数量	
		/// </summary>
		public int size { get; set; }
		/// <summary>
		/// geotable列表
		/// </summary>
        public List<content> contents { get; set; }

	}
    public class content
	{
		/// <summary>
        /// 数据id
		/// </summary>
        public int uid { get; set; }
        /// <summary>
        /// geotable_id
        /// </summary>
        public int geotable_id { get; set; }
		/// <summary>
        /// poi名称
		/// </summary>
        public string title { get; set; }
		/// <summary>
        /// poi地址
		/// </summary>
        public string address { get; set; }
        /// <summary>
        /// poi所属省
        /// </summary>
        public string province { get; set; }
        /// <summary>
        /// poi所属城市
        /// </summary>
        public string city { get; set; }
        /// <summary>
        /// poi所属区
        /// </summary>
        public string district { get; set; }
		/// <summary>
        /// 坐标系定义
		/// </summary>
        public int coord_type { get; set; }
		/// <summary>
        /// 经纬度
		/// </summary>
        public string location { get; set; }
		/// <summary>
        /// poi的标签
		/// </summary>
        public string tags { get; set; }
        /// <summary>
        /// 距离
        /// </summary>
        public int distance { get; set; }
        /// <summary>
        /// 权重
        /// </summary>
        public int weight { get; set; }
	}
}
