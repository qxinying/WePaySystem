﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BaiduMapHelper
{
	/// <summary>
	/// 违章记录对象
	/// </summary>
	public class illegal
	{
		/// <summary>
		/// 违章时间
		/// </summary>
		public string time { get; set; }
		/// <summary>
		/// 地址
		/// </summary>
		public string address { get; set; }
		/// <summary>
		/// 内容
		/// </summary>
		public string content { get; set; }
		/// <summary>
		/// 价格
		/// </summary>
		public string price { get; set; }
		/// <summary>
		/// 分数
		/// </summary>
		public string score { get; set; }
	}
}
