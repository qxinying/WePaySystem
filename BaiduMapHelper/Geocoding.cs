﻿using Xinying.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LitJson;
namespace BaiduMapHelper
{
    public class Geocoding
    {

        public static string getCity(string location)
        {
            string returndata = Utils.HttpGet("http://api.map.baidu.com/geocoder/v2/?ak=GpPCTyBEaUBAuivsQyiTHaK7&callback=renderReverse&location=" + location + "&output=json&pois=0");
            string data = returndata.Replace("(", "|").Replace(")", "|");
            JsonData jd = (JsonData)JsonMapper.ToObject(data.Split('|')[1]);
            if ((int)jd["status"]==0)
            {
                JsonData addressComponent = (JsonData)jd["result"]["addressComponent"];
                return (string)addressComponent["city"];
                
            }
            return "";
        }
    }
}
