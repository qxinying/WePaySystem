﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BaiduMapHelper
{
	/// <summary>
	/// 查询指定条件的数据（poi）列表,返回对象
	/// </summary>
	public class Poi
	{
		/// <summary>
		/// 状态码
		/// </summary>
		public int status { get; set; }
		/// <summary>
		/// 全部的数据条数
		/// </summary>
		public int total { get; set; }
		/// <summary>
		/// 返回数据条数
		/// </summary>
		public int size { get; set; }
		/// <summary>
		/// 响应的信息
		/// </summary>
		public string message { get; set; }
		/// <summary>
		/// geotable列表
		/// </summary>
		public List<poi> pois { get; set; }

	}
	public class poi
	{
		/// <summary>
		/// 数据id
		/// </summary>
		public int id { get; set; }
		/// <summary>
		/// geotable_id
		/// </summary>
		public int geotable_id { get; set; }
		/// <summary>
		/// poi名称
		/// </summary>
		public string title { get; set; }
		/// <summary>
		/// poi地址
		/// </summary>
		public string address { get; set; }
		/// <summary>
		/// poi所属省
		/// </summary>
		public string province { get; set; }
		/// <summary>
		/// poi所属城市
		/// </summary>
		public string city { get; set; }
		/// <summary>
		/// poi所属区
		/// </summary>
		public string district { get; set; }
		/// <summary>
		/// 经纬度
		/// </summary>
		public string location { get; set; }
		/// <summary>
		/// poi的标签
		/// </summary>
		public string tags { get; set; }
		/// <summary>
		/// 用户修改时间
		/// </summary>
		public string modify_time { get; set; }
		/// <summary>
		/// 创建时间
		/// </summary>
		public string create_time { get; set; }
		public string tel { get; set; }
	}
}
