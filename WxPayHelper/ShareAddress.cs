﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WxPayHelper
{
    public class ShareAddress
    {
        /// <summary>
        /// 公众号id
        /// </summary>
        public string appId { get; set; }
        /// <summary>
        /// 填写“jsapi_address”，获得编辑地址权限
        /// </summary>
        public string scope { get; set; }
        /// <summary>
        /// 签名方式，目前仅支持SHA1
        /// </summary>
        public string signType { get; set; }
        /// <summary>
        /// 签名
        /// </summary>
        public string addrSign { get; set; }
        /// <summary>
        /// 时间戳
        /// </summary>
        public string timeStamp { get; set; }
        /// <summary>
        /// 随机字符串
        /// </summary>
        public string nonceStr { get; set; }
    }
}
