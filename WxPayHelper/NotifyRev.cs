﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml.Linq;

namespace WxPayHelper
{
    public class NotifyRev
    {
        /// <summary>
        /// 返回状态码SUCCESS/FAIL此字段是通信标识，非交易标识，交易是否成功需要查看result_code来判断
        /// </summary>
        public string return_code { get; set; }
        /// <summary>
        /// 微信分配的公众账号ID
        /// </summary>
        public string appid { get; set; }
        /// <summary>
        /// 微信支付分配的商户号
        /// </summary>
        public string mch_id { get; set; }
        /// <summary>
        /// 随机字符串，不长于32位
        /// </summary>
        public string nonce_str { get; set; }
        /// <summary>
        /// 签名
        /// </summary>
        public string sign { get; set; }
        /// <summary>
        /// 业务结果SUCCESS/FAIL
        /// </summary>
        public string result_code { get; set; }
        /// <summary>
        /// 用户表示
        /// </summary>
        public string openid { get; set; }
        /// <summary>
        /// 是否订阅
        /// </summary>
        public string is_subscribe { get; set; }
        /// <summary>
        /// 交易类型
        /// </summary>
        public string trade_type { get; set; }
        /// <summary>
        /// 付款银行
        /// </summary>
        public string bank_type { get; set; }
        /// <summary>
        /// 总金额
        /// </summary>
        public string total_fee { get; set; }
        /// <summary>
        /// 现金券金额
        /// </summary>
        public string coupon_fee { get; set; }
        /// <summary>
        /// 货币种类
        /// </summary>
        public string fee_type { get; set; }
        /// <summary>
        /// 微信支付订单号
        /// </summary>
        public string transaction_id { get; set; }
        /// <summary>
        /// 商户订单号
        /// </summary>
        public string out_trade_no { get; set; }
        /// <summary>
        /// 商家数据包
        /// </summary>
        public string attach { get; set; }
        /// <summary>
        /// 支付完成时间
        /// </summary>
        public string time_end { get; set; }
        public string err_code { get; set; }
        public string err_code_des { get; set; }
        public string return_msg { get; set; }
        public NotifyRev()
        {
            NotifyRev data = new NotifyRev();
            string postStr = "";
            Stream s = HttpContext.Current.Request.InputStream;
            byte[] b = new byte[s.Length];
            s.Read(b, 0, (int)s.Length);
            postStr = Encoding.UTF8.GetString(b);
            //Utils.WriteTxt(postStr);
            XElement doc = XElement.Parse(postStr);
            data.return_code = doc.Element("return_code").Value;
            if (data.return_code == "SUCCESS")
            {
                data.appid = doc.Element("appid").Value;
                data.mch_id = doc.Element("mch_id").Value;
                data.nonce_str = doc.Element("nonce_str").Value;
                data.sign = doc.Element("sign").Value;
                data.result_code = doc.Element("result_code").Value;
                if (data.result_code == "SUCCESS")
                {
                    data.openid = doc.Element("openid").Value;
                    data.is_subscribe = doc.Element("is_subscribe").Value;
                    data.trade_type = doc.Element("trade_type").Value;
                    data.bank_type = doc.Element("bank_type").Value;
                    data.total_fee = doc.Element("total_fee").Value;
                    //  data.coupon_fee = doc.Element("coupon_fee").Value;
                    data.fee_type = doc.Element("fee_type").Value;
                    data.transaction_id = doc.Element("transaction_id").Value;
                    data.out_trade_no = doc.Element("out_trade_no").Value;
                    data.attach = doc.Element("attach").Value;
                    data.time_end = doc.Element("time_end").Value;

                    //Dictionary<string, string> dic = new Dictionary<string, string>();
                    //dic.Add("return_code", "SUCCESS");
                    //HttpContext.Current.Response.Write(Utils.parseXML(dic));
                }
                else
                {
                    data.err_code = doc.Element("err_code").Value;
                    data.err_code_des = doc.Element("err_code_des").Value;

                }

            }
            else
            {
                data.return_msg = doc.Element("return_msg").Value;
            }

        }
        public void Handler(NotifyRev rev, Action CallBack)
        {
            if (rev.return_code == "SUCCESS")
            {
                if (rev.result_code == "SUCCESS")
                {
                    try
                    {
                        //回调函数，业务逻辑处理，处理结束后返回信息给微信
                        CallBack();
                    }
                    catch (Exception e)
                    {
                        //Utils.WriteTxt("回调函数处理错误：" + e.ToString());
                        BackMessage("回调函数处理错误");
                    }

                }
                else
                {
                    //Utils.WriteTxt(string.Format("业务出错({0})：{1}", rev.err_code, rev.err_code_des));
                    BackMessage("业务出错");
                }
            }
            else
            {
                //Utils.WriteTxt(string.Format("通讯错误：{0}", rev.return_msg));
                BackMessage("通讯错误");
            }

        }

        public static void BackMessage(string msg = "")
        {
            if (msg == "")
            {
                Dictionary<string, string> dic = new Dictionary<string, string>();
                dic.Add("return_code", "SUCCESS");
                HttpContext.Current.Response.Write(Utils.parseXML(dic));
            }
            else
            {
                Dictionary<string, string> dic = new Dictionary<string, string>();
                dic.Add("return_code", "FAIL");
                dic.Add("return_msg", msg);
                HttpContext.Current.Response.Write(Utils.parseXML(dic));
            }
        }

    }
}
