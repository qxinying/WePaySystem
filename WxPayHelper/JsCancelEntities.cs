﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WxPayHelper
{
   public class JsCancelEntities
    {
        /// <summary>
        /// 公众号id*
        /// </summary>
        public string appid { get; set; }
        /// <summary>
        /// 商户号*
        /// </summary>
        public string mch_id { get; set; }
        /// <summary>
        /// 商户订单号
        /// </summary>
        public string out_trade_no { get; set; }
        /// <summary>
        /// 随机字符串
        /// </summary>
        public string nonce_str { get; set; }
        /// <summary>
        /// 签名
        /// </summary>
        public string sign { get; set; }

       
    }
}
