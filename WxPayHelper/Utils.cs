﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Web;
using Gma.QrCodeNet.Encoding.Windows.Render;
using System.Web.Security;
using Gma.QrCodeNet.Encoding;
using Xinying.Common;

namespace WxPayHelper
{
    public class Utils
    {
        public static string GetUnifyUrlXml<T>(T t, string key, out string url, out string _sign)
        {
            Type type = typeof(T);
            Dictionary<string, string> dic = new Dictionary<string, string>();
            PropertyInfo[] pis = type.GetProperties();
            #region 组合url参数到字典里
            foreach (PropertyInfo pi in pis)
            {
                object val = pi.GetValue(t, null);
                if (val != null)
                {
                    dic.Add(pi.Name, val.ToString());
                }
            }
            #endregion
            //字典排序
            var dictemp = dic.OrderBy(d => d.Key);
            #region 生成url字符串
            StringBuilder str = new StringBuilder();
            foreach (var item in dictemp)
            {
                str.AppendFormat("{0}={1}&", item.Key, item.Value);
            }
            #endregion
            var ourl = str.ToString().Trim('&');
            //加上key
            string tempsign = ourl + "&key=" + key;
            //md5加密后，转换成大写
            string sign = MD5(tempsign).ToUpper();
            //将签名添加到字典中
            dic.Add("sign", sign);
            _sign = sign;
            url = str.AppendFormat("sign={0}", sign).ToString();
            //生成请求的内容，并返回
            return parseXML(dic);
        }


        public static string GetUnifyUrlXml1<T>(T t, string key, out string url, out string _sign)
        {
            Type type = typeof(T);
            Dictionary<string, string> dic = new Dictionary<string, string>();
            PropertyInfo[] pis = type.GetProperties();
            #region 组合url参数到字典里
            foreach (PropertyInfo pi in pis)
            {
                object val = pi.GetValue(t, null);
                if (val != null)
                {
                    dic.Add(pi.Name, val.ToString());
                }
            }
            #endregion
            //字典排序
            var dictemp = dic.OrderBy(d => d.Key);
            #region 生成url字符串
            StringBuilder str = new StringBuilder();
            foreach (var item in dictemp)
            {
                str.AppendFormat("{0}={1}&", item.Key, item.Value);
            }
            #endregion
            var ourl = str.ToString().Trim('&');
            //加上key
            string tempsign = ourl + "&key=" + key;
            //md5加密后，转换成大写
            string sign = MD5(tempsign).ToUpper();
            //将签名添加到字典中
            dic.Add("sign", sign);
            _sign = sign;
            url = str.AppendFormat("sign={0}", sign).ToString();
            //生成请求的内容，并返回
            return parseXML(dic);
        }

        /// <summary>
        /// 微信共享地址参数签名
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="t"></param>
        /// <param name="key"></param>
        /// <param name="url"></param>
        /// <param name="_sign"></param>
        /// <returns></returns>
        public static string GetAddrSign(string appId, string uri, string timeStamp, string nonceStr, string accessToken)
        {
            string result = "";
            //Utils.WriteTxt("appId:" + appId + ";uri:" + uri + ";timeStamp:" + timeStamp + ";nonceStr:" + nonceStr + ";accessToken:" + accessToken);
            if (string.IsNullOrEmpty(appId) || string.IsNullOrEmpty(uri) || string.IsNullOrEmpty(timeStamp) || string.IsNullOrEmpty(nonceStr) || string.IsNullOrEmpty(accessToken))
            {

            }
            else
            {
                return SHA1("accesstoken=" + accessToken + "&appid=" + appId + "&noncestr=" + nonceStr + "&timestamp=" + timeStamp + "&url=" + uri);
            }
            return result;
        }

        /// <summary>
        /// 获取微信js-sdk签名
        /// </summary>
        /// <param name="appId"></param>
        /// <param name="uri"></param>
        /// <param name="timeStamp"></param>
        /// <param name="nonceStr"></param>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        public static string GetJSSDKSign(string uri, string timeStamp, string nonceStr, string jsapiticket)
        {
            string result = "";
            //Utils.WriteTxt("uri:" + uri + ";timeStamp:" + timeStamp + ";nonceStr:" + nonceStr + ";jsapiticket:" + jsapiticket);
            if (string.IsNullOrEmpty(uri) || string.IsNullOrEmpty(timeStamp) || string.IsNullOrEmpty(nonceStr) || string.IsNullOrEmpty(jsapiticket))
            {

            }
            else
            {
                return SHA1("jsapi_ticket=" + jsapiticket + "&noncestr=" + nonceStr + "&timestamp=" + timeStamp + "&url=" + uri);
            }
            return result;
        }

        public static string parseXML(Dictionary<string, string> parameters)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<xml>");
            foreach (string k in parameters.Keys)
            {
                string v = (string)parameters[k];
                if (Regex.IsMatch(v, @"^[0-9.]$"))
                {

                    sb.Append("<" + k + ">" + v + "</" + k + ">");
                }
                else
                {
                    sb.Append("<" + k + "><![CDATA[" + v + "]]></" + k + ">");
                }

            }
            sb.Append("</xml>");
            return sb.ToString();
        }
        /// <summary>
        /// 获取32位随机数（GUID）
        /// </summary>
        /// <returns></returns>
        public static string GetRandom()
        {
            return Guid.NewGuid().ToString("N");
        }
        /// <summary>
        /// 获取微信版本
        /// </summary>
        /// <param name="ua"></param>
        /// <returns></returns>
        public static string GetWeiXinVersion(string ua)
        {
            int Last = ua.LastIndexOf("MicroMessenger");
            string[] wxversion = ua.Remove(0, Last).Split(' ');
            return wxversion[0].Split('/')[1].Substring(0, 3);
        }

        #region MD5加密
        public static string MD5(string pwd)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] data = System.Text.Encoding.UTF8.GetBytes(pwd);
            byte[] md5data = md5.ComputeHash(data);
            md5.Clear();
            string str = "";
            for (int i = 0; i < md5data.Length; i++)
            {
                str += md5data[i].ToString("x").PadLeft(2, '0');
            }
            return str;
        }


        #endregion

        public static string HttpPost(string url, string param)
        {
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(url);
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.Accept = "*/*";
            request.Timeout = 15000;
            request.AllowAutoRedirect = false;

            StreamWriter requestStream = null;
            WebResponse response = null;
            string responseStr = null;

            try
            {
                requestStream = new StreamWriter(request.GetRequestStream());
                requestStream.Write(param);
                requestStream.Close();

                response = request.GetResponse();
                if (response != null)
                {
                    StreamReader reader = new StreamReader(response.GetResponseStream(), Encoding.UTF8);
                    responseStr = reader.ReadToEnd();
                    //Utils.WriteTxt("responseStr:" + responseStr);
                    reader.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                request = null;
                requestStream = null;
                response = null;
            }

            return responseStr;
        }

        /// <summary>
        /// datetime转换为unixtime
        /// </summary>
        /// <param name="time"></param>
        /// <returns></returns>
        public static int ConvertDateTimeInt(System.DateTime time)
        {
            System.DateTime startTime = TimeZone.CurrentTimeZone.ToLocalTime(new System.DateTime(1970, 1, 1));
            return (int)(time - startTime).TotalSeconds;
        }
        public static bool WriteTxt(string str)
        {
            try
            {
                FileStream fs = new FileStream(HttpContext.Current.Request.MapPath("/bugLog.txt"), FileMode.Append);
                StreamWriter sw = new StreamWriter(fs);
                //开始写入
                sw.WriteLine(str + "\r\naddtime:" + DateTime.Now.ToDateTimeStr() + "\r\n");
                //清空缓冲区
                sw.Flush();
                //关闭流
                sw.Close();
                fs.Close();
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// 生成二维码流
        /// </summary>
        /// <param name="qrcontent"></param>
        /// <returns></returns>
        public static MemoryStream GetQrCodeStream(string qrcontent)
        {
            //误差校正水平
            ErrorCorrectionLevel ecLevel = ErrorCorrectionLevel.M;
            //空白区域
            QuietZoneModules quietZone = QuietZoneModules.Zero;
            int ModuleSize = 120;//大小
            QrCode qrCode;
            var encoder = new QrEncoder(ecLevel);
            //对内容进行编码，并保存生成的矩阵
            if (encoder.TryEncode(qrcontent, out qrCode))
            {
                var render = new GraphicsRenderer(new FixedCodeSize(ModuleSize, quietZone));
                MemoryStream stream = new MemoryStream();
                render.WriteToStream(qrCode.Matrix, ImageFormat.Jpeg, stream);
                return stream;
            }
            return null;
        }

        public static void GetQrCode(string qrcontent)
        {
            MemoryStream ms = GetQrCodeStream(qrcontent);
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.ContentType = "image/Png";
            HttpContext.Current.Response.BinaryWrite(ms.ToArray());

        }

        /// <summary>
        /// SHA1加密字符串
        /// </summary>
        /// <param name="source">源字符串</param>
        /// <returns>加密后的字符串</returns>
        public static string SHA1(string source)
        {
            return FormsAuthentication.HashPasswordForStoringInConfigFile(source, "SHA1");
        }

        /// <summary>
        /// 获取当前请求的数据包内容
        /// </summary>
        public static string GetRequestData()
        {
            //获取传入的 HTTP 实体主体的内容	
            using (var stream = HttpContext.Current.Request.InputStream)
            {
                using (var reader = new StreamReader(stream, Encoding.UTF8))
                {
                    return reader.ReadToEnd();
                }
            }
        }

        /// <summary>
        /// XML转换成实体对象
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="xml"></param>
        /// <returns></returns>
        public static T XmlToEntity<T>(string xml)
        {
            var type = typeof(T);
            //创建实例
            var t = Activator.CreateInstance<T>();
            var pr = type.GetProperties();
            var xdoc = XElement.Parse(xml);
            var eles = xdoc.Elements();
            var ele = eles.Where(e => new Regex(@"_\d{1,}$").IsMatch(e.Name.
            ToString()));//获取带下标的节点
            if (ele.Count() > 0)
            {
                var selele = ele.Select(e =>
                {
                    var temp = e.Name.ToString().Split('_');
                    var index = int.Parse(temp[temp.Length - 1]);
                    return new
                    {
                        Index = index,
                        Property = e.Name.ToString().
                    Replace("_" + index.ToString(), ""),
                        Value = e.Value
                    };
                });//转换为方便操作的匿名对象。
                var max = selele.Max(m => m.Index);//获取最大索引的值
                var infos = pr.FirstOrDefault(f => f.PropertyType.IsGenericType);
                //获取类型为泛型的属性
                if (infos != null)
                {
                    var infotype = infos.PropertyType.GetGenericArguments().First();                            //获取泛型的真实类型
                    Type listType = typeof(List<>).MakeGenericType(new[] { infotype });                         //创建泛型列表
                    var datalist = Activator.CreateInstance(listType);//创建对象
                    var infoprs = infotype.GetProperties();
                    for (int j = 0; j <= max; j++)
                    {
                        var temp = Activator.CreateInstance(infotype);
                        var list = selele.Where(s => s.Index == 0);
                        foreach (var v in list)
                        {
                            var p = infoprs.FirstOrDefault(f => f.Name == v.Property);
                            if (p == null) continue;
                            p.SetValue(temp, v.Value, null);
                        }
                        listType.GetMethod("Add").Invoke((object)datalist, new[] { temp });//将对象添加到列表中
                    }
                    infos.SetValue(t, datalist, null);//最后给泛型属性赋值
                }
                ele.Remove();//将有下标的节点从集合中移除
            }
            foreach (var element in eles)
            {
                var p = pr.First(f => f.Name == element.Name);
                p.SetValue(t, Convert.ChangeType(element.Value, p.PropertyType),
                null);
            }
            return t;
        }

        /// <summary>
        /// 获取支付签名
        ///</summary>
        ///<param name="dictionary">数据集合</param>
        ///<param name="key">支付密钥</param>
        ///<returns>签名</returns>
        public static string GetPaySign(Dictionary<string, string> dictionary, string key)
        {
            var arr = dictionary.OrderBy(d => d.Key).
                Select(d => string.Format("{0}={1}", d.Key, d.Value)).ToArray();
            string stringA = string.Join("&", arr);
            return MD5(string.Format("{0}&key={1}", stringA, key)).ToUpper();
        }
        /// <summary>
        /// 获取支付签名
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetPaySign(object obj, string key)
        {
            var dic = EntityToDictionary(obj);
            return GetPaySign(dic, key);
        }
        /// <summary>
        /// 将实体对象转换成键值对。移除值为null的属性
        /// </summary>
        /// <param name="obj">参数实体</param>
        /// <returns>数据键值对</returns>
        public static Dictionary<string, string> EntityToDictionary(object obj, int? index = null)
        {
            var type = obj.GetType();
            var dic = new Dictionary<string, string>();
            var pis = type.GetProperties();
            foreach (var pi in pis)
            {
                //获取属性的值
                var val = pi.GetValue(obj, null);
                //移除值为null，以及字符串类型的值为空字符的。另外，签名字符串本身不参与签名，
                //在验证签名正确性时，需移除sign
                if (val == null || val.ToString() == "" || pi.Name == "sign" ||
              pi.PropertyType.IsGenericType)
                    continue;
                if (index != null)
                {
                    dic.Add(pi.Name + "_" + index, val.ToString());
                }
                else
                {
                    dic.Add(pi.Name, val.ToString());
                }
            }
            var classlist = pis.Where(p => p.PropertyType.IsGenericType);
            foreach (var info in classlist)
            {
                var val = info.GetValue(obj, null);
                if (val == null)
                {
                    continue;
                }
                int count = (int)info.PropertyType.GetProperty("Count").GetValue(val, null);
                if (count > 0)
                {
                    for (int i = 0; i < count; i++)
                    {
                        object ol = info.PropertyType.GetMethod("get_Item").Invoke
                        (val, new object[] { i });
                        var tem = EntityToDictionary(ol, i);//递归调用
                        foreach (var t in tem)
                        {
                            dic.Add(t.Key, t.Value);
                        }
                    }
                }
            }
            return dic;
        }
    }
}
