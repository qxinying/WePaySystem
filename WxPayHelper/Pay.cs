﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using WxPayHelper.PayEntity;

namespace WxPayHelper
{
    /// <summary>
    /// 微信支付相关
    /// </summary>
    public class Pay
    {
        /// <summary>
        /// 异步通知回调方法
        /// </summary>
        /// <param name="key"></param>
        /// <param name="callBack"></param>
        public static void GetNotifyRes(string key, Action<OrderInfo> callBack)
        {
            try
            {
                var reqdata = Utils.GetRequestData();
                //写错误日志
                //System.IO.File.AppendAllText(Xinying.SecKill.Common.Utils.GetMapPath("/log/wpaylog.txt"), reqdata + "\n\r", System.Text.Encoding.UTF8);
                var rev = Utils.XmlToEntity<OrderInfo>(reqdata);
                //写错误日志
                //System.IO.File.AppendAllText(Xinying.SecKill.Common.Utils.GetMapPath("/log/wpaylog.txt"), rev.return_code + "\n\r", System.Text.Encoding.UTF8);
                if (rev.return_code != "SUCCESS")
                { BackMessage("通信错误"); return; }
                if (rev.result_code != "SUCCESS")
                { BackMessage("业务出错"); return; }
                //写错误日志
                //System.IO.File.AppendAllText(Xinying.SecKill.Common.Utils.GetMapPath("/log/wpaylog.txt"), "sign:" + rev.sign + "\n\r", System.Text.Encoding.UTF8);
                if (rev.sign == Utils.GetPaySign(rev, key))
                {
                    //写错误日志
                    //System.IO.File.AppendAllText(Xinying.SecKill.Common.Utils.GetMapPath("/log/wpaylog.txt"), "_sign:" + Utils.GetPaySign(rev, key) + "\n\r", System.Text.Encoding.UTF8);
                    //回调函数，业务逻辑处理，处理结束后返回信息给微信
                    callBack(rev);
                }
            }
            catch (Exception e)
            {
                BackMessage("回调函数处理错误");
            }
        }
        public static void BackMessage(string msg = "")
        {
            Dictionary<string, string> dic = new Dictionary<string, string>();
            if (msg != "")
            {
                dic.Add("return_code", "FAIL");
                dic.Add("return_msg", msg);
                HttpContext.Current.Response.Write(Utils.parseXML(dic));
                //写错误日志
                System.IO.File.AppendAllText(Xinying.Common.Utils.GetMapPath("/log/wpaylog.txt"), msg + "\n\r", System.Text.Encoding.UTF8);
            }
            else
            {
                dic.Add("return_code", "SUCCESS");
                HttpContext.Current.Response.Write(Utils.parseXML(dic));
            }
        }
    }
}
